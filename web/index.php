<?php


require_once(dirname(__FILE__).'/../museo/config/ProjectConfiguration.class.php');

$configuration = ProjectConfiguration::getApplicationConfiguration('public', 'prod', false);
sfContext::createInstance($configuration)->dispatch();
