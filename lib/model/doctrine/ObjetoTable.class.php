<?php


class ObjetoTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Objeto');
    }
    
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Objeto j')
        ->orderBy('j.nombre_objeto ASC');

       return $q->execute();
    }


    function retrieveForSelect($q, $limit) {
	    $q = Doctrine_Query::create()
              ->from('Objeto o')
	          ->andWhere('o.nombre_objeto like ?', '%'.$q.'%')
              ->addOrderBy('o.nombre_objeto')
	          ->limit($limit);

	    $objetoss = array();
	    foreach ($q->execute() as $objeto) {
	        $objetoss[$objeto->getId()] = $objeto->__toString();
	    }
	    return $objetoss;
	  }


  public static function doSelectAdminAdmin($query)
  {
   return $query->from('Objeto o')
                 ->where('o.fecha_baja is NULL')
                 ->orWhere('o.motivo_baja like ?', '%error%' );

    }

    function retrieveForSelectNombreId($q, $limit) {
        $q = Doctrine_Query::create()
              ->from('Objeto o')
              ->andWhere('o.nombre_objeto like ?', '%'.$q.'%')
              ->orWhere('o.id like ?', '%'.$q.'%')
              ->addOrderBy('o.nombre_objeto')
              ->limit($limit);

        $objetoss = array();
        foreach ($q->execute() as $objeto) {
            $objetoss[$objeto->getId()] = $objeto->__toString();
        }
        return $objetoss;
    }
}