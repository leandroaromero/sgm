<?php


class DimensionTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Dimension');
    }
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Dimension j')
        ->orderBy('j.nombre_dimension ASC');

       return $q->execute();
    }
}