<?php


class UbicacionTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Ubicacion');
    }
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Ubicacion j')
        ->orderBy('j.lugar ASC');

  return $q->execute();

    }
}