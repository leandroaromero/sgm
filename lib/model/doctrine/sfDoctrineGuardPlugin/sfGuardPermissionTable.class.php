<?php


class sfGuardPermissionTable extends PluginsfGuardPermissionTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('sfGuardPermission');
    }
    
    public static function doSelectPermisos($query)
    {
        
        $user = sfContext::getInstance()->getUser();
		
        if ($user->hasCredential('gerente')){
         return $query->from('sfGuardPermission p')
                      ->orderBy('p.name');
        }else{
         return $query->from('sfGuardPermission p')
                      ->where('p.name != ?', 'gerente')
                      ->andWhere('p.name != ?', 'gestor') 
                      ->orderBy('p.name');
         
        }

    }
    
      public static function getOrderNombre(){

          
        $user = sfContext::getInstance()->getUser();
     
        if ($user->hasCredential('gerente')){
            
          $q = Doctrine_Query::create()
                      ->from('sfGuardPermission p')
                      ->orderBy('p.name ');
        }else{
            
          $q = Doctrine_Query::create()
                      ->from('sfGuardPermission  p')
                      ->where('p.name != ?', 'gerente')
                      ->andWhere('p.name != ?', 'gestor')
                      ->orderBy('p.name ');
        }
   
        
        
       return $q->execute();
    }
}
