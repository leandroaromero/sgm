<?php //


class sfGuardUserTable extends PluginsfGuardUserTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('sfGuardUser');
    }
    
    public static function getOrderNombre(){

          $qrs = Doctrine_Query::create()
                  ->select('o.id') 
                  ->from('sfGuardUser o')
                  ->leftJoin('o.sfGuardUserPermission up')
                  ->leftJoin('up.Permission p')  
                  ->where('p.name = ?', 'gerente')
                  ->orwhere('p.name = ?', 'gestor')
                  ->fetchArray();
          $array= array();
          foreach ($qrs as $s)
              $array[]= $s['id'];
          
        $user = sfContext::getInstance()->getUser();
     
        if ($user->hasCredential('gerente')){
            
          $q = Doctrine_Query::create()
                      ->from('sfGuardUser o')
                      ->orderBy('o.first_name 	');
        }else{
            
          $q = Doctrine_Query::create()
                      ->from('sfGuardUser o')
                      ->andWhereNotIn( 'o.id', $array)
                      ->orderBy('o.first_name 	');
        }
   
        
        
       return $q->execute();
    }
    
  public static function doSelectUserAdmin($query)
    {
       $qrs = Doctrine_Query::create()
                  ->select('o.id') 
                  ->from('sfGuardUser o')
                  ->leftJoin('o.sfGuardUserPermission up')
                  ->leftJoin('up.Permission p')  
                  ->where('p.name = ?', 'gerente')
                  ->orwhere('p.name = ?', 'gestor')
                  ->fetchArray();
          $array= array();
          foreach ($qrs as $s)
              $array[]= $s['id'];
          
        $user = sfContext::getInstance()->getUser();
     
        if ($user->hasCredential('gerente')){
            
         return $query->from('sfGuardUser o')
                      ->orderBy('o.first_name 	');
        }else{
            
         return $query->from('sfGuardUser o')
                      ->andWhereNotIn( 'o.id', $array)
                      ->orderBy('o.first_name 	');
        }
   
    }
}