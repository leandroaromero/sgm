<?php

/**
 * IntervencionTipoFotografiaTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class IntervencionTipoFotografiaTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object IntervencionTipoFotografiaTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('IntervencionTipoFotografia');
    }
}