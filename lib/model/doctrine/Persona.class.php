<?php

/**
 * Persona
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    museo
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Persona extends BasePersona
{
    public function getUnAutor(){
        $res = Doctrine::getTable('Autor')->find($this->getId());
        return $res;
    }
   public function getUnDonante(){
        $res = Doctrine::getTable('Donante')->find($this->getId());
        return $res;
    }
   public function __toString() {
       return $this->getApellido().', '.$this->getNombre();
   }
   public function getUsuario() {
       return $this->getApellido().', '.$this->getNombre().' Tel:'.$this->getTelefonos();
   }
   
   public function getTelefonos() {
       $telefonos= '';
       foreach ($this->getTelefono() as $tele){
           
           $telefonos =  $telefonos.' - '.$tele->getNumeroTelefono();
       }
       
       return $telefonos;
   }
}
