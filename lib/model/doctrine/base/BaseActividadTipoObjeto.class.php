<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('ActividadTipoObjeto', sfContext::getInstance()->getUser()->getDataBase());

/**
 * BaseActividadTipoObjeto
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $actividad_id
 * @property integer $tipo_objetos_id
 * @property Actividad $Actividad
 * @property TipoObjeto $TipoObjeto
 * 
 * @method integer             getActividadId()     Returns the current record's "actividad_id" value
 * @method integer             getTipoObjetosId()   Returns the current record's "tipo_objetos_id" value
 * @method Actividad           getActividad()       Returns the current record's "Actividad" value
 * @method TipoObjeto          getTipoObjeto()      Returns the current record's "TipoObjeto" value
 * @method ActividadTipoObjeto setActividadId()     Sets the current record's "actividad_id" value
 * @method ActividadTipoObjeto setTipoObjetosId()   Sets the current record's "tipo_objetos_id" value
 * @method ActividadTipoObjeto setActividad()       Sets the current record's "Actividad" value
 * @method ActividadTipoObjeto setTipoObjeto()      Sets the current record's "TipoObjeto" value
 * 
 * @package    museo
 * @subpackage model
 * @author     LeandroARomero
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseActividadTipoObjeto extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('actividad_tipo_objeto');
        $this->hasColumn('actividad_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('tipo_objetos_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Actividad', array(
             'local' => 'actividad_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('TipoObjeto', array(
             'local' => 'tipo_objetos_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
    }
}
