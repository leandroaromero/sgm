<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('ActividadSfGuardUser', sfContext::getInstance()->getUser()->getDataBase());

/**
 * BaseActividadSfGuardUser
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $actividad_id
 * @property integer $sf_guard_user_id
 * @property Actividad $Actividad
 * 
 * @method integer              getActividadId()      Returns the current record's "actividad_id" value
 * @method integer              getSfGuardUserId()    Returns the current record's "sf_guard_user_id" value
 * @method Actividad            getActividad()        Returns the current record's "Actividad" value
 * @method ActividadSfGuardUser setActividadId()      Sets the current record's "actividad_id" value
 * @method ActividadSfGuardUser setSfGuardUserId()    Sets the current record's "sf_guard_user_id" value
 * @method ActividadSfGuardUser setActividad()        Sets the current record's "Actividad" value
 * 
 * @package    museo
 * @subpackage model
 * @author     LeandroARomero
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseActividadSfGuardUser extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('actividad_sf_guard_user');
        $this->hasColumn('actividad_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('sf_guard_user_id', 'integer', 8, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => false,
             'length' => 8,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Actividad', array(
             'local' => 'actividad_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
    }
}