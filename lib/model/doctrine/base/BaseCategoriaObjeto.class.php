<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('CategoriaObjeto', sfContext::getInstance()->getUser()->getDataBase());

/**
 * BaseCategoriaObjeto
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $categoria_id
 * @property integer $objeto_id
 * @property Categoria $Categoria
 * @property Objeto $Objeto
 * 
 * @method integer         getCategoriaId()  Returns the current record's "categoria_id" value
 * @method integer         getObjetoId()     Returns the current record's "objeto_id" value
 * @method Categoria       getCategoria()    Returns the current record's "Categoria" value
 * @method Objeto          getObjeto()       Returns the current record's "Objeto" value
 * @method CategoriaObjeto setCategoriaId()  Sets the current record's "categoria_id" value
 * @method CategoriaObjeto setObjetoId()     Sets the current record's "objeto_id" value
 * @method CategoriaObjeto setCategoria()    Sets the current record's "Categoria" value
 * @method CategoriaObjeto setObjeto()       Sets the current record's "Objeto" value
 * 
 * @package    museo
 * @subpackage model
 * @author     LeandroARomero
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCategoriaObjeto extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('categoria_objeto');
        $this->hasColumn('categoria_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('objeto_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Categoria', array(
             'local' => 'categoria_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('Objeto', array(
             'local' => 'objeto_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
    }
}