<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Pais', sfContext::getInstance()->getUser()->getDataBase());

/**
 * BasePais
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $nombre_pais
 * @property Doctrine_Collection $Provincia
 * 
 * @method integer             getId()          Returns the current record's "id" value
 * @method string              getNombrePais()  Returns the current record's "nombre_pais" value
 * @method Doctrine_Collection getProvincia()   Returns the current record's "Provincia" collection
 * @method Pais                setId()          Sets the current record's "id" value
 * @method Pais                setNombrePais()  Sets the current record's "nombre_pais" value
 * @method Pais                setProvincia()   Sets the current record's "Provincia" collection
 * 
 * @package    museo
 * @subpackage model
 * @author     LeandroARomero
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePais extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('pais');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('nombre_pais', 'string', 45, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 45,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('Provincia', array(
             'local' => 'id',
             'foreign' => 'pais_id'));
    }
}