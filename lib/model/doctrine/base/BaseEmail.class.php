<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Email', sfContext::getInstance()->getUser()->getDataBase());

/**
 * BaseEmail
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $direccion_email
 * @property integer $persona_id
 * @property Persona $Persona
 * 
 * @method integer getId()              Returns the current record's "id" value
 * @method string  getDireccionEmail()  Returns the current record's "direccion_email" value
 * @method integer getPersonaId()       Returns the current record's "persona_id" value
 * @method Persona getPersona()         Returns the current record's "Persona" value
 * @method Email   setId()              Sets the current record's "id" value
 * @method Email   setDireccionEmail()  Sets the current record's "direccion_email" value
 * @method Email   setPersonaId()       Sets the current record's "persona_id" value
 * @method Email   setPersona()         Sets the current record's "Persona" value
 * 
 * @package    museo
 * @subpackage model
 * @author     LeandroARomero
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseEmail extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('email');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('direccion_email', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('persona_id', 'integer', 8, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 8,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Persona', array(
             'local' => 'persona_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
    }
}
