<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('ActividadCategoria', sfContext::getInstance()->getUser()->getDataBase());

/**
 * BaseActividadCategoria
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $actividad_id
 * @property integer $categoria_id
 * @property Actividad $Actividad
 * @property Categoria $Categoria
 * 
 * @method integer            getActividadId()  Returns the current record's "actividad_id" value
 * @method integer            getCategoriaId()  Returns the current record's "categoria_id" value
 * @method Actividad          getActividad()    Returns the current record's "Actividad" value
 * @method Categoria          getCategoria()    Returns the current record's "Categoria" value
 * @method ActividadCategoria setActividadId()  Sets the current record's "actividad_id" value
 * @method ActividadCategoria setCategoriaId()  Sets the current record's "categoria_id" value
 * @method ActividadCategoria setActividad()    Sets the current record's "Actividad" value
 * @method ActividadCategoria setCategoria()    Sets the current record's "Categoria" value
 * 
 * @package    museo
 * @subpackage model
 * @author     LeandroARomero
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseActividadCategoria extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('actividad_categoria');
        $this->hasColumn('actividad_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('categoria_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Actividad', array(
             'local' => 'actividad_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('Categoria', array(
             'local' => 'categoria_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
    }
}
