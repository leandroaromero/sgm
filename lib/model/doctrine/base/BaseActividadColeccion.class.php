<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('ActividadColeccion', sfContext::getInstance()->getUser()->getDataBase());

/**
 * BaseActividadColeccion
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $actividad_id
 * @property integer $coleccion_id
 * @property Actividad $Actividad
 * @property Coleccion $Coleccion
 * 
 * @method integer            getActividadId()  Returns the current record's "actividad_id" value
 * @method integer            getColeccionId()  Returns the current record's "coleccion_id" value
 * @method Actividad          getActividad()    Returns the current record's "Actividad" value
 * @method Coleccion          getColeccion()    Returns the current record's "Coleccion" value
 * @method ActividadColeccion setActividadId()  Sets the current record's "actividad_id" value
 * @method ActividadColeccion setColeccionId()  Sets the current record's "coleccion_id" value
 * @method ActividadColeccion setActividad()    Sets the current record's "Actividad" value
 * @method ActividadColeccion setColeccion()    Sets the current record's "Coleccion" value
 * 
 * @package    museo
 * @subpackage model
 * @author     LeandroARomero
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseActividadColeccion extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('actividad_coleccion');
        $this->hasColumn('actividad_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('coleccion_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Actividad', array(
             'local' => 'actividad_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('Coleccion', array(
             'local' => 'coleccion_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
    }
}
