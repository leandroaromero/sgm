<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Categoria', sfContext::getInstance()->getUser()->getDataBase());

/**
 * BaseCategoria
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $nombre
 * @property Doctrine_Collection $CategoriaObjeto
 * @property Doctrine_Collection $Objetos
 * @property Doctrine_Collection $Actividades
 * @property Doctrine_Collection $ActividadCategoria
 * 
 * @method integer             getId()                 Returns the current record's "id" value
 * @method string              getNombre()             Returns the current record's "nombre" value
 * @method Doctrine_Collection getCategoriaObjeto()    Returns the current record's "CategoriaObjeto" collection
 * @method Doctrine_Collection getObjetos()            Returns the current record's "Objetos" collection
 * @method Doctrine_Collection getActividades()        Returns the current record's "Actividades" collection
 * @method Doctrine_Collection getActividadCategoria() Returns the current record's "ActividadCategoria" collection
 * @method Categoria           setId()                 Sets the current record's "id" value
 * @method Categoria           setNombre()             Sets the current record's "nombre" value
 * @method Categoria           setCategoriaObjeto()    Sets the current record's "CategoriaObjeto" collection
 * @method Categoria           setObjetos()            Sets the current record's "Objetos" collection
 * @method Categoria           setActividades()        Sets the current record's "Actividades" collection
 * @method Categoria           setActividadCategoria() Sets the current record's "ActividadCategoria" collection
 * 
 * @package    museo
 * @subpackage model
 * @author     LeandroARomero
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCategoria extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('categoria');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('nombre', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 50,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('CategoriaObjeto', array(
             'local' => 'id',
             'foreign' => 'categoria_id'));

        $this->hasMany('Objeto as Objetos', array(
             'refClass' => 'CategoriaObjeto',
             'local' => 'categoria_id',
             'foreign' => 'objeto_id'));

        $this->hasMany('Actividad as Actividades', array(
             'refClass' => 'ActividadCategoria',
             'local' => 'categoria_id',
             'foreign' => 'actividad_id'));

        $this->hasMany('ActividadCategoria', array(
             'local' => 'id',
             'foreign' => 'categoria_id'));
    }
}
