<?php


class ProvinciaTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Provincia');
    }
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Provincia j')
        ->orderBy('j.nombre_provincia ASC');

       return $q->execute();
    }
}