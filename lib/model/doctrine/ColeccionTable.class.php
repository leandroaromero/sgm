<?php


class ColeccionTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Coleccion');
    }
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Coleccion j')
        ->orderBy('j.nombre_coleccion ASC');

       return $q->execute();
    }
}