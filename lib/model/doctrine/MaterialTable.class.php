<?php


class MaterialTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Material');
    }
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Material j')
        ->orderBy('j.nombre_material ASC');

       return $q->execute();
    }
}