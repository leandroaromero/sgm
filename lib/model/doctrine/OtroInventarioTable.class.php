<?php

/**
 * OtroInventarioTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class OtroInventarioTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object OtroInventarioTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('OtroInventario');
    }
}