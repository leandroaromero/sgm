<?php


class DonanteTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Donante');
    }
function retrieveForSelect($q, $limit) {
	    $q = Doctrine_Query::create()
	         ->from('Donante d')
                  ->innerJoin('d.Persona p')
	          ->andWhere('p.apellido like ?', '%'.$q.'%')
	          ->addOrderBy('p.apellido')
	          ->limit($limit);
	    $donante = array();
	    foreach ($q->execute() as $donacion) {
	        $donante[$donacion->getId()] = (string) $donacion;
	    }
	    return $donante;
	}

public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Donante j')
        ->leftJoin('j.Persona p')
        ->orderBy('p.apellido ASC');

       return $q->execute();
    }


}