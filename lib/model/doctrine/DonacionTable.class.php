<?php


class DonacionTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Donacion');
    }

   function retrieveForSelect($q, $limit) {
	    $q = Doctrine_Query::create()
	          ->from('Donacion d')
	          ->innerJoin('d.Donante dp')
              ->innerJoin('dp.Persona p')
              ->orWhere('p.apellido like ?', '%'.$q.'%')
              ->orWhere('p.nombre like ?', '%'.$q.'%')      
              ->orWhere('d.nro_acta like ?', '%'.$q.'%')      
	          ->addOrderBy('p.apellido')
	          ->limit($limit);
	    $donaciones = array();
	    foreach ($q->execute() as $donacion) {
	        $donaciones[$donacion->getId()] = (string) $donacion;
	    }
	    return $donaciones;
	}
     public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Donacion j')
        ->orderBy('j.nro_acta ASC')
        ->leftJoin('j.Donante d')
        ->leftJoin('d.Persona p')
        ->orderBy('p.apellido ASC');

       return $q->execute();
    }
}
