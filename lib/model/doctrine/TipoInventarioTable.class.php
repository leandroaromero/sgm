<?php

/**
 * TipoInventarioTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class TipoInventarioTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object TipoInventarioTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TipoInventario');
    }
}