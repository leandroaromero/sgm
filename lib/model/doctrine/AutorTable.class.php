<?php


class AutorTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Autor');
    }

     function retrieveForSelect($q, $limit) {
	    $q = Doctrine_Query::create()
	          ->from('Autor a')
              ->innerJoin('a.Persona p')
	          ->orWhere('p.apellido like ?', '%'.$q.'%')
                  ->orWhere('p.nombre like ?', '%'.$q.'%')  
	          ->addOrderBy('p.apellido')
	          ->limit($limit);
	    $personas = array();
	    foreach ($q->execute() as $persona) {
	        $personas[$persona->getId()] = (string) $persona;
	    }
	    return $personas;
	}
   public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Autor j')
        ->leftJoin('j.Persona p')
        ->orderBy('p.apellido ASC');

       return $q->execute();
    }

}