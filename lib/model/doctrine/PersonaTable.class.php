<?php


class PersonaTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Persona');
    }
    
   public static function doSelectAdminActor($query)
    {
    return $query->from('Persona p')
                 ->innerJoin('p.Autor a')
                 ->where('a.id is NOT NULL');
    }
    public static function doSelectAdminDonante($query)
    {
    return $query->from('Persona p')
                 ->innerJoin('p.Donante d')
                 ->where('d.id is NOT NULL');
    } 
    
}
