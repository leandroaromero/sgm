<?php


class PaisTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Pais');
    }
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Pais j')
        ->orderBy('j.nombre_pais ASC');

       return $q->execute();
    }
}