<?php


class FuenteBibliograficaTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('FuenteBibliografica');
    }
}