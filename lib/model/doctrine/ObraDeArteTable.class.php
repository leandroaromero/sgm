<?php

/**
 * ObraDeArteTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ObraDeArteTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object ObraDeArteTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ObraDeArte');
    }
}