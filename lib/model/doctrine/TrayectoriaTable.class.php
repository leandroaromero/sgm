<?php


class TrayectoriaTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Trayectoria');
    }
    
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Trayectoria j')
        ->orderBy('j.nombre_evento ASC');

       return $q->execute();
    }
}
