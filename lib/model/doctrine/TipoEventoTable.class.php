<?php


class TipoEventoTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TipoEvento');
    }
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('TipoEvento j')
        ->orderBy('j.denominacion_tipo_evento ASC');

     return $q->execute();

    }
}