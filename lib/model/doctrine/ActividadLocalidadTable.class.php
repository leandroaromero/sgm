<?php

/**
 * ActividadLocalidadTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ActividadLocalidadTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object ActividadLocalidadTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ActividadLocalidad');
    }
}