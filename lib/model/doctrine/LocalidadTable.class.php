<?php


class LocalidadTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Localidad');
    }
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Localidad j')
        ->orderBy('j.nombre_localidad ASC');

       return $q->execute();
    }
    public static function getOrderPaisNombre(){
        $q = Doctrine_Query::create()
        ->from('Localidad l')
        ->leftJoin('l.Provincia p')
        ->leftJoin('p.Pais pp')
        ->orderBy('pp.nombre_pais ASC')
        ->addOrderBy(' p.nombre_provincia ASC')
         ->addOrderBy('l.nombre_localidad ASC') ;
       return $q->execute();
    }

      public function retrieveForSelect($q, $limit) {
	    $q = Doctrine_Query::create()
	        ->from('Localidad l')
                ->orWhere('l.nombre_localidad like ?', '%'.$q.'%')
                ->innerJoin('l.Provincia p')
                ->orWhere('p.nombre_provincia like ?', '%'.$q.'%')
                ->innerJoin('p.Pais pa')
                ->orWhere('pa.nombre_pais like ?', '%'.$q.'%')
                ->orderBy('pa.nombre_pais ASC')
                ->addOrderBy(' p.nombre_provincia ASC')
                ->addOrderBy('l.nombre_localidad ASC')
	         ->limit(30);
	    $localidads = array();
	    foreach ($q->execute() as $localidad) {
	        $localidads[$localidad->getId()] = (string) $localidad->getProcedencia();
	    }
	    return $localidads;
	}
}