<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Publico
 *
 * @author leandro
 */
class Publico {
    
  public $index;  
  public $id;
  public $nombre;
  public $descripcion;
  public $museoName;  
  public $museoSubName;
  public $file;
  public $anio;
  public $texto_antiguedad;  
  
  public $tipo;
  public $tipoId;
  public $autor;
  public $autorId;
  public $colecciones;
  public $materiales;
  
  public $imagen1;
  public $imagenes;



  public function __construct(Objeto $obj, Array $arra, $index = 1 )
  {

        $this->index=$index;
        $this->id= $obj->getId();
        $this->nombre = $obj->getNombreObjeto();
  	$this->descripcion = $obj->getDescripcionGenerica();
  	$this->museoName = $arra['name'] ; 
        $this->museoSubName = $arra['subname'] ; 
  	$this->file = $arra['file'] ;
        $this->anio= $obj->getAnioOrigen();
        $this->texto_antiguedad= $obj->getTextoAntiguedad();

        
        if($obj->getAutorId() > 0 && $obj->getAutorId()!=NULL){ 
            $this->autorId =$obj->getAutorId();
        }     
        
        
        if($obj->getTipoObjetoId() > 0 ){
            $this->tipoId = $obj->getTipoObjetoId();
        }
            
  }
  
      public function getId(){
            return $this->id;
        }
      public function getIndex(){
            return $this->index;
        }
      public function getNombre(){
            return $this->nombre;
        }
      public function getDescripcion(){
          return $this->descripcion;
      }
      public function getMuseo(){
         return $this->museoName;
      }
      public function getMuseoSub(){
           return $this->museoSubName;
      }
      public function getNombreTitulo(){  
		return substr($this->nombre, 0, 25).'...';
        }
      public function getFile(){
           return $this->file;
      }
      public function getAnio(){
           return $this->anio;
      }
      public function getTextoAntiguedad(){
           return $this->texto_antiguedad;
      }
      
      public function getAutor(){
          
          if( ($this->autor== Null) ||($this->autor== '')){
              if($this->autorId > 0){
                  $conn= Doctrine_Manager::getInstance()->getConnection($this->file );
                  $autor = $this->getAutorSearch($conn, $this->autorId );
                    if(count($autor)> 0 && $autor != Null){
                        $this->autor = $autor->getApellido().', '.$autor->getNombre();
                    }
              }
          }
          return $this->autor;          
      }
      
      public function getTipo(){
          
          if( ($this->tipo== Null) ||($this->tipo== '')){
              if($this->tipoId > 0){
                  $conn= Doctrine_Manager::getInstance()->getConnection($this->file );
                  $tipo = $this->getTipoObjetoSearch($conn, $this->tipoId );
                    if(count($tipo)> 0 && $tipo != Null){
                        $this->tipo = $tipo->getNombre();
                    }
              }
          }
          return $this->tipo;
      }
      public function getColecciones(){
          
            if( ($this->colecciones == Null) ||($this->colecciones == '')){
                  $conn= Doctrine_Manager::getInstance()->getConnection($this->file );
                  foreach ($this->getColectionSearch( $conn) as $colecc){
                          $this->colecciones.=$colecc.',';
                   }  
              }
          
          return $this->colecciones;
          
      }
      public function getMateriales(){
          
            if( ($this->materiales == Null) ||($this->materiales == '')){
                  $conn= Doctrine_Manager::getInstance()->getConnection($this->file );
                  foreach ($this->getMaterialSearch( $conn) as $mater){
                          $this->materiales.=$mater.',';
                   }
              }
              
          return $this->materiales;
      }
      
      public function getImagenes(){
            $this->imagenes= array();
            
             if( ($this->imagenes == Null) ||($this->imagenes == '')){
                  $conn= Doctrine_Manager::getInstance()->getConnection($this->file );
                  foreach($this->getImagenSearch($conn) as $ima){
                          $imagen= array();  
                          $imagen['titulo']= $ima->getTitulo();
                          $imagen['archivo'] = $ima->getArchivo();        
                          $this->imagenes[] =$imagen;
                   }
              }
          
          return $this->imagenes;
      }

      public function getImagen1(){
          
          
           if( ($this->imagen1 == Null) ||($this->imagen1 == '')){
                  $conn= Doctrine_Manager::getInstance()->getConnection($this->file );                
                  $ima1 =$this->getImagen1Search($conn);
                  if($ima1 != null )
                        $this->imagen1= $ima1->getArchivo();
              }
          
          return $this->imagen1;
      }
  public function getAutorSearch($conn, $persona){
      
          $q = Doctrine_Query::create($conn)
                ->from('Persona p')
	        ->Where('p.id = ?', $persona)
	        ->limit(1);
	    return $foto = $q->fetchOne();
            
  }
  public function getTipoObjetoSearch($conn, $to){
      
          $q = Doctrine_Query::create($conn)
                ->from('TipoObjeto t')
	        ->Where('t.id = ?', $to)
	        ->limit(1);
	    return $foto = $q->fetchOne();
            
  }

  public function getColectionSearch($conn){

          $q = Doctrine_Query::create($conn)
                   ->from('coleccion c')
                   ->leftJoin('c.ObjetoColeccion oa')     
                   ->Where('oa.objeto_id = ?', $this->id);
          return $q->execute();
  }
  public function getMaterialSearch($conn){

          $q = Doctrine_Query::create($conn)
                   ->from('Material c')
                   ->leftJoin('c.ObjetoMaterial om')     
                   ->Where('om.objeto_id = ?', $this->id);
          return $q->execute();
  }
  public function getImagenSearch($conn){

          $q = Doctrine_Query::create($conn)
                   ->from('Imagen i')
	           ->andWhere('i.objeto_id = ?', $this->id);
          return $q->execute();
  }
  public function getImagen1Search($conn){
      
          $q = Doctrine_Query::create($conn)
                ->from('Imagen i')
	        ->andWhere('i.objeto_id = ?', $this->id)
                ->andWhere('i.titulo like ?',  '%frente%')
	        ->limit(1);
	    return $foto = $q->fetchOne();
  }

}


?>
