<?php //sfLoader::loadHelpers(array('Number','I18N','Date')); ?>
<?php

class ExcelBuilder 
{

  public function arrayToString($arr,$sep="\n")
  {
    $ret='';
    foreach($arr as $str){
      $ret.=(empty($ret)?'':$sep).$str;
    }
    return  $ret;
  }

	
	private function writeExcelCellValue($objPHPExcel,$row,$column,$value)
	{
		$objPHPExcel->getActiveSheet()->setCellValue($column.$row, $value);
	}

	private function writeExcelCellValueSheetTitle($objPHPExcel,$row,$column,$value)
	{
		$objPHPExcel->getActiveSheet()->setCellValue($column.$row, $value);
		$format = array(
			 'font'    => array('name'      => 'Arial',
								'bold'      => true,
								'italic'    => false,
								'underline' => false, //PHPExcel_Style_Font::UNDERLINE_DOUBLE,
								'strike'    => false,
								'size'      => '12',
								'color'     => array('rgb' => 'FF0000' )),
			'alignment'=>array('center'=>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
			'fill'    => array(	 			
								'type'		 => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
								'rotation'   => 0,
								'startcolor' => array('argb' => 'FFFFFFFF'	),
								'endcolor'   => array('argb' => 'FFFFFFFF'	),
								),
		);
		//$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setSize(20);
		$this->applyExcellStyleToCell($objPHPExcel,$row,$column,$format);
	}
	

	private function applyExcellStyleToCell($objPHPExcel,$row,$column,$format)
	{
		$objPHPExcel->getActiveSheet()->getStyle($column.$row)->applyFromArray($format);
	}
	
	private function writeExcelRow($objPHPExcel,$row, $from_column,$values, $format=array())
	{
		$column = $from_column;
		foreach($values as $value)
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
			$this->writeExcelCellValue($objPHPExcel,$row,$column,$value);
			$this->applyExcellStyleToCell($objPHPExcel,$row,$column,$format);			
			$column = chr(ord($column) + 1);
			
		}
	}
	
	private function buildExcelHeaderFormat()
	{
		$ret = array(
			'font' => array('bold'=>true,),
			'aligment'=>array('center'=>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
		);
		return array_merge($ret,$this->buildExcelGeneralFormat());
	}
	
	
	private function buildExcelGeneralFormat()
	{
		
		$ret = array(
				'borders' => array(
									'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,),
									'bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,),
									'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,),
									'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,),
 				)
		);
		return $ret;
	}

	private function buildExcelHeaderFormatColor($font_color='FFFFFF',$fill_color='FF5566AA')
	{
		$ret = array(
			 'font'    => array('name'      => 'Arial',
								'bold'      => true,
								'italic'    => false,
								'underline' => false, //PHPExcel_Style_Font::UNDERLINE_DOUBLE,
								'strike'    => false,
								'color'     => array('rgb' => $font_color )),
			'alignment'=>array('center'=>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
			'fill'    => array(	 			
								'type'		 => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
								'rotation'   => 0,
								'startcolor' => array('argb' => $fill_color	),
								'endcolor'   => array('argb' => $fill_color ),
								),
		);
		return array_merge($ret,$this->buildExcelGeneralFormat());
	}

	private function buildExcelGeneralFormatWithParam($color, $val)
	{
		switch ($color) {
		case 1:
			$colour = 'FFFFFF66';
			break;
		case 2:
			$colour = 'FFFFFFFF';
			break;
		case 3:
			$colour = 'FFCCCCCC';
			break;
		}

		$ret = array(
				'borders' => array(
									'top'     => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,),
									'bottom'  => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,),
									'left'    => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,),
									'right'   => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,),
									),
				'fill'    => array(	 			
									'type'		 => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
									'rotation'   => 0,
									'startcolor' => array('argb' => $colour	),	 			
									'endcolor'   => array('argb' => $colour	),
									),
				'font'    => array('name'      => 'Arial',
									'bold'      => $val,
									'italic'    => false,
									'underline' => false,
									'strike'    => false,),
		);
		return $ret;
	}
	
        ///
  
       public function generateExcel($titulo, $cabecera, $data)
	{
           
		$objPHPExcel = new PHPExcel();//new sfPhpExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->writeExcelCellValue($objPHPExcel,1,'A',$titulo);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
                $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
             
                
		$this->writeExcelHeader($objPHPExcel, $cabecera);		
		$this->writeExcelData($objPHPExcel,$data);
		return $objPHPExcel;
	}       
        
        public function writeExcelHeader($objPHPExcel, $values ){
		$this->writeExcelRow($objPHPExcel,3,'A',$values,$this->buildExcelHeaderFormat());
        }  
        
	private function writeExcelData($objPHPExcel, $datas)
	{
		$row = 4;
		foreach ($datas as $data) {
			$values = array();
                      
                        
                        foreach ($data as $d){
                            if(is_array( $d)){
                                foreach ($d as $di){
                                          $values[] =$di;
                                 }
                              }else{
                                  $values[] =$d;
                                
                            }
                        }                        
			$this->writeExcelRow($objPHPExcel,$row,'A',$values, $this->buildExcelGeneralFormat());
			$row ++;
		}

	 }
       private function writeExcelDataId($objPHPExcel, $datas)
	{
		$row = 4;
		foreach ($datas as $data) {
			$values = array();
                        $firts= true;
                        
                        foreach ($data as $d){
                            if(is_array( $d)){
                                $firtsi= true;
                                foreach ($d as $di){
                                    if(! $firtsi )
                                          $values[] =$di;
                                        else
                                            $firtsi=false;
                                 }
                              }else{
                                
                                if(!$firts)
                                  $values[] =$d;
                                else
                                    $firts=false;
                            }
                        }                        
			$this->writeExcelRow($objPHPExcel,$row,'A',$values, $this->buildExcelGeneralFormat());
			$row ++;
		}

	 }
////////////////////////////////////////informe general		
         
       public function generateExcelGeneral($titulos, $data, $filtros)
	{
           
		$objPHPExcel = new PHPExcel();//new sfPhpExcel();
                $cont=0;
                $date= date('d-m-Y');     
                foreach ($titulos as $titulo ){
                    $objPHPExcel->createSheet();
                    $objPHPExcel->setActiveSheetIndex($cont);
                    $objPHPExcel->getActiveSheet($cont)->setTitle($titulo);
                    $objPHPExcel->getActiveSheet($cont)->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                    
                    $this->writeExcelCellValue($objPHPExcel,1,'A',$titulo);
                    $this->writeExcelCellValue($objPHPExcel,1,'F','Emitido :'.$date);
                    $this->writeExcelCellValue($objPHPExcel,2,'A', $filtros[0]);
                    $this->writeExcelCellValue($objPHPExcel,2,'C', $filtros[1]);
                    if(!empty($filtros[2])){
                        $this->writeExcelCellValue($objPHPExcel,3,'A','Tipo de actividad :'.$filtros[2]);
                    }
                    $pos= $this->getLetra(sizeof($data[$cont][0]));
                    $objPHPExcel->getActiveSheet($cont)->mergeCells('A1:'.$pos.'1');
                    $objPHPExcel->getActiveSheet($cont)->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                    $this->writeExcelTable($objPHPExcel,$data[$cont]);
                    $cont++;
                }
		return $objPHPExcel;
	}       
           
           
       
        
	private function writeExcelTable($objPHPExcel, $datas)
	{
		$row = 5;
		foreach ($datas as $data) {
			$values = array();
                      
                        
                        foreach ($data as $d){
                            if(is_array( $d)){
                                foreach ($d as $di){
                                          $values[] =$di;
                                 }
                              }else{
                                  $values[] =$d;
                                
                            }
                        }                        
			$this->writeExcelRow($objPHPExcel,$row,'A',$values, $this->buildExcelGeneralFormat());
			$row ++;
		}

	 }
         ////////////////////////////////////////informe general
////////////////////////////////////////fin informe general    
	 private function getLetra( $numero ){
	 	
	 	switch($numero){
           	case 1 :
           		$pos= "A";	
            break;
           	case 2 :
           		$pos= "B";	
            break;
           	case 3 :
           		$pos= "C";	
            break;
           	case 4 :
           		$pos= "D";	
            break;
           	case 5 :
           		$pos= "E";	
            break;
           	case 6 :
           		$pos= "F";	
            break;
           	case 7 :
           		$pos= "G";	
            break;
           	case 8 :
           		$pos= "H";	
            break;
           	case 9 :
           		$pos= "I";	
            break;
           	case 10 :
           		$pos= "J";	
            break;
           	case 11 :
           		$pos= "K";	
            break;
           	case 12 :
           		$pos= "L";	
            break;
           	case 13 :
           		$pos= "M";	
            break;
           	case 14 :
           		$pos= "N";	
            break;
           	case 15 :
           		$pos= "O";	
            break;
           	
            default :
                $pos= "P";	
            break;      
        }
        return $pos;

	 }

}
?>
