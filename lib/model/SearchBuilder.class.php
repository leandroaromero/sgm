<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class SearchBuilder 
{
    public $museo;
    public $general;
    public $nombre;
    public $descripcion;
    public $autor;
    public $tipo;
    public $categoria;
    public $material;
    public $materialArray;
    public $coleccion;
    public $bellas_artes;
    public $ccias_naturales;
    public $medios_comunicacion;
    public $hombre_chaquenio;
    public $ichoalay;    
    public $isla_cerrito;
    public $jardin_botanico;

    public function __construct($dato)
  {   
        
       $this->museo= $dato['museo'];
       
       if(isset($dato['general']))
       $this->general= $dato['general'];
       
       if(isset($dato['nombre']))
       $this->nombre= $dato['nombre'];
       
       if(isset($dato['descripcion']))
            $this->descripcion= $dato['descripcion'];
       
       if(isset($dato['autor']))
            $this->autor= $dato['autor'];
       
       if(isset($dato['tipo']))
            $this->tipo= $dato['tipo'];
              
       if(isset($dato['categoria']))
            $this->categoria= $dato['categoria'];
       
       if(isset($dato['material'])){
           $this->materialArray = explode(",", $dato['material']);
           $this->material = $dato['material'];
       }
       
       if(isset($dato['coleccion']))
            $this->coleccion= $dato['coleccion'];
       
//       $this->bellas_artes = 'mysql://root:sistemas@localhost/cw000566_muba';
//       $this->ccias_naturales = 'mysql://root:sistemas@localhost/cw000566_naturales';
//       $this->medios_comunicacion = 'mysql://root:sistemas@localhost/cw000566_medios';
//       $this->hombre_chaquenio = 'mysql://root:sistemas@localhost/cw000566_mhch';
//       $this->ichoalay =     'mysql://root:sistemas@localhost/cw000566_ichoalay';
//       $this->isla_cerrito = 'mysql://root:sistemas@localhost/cw000566_isla';
//       $this->jardin_botanico = 'mysql://root:sistemas@localhost/cw000566_jardin';
//       
//       $this->bellas_artes =        'mysql://cw000566_sgm:CXZ958MHyzZJARzU@localhost/cw000566_muba';
//       $this->ccias_naturales =     'mysql://cw000566_sgm:CXZ958MHyzZJARzU@localhost/cw000566_naturales';
//       $this->medios_comunicacion = 'mysql://cw000566_sgm:CXZ958MHyzZJARzU@localhost/cw000566_medios';
//       $this->hombre_chaquenio =    'mysql://cw000566_sgm:CXZ958MHyzZJARzU@localhost/cw000566_mhch';
//       $this->ichoalay =            'mysql://cw000566_sgm:CXZ958MHyzZJARzU@localhost/cw000566_ichoalay';
//       $this->isla_cerrito =        'mysql://cw000566_sgm:CXZ958MHyzZJARzU@localhost/cw000566_isla';
//       $this->jardin_botanico =     'mysql://cw000566_sgm:CXZ958MHyzZJARzU@localhost/cw000566_jardin';

    }
    public function getBasesDatos(){
       $this->respuesta= array();
       $this->respuestaTotal= array();
       switch ($this->museo){
        case 'archivo_historico':
                 $this->respuesta['name'] = 'Archivo Histórico';
                 $this->respuesta['head'] = 'A. Histórico';
                 $this->respuesta['subname'] = 'Mons. José Alumni';
                 $this->respuesta['file'] = 'archivo_historico';            
              break;
        case 'geraldi':  
                 $this->respuesta['name'] = 'Casa, Museo y Sitio Histórico';
                 $this->respuesta['head'] = 'M. Luis Geraldi';
                 $this->respuesta['subname'] = 'Luis Geraldi';
                 $this->respuesta['file'] = 'geraldi';
            break;    
        case 'bellas_artes':
//                 $this->respuesta['conection'] = Doctrine_Manager::connection($this->bellas_artes , 'bellas_artes');
                 $this->respuesta['name'] = 'Museo Bellas Artes';
                 $this->respuesta['head'] = 'M. Bellas Artes';
                 $this->respuesta['subname'] = 'Rene Brusau';
                 $this->respuesta['file'] = 'bellas_artes';
            break;
        case 'ccias_naturales':
//                 $this->respuesta['conection'] = Doctrine_Manager::connection( $this->ccias_naturales , 'ccias_naturales');
                 $this->respuesta['name'] = 'Museo Ciencias Naturales';
                 $this->respuesta['head'] = 'M. Cs. Naturales';
                 $this->respuesta['subname'] = 'Augusto Schulz';
                 $this->respuesta['file'] = 'ccias_naturales';
             break;
        case 'medios_comunicacion':
//                 $this->respuesta['conection'] = Doctrine_Manager::connection($this->medios_comunicacion, 'medios_comunicacion');
                 $this->respuesta['name'] = 'Museo de Medios';
                 $this->respuesta['head'] = 'M. de Medios';
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'medios_comunicacion';
          break;
        case 'hombre_chaquenio':
//                 $this->respuesta['conection'] = Doctrine_Manager::connection($this->hombre_chaquenio, 'hombre_chaquenio');
                 $this->respuesta['name'] = 'Museo del Hombre Chaqueño';
                 $this->respuesta['head'] = 'M. del Hombre';
                 $this->respuesta['subname'] = 'Profesor Ertivio Acosta';
                 $this->respuesta['file'] = 'hombre_chaquenio';
            break;
        case 'ichoalay':
//                 $this->respuesta['conection'] = Doctrine_Manager::connection($this->ichoalay, 'ichoalay');
                 $this->respuesta['name'] = 'Museo Ichoalay';
                 $this->respuesta['head'] = 'M. Ichoalay';
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'ichoalay';
             break;
        case 'isla_cerrito':
//                 $this->respuesta['conection'] = Doctrine_Manager::connection( $this->isla_cerrito, 'isla_cerrito');
                 $this->respuesta['name'] = "Museo Histórico Regional";
                 $this->respuesta['head'] = 'M. De la Isla';
                 $this->respuesta['subname'] = 'De la Isla';
                 $this->respuesta['file'] = 'isla_cerrito';
              break;
        case 'jardin_botanico':

                 $this->respuesta['name'] = " Museo Casa Jardín Botánico";
                 $this->respuesta['head'] = " M. Jardín Botánico";
                 $this->respuesta['subname'] = 'Augusto Schulz';
                 $this->respuesta['file'] = 'jardin_botanico';
              break;
        case 'casa_meloni':

                 $this->respuesta['name'] = " Museo Casa Meloni";
                 $this->respuesta['head'] = " M.C. Meloni";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'casa_meloni';
              break;
        case 'so_sotelo':

                 $this->respuesta['name'] = " Museo Artesanal R. J. Sotelo";
                 $this->respuesta['head'] = " M.A. R.J.Sotelo";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_sotelo';
              break;
        case 'so_urbana':

                 $this->respuesta['name'] = " Museo de las Esculturas";
                 $this->respuesta['head'] = " M. de E. Urbanas";
                 $this->respuesta['subname'] = 'Urbanas del Mundo';
                 $this->respuesta['file'] = 'so_urbana';
              break;
        case 'so_fratti':

                 $this->respuesta['name'] = " Museo de la Educación";
                 $this->respuesta['head'] = " M.E. F.L. Fratti";
                 $this->respuesta['subname'] = 'Francisco Lázaro Fratti';
                 $this->respuesta['file'] = 'so_fratti';
              break;
        case 'so_malvin':
                 $this->respuesta['name'] = " Museo Malvinas Chaco";
                 $this->respuesta['head'] = " M. Malvinas C";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_malvin';
              break;
        case 'so_quijano':

                 $this->respuesta['name'] = " Museo casa Quijano";
                 $this->respuesta['head'] = " M. C. Quijano";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_quijano';
              break;
        case 'so_carnaval':
                 $this->respuesta['name'] = " Museo Del Carnaval";
                 $this->respuesta['head'] = " M. Carnaval";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_carnaval';
              break;
        case 'so_palmas':
                 $this->respuesta['name'] = " M. S. Las Palmas";
                 $this->respuesta['head'] = " M. Las Palmas";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_palmas';
              break;
        case 'so_peroni':
                 $this->respuesta['name'] = "M. H. P. del Peronismo C.";
                 $this->respuesta['head'] = " M. Peronismo";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_peroni';
              
      default :
          
//Archivo Historico
                 $this->respuesta['name'] = 'Archivo Histórico';
                 $this->respuesta['head'] = 'A. Histórico';
                 $this->respuesta['subname'] = 'Mons. José Alumni';
                 $this->respuesta['file'] = 'archivo_historico';
                 
                 $this->respuestaTotal[]=$this->respuesta;
//GERALDI        
                 $this->respuesta['name'] = 'Casa, Museo y Sitio Histórico';
                 $this->respuesta['head'] = 'M. Luis Geraldi';
                 $this->respuesta['subname'] = 'Luis Geraldi';
                 $this->respuesta['file'] = 'geraldi';
                 
                 $this->respuestaTotal[]=$this->respuesta;
        
        //COLONIA
                 $this->respuesta['name'] = " Museo Casa Jardín Botánico";
                 $this->respuesta['head'] = " M. Jardín Botánico";
                 $this->respuesta['subname'] = 'Augusto Schulz';
                 $this->respuesta['file'] = 'jardin_botanico';
                 
                 $this->respuestaTotal[]=$this->respuesta;             
        //MUBA         
                 $this->respuesta['name'] = 'Museo Bellas Artes';
                 $this->respuesta['head'] = 'M. Bellas Artes';
                 $this->respuesta['subname'] = 'Rene Brusau';
                 $this->respuesta['file'] = 'bellas_artes';
                 
                 $this->respuestaTotal[]=$this->respuesta;
                 
       //CIENCIAS             
                 $this->respuesta['name'] = 'Museo Ciencias Naturales';
                 $this->respuesta['head'] = 'M. Cs. Naturales';
                 $this->respuesta['subname'] = 'Augusto Schulz';
                 $this->respuesta['file'] = 'ccias_naturales';
                 
                 $this->respuestaTotal[]=$this->respuesta;
        
//MEDIOS           $this->respuesta['conection'] = Doctrine_Manager::connection($this->medios_comunicacion, 'medios_comunicacion');
                 $this->respuesta['name'] = 'Museo de Medios';
                 $this->respuesta['head'] = 'M. de Medios';
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'medios_comunicacion';
                 
                 $this->respuestaTotal[]=$this->respuesta;
        
//HOMBRE     $this->respuesta['conection'] = Doctrine_Manager::connection($this->hombre_chaquenio, 'hombre_chaquenio');
                 $this->respuesta['name'] = 'Museo del Hombre Chaqueño';
                 $this->respuesta['head'] = 'M. del Hombre';
                 $this->respuesta['subname'] = 'Profesor Ertivio Acosta';
                 $this->respuesta['file'] = 'hombre_chaquenio';
                 
                 $this->respuestaTotal[]=$this->respuesta;
        
//ISLA
                 $this->respuesta['name'] = "Museo Histórico Regional";
                 $this->respuesta['head'] = 'M. De la Isla';
                 $this->respuesta['subname'] = 'De la Isla';
                 $this->respuesta['file'] = 'isla_cerrito';
                 
                 $this->respuestaTotal[]=$this->respuesta;
              
//ICHOALAY   
                 $this->respuesta['name'] = 'Museo Ichoalay';
                 $this->respuesta['head'] = 'M. Ichoalay';
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'ichoalay';
   
                 $this->respuestaTotal[]=$this->respuesta;
              
//MELONI
                 $this->respuesta['name'] = 'Museo Casa Meloni';
                 $this->respuesta['head'] = 'M.C. Meloni';
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'casa_meloni';
   
                 $this->respuestaTotal[]=$this->respuesta;
                 
//SOTELO

                 $this->respuesta['name'] = " Museo Artesanal R. J. Sotelo";
                 $this->respuesta['head'] = " M.A. R.J.Sotelo";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_sotelo';        
   
                 $this->respuestaTotal[]=$this->respuesta;         
//Urbanas
                 $this->respuesta['name'] = " Museo de las Esculturas";
                 $this->respuesta['head'] = " M. de E. Urbanas";
                 $this->respuesta['subname'] = 'Urbanas del Mundo';
                 $this->respuesta['file'] = 'so_urbana';
   
                 $this->respuestaTotal[]=$this->respuesta;

//Fratti
                 $this->respuesta['name'] = " Museo de la Educación";
                 $this->respuesta['head'] = " M.E. F.L. Fratti";
                 $this->respuesta['subname'] = 'Francisco Lázaro Fratti';
                 $this->respuesta['file'] = 'so_fratti';
   
                 $this->respuestaTotal[]=$this->respuesta;

//Malvinas        
                 $this->respuesta['name'] = " Museo Malvinas Chaco";
                 $this->respuesta['head'] = " M. Malvinas C";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_malvin';
   
                 $this->respuestaTotal[]=$this->respuesta;

//QUIJANO
                 $this->respuesta['name'] = " Museo casa Quijano";
                 $this->respuesta['head'] = " M. C. Quijano";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_quijano';
   
                 $this->respuestaTotal[]=$this->respuesta;

//Malvinas        
                 $this->respuesta['name'] = " Museo del Carnaval";
                 $this->respuesta['head'] = " M. Carnaval";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_carnaval';
   
                 $this->respuestaTotal[]=$this->respuesta;
//Las Palmas
                 $this->respuesta['name'] = " M. S. Las Palmas";
                 $this->respuesta['head'] = " M. Las Palmas";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_palmas';
   
                 $this->respuestaTotal[]=$this->respuesta;
//Del Peronismo
                 $this->respuesta['name'] = "M. H. P. del Peronismo C.";
                 $this->respuesta['head'] = " M. Peronismo";
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'so_peroni';

              break;
          
       }
        $this->respuestaTotal[]=$this->respuesta;
        return $this->respuestaTotal;
    }
    
    public function getResultPager(Array $result){
       
        
    }
    public function getInstitutionName(){
    
        $name='';
        switch ($this->museo){
          case 'archivo_historico':
              $name='ARCHIVO HISTORICO';
              break;
          case 'bellas_artes':
              $name='MUSEO DE BELLAS ARTES';
              break;
          case 'ccias_naturales':
              $name='MUSEO CIENCIAS NATURALES';
              break;
          case 'medios_comunicacion':
              $name='MUSEO DE MEDIOS DE COMUNICACION';
              break;
          case 'ichoalay':
              $name='MUSEO HISTORICO REGIONAL ICHOALAY';
              break;
          case 'isla_cerrito':
              $name='MUSEO DE LA ISLA';
              break;
          case 'geraldi':
              $name='CASA, MUSEO Y SITIO HISTORICO GERALDI';
              break;
          case 'hombre_chaquenio':
              $name='MUSEO DEL HOMBRE CHAQUEÑO';
              break;
          case 'casa_meloni':
              $name='MUSEO CASA MELONI';
              break;
		  case 'so_sotelo':
              $name='MUSEO ARTESANAL';
              break;
          case 'so_malvin':
              $name='MUSEO MALVINAS C.';
              break;
          case 'so_quijano':
              $name='MUSEO CASA QUIJANO';
              break;
          case 'so_carnaval':
              $name='MUSEO DEL CARNAVAL.';
              break;
          case 'so_carnaval':
              $name='MUSEO de S. LAS PALMAS';
              break;
          case 'so_peroni':
              $name='MUSEO del PERONISMO';
              break;
          default :
              $name='Todas las instituciones';
        }
              return $name;

    }
    public function getGeneral(){
        return $this->general;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function getTipo(){
        return $this->tipo;
    }
    public function getAutor(){
        return $this->autor;
    }
    public function getColeccion(){
        return $this->coleccion;
    }
    public function getMaterial(){
        return $this->material;
    }
    public function getMaterialArray(){
        return $this->materialArray;
    }
    public function getCategoria(){
        return $this->categoria;
    }
    
    
    public function getBusquedas(){
        $dato=array();
        $dato['museo']       = $this->getInstitutionName() ;
        $dato['nombre']     = $this->nombre;
        $dato['general']     = $this->general;
        $dato['descripcion'] = $this->descripcion;
        $dato['autor']       = $this->autor;
        $dato['tipo']        = $this->tipo;
        $dato['material']    = $this->material;
        $dato['coleccion']   = $this->coleccion;
        $dato['categoria']   = $this->categoria;
        
        return $dato;
    }
} 
?>
