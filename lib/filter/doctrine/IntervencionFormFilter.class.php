<?php

/**
 * Intervencion filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class IntervencionFormFilter extends BaseIntervencionFormFilter
{
  public function configure()
  {
        $format = '%day%/%month%/%year%';
        $web = sfConfig::get('app_url_web');
        $rango = range(2021, 2025);
        $arreglo_rango = array_combine($rango, $rango);
        
        $this->widgetSchema['fecha_de_diagnostico']= new sfWidgetFormFilterDate(array(
          'from_date' => new sfWidgetFormJQueryDate(array(
             'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))


            )), 'to_date' => new sfWidgetFormJQueryDate(array(
                 'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))

            )), 'with_empty' => false));

      //tratamiento del objeto a ser tratado
      sfProjectConfiguration::getActive()->loadHelpers('Url');
      $this->widgetSchema['objeto_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['objeto_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
          array(
            'model' => "Objeto",
            'url'   => url_for("@ajax_objeto"),
            'config' => '{ max: 30}'
            ));
   

      
        $this->widgetSchema['intervencion_estado_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
        $this->widgetSchema['intervencion_estado_list']->setOption('label', 'Detalle del estado');
        $this->widgetSchema['intervencion_estado_list']->setOption('table_method', 'getOrderNombre');


 
  }
}
