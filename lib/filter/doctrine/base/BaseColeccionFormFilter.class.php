<?php

/**
 * Coleccion filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseColeccionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre_coleccion' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'coleccion_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Objeto')),
      'actividades_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Actividad')),
    ));

    $this->setValidators(array(
      'nombre_coleccion' => new sfValidatorPass(array('required' => false)),
      'coleccion_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Objeto', 'required' => false)),
      'actividades_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Actividad', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('coleccion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addColeccionListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ObjetoColeccion ObjetoColeccion')
      ->andWhereIn('ObjetoColeccion.objeto_id', $values)
    ;
  }

  public function addActividadesListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ActividadColeccion ActividadColeccion')
      ->andWhereIn('ActividadColeccion.actividad_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Coleccion';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'nombre_coleccion' => 'Text',
      'coleccion_list'   => 'ManyKey',
      'actividades_list' => 'ManyKey',
    );
  }
}
