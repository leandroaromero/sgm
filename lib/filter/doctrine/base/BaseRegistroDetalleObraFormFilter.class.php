<?php

/**
 * RegistroDetalleObra filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseRegistroDetalleObraFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'objeto_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => true)),
      'detalle_obra_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DetalleObra'), 'add_empty' => true)),
      'valor'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'objeto_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Objeto'), 'column' => 'id')),
      'detalle_obra_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('DetalleObra'), 'column' => 'id')),
      'valor'           => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('registro_detalle_obra_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'RegistroDetalleObra';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'objeto_id'       => 'ForeignKey',
      'detalle_obra_id' => 'ForeignKey',
      'valor'           => 'Text',
    );
  }
}
