<?php

/**
 * Trayectoria filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTrayectoriaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre_evento'       => new sfWidgetFormFilterInput(),
      'fecha'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'lugar'               => new sfWidgetFormFilterInput(),
      'relato'              => new sfWidgetFormFilterInput(),
      'tipo_evento_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoEvento'), 'add_empty' => true)),
      'galeria_de_imagenes' => new sfWidgetFormFilterInput(),
      'nro_acta'            => new sfWidgetFormFilterInput(),
      'trayectoria_list'    => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Objeto')),
    ));

    $this->setValidators(array(
      'nombre_evento'       => new sfValidatorPass(array('required' => false)),
      'fecha'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'lugar'               => new sfValidatorPass(array('required' => false)),
      'relato'              => new sfValidatorPass(array('required' => false)),
      'tipo_evento_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TipoEvento'), 'column' => 'id')),
      'galeria_de_imagenes' => new sfValidatorPass(array('required' => false)),
      'nro_acta'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'trayectoria_list'    => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Objeto', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('trayectoria_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addTrayectoriaListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ObjetoTrayectoria ObjetoTrayectoria')
      ->andWhereIn('ObjetoTrayectoria.objeto_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Trayectoria';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'nombre_evento'       => 'Text',
      'fecha'               => 'Date',
      'lugar'               => 'Text',
      'relato'              => 'Text',
      'tipo_evento_id'      => 'ForeignKey',
      'galeria_de_imagenes' => 'Text',
      'nro_acta'            => 'Number',
      'trayectoria_list'    => 'ManyKey',
    );
  }
}
