<?php

/**
 * IntervencionTipoFotografia filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseIntervencionTipoFotografiaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'intervencion_tipo_imagen_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('IntervencionTipoImagen'), 'add_empty' => true)),
      'nombre'                      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'intervencion_tipo_imagen_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('IntervencionTipoImagen'), 'column' => 'id')),
      'nombre'                      => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('intervencion_tipo_fotografia_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IntervencionTipoFotografia';
  }

  public function getFields()
  {
    return array(
      'id'                          => 'Number',
      'intervencion_tipo_imagen_id' => 'ForeignKey',
      'nombre'                      => 'Text',
    );
  }
}
