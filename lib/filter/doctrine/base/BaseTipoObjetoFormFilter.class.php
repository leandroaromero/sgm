<?php

/**
 * TipoObjeto filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTipoObjetoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tipo_objeto_id'   => new sfWidgetFormFilterInput(),
      'actividades_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Actividad')),
    ));

    $this->setValidators(array(
      'nombre'           => new sfValidatorPass(array('required' => false)),
      'tipo_objeto_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'actividades_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Actividad', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('tipo_objeto_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addActividadesListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ActividadTipoObjeto ActividadTipoObjeto')
      ->andWhereIn('ActividadTipoObjeto.actividad_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'TipoObjeto';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'nombre'           => 'Text',
      'tipo_objeto_id'   => 'Number',
      'actividades_list' => 'ManyKey',
    );
  }
}
