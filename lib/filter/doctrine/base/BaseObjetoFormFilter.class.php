<?php

/**
 * Objeto filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseObjetoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre_objeto'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'anio_origen'          => new sfWidgetFormFilterInput(),
      'texto_antiguedad'     => new sfWidgetFormFilterInput(),
      'autor_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Autor'), 'add_empty' => true)),
      'donacion_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Donacion'), 'add_empty' => true)),
      'fecha_ingreso'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'forma_ingreso'        => new sfWidgetFormFilterInput(),
      'fecha_baja'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'motivo_baja'          => new sfWidgetFormFilterInput(),
      'ubicacion_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Ubicacion'), 'add_empty' => true)),
      'historia_del_objeto'  => new sfWidgetFormFilterInput(),
      'localidad_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true)),
      'tasacion_valor'       => new sfWidgetFormFilterInput(),
      'tasacion_unidades'    => new sfWidgetFormFilterInput(),
      'tasacion_fuente'      => new sfWidgetFormFilterInput(),
      'galeria_de_imagenes'  => new sfWidgetFormFilterInput(),
      'descripcion_generica' => new sfWidgetFormFilterInput(),
      'tipo_objeto_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoObjeto'), 'add_empty' => true)),
      'created_by'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'coleccion_list'       => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Coleccion')),
      'dimension_list'       => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Dimension')),
      'material_list'        => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Material')),
      'trayectoria_list'     => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Trayectoria')),
      'categoria_list'       => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Categoria')),
    ));

    $this->setValidators(array(
      'nombre_objeto'        => new sfValidatorPass(array('required' => false)),
      'anio_origen'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'texto_antiguedad'     => new sfValidatorPass(array('required' => false)),
      'autor_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Autor'), 'column' => 'id')),
      'donacion_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Donacion'), 'column' => 'id')),
      'fecha_ingreso'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'forma_ingreso'        => new sfValidatorPass(array('required' => false)),
      'fecha_baja'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'motivo_baja'          => new sfValidatorPass(array('required' => false)),
      'ubicacion_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Ubicacion'), 'column' => 'id')),
      'historia_del_objeto'  => new sfValidatorPass(array('required' => false)),
      'localidad_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Localidad'), 'column' => 'id')),
      'tasacion_valor'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'tasacion_unidades'    => new sfValidatorPass(array('required' => false)),
      'tasacion_fuente'      => new sfValidatorPass(array('required' => false)),
      'galeria_de_imagenes'  => new sfValidatorPass(array('required' => false)),
      'descripcion_generica' => new sfValidatorPass(array('required' => false)),
      'tipo_objeto_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TipoObjeto'), 'column' => 'id')),
      'created_by'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Creator'), 'column' => 'id')),
      'updated_by'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Updator'), 'column' => 'id')),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'coleccion_list'       => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Coleccion', 'required' => false)),
      'dimension_list'       => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Dimension', 'required' => false)),
      'material_list'        => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Material', 'required' => false)),
      'trayectoria_list'     => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Trayectoria', 'required' => false)),
      'categoria_list'       => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Categoria', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('objeto_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addColeccionListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ObjetoColeccion ObjetoColeccion')
      ->andWhereIn('ObjetoColeccion.coleccion_id', $values)
    ;
  }

  public function addDimensionListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ObjetoDimension ObjetoDimension')
      ->andWhereIn('ObjetoDimension.dimension_id', $values)
    ;
  }

  public function addMaterialListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ObjetoMaterial ObjetoMaterial')
      ->andWhereIn('ObjetoMaterial.material_id', $values)
    ;
  }

  public function addTrayectoriaListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ObjetoTrayectoria ObjetoTrayectoria')
      ->andWhereIn('ObjetoTrayectoria.trayectoria_id', $values)
    ;
  }

  public function addCategoriaListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.CategoriaObjeto CategoriaObjeto')
      ->andWhereIn('CategoriaObjeto.categoria_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Objeto';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'nombre_objeto'        => 'Text',
      'anio_origen'          => 'Number',
      'texto_antiguedad'     => 'Text',
      'autor_id'             => 'ForeignKey',
      'donacion_id'          => 'ForeignKey',
      'fecha_ingreso'        => 'Date',
      'forma_ingreso'        => 'Text',
      'fecha_baja'           => 'Date',
      'motivo_baja'          => 'Text',
      'ubicacion_id'         => 'ForeignKey',
      'historia_del_objeto'  => 'Text',
      'localidad_id'         => 'ForeignKey',
      'tasacion_valor'       => 'Number',
      'tasacion_unidades'    => 'Text',
      'tasacion_fuente'      => 'Text',
      'galeria_de_imagenes'  => 'Text',
      'descripcion_generica' => 'Text',
      'tipo_objeto_id'       => 'ForeignKey',
      'created_by'           => 'ForeignKey',
      'updated_by'           => 'ForeignKey',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
      'coleccion_list'       => 'ManyKey',
      'dimension_list'       => 'ManyKey',
      'material_list'        => 'ManyKey',
      'trayectoria_list'     => 'ManyKey',
      'categoria_list'       => 'ManyKey',
    );
  }
}
