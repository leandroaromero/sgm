<?php

/**
 * FuenteBibliografica filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseFuenteBibliograficaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'titulo'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'editorial'     => new sfWidgetFormFilterInput(),
      'lugar'         => new sfWidgetFormFilterInput(),
      'anio'          => new sfWidgetFormFilterInput(),
      'direccion_web' => new sfWidgetFormFilterInput(),
      'relato'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'titulo'        => new sfValidatorPass(array('required' => false)),
      'editorial'     => new sfValidatorPass(array('required' => false)),
      'lugar'         => new sfValidatorPass(array('required' => false)),
      'anio'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'direccion_web' => new sfValidatorPass(array('required' => false)),
      'relato'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fuente_bibliografica_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FuenteBibliografica';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'titulo'        => 'Text',
      'editorial'     => 'Text',
      'lugar'         => 'Text',
      'anio'          => 'Number',
      'direccion_web' => 'Text',
      'relato'        => 'Text',
    );
  }
}
