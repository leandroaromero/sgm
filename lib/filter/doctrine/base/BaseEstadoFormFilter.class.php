<?php

/**
 * Estado filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEstadoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'estado_de_conservacion' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'fecha_de_cambio'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'motivo_del_cambio'      => new sfWidgetFormFilterInput(),
      'objeto_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'estado_de_conservacion' => new sfValidatorPass(array('required' => false)),
      'fecha_de_cambio'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'motivo_del_cambio'      => new sfValidatorPass(array('required' => false)),
      'objeto_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Objeto'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('estado_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Estado';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'estado_de_conservacion' => 'Text',
      'fecha_de_cambio'        => 'Date',
      'motivo_del_cambio'      => 'Text',
      'objeto_id'              => 'ForeignKey',
    );
  }
}
