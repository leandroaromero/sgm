<?php

/**
 * PublicObject filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePublicObjectFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'objeto_id'        => new sfWidgetFormFilterInput(),
      'nombre'           => new sfWidgetFormFilterInput(),
      'descripcion'      => new sfWidgetFormFilterInput(),
      'museo_name'       => new sfWidgetFormFilterInput(),
      'museo_sub_name'   => new sfWidgetFormFilterInput(),
      'dato'             => new sfWidgetFormFilterInput(),
      'anio'             => new sfWidgetFormFilterInput(),
      'texto_antiguedad' => new sfWidgetFormFilterInput(),
      'categoria'        => new sfWidgetFormFilterInput(),
      'tipo'             => new sfWidgetFormFilterInput(),
      'tipo_id'          => new sfWidgetFormFilterInput(),
      'autor'            => new sfWidgetFormFilterInput(),
      'autor_id'         => new sfWidgetFormFilterInput(),
      'colecciones'      => new sfWidgetFormFilterInput(),
      'materiales'       => new sfWidgetFormFilterInput(),
      'imagen'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'objeto_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nombre'           => new sfValidatorPass(array('required' => false)),
      'descripcion'      => new sfValidatorPass(array('required' => false)),
      'museo_name'       => new sfValidatorPass(array('required' => false)),
      'museo_sub_name'   => new sfValidatorPass(array('required' => false)),
      'dato'             => new sfValidatorPass(array('required' => false)),
      'anio'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'texto_antiguedad' => new sfValidatorPass(array('required' => false)),
      'categoria'        => new sfValidatorPass(array('required' => false)),
      'tipo'             => new sfValidatorPass(array('required' => false)),
      'tipo_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'autor'            => new sfValidatorPass(array('required' => false)),
      'autor_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'colecciones'      => new sfValidatorPass(array('required' => false)),
      'materiales'       => new sfValidatorPass(array('required' => false)),
      'imagen'           => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('public_object_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PublicObject';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'objeto_id'        => 'Number',
      'nombre'           => 'Text',
      'descripcion'      => 'Text',
      'museo_name'       => 'Text',
      'museo_sub_name'   => 'Text',
      'dato'             => 'Text',
      'anio'             => 'Number',
      'texto_antiguedad' => 'Text',
      'categoria'        => 'Text',
      'tipo'             => 'Text',
      'tipo_id'          => 'Number',
      'autor'            => 'Text',
      'autor_id'         => 'Number',
      'colecciones'      => 'Text',
      'materiales'       => 'Text',
      'imagen'           => 'Text',
    );
  }
}
