<?php

/**
 * Material filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMaterialFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre_material' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'material_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Objeto')),
    ));

    $this->setValidators(array(
      'nombre_material' => new sfValidatorPass(array('required' => false)),
      'material_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Objeto', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('material_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addMaterialListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ObjetoMaterial ObjetoMaterial')
      ->andWhereIn('ObjetoMaterial.objeto_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Material';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'nombre_material' => 'Text',
      'material_list'   => 'ManyKey',
    );
  }
}
