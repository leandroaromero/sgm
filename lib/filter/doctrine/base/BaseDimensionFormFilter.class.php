<?php

/**
 * Dimension filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseDimensionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre_dimension' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'dimension_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Objeto')),
    ));

    $this->setValidators(array(
      'nombre_dimension' => new sfValidatorPass(array('required' => false)),
      'dimension_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Objeto', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('dimension_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addDimensionListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ObjetoDimension ObjetoDimension')
      ->andWhereIn('ObjetoDimension.objeto_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Dimension';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'nombre_dimension' => 'Text',
      'dimension_list'   => 'ManyKey',
    );
  }
}
