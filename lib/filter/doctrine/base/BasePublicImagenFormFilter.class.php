<?php

/**
 * PublicImagen filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePublicImagenFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'imagen_id'        => new sfWidgetFormFilterInput(),
      'titulo'           => new sfWidgetFormFilterInput(),
      'archivo'          => new sfWidgetFormFilterInput(),
      'public_object_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PublicObject'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'imagen_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'titulo'           => new sfValidatorPass(array('required' => false)),
      'archivo'          => new sfValidatorPass(array('required' => false)),
      'public_object_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PublicObject'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('public_imagen_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PublicImagen';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'imagen_id'        => 'Number',
      'titulo'           => 'Text',
      'archivo'          => 'Text',
      'public_object_id' => 'ForeignKey',
    );
  }
}
