<?php

/**
 * IntervencionEstado filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseIntervencionEstadoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'            => new sfWidgetFormFilterInput(),
      'intervencion_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Intervencion')),
    ));

    $this->setValidators(array(
      'nombre'            => new sfValidatorPass(array('required' => false)),
      'intervencion_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Intervencion', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('intervencion_estado_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addIntervencionListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.IntervencionDescripcionEstado IntervencionDescripcionEstado')
      ->andWhereIn('IntervencionDescripcionEstado.intervencion_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'IntervencionEstado';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'nombre'            => 'Text',
      'intervencion_list' => 'ManyKey',
    );
  }
}
