<?php

/**
 * Intervencion filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseIntervencionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'objeto_id'                            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => true)),
      'fecha_de_diagnostico'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'estado_general'                       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'observaciones_estado'                 => new sfWidgetFormFilterInput(),
      'mapa_de_deterioro'                    => new sfWidgetFormFilterInput(),
      'observaciones_propuesta_intervencion' => new sfWidgetFormFilterInput(),
      'observaciones_recomendaciones'        => new sfWidgetFormFilterInput(),
      'created_by'                           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'                           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
      'created_at'                           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'intervencion_estado_list'             => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'IntervencionEstado')),
      'persona_list'                         => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Persona')),
    ));

    $this->setValidators(array(
      'objeto_id'                            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Objeto'), 'column' => 'id')),
      'fecha_de_diagnostico'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'estado_general'                       => new sfValidatorPass(array('required' => false)),
      'observaciones_estado'                 => new sfValidatorPass(array('required' => false)),
      'mapa_de_deterioro'                    => new sfValidatorPass(array('required' => false)),
      'observaciones_propuesta_intervencion' => new sfValidatorPass(array('required' => false)),
      'observaciones_recomendaciones'        => new sfValidatorPass(array('required' => false)),
      'created_by'                           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Creator'), 'column' => 'id')),
      'updated_by'                           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Updator'), 'column' => 'id')),
      'created_at'                           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'intervencion_estado_list'             => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'IntervencionEstado', 'required' => false)),
      'persona_list'                         => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Persona', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('intervencion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addIntervencionEstadoListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.IntervencionDescripcionEstado IntervencionDescripcionEstado')
      ->andWhereIn('IntervencionDescripcionEstado.intervencion_estado_id', $values)
    ;
  }

  public function addPersonaListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.IntervencionTienePersona IntervencionTienePersona')
      ->andWhereIn('IntervencionTienePersona.persona_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Intervencion';
  }

  public function getFields()
  {
    return array(
      'id'                                   => 'Number',
      'objeto_id'                            => 'ForeignKey',
      'fecha_de_diagnostico'                 => 'Date',
      'estado_general'                       => 'Text',
      'observaciones_estado'                 => 'Text',
      'mapa_de_deterioro'                    => 'Text',
      'observaciones_propuesta_intervencion' => 'Text',
      'observaciones_recomendaciones'        => 'Text',
      'created_by'                           => 'ForeignKey',
      'updated_by'                           => 'ForeignKey',
      'created_at'                           => 'Date',
      'updated_at'                           => 'Date',
      'intervencion_estado_list'             => 'ManyKey',
      'persona_list'                         => 'ManyKey',
    );
  }
}
