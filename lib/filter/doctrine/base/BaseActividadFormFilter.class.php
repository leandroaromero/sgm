<?php

/**
 * Actividad filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseActividadFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'tipo_actividad_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoActividad'), 'add_empty' => true)),
      'nombre_actividad'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'fecha'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'hora_desde'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'hora_hasta'         => new sfWidgetFormFilterInput(),
      'atencion_actividad' => new sfWidgetFormFilterInput(),
      'lugar_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Lugar'), 'add_empty' => true)),
      'created_by'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'categorias_list'    => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Categoria')),
      'colecciones_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Coleccion')),
      'tipo_objetos_list'  => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'TipoObjeto')),
      'usuarios_list'      => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'sfGuardUser')),
    ));

    $this->setValidators(array(
      'tipo_actividad_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TipoActividad'), 'column' => 'id')),
      'nombre_actividad'   => new sfValidatorPass(array('required' => false)),
      'fecha'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'hora_desde'         => new sfValidatorPass(array('required' => false)),
      'hora_hasta'         => new sfValidatorPass(array('required' => false)),
      'atencion_actividad' => new sfValidatorPass(array('required' => false)),
      'lugar_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Lugar'), 'column' => 'id')),
      'created_by'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Creator'), 'column' => 'id')),
      'updated_by'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Updator'), 'column' => 'id')),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'categorias_list'    => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Categoria', 'required' => false)),
      'colecciones_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Coleccion', 'required' => false)),
      'tipo_objetos_list'  => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'TipoObjeto', 'required' => false)),
      'usuarios_list'      => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'sfGuardUser', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('actividad_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addCategoriasListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ActividadCategoria ActividadCategoria')
      ->andWhereIn('ActividadCategoria.categoria_id', $values)
    ;
  }

  public function addColeccionesListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ActividadColeccion ActividadColeccion')
      ->andWhereIn('ActividadColeccion.coleccion_id', $values)
    ;
  }

  public function addTipoObjetosListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ActividadTipoObjeto ActividadTipoObjeto')
      ->andWhereIn('ActividadTipoObjeto.tipo_objetos_id', $values)
    ;
  }

  public function addUsuariosListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.ActividadSfGuardUser ActividadSfGuardUser')
      ->andWhereIn('ActividadSfGuardUser.sf_guard_user_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Actividad';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'tipo_actividad_id'  => 'ForeignKey',
      'nombre_actividad'   => 'Text',
      'fecha'              => 'Date',
      'hora_desde'         => 'Text',
      'hora_hasta'         => 'Text',
      'atencion_actividad' => 'Text',
      'lugar_id'           => 'ForeignKey',
      'created_by'         => 'ForeignKey',
      'updated_by'         => 'ForeignKey',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
      'categorias_list'    => 'ManyKey',
      'colecciones_list'   => 'ManyKey',
      'tipo_objetos_list'  => 'ManyKey',
      'usuarios_list'      => 'ManyKey',
    );
  }
}
