<?php

/**
 * InvestigacionSfGuardUser filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseInvestigacionSfGuardUserFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
    ));

    $this->setValidators(array(
    ));

    $this->widgetSchema->setNameFormat('investigacion_sf_guard_user_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InvestigacionSfGuardUser';
  }

  public function getFields()
  {
    return array(
      'investigacion_id' => 'Number',
      'sf_guard_user_id' => 'Number',
    );
  }
}
