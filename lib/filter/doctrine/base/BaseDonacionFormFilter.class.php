<?php

/**
 * Donacion filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseDonacionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nro_acta'   => new sfWidgetFormFilterInput(),
      'donante_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Donante'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'nro_acta'   => new sfValidatorPass(array('required' => false)),
      'donante_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Donante'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('donacion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Donacion';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'nro_acta'   => 'Text',
      'donante_id' => 'ForeignKey',
    );
  }
}
