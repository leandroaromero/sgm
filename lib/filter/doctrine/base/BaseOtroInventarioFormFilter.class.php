<?php

/**
 * OtroInventario filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseOtroInventarioFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'numero'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'objeto_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => true)),
      'tipo_inventario_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoInventario'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'numero'             => new sfValidatorPass(array('required' => false)),
      'objeto_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Objeto'), 'column' => 'id')),
      'tipo_inventario_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TipoInventario'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('otro_inventario_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OtroInventario';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'numero'             => 'Text',
      'objeto_id'          => 'ForeignKey',
      'tipo_inventario_id' => 'ForeignKey',
    );
  }
}
