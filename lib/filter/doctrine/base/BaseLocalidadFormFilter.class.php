<?php

/**
 * Localidad filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseLocalidadFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre_localidad' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'provincia_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Provincia'), 'add_empty' => true)),
      'tipo_origen_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoOrigen'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'nombre_localidad' => new sfValidatorPass(array('required' => false)),
      'provincia_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Provincia'), 'column' => 'id')),
      'tipo_origen_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TipoOrigen'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('localidad_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Localidad';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'nombre_localidad' => 'Text',
      'provincia_id'     => 'ForeignKey',
      'tipo_origen_id'   => 'ForeignKey',
    );
  }
}
