<?php

/**
 * ObjetoDimension filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseObjetoDimensionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'objeto_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => true)),
      'dimension_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Dimension'), 'add_empty' => true)),
      'valor'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'unidad'       => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'objeto_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Objeto'), 'column' => 'id')),
      'dimension_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Dimension'), 'column' => 'id')),
      'valor'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'unidad'       => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('objeto_dimension_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ObjetoDimension';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'objeto_id'    => 'ForeignKey',
      'dimension_id' => 'ForeignKey',
      'valor'        => 'Number',
      'unidad'       => 'Text',
    );
  }
}
