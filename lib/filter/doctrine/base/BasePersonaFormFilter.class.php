<?php

/**
 * Persona filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePersonaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'apellido'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nombre'            => new sfWidgetFormFilterInput(),
      'nro_documento'     => new sfWidgetFormFilterInput(),
      'direccion'         => new sfWidgetFormFilterInput(),
      'localidad_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true)),
      'intervencion_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Intervencion')),
    ));

    $this->setValidators(array(
      'apellido'          => new sfValidatorPass(array('required' => false)),
      'nombre'            => new sfValidatorPass(array('required' => false)),
      'nro_documento'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'direccion'         => new sfValidatorPass(array('required' => false)),
      'localidad_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Localidad'), 'column' => 'id')),
      'intervencion_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Intervencion', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('persona_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addIntervencionListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.IntervencionTienePersona IntervencionTienePersona')
      ->andWhereIn('IntervencionTienePersona.intervencion_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Persona';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'apellido'          => 'Text',
      'nombre'            => 'Text',
      'nro_documento'     => 'Number',
      'direccion'         => 'Text',
      'localidad_id'      => 'ForeignKey',
      'intervencion_list' => 'ManyKey',
    );
  }
}
