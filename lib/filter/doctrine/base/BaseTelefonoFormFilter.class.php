<?php

/**
 * Telefono filter form base class.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTelefonoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'numero_telefono' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tipo_telefono'   => new sfWidgetFormFilterInput(),
      'persona_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Persona'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'numero_telefono' => new sfValidatorPass(array('required' => false)),
      'tipo_telefono'   => new sfValidatorPass(array('required' => false)),
      'persona_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Persona'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('telefono_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Telefono';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'numero_telefono' => 'Text',
      'tipo_telefono'   => 'Text',
      'persona_id'      => 'ForeignKey',
    );
  }
}
