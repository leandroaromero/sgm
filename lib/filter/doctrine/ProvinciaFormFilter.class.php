<?php

/**
 * Provincia filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProvinciaFormFilter extends BaseProvinciaFormFilter
{
  public function configure()
  {
      $this->widgetSchema['pais_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'País',
              'model'   => 'Pais',
             'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
  }
}
