<?php

/**
 * Pictures filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PicturesFormFilter extends BasePicturesFormFilter
{
  public function configure()
  {
  }
}
