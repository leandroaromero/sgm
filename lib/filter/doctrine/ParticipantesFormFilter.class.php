<?php

/**
 * Participantes filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ParticipantesFormFilter extends BaseParticipantesFormFilter
{
  public function configure()
  {
	  unset ($this['created_by'], $this['updated_by'] );
  }
}
