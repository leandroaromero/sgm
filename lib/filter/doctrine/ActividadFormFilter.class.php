<?php

/**
 * Actividad filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ActividadFormFilter extends BaseActividadFormFilter
{
  public function configure()
  {
    unset($this['created_at'],$this['updated_at']);
	      $web = sfConfig::get('app_url_web');   
          $this->widgetSchema['tipo_actividad_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Tipo de actividad',
              'model'   => 'TipoActividad',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
          $this->widgetSchema['lugar_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Lugares',
              'model'   => 'Lugar',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
      
        $format = '%day%/%month%/%year%';
        $web = sfConfig::get('app_url_web');
        $rango = range(2012, 2025);
        $arreglo_rango = array_combine($rango, $rango);
        
        $this->widgetSchema['fecha']= new sfWidgetFormFilterDate(array(
          'from_date' => new sfWidgetFormJQueryDate(array(
             'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))




            )), 'to_date' => new sfWidgetFormJQueryDate(array(
                 'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))

            )), 'with_empty' => false));


            $this->widgetSchema['pais_id'] = new sfWidgetFormDoctrineChoice(array(
                'model'     => 'Pais',
                'add_empty' => true,
            ));
            $this->validatorSchema['pais_id']= new sfValidatorPass(array('required' => false));
            
            
            $this->widgetSchema['provincia_id'] = new sfWidgetFormDoctrineDependentSelect(array(
                'model'   => 'Provincia', 
                'depends' => 'Pais'
            ));
            
            $this->widgetSchema['localidad_id'] = new sfWidgetFormDoctrineDependentSelect(array(
                'model'   => 'Localidad', 
                'depends' => 'Provincia'
            ));
            
            
            $this->validatorSchema['provincia_id']  = new sfValidatorPass(array('required' => false));
            $this->validatorSchema['localidad_id']  = new sfValidatorPass(array('required' => false)); 

          unset ($this['origenes_list'],$this['usuarios_list'], $this['created_by'], $this['updated_by'],
                 $this['categorias_list'], $this['colecciones_list'], $this['tipo_objetos_list'],
                 $this['hora_desde'], $this['hora_hasta']  
           );
  }
  
//  public function getFields(){
//    return parent::getFields() + array('pais' => 'Text') + array('provincia' => 'Text')+ array('localidad' => 'Text');
//  }

      public function addPaisIdColumnQuery(Doctrine_Query $query, $field, $value) 
        {

               if ( isset($value) && '' != $value)
               {
                        $query->leftJoin($query->getRootAlias().'.ActividadLocalidad al' )
                              ->leftJoin('al.Localidad l')
                              ->leftJoin('l.Provincia p')  
                              ->leftJoin('p.Pais pp')  
                              ->andWhere('pp.id = ?', $value);
               }

        }
      public function addProvinciaIdColumnQuery(Doctrine_Query $query, $field, $value) 
        {

               if ( isset($value) && '' != $value)
               {
                        $query->leftJoin($query->getRootAlias().'.ActividadLocalidad al2' )
                              ->leftJoin('al2.Localidad l2')
                              ->leftJoin('l2.Provincia p2')  
                              ->andWhere('p2.id = ?', $value);
               }

        }
      public function addLocalidadIdColumnQuery(Doctrine_Query $query, $field, $value) 
        {

               if ( isset($value) && '' != $value)
               {
                        $query->leftJoin($query->getRootAlias().'.ActividadLocalidad al3' )
                              ->leftJoin('al3.Localidad l3')
                              ->andWhere('l3.id = ?', $value);
               }

        }
}
