<?php

/**
 * Localidad filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class LocalidadFormFilter extends BaseLocalidadFormFilter
{
  public function configure()
  {
      $this->widgetSchema['provincia_id'] = new sfWidgetFormDoctrineChoice(array(
              'label'        => 'Provincia',
              'model'        => 'Provincia',
              'table_method' => 'getOrderNombre',
              'add_empty'    => true,
            ));
      $this->widgetSchema['tipo_origen_id'] = new sfWidgetFormDoctrineChoice(array(
              'label'        => 'Tipo de origen',
              'model'        => 'TipoOrigen',
              'table_method' => 'getOrderNombre',
              'add_empty'    => true,
            ));

  }
}
