<?php

/**
 * Trayectoria filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TrayectoriaFormFilter extends BaseTrayectoriaFormFilter
{
  public function configure()
  {
       $this->widgetSchema['tipo_evento_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Tipo de evento',
              'model'   => 'TipoEvento',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
  }
}
