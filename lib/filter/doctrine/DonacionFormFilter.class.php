<?php

/**
 * Donacion filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class DonacionFormFilter extends BaseDonacionFormFilter
{
  public function configure()
  {
      $this->widgetSchema['donante_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Donante ',
              'model'   => 'Donante',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
  }
}
