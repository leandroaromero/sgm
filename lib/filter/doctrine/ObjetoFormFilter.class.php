<?php

/**
 * Objeto filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObjetoFormFilter extends BaseObjetoFormFilter
{
  public function configure()
  {
        $format = '%day%/%month%/%year%';
        $web = sfConfig::get('app_url_web');
        $rango = range(1800, 2020);
        $arreglo_rango = array_combine($rango, $rango);
        
        $this->widgetSchema['fecha_ingreso']= new sfWidgetFormFilterDate(array(
          'from_date' => new sfWidgetFormJQueryDate(array(
             'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))


            )), 'to_date' => new sfWidgetFormJQueryDate(array(
                 'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))

            )), 'with_empty' => false));





     $this->widgetSchema['autor_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Autor ',
              'model'   => 'Autor',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
     $this->widgetSchema['ubicacion_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Ubicación ',
              'model'   => 'Ubicacion',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
     $this->widgetSchema['donacion_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Donacion ',
              'model'   => 'Donacion',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
     $this->widgetSchema['coleccion_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
     $this->widgetSchema['coleccion_list']->setOption('table_method', 'getOrderNombre');
     $this->widgetSchema['material_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
     $this->widgetSchema['material_list']->setOption('table_method', 'getOrderNombre');
     $this->widgetSchema['coleccion_list']->setOption('table_method', 'getOrderNombre');
     $this->widgetSchema['material_list']->setOption('table_method', 'getOrderNombre');

      $this->widgetSchema['id'] = new sfWidgetFormInputText();
      $this->validatorSchema['id'] = new sfValidatorInteger(array('required' => false));




      $this->widgetSchema['obra_de_arte_id'] = new sfWidgetFormDoctrineChoice(array(
          'model'     => 'ObraDeArte',
          'method'    => 'getNombre',
          'add_empty' => '>> Seleccione <<',
          'label'     => 'Lenguaje',
        ));
      $this->widgetSchema['aspecto_obra_id'] = new sfWidgetFormDoctrineDependentSelect(array(
          'model'   => 'AspectoObra', 
          'depends' => 'ObraDeArte',
          'method'       => 'getNombre',
            'add_empty' => '>> Seleccione <<',
          'label' => 'Descripción'
      ));
      
      $this->widgetSchema['detalle_obra_id'] = new sfWidgetFormDoctrineDependentSelect(array(
          'model'   => 'DetalleObra', 
          'depends' => 'AspectoObra',
          'method'       => 'getNombre',
          'add_empty' => '>> Seleccione <<',
      ));

          // validadores
            
        $this->validatorSchema['obra_de_arte_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'ObraDeArte',            
            'required' => false,
        ));
               
        $this->validatorSchema['aspecto_obra_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'AspectoObra',
            'required' => false,
        ));    
               
        $this->validatorSchema['detalle_obra_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'DetalleObra',
            'required' => false,
        ));          
                
  }

    public function addObraDeArteIdColumnQuery(Doctrine_Query $query, $field, $value) 
        {

               if ( isset($value) && '' != $value)
               {
                        $query->leftJoin($query->getRootAlias().'.RegistroDetalleObra rdo' )
                              ->leftJoin('rdo.DetalleObra do' )
                              ->leftJoin('do.AspectoObra ao' )
                              ->leftJoin('ao.ObraDeArte oda')
                              ->andWhere('oda.id = ?', $value);
               }

        }

       public function addAspectoObraIdColumnQuery(Doctrine_Query $query, $field, $value) 
        {

               if ( isset($value) && '' != $value)
               {
                                $query->leftJoin($query->getRootAlias().'.RegistroDetalleObra asrdo' )                  
                                      ->leftJoin('asrdo.DetalleObra asdo' )
                                      ->andWhere('asdo.aspecto_obra_id = ?', $value);
               }

        }    


       public function addDetalleObraIdColumnQuery(Doctrine_Query $query, $field, $value) 
        {

               if ( isset($value) && '' != $value)
               {
                                $query->leftJoin($query->getRootAlias().'.RegistroDetalleObra derdo' )                  
                                      ->andWhere('derdo.detalle_obra_id = ?', $value);
               }

        } 


  
 /*  public function addColeccionListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }
    $q = Doctrine_Query::create()
    ->from('Objeto o');
    $res=$q->fetchArray();



       $arr2= array();
        foreach ($res as $re2){

        $cont2=0;
       $qq2 = Doctrine_Query::create();
       $qq2->from('ObjetoColeccion c');
       foreach ($values as $va){
        $qq2->where('c.coleccion_id = ?', $va );
        $qq2->AndWhere('c.objeto_id = ?', $re2['id']);
        $objetos2 = $qq2->fetchArray();
        if( count($objetos2) > 0 ){
            $cont2++;
        }

   }

   if( $cont2  == count($values)){
    $arr2[]= $re2['id'];
    }
   }
    if(count( $arr2 ) > 0){
    $query->whereIn($query->getRootAlias().'.id ', $arr2);
}

  }*/

  /////////////////////////////////////////////////////////
    public function addMaterialListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }
     $q = Doctrine_Query::create()
          ->from('Objeto o');
     $res=$q->fetchArray();



       $arr= array();
        foreach ($res as $re){

        $cont=0;
       $qq = Doctrine_Query::create();
       $qq->from('ObjetoMaterial m');
       foreach ($values as $va){
        $qq->where('m.material_id = ?', $va );
        $qq->AndWhere('m.objeto_id = ?', $re['id']);
        $objetos = $qq->fetchArray();
        if( count($objetos) > 0 ){
            $cont++;
        }

   }

   if( $cont  == count($values)){
    $arr[]= $re['id'];
    }

    }

    if(count( $arr ) > 0){

    $query->whereIn($query->getRootAlias().'.id ', $arr);
    
}


  }
  ////////////////////////////////////////////
   public function addIdColumnQuery(Doctrine_Query $query, $field, $values)
  {  
    if( $values )
        $query->where($query->getRootAlias().'.id like ? ', '%'.$values.'%');
    
  }


}
