<?php

/**
 * Investigacion filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvestigacionFormFilter extends BaseInvestigacionFormFilter
{
  public function configure()
  {
    unset($this['created_by'],$this['updated_by'], $this['updated_at'],$this['created_at']);
    
    $this->widgetSchema['objeto_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Objeto ',
              'model'   => 'Objeto',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
  }
}
