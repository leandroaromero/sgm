<?php

/**
 * Base project form.
 * 
 * @package    museo
 * @subpackage form
 * @author     Your name here 
 * @version    SVN: $Id: BaseForm.class.php 20147 2009-07-13 11:46:57Z FabianLange $
 */
class InstitucionForm extends BaseForm
{
    public function configure()
  {

    $parameter = sfConfig::get('app_institucion_data', array(''=>'') );

    $choices    = $parameter; //array('' => '') + $parameter; 

$this->widgetSchema['cambio'] = new sfWidgetFormChoice(
    array(
        'choices'   => $choices,
        'multiple'  => false,
        'label'     => 'Instituciones',
        'expanded'  => false,
        ),
    array()
);
    $this->widgetSchema['cambio']->setAttributes(array(
              'onChange' =>'verLogin(this.value)',
              'style' =>   'font-size: 16px, background-color:ffffff', 
      )); 
$this->widgetSchema['cambio']->setDefault(sfContext::getInstance()->getUser()->getDataBase());
$this->widgetSchema->setNameFormat('institucion[%s]');
  }
}
