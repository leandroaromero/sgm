<?php

/**
 * Imagen form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ImagenForm extends BaseImagenForm
{
  public function configure()
  {
      $this->validatorSchema['created_at']= new sfValidatorDateTime(array('required'=>false) );
      $this->validatorSchema['updated_at']= new sfValidatorDateTime(array('required'=>false) );
	$this->widgetSchema['archivo'] = new sfWidgetFormInputFileEditable(array(
   'label'     => 'Imagen Principal',
   'file_src'  => '/uploads/fotos/'.$this->getObject()->getArchivo(),
   'is_image'  => true,
   'edit_mode' => !$this->isNew(),
   'template'  => '<div>%file%<br /><label></label>%input%<br /><label></label>%delete% Eliminar imagen actual</div>',
));
    
$this->validatorSchema['archivo'] = new sfValidatorFile(array(
   'required'   => false,
   'mime_types' => 'web_images',
   'path' => sfConfig::get('sf_upload_dir').'/fotos/',
   'validated_file_class' => 'sfResizedFile',
));
	  
  }
}
