<?php

/**
 * PublicObject form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PublicObjectForm extends BasePublicObjectForm
{
  public function configure()
  {
  }
}
