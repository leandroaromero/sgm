<?php

/**
 * Email form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EmailForm extends BaseEmailForm
{
  public function configure()
  {
          unset($this->widgetSchema['created_at']);
          unset($this->widgetSchema['updated_at']);
          $this->validatorSchema['created_at']= new sfValidatorDateTime(array('required'=>false) );
          $this->validatorSchema['updated_at']= new sfValidatorDateTime(array('required'=>false) );
	  $this->widgetSchema['persona_id'] = new sfWidgetFormInputHidden();
   }
  public function checkDelete($validator, $values)
    {
			$email= $values['delete'];
			if($email){
				$this->getObject()->delete();
				}
			$isValid= true;	
		}	
}
