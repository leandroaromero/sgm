<?php

/**
 * Institution form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InstitutionForm extends BaseInstitutionForm
{
  public function configure()
  {
      unset($this['file']);
      $dir= sfContext::getInstance()->getUser()->getFile();
      $this->getObject()->setFile($dir);
      
      $this->widgetSchema['summary'] = new sfWidgetFormTextareaTinyMCE( array(
       
        'width'=>550,
        'height'=>350,
      ),
      array(
        'class'   =>  'tiny_mce'
      )
            );
      
  }
}
