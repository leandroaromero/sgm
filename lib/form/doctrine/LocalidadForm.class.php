<?php

/**
 * Localidad form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class LocalidadForm extends BaseLocalidadForm
{
  public function configure()
  {
	$this->validatorSchema['created_at']= new sfValidatorDateTime(array('required'=>false) );
      $this->validatorSchema['updated_at']= new sfValidatorDateTime(array('required'=>false) );
      $this->widgetSchema['pais_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'País',
              'model'   => 'Pais',
             'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
        $this->widgetSchema['provincia_id'] = new sfWidgetFormDoctrineDependentSelect(array(
              'label' => 'Provincia',
              'model'   => 'Provincia',
              'table_method' => 'getOrderNombre',
              'depends' => 'Pais'));
       $this->validatorSchema['pais_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'Pais',
        ));
        $this->widgetSchema['tipo_origen_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Tipo de origen',
              'model'   => 'TipoOrigen',
             'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
  }
}
     
