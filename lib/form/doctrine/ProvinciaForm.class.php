<?php

/**
 * Provincia form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProvinciaForm extends BaseProvinciaForm
{
  public function configure()
  {
      $this->validatorSchema['created_at']= new sfValidatorDateTime(array('required'=>false) );
      $this->validatorSchema['updated_at']= new sfValidatorDateTime(array('required'=>false) );
      $this->widgetSchema['pais_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'País',
              'model'   => 'Pais',
             'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
  }
}
