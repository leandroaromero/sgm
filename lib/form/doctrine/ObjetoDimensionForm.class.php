<?php

/**
 * ObjetoDimension form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObjetoDimensionForm extends BaseObjetoDimensionForm
{
  public function configure()
  {
	  $this->widgetSchema['objeto_id'] = new sfWidgetFormInputHidden();
          $this->widgetSchema['dimension_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Dimensión',
              'model'   => 'Dimension',
              'table_method'=> 'getOrderNombre',
              'add_empty' => false,
            ));
  }
}
