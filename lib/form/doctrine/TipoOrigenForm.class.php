<?php

/**
 * TipoOrigen form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TipoOrigenForm extends BaseTipoOrigenForm
{
  public function configure()
  {
      unset($this['created_by'],$this['updated_by'], $this['updated_at'],$this['created_at']);
  }
}
