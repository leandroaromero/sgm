<?php

/**
 * Trayectoria form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TrayectoriaForm extends BaseTrayectoriaForm
{
  public function configure()
  {

  $this->validatorSchema['created_at']= new sfValidatorDateTime(array('required'=>false) );
  $this->validatorSchema['updated_at']= new sfValidatorDateTime(array('required'=>false) );
  sfProjectConfiguration::getActive()->loadHelpers('Url');
  $rango = range(1800, 2020);
  $arreglo_rango = array_combine($rango, $rango);
  $web = sfConfig::get('app_url_web');  
    $this->widgetSchema['fecha'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
       monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
       buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           ))
        ));
    $this->widgetSchema['trayectoria_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
    $this->widgetSchema['trayectoria_list']->setOption('table_method', 'getOrderNombre');
    $this->widgetSchema['trayectoria_list']->setOption('label', 'Acervos');
    $this->widgetSchema['tipo_evento_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Tipo de evento',
              'model'   => 'TipoEvento',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
    
  }
}
