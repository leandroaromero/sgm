<?php

/**
 * Actividad form base class.
 *
 * @method Actividad getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseActividadForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'tipo_actividad_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoActividad'), 'add_empty' => false)),
      'nombre_actividad'   => new sfWidgetFormInputText(),
      'fecha'              => new sfWidgetFormDate(),
      'hora_desde'         => new sfWidgetFormTime(),
      'hora_hasta'         => new sfWidgetFormTime(),
      'atencion_actividad' => new sfWidgetFormTextarea(),
      'lugar_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Lugar'), 'add_empty' => true)),
      'created_by'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
      'categorias_list'    => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Categoria')),
      'colecciones_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Coleccion')),
      'tipo_objetos_list'  => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'TipoObjeto')),
      'usuarios_list'      => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'sfGuardUser')),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'tipo_actividad_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TipoActividad'))),
      'nombre_actividad'   => new sfValidatorString(array('max_length' => 255)),
      'fecha'              => new sfValidatorDate(),
      'hora_desde'         => new sfValidatorTime(),
      'hora_hasta'         => new sfValidatorTime(array('required' => false)),
      'atencion_actividad' => new sfValidatorString(array('required' => false)),
      'lugar_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Lugar'), 'required' => false)),
      'created_by'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'required' => false)),
      'updated_by'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
      'categorias_list'    => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Categoria', 'required' => false)),
      'colecciones_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Coleccion', 'required' => false)),
      'tipo_objetos_list'  => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'TipoObjeto', 'required' => false)),
      'usuarios_list'      => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'sfGuardUser', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('actividad[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Actividad';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['categorias_list']))
    {
      $this->setDefault('categorias_list', $this->object->Categorias->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['colecciones_list']))
    {
      $this->setDefault('colecciones_list', $this->object->Colecciones->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['tipo_objetos_list']))
    {
      $this->setDefault('tipo_objetos_list', $this->object->TipoObjetos->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['usuarios_list']))
    {
      $this->setDefault('usuarios_list', $this->object->Usuarios->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveCategoriasList($con);
    $this->saveColeccionesList($con);
    $this->saveTipoObjetosList($con);
    $this->saveUsuariosList($con);

    parent::doSave($con);
  }

  public function saveCategoriasList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['categorias_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Categorias->getPrimaryKeys();
    $values = $this->getValue('categorias_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Categorias', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Categorias', array_values($link));
    }
  }

  public function saveColeccionesList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['colecciones_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Colecciones->getPrimaryKeys();
    $values = $this->getValue('colecciones_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Colecciones', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Colecciones', array_values($link));
    }
  }

  public function saveTipoObjetosList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['tipo_objetos_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->TipoObjetos->getPrimaryKeys();
    $values = $this->getValue('tipo_objetos_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('TipoObjetos', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('TipoObjetos', array_values($link));
    }
  }

  public function saveUsuariosList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['usuarios_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Usuarios->getPrimaryKeys();
    $values = $this->getValue('usuarios_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Usuarios', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Usuarios', array_values($link));
    }
  }

}
