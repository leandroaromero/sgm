<?php

/**
 * Institution form base class.
 *
 * @method Institution getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseInstitutionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'name'      => new sfWidgetFormInputText(),
      'subtitle'  => new sfWidgetFormInputText(),
      'summary'   => new sfWidgetFormTextarea(),
      'holder'    => new sfWidgetFormInputText(),
      'phone'     => new sfWidgetFormInputText(),
      'address'   => new sfWidgetFormTextarea(),
      'email'     => new sfWidgetFormInputText(),
      'facebook'  => new sfWidgetFormInputText(),
      'is_active' => new sfWidgetFormInputCheckbox(),
      'file'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'      => new sfValidatorString(array('max_length' => 145, 'required' => false)),
      'subtitle'  => new sfValidatorString(array('max_length' => 145, 'required' => false)),
      'summary'   => new sfValidatorString(array('required' => false)),
      'holder'    => new sfValidatorString(array('max_length' => 245, 'required' => false)),
      'phone'     => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'address'   => new sfValidatorString(array('required' => false)),
      'email'     => new sfValidatorString(array('max_length' => 245, 'required' => false)),
      'facebook'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'is_active' => new sfValidatorBoolean(array('required' => false)),
      'file'      => new sfValidatorString(array('max_length' => 45, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('institution[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Institution';
  }

}
