<?php

/**
 * PublicObject form base class.
 *
 * @method PublicObject getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePublicObjectForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'objeto_id'        => new sfWidgetFormInputText(),
      'nombre'           => new sfWidgetFormInputText(),
      'descripcion'      => new sfWidgetFormTextarea(),
      'museo_name'       => new sfWidgetFormInputText(),
      'museo_sub_name'   => new sfWidgetFormInputText(),
      'dato'             => new sfWidgetFormInputText(),
      'anio'             => new sfWidgetFormInputText(),
      'texto_antiguedad' => new sfWidgetFormInputText(),
      'categoria'        => new sfWidgetFormInputText(),
      'tipo'             => new sfWidgetFormInputText(),
      'tipo_id'          => new sfWidgetFormInputText(),
      'autor'            => new sfWidgetFormInputText(),
      'autor_id'         => new sfWidgetFormInputText(),
      'colecciones'      => new sfWidgetFormTextarea(),
      'materiales'       => new sfWidgetFormTextarea(),
      'imagen'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'objeto_id'        => new sfValidatorInteger(array('required' => false)),
      'nombre'           => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'descripcion'      => new sfValidatorString(array('required' => false)),
      'museo_name'       => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'museo_sub_name'   => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'dato'             => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'anio'             => new sfValidatorInteger(array('required' => false)),
      'texto_antiguedad' => new sfValidatorString(array('max_length' => 75, 'required' => false)),
      'categoria'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'tipo'             => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'tipo_id'          => new sfValidatorInteger(array('required' => false)),
      'autor'            => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'autor_id'         => new sfValidatorInteger(array('required' => false)),
      'colecciones'      => new sfValidatorString(array('required' => false)),
      'materiales'       => new sfValidatorString(array('required' => false)),
      'imagen'           => new sfValidatorString(array('max_length' => 45, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('public_object[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PublicObject';
  }

}
