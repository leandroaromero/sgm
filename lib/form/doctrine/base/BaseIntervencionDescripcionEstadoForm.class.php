<?php

/**
 * IntervencionDescripcionEstado form base class.
 *
 * @method IntervencionDescripcionEstado getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseIntervencionDescripcionEstadoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'intervencion_id'        => new sfWidgetFormInputHidden(),
      'intervencion_estado_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'intervencion_id'        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('intervencion_id')), 'empty_value' => $this->getObject()->get('intervencion_id'), 'required' => false)),
      'intervencion_estado_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('intervencion_estado_id')), 'empty_value' => $this->getObject()->get('intervencion_estado_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('intervencion_descripcion_estado[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IntervencionDescripcionEstado';
  }

}
