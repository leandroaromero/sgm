<?php

/**
 * IntervencionTipoFotografia form base class.
 *
 * @method IntervencionTipoFotografia getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseIntervencionTipoFotografiaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                          => new sfWidgetFormInputHidden(),
      'intervencion_tipo_imagen_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('IntervencionTipoImagen'), 'add_empty' => true)),
      'nombre'                      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'intervencion_tipo_imagen_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('IntervencionTipoImagen'), 'required' => false)),
      'nombre'                      => new sfValidatorString(array('max_length' => 45, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('intervencion_tipo_fotografia[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IntervencionTipoFotografia';
  }

}
