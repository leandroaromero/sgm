<?php

/**
 * Intervencion form base class.
 *
 * @method Intervencion getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseIntervencionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                                   => new sfWidgetFormInputHidden(),
      'objeto_id'                            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => true)),
      'fecha_de_diagnostico'                 => new sfWidgetFormDate(),
      'estado_general'                       => new sfWidgetFormInputText(),
      'observaciones_estado'                 => new sfWidgetFormTextarea(),
      'mapa_de_deterioro'                    => new sfWidgetFormTextarea(),
      'observaciones_propuesta_intervencion' => new sfWidgetFormTextarea(),
      'observaciones_recomendaciones'        => new sfWidgetFormTextarea(),
      'created_by'                           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'                           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
      'created_at'                           => new sfWidgetFormDateTime(),
      'updated_at'                           => new sfWidgetFormDateTime(),
      'intervencion_estado_list'             => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'IntervencionEstado')),
      'persona_list'                         => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Persona')),
    ));

    $this->setValidators(array(
      'id'                                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'objeto_id'                            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'required' => false)),
      'fecha_de_diagnostico'                 => new sfValidatorDate(),
      'estado_general'                       => new sfValidatorString(array('max_length' => 7)),
      'observaciones_estado'                 => new sfValidatorString(array('required' => false)),
      'mapa_de_deterioro'                    => new sfValidatorString(array('required' => false)),
      'observaciones_propuesta_intervencion' => new sfValidatorString(array('required' => false)),
      'observaciones_recomendaciones'        => new sfValidatorString(array('required' => false)),
      'created_by'                           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'required' => false)),
      'updated_by'                           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'required' => false)),
      'created_at'                           => new sfValidatorDateTime(),
      'updated_at'                           => new sfValidatorDateTime(),
      'intervencion_estado_list'             => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'IntervencionEstado', 'required' => false)),
      'persona_list'                         => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Persona', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('intervencion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Intervencion';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['intervencion_estado_list']))
    {
      $this->setDefault('intervencion_estado_list', $this->object->IntervencionEstado->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['persona_list']))
    {
      $this->setDefault('persona_list', $this->object->Persona->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveIntervencionEstadoList($con);
    $this->savePersonaList($con);

    parent::doSave($con);
  }

  public function saveIntervencionEstadoList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['intervencion_estado_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->IntervencionEstado->getPrimaryKeys();
    $values = $this->getValue('intervencion_estado_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('IntervencionEstado', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('IntervencionEstado', array_values($link));
    }
  }

  public function savePersonaList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['persona_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Persona->getPrimaryKeys();
    $values = $this->getValue('persona_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Persona', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Persona', array_values($link));
    }
  }

}
