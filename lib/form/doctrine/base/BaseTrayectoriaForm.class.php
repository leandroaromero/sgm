<?php

/**
 * Trayectoria form base class.
 *
 * @method Trayectoria getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTrayectoriaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'nombre_evento'       => new sfWidgetFormInputText(),
      'fecha'               => new sfWidgetFormDate(),
      'lugar'               => new sfWidgetFormInputText(),
      'relato'              => new sfWidgetFormTextarea(),
      'tipo_evento_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoEvento'), 'add_empty' => false)),
      'galeria_de_imagenes' => new sfWidgetFormInputText(),
      'nro_acta'            => new sfWidgetFormInputText(),
      'trayectoria_list'    => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Objeto')),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'nombre_evento'       => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'fecha'               => new sfValidatorDate(),
      'lugar'               => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'relato'              => new sfValidatorString(array('required' => false)),
      'tipo_evento_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TipoEvento'))),
      'galeria_de_imagenes' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'nro_acta'            => new sfValidatorInteger(array('required' => false)),
      'trayectoria_list'    => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Objeto', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('trayectoria[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Trayectoria';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['trayectoria_list']))
    {
      $this->setDefault('trayectoria_list', $this->object->Trayectoria->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveTrayectoriaList($con);

    parent::doSave($con);
  }

  public function saveTrayectoriaList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['trayectoria_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Trayectoria->getPrimaryKeys();
    $values = $this->getValue('trayectoria_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Trayectoria', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Trayectoria', array_values($link));
    }
  }

}
