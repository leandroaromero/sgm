<?php

/**
 * ObjetoDimension form base class.
 *
 * @method ObjetoDimension getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseObjetoDimensionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'objeto_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => false)),
      'dimension_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Dimension'), 'add_empty' => false)),
      'valor'        => new sfWidgetFormInputText(),
      'unidad'       => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'objeto_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'))),
      'dimension_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Dimension'))),
      'valor'        => new sfValidatorNumber(),
      'unidad'       => new sfValidatorString(array('max_length' => 25, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('objeto_dimension[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ObjetoDimension';
  }

}
