<?php

/**
 * Contact form base class.
 *
 * @method Contact getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseContactForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'institucion_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Institution'), 'add_empty' => true)),
      'email'           => new sfWidgetFormInputText(),
      'name'            => new sfWidgetFormInputText(),
      'comment'         => new sfWidgetFormTextarea(),
      'ip_address'      => new sfWidgetFormInputText(),
      'is_active'       => new sfWidgetFormInputCheckbox(),
      'is_report'       => new sfWidgetFormInputCheckbox(),
      'is_spam_actived' => new sfWidgetFormInputCheckbox(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'institucion_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Institution'), 'required' => false)),
      'email'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'name'            => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'comment'         => new sfValidatorString(array('required' => false)),
      'ip_address'      => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'is_active'       => new sfValidatorBoolean(array('required' => false)),
      'is_report'       => new sfValidatorBoolean(array('required' => false)),
      'is_spam_actived' => new sfValidatorBoolean(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('contact[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Contact';
  }

}
