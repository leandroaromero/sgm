<?php

/**
 * IntervencionTienePersona form base class.
 *
 * @method IntervencionTienePersona getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseIntervencionTienePersonaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'intervencion_id' => new sfWidgetFormInputHidden(),
      'persona_id'      => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'intervencion_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('intervencion_id')), 'empty_value' => $this->getObject()->get('intervencion_id'), 'required' => false)),
      'persona_id'      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('persona_id')), 'empty_value' => $this->getObject()->get('persona_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('intervencion_tiene_persona[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IntervencionTienePersona';
  }

}
