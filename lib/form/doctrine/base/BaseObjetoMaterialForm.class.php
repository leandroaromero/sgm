<?php

/**
 * ObjetoMaterial form base class.
 *
 * @method ObjetoMaterial getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseObjetoMaterialForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'objeto_id'   => new sfWidgetFormInputHidden(),
      'material_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'objeto_id'   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('objeto_id')), 'empty_value' => $this->getObject()->get('objeto_id'), 'required' => false)),
      'material_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('material_id')), 'empty_value' => $this->getObject()->get('material_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('objeto_material[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ObjetoMaterial';
  }

}
