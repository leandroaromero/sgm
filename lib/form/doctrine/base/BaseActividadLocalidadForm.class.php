<?php

/**
 * ActividadLocalidad form base class.
 *
 * @method ActividadLocalidad getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseActividadLocalidadForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'actividad_id' => new sfWidgetFormInputHidden(),
      'localidad_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'actividad_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('actividad_id')), 'empty_value' => $this->getObject()->get('actividad_id'), 'required' => false)),
      'localidad_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('localidad_id')), 'empty_value' => $this->getObject()->get('localidad_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('actividad_localidad[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ActividadLocalidad';
  }

}
