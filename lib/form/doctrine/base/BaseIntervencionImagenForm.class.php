<?php

/**
 * IntervencionImagen form base class.
 *
 * @method IntervencionImagen getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseIntervencionImagenForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                              => new sfWidgetFormInputHidden(),
      'intervencion_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Intervencion'), 'add_empty' => true)),
      'intervencion_tipo_fotografia_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('IntervencionTipoFotografia'), 'add_empty' => true)),
      'archivo'                         => new sfWidgetFormInputText(),
      'descripcion'                     => new sfWidgetFormInputText(),
      'created_by'                      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'                      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
      'created_at'                      => new sfWidgetFormDateTime(),
      'updated_at'                      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'intervencion_id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Intervencion'), 'required' => false)),
      'intervencion_tipo_fotografia_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('IntervencionTipoFotografia'), 'required' => false)),
      'archivo'                         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'descripcion'                     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_by'                      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'required' => false)),
      'updated_by'                      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'required' => false)),
      'created_at'                      => new sfValidatorDateTime(),
      'updated_at'                      => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('intervencion_imagen[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IntervencionImagen';
  }

}
