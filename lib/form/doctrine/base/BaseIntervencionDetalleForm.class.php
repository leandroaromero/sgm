<?php

/**
 * IntervencionDetalle form base class.
 *
 * @method IntervencionDetalle getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseIntervencionDetalleForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                                   => new sfWidgetFormInputHidden(),
      'intervencion_id'                      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Intervencion'), 'add_empty' => true)),
      'intervencion_tipo_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('IntervencionTipo'), 'add_empty' => true)),
      'fecha'                                => new sfWidgetFormDate(),
      'observaciones_intervencion_realizada' => new sfWidgetFormTextarea(),
      'persona_id'                           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Persona'), 'add_empty' => true)),
      'created_by'                           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'                           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
      'created_at'                           => new sfWidgetFormDateTime(),
      'updated_at'                           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'intervencion_id'                      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Intervencion'), 'required' => false)),
      'intervencion_tipo_id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('IntervencionTipo'), 'required' => false)),
      'fecha'                                => new sfValidatorDate(),
      'observaciones_intervencion_realizada' => new sfValidatorString(array('required' => false)),
      'persona_id'                           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Persona'), 'required' => false)),
      'created_by'                           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'required' => false)),
      'updated_by'                           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'required' => false)),
      'created_at'                           => new sfValidatorDateTime(),
      'updated_at'                           => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('intervencion_detalle[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IntervencionDetalle';
  }

}
