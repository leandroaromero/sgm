<?php

/**
 * RegistroDetalleObra form base class.
 *
 * @method RegistroDetalleObra getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseRegistroDetalleObraForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'objeto_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => false)),
      'detalle_obra_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DetalleObra'), 'add_empty' => false)),
      'valor'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'objeto_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'))),
      'detalle_obra_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('DetalleObra'))),
      'valor'           => new sfValidatorString(array('max_length' => 45)),
    ));

    $this->widgetSchema->setNameFormat('registro_detalle_obra[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'RegistroDetalleObra';
  }

}
