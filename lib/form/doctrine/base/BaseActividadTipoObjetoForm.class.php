<?php

/**
 * ActividadTipoObjeto form base class.
 *
 * @method ActividadTipoObjeto getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseActividadTipoObjetoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'actividad_id'    => new sfWidgetFormInputHidden(),
      'tipo_objetos_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'actividad_id'    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('actividad_id')), 'empty_value' => $this->getObject()->get('actividad_id'), 'required' => false)),
      'tipo_objetos_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('tipo_objetos_id')), 'empty_value' => $this->getObject()->get('tipo_objetos_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('actividad_tipo_objeto[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ActividadTipoObjeto';
  }

}
