<?php

/**
 * Objeto form base class.
 *
 * @method Objeto getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseObjetoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'nombre_objeto'        => new sfWidgetFormInputText(),
      'anio_origen'          => new sfWidgetFormInputText(),
      'texto_antiguedad'     => new sfWidgetFormInputText(),
      'autor_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Autor'), 'add_empty' => true)),
      'donacion_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Donacion'), 'add_empty' => true)),
      'fecha_ingreso'        => new sfWidgetFormDate(),
      'forma_ingreso'        => new sfWidgetFormInputText(),
      'fecha_baja'           => new sfWidgetFormDate(),
      'motivo_baja'          => new sfWidgetFormTextarea(),
      'ubicacion_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Ubicacion'), 'add_empty' => false)),
      'historia_del_objeto'  => new sfWidgetFormTextarea(),
      'localidad_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true)),
      'tasacion_valor'       => new sfWidgetFormInputText(),
      'tasacion_unidades'    => new sfWidgetFormInputText(),
      'tasacion_fuente'      => new sfWidgetFormInputText(),
      'galeria_de_imagenes'  => new sfWidgetFormInputText(),
      'descripcion_generica' => new sfWidgetFormTextarea(),
      'tipo_objeto_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoObjeto'), 'add_empty' => false)),
      'created_by'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
      'coleccion_list'       => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Coleccion')),
      'dimension_list'       => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Dimension')),
      'material_list'        => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Material')),
      'trayectoria_list'     => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Trayectoria')),
      'categoria_list'       => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Categoria')),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'nombre_objeto'        => new sfValidatorString(array('max_length' => 50)),
      'anio_origen'          => new sfValidatorInteger(array('required' => false)),
      'texto_antiguedad'     => new sfValidatorString(array('max_length' => 75, 'required' => false)),
      'autor_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Autor'), 'required' => false)),
      'donacion_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Donacion'), 'required' => false)),
      'fecha_ingreso'        => new sfValidatorDate(array('required' => false)),
      'forma_ingreso'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'fecha_baja'           => new sfValidatorDate(array('required' => false)),
      'motivo_baja'          => new sfValidatorString(array('required' => false)),
      'ubicacion_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Ubicacion'))),
      'historia_del_objeto'  => new sfValidatorString(array('required' => false)),
      'localidad_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'required' => false)),
      'tasacion_valor'       => new sfValidatorNumber(array('required' => false)),
      'tasacion_unidades'    => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'tasacion_fuente'      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'galeria_de_imagenes'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'descripcion_generica' => new sfValidatorString(array('required' => false)),
      'tipo_objeto_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TipoObjeto'))),
      'created_by'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'required' => false)),
      'updated_by'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
      'coleccion_list'       => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Coleccion', 'required' => false)),
      'dimension_list'       => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Dimension', 'required' => false)),
      'material_list'        => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Material', 'required' => false)),
      'trayectoria_list'     => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Trayectoria', 'required' => false)),
      'categoria_list'       => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Categoria', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('objeto[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Objeto';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['coleccion_list']))
    {
      $this->setDefault('coleccion_list', $this->object->Coleccion->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['dimension_list']))
    {
      $this->setDefault('dimension_list', $this->object->Dimension->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['material_list']))
    {
      $this->setDefault('material_list', $this->object->Material->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['trayectoria_list']))
    {
      $this->setDefault('trayectoria_list', $this->object->Trayectoria->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['categoria_list']))
    {
      $this->setDefault('categoria_list', $this->object->Categoria->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveColeccionList($con);
    $this->saveDimensionList($con);
    $this->saveMaterialList($con);
    $this->saveTrayectoriaList($con);
    $this->saveCategoriaList($con);

    parent::doSave($con);
  }

  public function saveColeccionList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['coleccion_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Coleccion->getPrimaryKeys();
    $values = $this->getValue('coleccion_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Coleccion', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Coleccion', array_values($link));
    }
  }

  public function saveDimensionList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['dimension_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Dimension->getPrimaryKeys();
    $values = $this->getValue('dimension_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Dimension', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Dimension', array_values($link));
    }
  }

  public function saveMaterialList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['material_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Material->getPrimaryKeys();
    $values = $this->getValue('material_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Material', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Material', array_values($link));
    }
  }

  public function saveTrayectoriaList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['trayectoria_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Trayectoria->getPrimaryKeys();
    $values = $this->getValue('trayectoria_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Trayectoria', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Trayectoria', array_values($link));
    }
  }

  public function saveCategoriaList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['categoria_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Categoria->getPrimaryKeys();
    $values = $this->getValue('categoria_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Categoria', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Categoria', array_values($link));
    }
  }

}
