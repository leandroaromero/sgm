<?php

/**
 * ParticipantesLocalidad form base class.
 *
 * @method ParticipantesLocalidad getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseParticipantesLocalidadForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'participantes_id' => new sfWidgetFormInputHidden(),
      'localidad_id'     => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'participantes_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('participantes_id')), 'empty_value' => $this->getObject()->get('participantes_id'), 'required' => false)),
      'localidad_id'     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('localidad_id')), 'empty_value' => $this->getObject()->get('localidad_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('participantes_localidad[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ParticipantesLocalidad';
  }

}
