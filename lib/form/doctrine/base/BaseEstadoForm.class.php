<?php

/**
 * Estado form base class.
 *
 * @method Estado getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEstadoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'estado_de_conservacion' => new sfWidgetFormInputText(),
      'fecha_de_cambio'        => new sfWidgetFormDate(),
      'motivo_del_cambio'      => new sfWidgetFormTextarea(),
      'objeto_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'estado_de_conservacion' => new sfValidatorString(array('max_length' => 50)),
      'fecha_de_cambio'        => new sfValidatorDate(array('required' => false)),
      'motivo_del_cambio'      => new sfValidatorString(array('required' => false)),
      'objeto_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'))),
    ));

    $this->widgetSchema->setNameFormat('estado[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Estado';
  }

}
