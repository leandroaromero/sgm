<?php

/**
 * CategoriaObjeto form base class.
 *
 * @method CategoriaObjeto getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCategoriaObjetoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'categoria_id' => new sfWidgetFormInputHidden(),
      'objeto_id'    => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'categoria_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('categoria_id')), 'empty_value' => $this->getObject()->get('categoria_id'), 'required' => false)),
      'objeto_id'    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('objeto_id')), 'empty_value' => $this->getObject()->get('objeto_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('categoria_objeto[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CategoriaObjeto';
  }

}
