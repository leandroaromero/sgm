<?php

/**
 * Telefono form base class.
 *
 * @method Telefono getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTelefonoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'numero_telefono' => new sfWidgetFormInputText(),
      'tipo_telefono'   => new sfWidgetFormInputText(),
      'persona_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Persona'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'numero_telefono' => new sfValidatorString(array('max_length' => 45)),
      'tipo_telefono'   => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'persona_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Persona'))),
    ));

    $this->widgetSchema->setNameFormat('telefono[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Telefono';
  }

}
