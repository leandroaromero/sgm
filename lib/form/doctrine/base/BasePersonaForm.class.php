<?php

/**
 * Persona form base class.
 *
 * @method Persona getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePersonaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'apellido'          => new sfWidgetFormInputText(),
      'nombre'            => new sfWidgetFormInputText(),
      'nro_documento'     => new sfWidgetFormInputText(),
      'direccion'         => new sfWidgetFormInputText(),
      'localidad_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true)),
      'intervencion_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Intervencion')),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'apellido'          => new sfValidatorString(array('max_length' => 100)),
      'nombre'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'nro_documento'     => new sfValidatorInteger(array('required' => false)),
      'direccion'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'localidad_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'required' => false)),
      'intervencion_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Intervencion', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('persona[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Persona';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['intervencion_list']))
    {
      $this->setDefault('intervencion_list', $this->object->Intervencion->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveIntervencionList($con);

    parent::doSave($con);
  }

  public function saveIntervencionList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['intervencion_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Intervencion->getPrimaryKeys();
    $values = $this->getValue('intervencion_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Intervencion', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Intervencion', array_values($link));
    }
  }

}
