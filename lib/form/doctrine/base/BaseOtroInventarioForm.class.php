<?php

/**
 * OtroInventario form base class.
 *
 * @method OtroInventario getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseOtroInventarioForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'numero'             => new sfWidgetFormInputText(),
      'objeto_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => false)),
      'tipo_inventario_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoInventario'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'numero'             => new sfValidatorString(array('max_length' => 50)),
      'objeto_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'))),
      'tipo_inventario_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TipoInventario'))),
    ));

    $this->widgetSchema->setNameFormat('otro_inventario[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OtroInventario';
  }

}
