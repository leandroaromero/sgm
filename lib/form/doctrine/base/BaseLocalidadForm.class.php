<?php

/**
 * Localidad form base class.
 *
 * @method Localidad getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseLocalidadForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'nombre_localidad' => new sfWidgetFormInputText(),
      'provincia_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Provincia'), 'add_empty' => false)),
      'tipo_origen_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoOrigen'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'nombre_localidad' => new sfValidatorString(array('max_length' => 45)),
      'provincia_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Provincia'))),
      'tipo_origen_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TipoOrigen'))),
    ));

    $this->widgetSchema->setNameFormat('localidad[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Localidad';
  }

}
