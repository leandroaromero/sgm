<?php

/**
 * Dimension form base class.
 *
 * @method Dimension getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseDimensionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'nombre_dimension' => new sfWidgetFormInputText(),
      'dimension_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Objeto')),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'nombre_dimension' => new sfValidatorString(array('max_length' => 25)),
      'dimension_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Objeto', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('dimension[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Dimension';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['dimension_list']))
    {
      $this->setDefault('dimension_list', $this->object->Dimension->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveDimensionList($con);

    parent::doSave($con);
  }

  public function saveDimensionList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['dimension_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Dimension->getPrimaryKeys();
    $values = $this->getValue('dimension_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Dimension', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Dimension', array_values($link));
    }
  }

}
