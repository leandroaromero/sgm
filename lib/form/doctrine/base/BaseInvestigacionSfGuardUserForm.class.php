<?php

/**
 * InvestigacionSfGuardUser form base class.
 *
 * @method InvestigacionSfGuardUser getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseInvestigacionSfGuardUserForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'investigacion_id' => new sfWidgetFormInputHidden(),
      'sf_guard_user_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'investigacion_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('investigacion_id')), 'empty_value' => $this->getObject()->get('investigacion_id'), 'required' => false)),
      'sf_guard_user_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('sf_guard_user_id')), 'empty_value' => $this->getObject()->get('sf_guard_user_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('investigacion_sf_guard_user[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InvestigacionSfGuardUser';
  }

}
