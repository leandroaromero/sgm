<?php

/**
 * AspectoObra form base class.
 *
 * @method AspectoObra getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseAspectoObraForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'nombre'          => new sfWidgetFormInputText(),
      'obra_de_arte_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ObraDeArte'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'nombre'          => new sfValidatorString(array('max_length' => 50)),
      'obra_de_arte_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ObraDeArte'))),
    ));

    $this->widgetSchema->setNameFormat('aspecto_obra[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AspectoObra';
  }

}
