<?php

/**
 * FuenteBibliografica form base class.
 *
 * @method FuenteBibliografica getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFuenteBibliograficaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'titulo'        => new sfWidgetFormInputText(),
      'editorial'     => new sfWidgetFormInputText(),
      'lugar'         => new sfWidgetFormInputText(),
      'anio'          => new sfWidgetFormInputText(),
      'direccion_web' => new sfWidgetFormInputText(),
      'relato'        => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'titulo'        => new sfValidatorString(array('max_length' => 50)),
      'editorial'     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'lugar'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'anio'          => new sfValidatorInteger(array('required' => false)),
      'direccion_web' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'relato'        => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fuente_bibliografica[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FuenteBibliografica';
  }

}
