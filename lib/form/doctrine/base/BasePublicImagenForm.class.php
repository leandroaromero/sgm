<?php

/**
 * PublicImagen form base class.
 *
 * @method PublicImagen getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePublicImagenForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'imagen_id'        => new sfWidgetFormInputText(),
      'titulo'           => new sfWidgetFormInputText(),
      'archivo'          => new sfWidgetFormInputText(),
      'public_object_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PublicObject'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'imagen_id'        => new sfValidatorInteger(array('required' => false)),
      'titulo'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'archivo'          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'public_object_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PublicObject'))),
    ));

    $this->widgetSchema->setNameFormat('public_imagen[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PublicImagen';
  }

}
