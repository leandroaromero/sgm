<?php

/**
 * ObjetoTrayectoria form base class.
 *
 * @method ObjetoTrayectoria getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseObjetoTrayectoriaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'objeto_id'      => new sfWidgetFormInputHidden(),
      'trayectoria_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'objeto_id'      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('objeto_id')), 'empty_value' => $this->getObject()->get('objeto_id'), 'required' => false)),
      'trayectoria_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('trayectoria_id')), 'empty_value' => $this->getObject()->get('trayectoria_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('objeto_trayectoria[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ObjetoTrayectoria';
  }

}
