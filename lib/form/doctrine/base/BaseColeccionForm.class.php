<?php

/**
 * Coleccion form base class.
 *
 * @method Coleccion getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseColeccionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'nombre_coleccion' => new sfWidgetFormInputText(),
      'coleccion_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Objeto')),
      'actividades_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Actividad')),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'nombre_coleccion' => new sfValidatorString(array('max_length' => 100)),
      'coleccion_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Objeto', 'required' => false)),
      'actividades_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Actividad', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('coleccion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Coleccion';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['coleccion_list']))
    {
      $this->setDefault('coleccion_list', $this->object->Coleccion->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['actividades_list']))
    {
      $this->setDefault('actividades_list', $this->object->Actividades->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveColeccionList($con);
    $this->saveActividadesList($con);

    parent::doSave($con);
  }

  public function saveColeccionList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['coleccion_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Coleccion->getPrimaryKeys();
    $values = $this->getValue('coleccion_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Coleccion', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Coleccion', array_values($link));
    }
  }

  public function saveActividadesList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['actividades_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Actividades->getPrimaryKeys();
    $values = $this->getValue('actividades_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Actividades', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Actividades', array_values($link));
    }
  }

}
