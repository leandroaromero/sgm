<?php

/**
 * ObjetoColeccion form base class.
 *
 * @method ObjetoColeccion getObject() Returns the current form's model object
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseObjetoColeccionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'objeto_id'    => new sfWidgetFormInputHidden(),
      'coleccion_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'objeto_id'    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('objeto_id')), 'empty_value' => $this->getObject()->get('objeto_id'), 'required' => false)),
      'coleccion_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('coleccion_id')), 'empty_value' => $this->getObject()->get('coleccion_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('objeto_coleccion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ObjetoColeccion';
  }

}
