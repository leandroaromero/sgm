<?php

/**
 * Donacion form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class DonacionForm extends BaseDonacionForm
{
  public function configure()
  {
      $this->validatorSchema['created_at']= new sfValidatorDateTime(array('required'=>false) );
      $this->validatorSchema['updated_at']= new sfValidatorDateTime(array('required'=>false) );
      sfProjectConfiguration::getActive()->loadHelpers('Url');
      $this->widgetSchema['donante_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['donante_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
    array(
      'model' => "Donante",
      'url'   => url_for("@ajax_donante"),
      'config' => '{ max: 30}'
    ));
  }
}
