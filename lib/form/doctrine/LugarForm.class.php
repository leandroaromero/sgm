<?php

/**
 * Lugar form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class LugarForm extends BaseLugarForm
{
  public function configure()
  {
       $this->widgetSchema['pais_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'País',
              'model'   => 'Pais',
             'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
        $this->widgetSchema['provincia_id'] = new sfWidgetFormDoctrineDependentSelect(array(
              'label' => 'Provincia',
              'model'   => 'Provincia',
              'table_method' => 'getOrderNombre',
              'depends' => 'Pais'));
       $this->widgetSchema['localidad_id'] = new sfWidgetFormDoctrineDependentSelect(array(
              'label' => 'Localidad',
              'model'   => 'Localidad',
              'table_method' => 'getOrderNombre',
              'depends' => 'Provincia'));

       $this->validatorSchema['pais_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'Pais',
        ));
       $this->validatorSchema['provincia_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'Provincia',
        ));

  }
}
