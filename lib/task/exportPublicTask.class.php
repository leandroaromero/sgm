<?php

class exportPublicTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));
/*
    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));*/

    $this->namespace        = 'project';
    $this->name             = 'exportPublic';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [exportPublic|INFO] task does things.
Call it with:

  [php symfony exportPublic|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
      /* EVITAMOS EL CORTE*/
      ini_set("max_execution_time",0);
      ini_set('memory_limit', '-1');
      
      $indice = 1;

      $config= sfProjectConfiguration::getApplicationConfiguration('frontend', 'dev', true);      
      sfContext::createInstance($config);
      // initialize the database connection
      $databaseManager = new sfDatabaseManager($this->configuration);
     // $connection = $databaseManager->getDatabase($options['connection'])->getConnection();


          //RECORRE TODAS LAS BASES DE DATOS
          foreach($this->getBasesDatos() as $bd){
            //CONECTA LA BASE DE DATOS
            $connection = $databaseManager->getDatabase($bd['file'])->getConnection();
            $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
            $q = Doctrine_Query::create($conn)
              ->from('Objeto c')  
              ->orderBy('c.nombre_objeto ASC');

            foreach ($q->execute() as $key => $value) 
            {       
               if ( $value->getImagen()->count() > 0 )
                  {
                   //$this->cargarObject($value, $bd , $indice );

                    $public_object= new PublicObject();  
                    $public_object->setId($indice);
                    $public_object->setObjetoId($value->getId());
                    $public_object->setNombre($value->getNombreObjeto());
                    $public_object->setDescripcion($value->getDescripcionGenerica());
                    $public_object->setMuseoName($bd['name']); 
                    $public_object->setMuseoSubName($bd['subname']); 
                    $public_object->setDato($bd['file']);
                    $public_object->setAnio($value->getAnioOrigen());
                    $public_object->setTextoAntiguedad($value->getTextoAntiguedad());

                    if($value->getAutorId() > 0 && $value->getAutorId()!=NULL)
                    {
                      $public_object->setAutorId( $value->getAutorId());                      
                      $public_object->setAutor( $this->getColectionSearch($conn, $value->getAutorId()) );
                    }     
        
        
                    if($value->getTipoObjetoId() > 0 )
                    {
                      $public_object->setTipoId($value->getTipoObjetoId());
                      $public_object->setTipo($this->getTipoObjetoSearch($conn, $value->getTipoObjetoId()) );
                    }
                      
                    $public_object->setColecciones($this->getColectionSearch($conn, $value->getId() ));
                    $public_object->setMateriales($this->getMaterialSearch($conn, $value->getId() ));
                    $public_object->setImagen($this->getImagen1Search($conn, $value->getId() ));
                    $public_object->setCategoria($this->getCategoriaSearch($conn, $value->getId() ));

                    $public_object->save(); 
                    $public_object->setImagenes( $this->getImagenSearch($conn, $value->getId())); 
                    
                    $indice++;
                  }
                }  
          } 


    // add your code here
  }

   protected function getBasesDatos(){
       $this->respuesta= array();
       $this->respuestaTotal= array();
          
        //Archivo Historico
                 $this->respuesta['name'] = 'Archivo Histórico';
                 $this->respuesta['head'] = 'Archivo Histórico';
                 $this->respuesta['subname'] = 'Mons. José Alumni';
                 $this->respuesta['file'] = 'archivo_historico';
                 
                 $this->respuestaTotal[]=$this->respuesta;
        //GERALDI        
                 $this->respuesta['name'] = 'Casa, Museo y Sitio Histórico';
                 $this->respuesta['head'] = 'M. Luis Geraldi';
                 $this->respuesta['subname'] = 'Luis Geraldi';
                 $this->respuesta['file'] = 'geraldi';
                 
                 $this->respuestaTotal[]=$this->respuesta;
        
        //COLONIA
                 $this->respuesta['name'] = " Museo Casa Jardín Botánico";
                 $this->respuesta['head'] = " M. Jardín Botánico";
                 $this->respuesta['subname'] = 'Agusto Schulz';
                 $this->respuesta['file'] = 'jardin_botanico';
                 
                 $this->respuestaTotal[]=$this->respuesta;             
        //MUBA         
                 $this->respuesta['name'] = 'Museo Bellas Artes';
                 $this->respuesta['head'] = 'M. Bellas Artes';
                 $this->respuesta['subname'] = 'Rene Brusau';
                 $this->respuesta['file'] = 'bellas_artes';
                 
                 $this->respuestaTotal[]=$this->respuesta;
                 
        //CIENCIAS             
                 $this->respuesta['name'] = 'Museo Ciencias Naturales';
                 $this->respuesta['head'] = 'M. Ciencias Naturales';
                 $this->respuesta['subname'] = 'Agusto Schulz';
                 $this->respuesta['file'] = 'ccias_naturales';
                 
                 $this->respuestaTotal[]=$this->respuesta;
        
        //MEDIOS     
                 $this->respuesta['name'] = 'Museo de Medios';
                 $this->respuesta['head'] = 'M. de Medios';
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'medios_comunicacion';
                 
                 $this->respuestaTotal[]=$this->respuesta;
        
        //HOMBRE
                 $this->respuesta['name'] = 'Museo del Hombre Chaqueño';
                 $this->respuesta['head'] = 'M. del Hombre';
                 $this->respuesta['subname'] = 'Profesor Ertivio Acosta';
                 $this->respuesta['file'] = 'hombre_chaquenio';
                 
                 $this->respuestaTotal[]=$this->respuesta;
        
        //ISLA
                 $this->respuesta['name'] = "Museo Histórico Regional";
                 $this->respuesta['head'] = 'M. De la Isla';
                 $this->respuesta['subname'] = 'De la Isla';
                 $this->respuesta['file'] = 'isla_cerrito';
                 
                 $this->respuestaTotal[]=$this->respuesta;
              
        //ICHOALAY         
                 $this->respuesta['name'] = 'Museo Ichoalay';
                 $this->respuesta['head'] = 'M. Ichoalay';
                 $this->respuesta['subname'] = '';
                 $this->respuesta['file'] = 'ichoalay';
          
                 $this->respuestaTotal[]=$this->respuesta;

        return $this->respuestaTotal;
    }

  protected function cargarObject(Objeto $obj, Array $arra, Integer $id ){

        $public_object= new PublicObject();  
        $public_object->setId($id);
        $public_object->setIdObject($obj->getId());
        $public_object->setNombre($obj->getNombreObjeto());
        $public_object->setDescripcion($obj->getDescripcionGenerica());
        $public_object->setMuseoName($arra['name']); 
        $public_object->setMuseoSubName($arra['subname']); 
        $public_object->setArchivo($arra['file']);
        $public_object->setAnio($obj->getAnioOrigen());
        $public_object->setTextoAntiguedad($obj->getTextoAntiguedad());
        $public_object->setImagen($obj->getTextoAntiguedad());

        $public_object->save();            

  }   
  public function getAutorSearch($conn, $persona){
      
          $q = Doctrine_Query::create($conn)
              ->from('Persona p')
              ->Where('p.id = ?', $persona)
              ->fetchOne();
          $autor = $q->__toString();    
      return $autor;
            
  }
public function getTipoObjetoSearch($conn, $to){
      
          $q = Doctrine_Query::create($conn)
              ->from('TipoObjeto t')
              ->Where('t.id = ?', $to)
              ->fetchOne();
              $tipo = $q->__toString();

      return $tipo;
            
  }

  public function getColectionSearch($conn, $id){

          $q = Doctrine_Query::create($conn)
                   ->from('coleccion c')
                   ->leftJoin('c.ObjetoColeccion oa')     
                   ->Where('oa.objeto_id = ?', $id);
          $colecciones= '';

          foreach ($q->execute() as $key => $value) {
                     $colecciones.= $value.' ' ;
                   }         
          return $colecciones;
  }
  public function getCategoriaSearch($conn, $id){

          $q = Doctrine_Query::create($conn)
                   ->from('Categoria c')
                   ->leftJoin('c.CategoriaObjeto oc')     
                   ->Where('oc.objeto_id = ?', $id);
          $categoria= '';

          foreach ($q->execute() as $key => $value) {
                     $categoria.= $value.' ' ;
                   }         
          return $categoria;
  }
  public function getMaterialSearch($conn, $id ){

          $q = Doctrine_Query::create($conn)
                   ->from('Material c')
                   ->leftJoin('c.ObjetoMaterial om')     
                   ->Where('om.objeto_id = ?', $id);
          $materiales= '';

          foreach ($q->execute() as $key => $value) {
                     $materiales.= $value.' ' ;
                   }         
          return $materiales;
  }
  public function getImagenSearch($conn,$id){

          $q = Doctrine_Query::create($conn)
                ->select('i.archivo, i.titulo')
                ->from('Imagen i')
                ->andWhere('i.objeto_id = ?', $id)
                ->fetchArray();
          return $q;
  }

  public function getImagen1Search($conn, $id ){
      
          $q = Doctrine_Query::create($conn)
                ->from('Imagen i')
                ->andWhere('i.objeto_id = ?', $id)
                ->andWhere('i.titulo like ?',  '%frente%');
          
          $foto = '';

          if( $q->count() > 0){
            $foto = $q->fetchOne()->getArchivo();
          }

      return $foto;
  }

    

}
