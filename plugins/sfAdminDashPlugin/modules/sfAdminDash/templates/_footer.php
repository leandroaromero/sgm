<?php
  use_helper('I18N');
?>  

<?php if($sf_user->isAuthenticated()):?>
<div id='sf_admin_theme_footer_1'>
<?php  image_tag('cultura.jpg'); ?>
</div>

<div id='sf_admin_theme_footer_2' >
<?php  image_tag('logo2.jpg'); ?>
</div>
<?php else: ?>
<div id='sf_admin_theme_footer_1'>
<?php  image_tag('logos2.jpg'); ?>
</div>

<div id='sf_admin_theme_footer_2' >
<?php  image_tag('LOGO3.jpg'); ?>
</div>
<?php endif?>


<div id='sf_admin_theme_footer'>
	<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licencia Creative Commons Atribución-NoComercial-CompartirIgual 4.0 Internacional</a>.<br/><br/> 
</div>
