vjMoreTimestampablePlugin
=================

This plugin provides an advanced timestampable behavior to use more than created_at and updated_at fields (ex: validated_at, deleted_at, archivated_at).

It extends Timestampable behavior and so can remplaces it.

Adding a new field creates two fields : timestamp field and boolean field. The boolean field is used to update the timestamp's.
You just have to manage boolean field to update timestamp's.


Installation
------------

  * Install the plugin

        $ symfony plugin:install vjMoreTimestampablePlugin
        $ symfony cc


How to make something more timestampable ?
-------------

Add behavior in your schema

  * edit config/doctrine/schema.yml

        News:
          actAs:
            MoreTimestampable: ~
            # others behaviors


How to use and param it ?
-------------

Configuration is exactly the same as Timestampable behavior : [official documentation](http://www.doctrine-project.org/projects/orm/1.2/docs/manual/behaviors/en#core-behaviors:timestampable)

  * Edit your schema

        News:
          actAs:
            MoreTimestampable:
              # by default, include created_at and updated_at fields, configuration is the same
              validated: ~
              deleted:
                name: removed_at
                link:
                  name: remove_it
          columns:
            titre: string(255)

  * For example :

          $news = new News();
          $news->titre = "title";
          $news->validated = true;
          $news->save();

          id: '1'
          titre: title
          created_at: '2010-09-06 16:08:18'
          updated_at: '2010-09-06 16:08:18'
          validated_at: '2010-09-06 16:08:18'
          is_validated: true
          removed_at: null
          remove_it: null

  * in lib/form/NewsForm.class.php

          class NewsForm extends BaseNewsForm
          {
            public function configure()
            {
              unset($this['created_at'], $this['updated_at'], $this['validated_at'], $this['removed_at']);
            }
          }

And use only checkbox or specials actions that update boolean field.


Contact
-------------
Please contact me if you see a problem, an error or if you think that something can be enhanced !
Advice are cool too !