<?php

class Doctrine_Template_Listener_MoreTimestampable extends Doctrine_Template_Listener_Timestampable
{
  public function preInsert(Doctrine_Event $event)
  {
    parent::preInsert($event);
    $this->_updateMoreTimestampable($event);
  }

  public function preUpdate(Doctrine_Event $event)
  {
    parent::preUpdate($event);
    $this->_updateMoreTimestampable($event);
  }

  protected function _updateMoreTimestampable($event)
  {
    foreach($this->_options as $option => $datas)
    {
      if($option != "updated" && $option != "created")
      {
        $name = $event->getInvoker()->getTable()->getFieldName($this->_options[$option]['name']);
        $link_name = $event->getInvoker()->getTable()->getFieldName($this->_options[$option]['link']['name']);
        $modified = $event->getInvoker()->getModified();
        if (isset($modified[$link_name]))
        {
          if($modified[$link_name] === true)
          {
            $event->getInvoker()->$name = $this->getTimestamp($option, $event->getInvoker()->getTable()->getConnection());
          }
          else
          {
            $event->getInvoker()->$name = null;
          }
        }
      }
    }
  }
}