<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php $ico= sfConfig::get('app_url_ico');  ?>
    <link rel="shortcut icon" href="<?php echo $ico ?>" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>

  </head>
  <body>
    <?php include_component('sfAdminDash','header'); ?>
      <div id="title_logo" >
        <?php $file= sfConfig::get('app_institution_file');?>
        <div style="display:inline; float: right; width:500px">
          <?php echo $file_1= sfConfig::get('app_institution_name');?> </br>
          <?php echo $file_2= sfConfig::get('app_institution_subtitle');?></br>
        </div>
      <div style="width:200px">
            <?php echo image_tag('museo/'.$file.'/tcpdf_logo.jpg', 'size=200x70')?>
      </div>
                  
        <hr>
    </div>
    
    <?php echo $sf_content ?>
    <?php include_partial('sfAdminDash/footer'); ?>
  </body>
</html>
