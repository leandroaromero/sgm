<?php

require_once dirname(__FILE__).'/../lib/intervencion_tipoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/intervencion_tipoGeneratorHelper.class.php';

/**
 * intervencion_tipo actions.
 *
 * @package    museo
 * @subpackage intervencion_tipo
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class intervencion_tipoActions extends autoIntervencion_tipoActions
{
}
