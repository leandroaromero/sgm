<?php use_helper('jQuery') ?>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<div id="dato_email">
<form action="<?php echo url_for('componentes/'.($form->getObject()->isNew() ? 'grabarEmail' : 'grabarEmail').(!$form->getObject()->isNew() ? '?e_id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>

            &nbsp;
          <?php echo jq_link_to_remote('Cancelar', array(
                                        'update' => 'dato_email',
                                        'url' => 'componentes/cancelarEmail?id='.$p_id,
                                        'script' => 'true',
                                                  )); ?>
           &nbsp;
          <?php  echo jq_submit_to_remote('agregar','Grabar', array(
          'update' => 'dato_email',
          'url'    => 'componentes/grabarEmail?t_id='.$form->getObject()->getId(),
           ))
            ?>







        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
        <tr>
            <th>
                <?php echo $form ?>
            </th>
        </tr>
    </tbody>
  </table>
</form>
</div>