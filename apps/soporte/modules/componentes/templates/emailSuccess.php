<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_imagenes">
<div id="dato_email">
 <?php if ($form->isNew()):?>
 <br/>
 <p style="color:red"> Los correos se podrán cargar una vez que la persona este guardada</p>
 <br>
 <?php else: ?>
<br/>
<center>
<table  class="sf_admin_list" width="70%">
                    <tr>
                        <th>Dirrección del correo</th>
                        <th>Acción</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($form->getObject()->getEmail() as $ema): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                      <td><?php echo $ema->getDireccionEmail()?></td>
                      <td>
                           <?php echo jq_link_to_remote(image_tag('edit16.png', array('alt' => 'Agregar', 'title' =>'Modificar')), array(
                                        'update' => 'dato_email',
                                        'url' => 'componentes/cargarEmail?email[id]='.$ema->getId().'&persona_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  )); ?>

                          <?php echo jq_link_to_remote(image_tag('cross.png', array('alt_title' => 'Eliminar email', 'height' => 20)), array(
                                        'update' => 'dato_email',
                                        'url' => 'componentes/deleteEmail?id='.$ema->getId(),
                                        'script' => 'true',
                                        'confirm' => '¿Esta usted seguro que desea eliminar esta direccion de correo?'
                                                  )); ?>
                      </td>

                   </tr>
                <?php endforeach; ?>

              </table>
</center>
<center>     <?php echo jq_link_to_remote('Nuevo Correo', array(
                                        'update' => 'dato_email',
                                        'url' => 'componentes/cargarEmail?persona_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  ), array('class'=>'button')); ?>
</center>
<?php endif?>
</div>

</div>