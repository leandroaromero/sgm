<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_imagenes">
<div id="dato_telefono">
 <?php if ($form->isNew()):?>
 <br/>
 <p style="color:red"> Los telefonos se podran cargar una vez que la persona este guardada</p>
 <br>
 <?php else: ?>
<br/>
<center>
<table  class="sf_admin_list" width="70%">
                    <tr>
                        <th>Tipo de telefono</th>
                        <th>Número</th>
                        <th>Acción</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($form->getObject()->getTelefono() as $tel): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                      <td><?php echo $tel->getTipoTelefono()?></td>
                      <td><?php echo $tel->getNumeroTelefono()?></td>
                      <td>
                          <?php echo jq_link_to_remote(image_tag('edit16.png', array('alt' =>'Agregar', 'title' =>'Modificar')), array(
                                        'update' => 'dato_telefono',
                                        'url' => 'componentes/cargarTelefono?telefono[id]='.$tel->getId().'&persona_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  )); ?>

                        <?php echo jq_link_to_remote(image_tag('cross.png', array('alt_title' => 'Eliminar email', 'height' => 20)), array(
                                        'update' => 'dato_telefono',
                                        'url' => 'componentes/deleteTelefono?id='.$tel->getId(),
                                        'script' => 'true',
                                        'confirm' => '¿Esta usted seguro que desea eliminar este telefono?'
                                                  )); ?>

                      </td>

                   </tr>
                <?php endforeach; ?>

</table>
</center>
<center>
      <?php echo jq_link_to_remote('Nuevo Telefono', array(
                                        'update' => 'dato_telefono',
                                        'url' => 'componentes/cargarTelefono?persona_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  ), array('class'=>'button')); ?>



    </center>
<?php endif?>
</div>

</div>