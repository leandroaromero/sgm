<?php

require_once dirname(__FILE__).'/../lib/intervencion_recomendacionGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/intervencion_recomendacionGeneratorHelper.class.php';

/**
 * intervencion_recomendacion actions.
 *
 * @package    museo
 * @subpackage intervencion_recomendacion
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class intervencion_recomendacionActions extends autoIntervencion_recomendacionActions
{
}
