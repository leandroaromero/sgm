<?php

/**
 * sfGuardFormSignin for sfGuardAuth signin action
 *
 * @package    sfDoctrineGuardPlugin
 * @subpackage form
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardFormSignin.class.php 23536 2009-11-02 21:41:21Z Kris.Wallsmith $
 */
class sfGuardFormAdminSignin extends BasesfGuardFormSignin
{
  /**
   * @see sfForm
   */
  public function configure()
  {
      unset($this->widgetSchema['remember']);
          $parameter = sfConfig::get('app_institucion_data', array(''=>'') );

    $choices    = $parameter; 

$this->widgetSchema['cambio'] = new sfWidgetFormChoice(
    array(
        'choices'   => $choices,
        'multiple'  => false,
        'label'     => 'Instituciones',
        'expanded'  => false
        ),
    array()
);
   $this->widgetSchema['cambio']->setAttributes(array(
          'onChange' =>'buscarLogo(this.value)'
      ));   

$this->validatorSchema['cambio']= new sfValidatorPass();

$this->widgetSchema['cambio']->setDefault(sfContext::getInstance()->getUser()->getDataBase());
  }
}
