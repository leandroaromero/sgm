<?php use_helper('jQuery') ?>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<div id="dato_estado_1">
<form action="<?php echo url_for('obj_dimension/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
          <td colspan="1" align="left">
          <?php echo $form->renderHiddenFields(false) ?>

            <?php 
                if(!$form->getObject()->isNew()){
                       echo jq_link_to_remote('Borrar', array(
                                        'update' => 'dato_estado',
                                        'url' => 'objeto/deleteEstado?e_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                        'confirm' => '¿Esta usted seguro que desea eliminar este Estado?'
                                                  ));
                }  ?>
           &nbsp;
           &nbsp;
        </td>
        <td>
        

          <?php echo jq_link_to_remote('Cancelar', array(
                                        'update' => 'dato_estado',
                                        'url' => 'objeto/cancelarCarga?o_id='.$objeto_id.'&template=estados',
                                        'script' => 'true',
                                                  )); ?>

          <?php  echo jq_submit_to_remote('agregar','Grabar', array(
          'update' => 'dato_estado',
          'url'    => 'objeto/grabarEstado?e_id='.$form->getObject()->getId(),
           ))
            ?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['estado_de_conservacion']->renderLabel() ?>*</th>
        <td>
          <?php echo $form['estado_de_conservacion']->renderError() ?>
          <?php echo $form['estado_de_conservacion'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['fecha_de_cambio']->renderLabel() ?>*</th>
        <td>
          <?php echo $form['fecha_de_cambio']->renderError() ?>
          <?php echo $form['fecha_de_cambio'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['motivo_del_cambio']->renderLabel() ?></th>
        <td>
          <?php echo $form['motivo_del_cambio']->renderError() ?>
          <?php echo $form['motivo_del_cambio'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
</div>
