<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="detalles_de_obra-group sf_admin_form_row sf_admin_text">

 <label class="detalles_de_obra-label" for="member_id"></label>
 <div class="controls">
   <table style="width: 100%">
    <tr>
        <td>Borrar</td>
        <td>Lenguaje Artístico</td>        
        <td>Descripción</td>        
        <td>Detalle</td>
        <td>Valor</td>
    </tr>
     <?php foreach ($form['detalles_de_obra'] as $key=>$r ):?>
       <tr>              
         <td>
            <?php echo $r['id']->render(); ?>
            <?php echo $r['delete']->render(); ?></td>
         <td> 
             <?php echo $r['obra_de_arte_id']->renderError() ?>
             <?php echo $r['obra_de_arte_id']->render(); ?></td>
         <td>
             <?php echo $r['aspecto_obra_id']->renderError() ?>
             <?php echo $r['aspecto_obra_id']->render(); ?></td>
         <td>
             <?php echo $r['detalle_obra_id']->renderError() ?>
             <?php echo $r['detalle_obra_id']->render(); ?></td>
         <td>
             <?php echo $r['valor']->renderError() ?>
             <?php echo $r['valor']->render(); ?></td>
         
        </tr>    
    <?php endforeach;?>
   </table>
     
    
 </div>
</div>