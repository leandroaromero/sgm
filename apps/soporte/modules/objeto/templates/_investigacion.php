<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_imagenes">
    <div id="dato_investigacion">
        <?php if (!$form->isNew()):?>

            <table  class="sf_admin_list" width="100%">
                    <tr>
                        <th>Fecha</th>
                        <th>Relato</th>
                        <th>Fuente B.</th>
                        <th>Investigador</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($form->getObject()->getInvestigacion() as $inves): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                       <td><?php echo $inves->getFecha() ?></td>
                       <td><?php echo $inves->getRelato()?></td>
                       <td><?php echo $inves->getFuenteBibliografica()?></td>
                       <td><?php foreach($inves->getInvestigadores() as $inve):?> 
                           <?php echo $inve; ?> <br />
                           <?php endforeach ?>
                       </td>    
                   </tr>
                <?php endforeach; ?>

            </table>
        <?php endif?>
    </div>
</div>