<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_inventarios">
	<div id="dato_inventarios">
		<?php if (!$form->isNew()):?>
 
            <table  class="sf_admin_list" width="100%">
                    <tr>
                        <th>Inventario</th>
                        <th>Numero</th>
                    </tr>
                    
                <?php  $con=0; $i=1; foreach ($form->getObject()->getOtroInventario() as $in): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                       <td><?php echo $in->getTipoInventario() ?></td>
                       <td><?php echo $in->getNumero()?></td>
                   </tr>
                <?php endforeach; ?>

            </table>
        <?php endif ?>

    </div>

</div>
