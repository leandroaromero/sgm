<?php

require_once dirname(__FILE__).'/../lib/objetoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/objetoGeneratorHelper.class.php';

/**
 * objeto actions.
 *
 * @package    museo
 * @subpackage objeto
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class objetoActions extends autoObjetoActions
{
    public function executeListShow($request)
    {
     $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($request->getParameter('id'))), sprintf('Esta persona no existe (%s).', $request->getParameter('id')));
        
    }

    public function executeListSoporte($request)
    {
      $this->redirect('etapas/new?objeto_id='.$request->getParameter('id'));
        
    }
    
    public function executeCargarEstado($request)
    {
         $d_id = $this->getRequestParameter('estado_id');
         $this->objeto_id = $this->getRequestParameter('objeto_id');
         if($d_id > 0){
              $this->forward404Unless($this->estado = Doctrine::getTable('Estado')->find(array($d_id)), sprintf('Este estado no exite (%s).', $d_id));
          }else{
              $this->estado= new Estado();
              $this->estado->setObjetoId($this->objeto_id);
               }
        $this->form= new EstadoAdminForm($this->estado);
    }

    public function  executeDeleteEstado($request) 
    {
        $this->forward404Unless($estado = Doctrine::getTable('Estado')->find(array($request->getParameter('e_id'))), sprintf('Object estado does not exist (%s).', $request->getParameter('id')));
        $id= $estado->getObjetoId();
        $estado->delete();
        $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));

        $this->form= new ObjetoForm($this->objeto);
        $this->setTemplate('estados');
   }
   
    public function executeGrabarEstado(sfWebRequest $request)
    {
         $e_id= $request->getParameter('e_id');
         $arr=array();
         $arr=$request->getParameter('estado');
         if( $e_id > 0){
             $this->forward404Unless($odim = Doctrine::getTable('Estado')->find(array($e_id)), sprintf('Object estado does not exist (%s).', $e_id));
         }else{
             $odim = new Estado();
             $odim->setObjetoId($arr['objeto_id']);

         }
         $fecha = $arr['fecha_de_cambio']['year'].'-'.$arr['fecha_de_cambio']['month'].'-'.$arr['fecha_de_cambio']['day'];
         $odim->setEstadoDeConservacion($arr['estado_de_conservacion']);
         $odim->setFechaDeCambio( $fecha );
         $odim->setMotivoDelCambio($arr['motivo_del_cambio']);
         $odim->save();
     
         $id= $odim->getObjetoId();
         $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
         $this->form= new ObjetoForm($this->objeto);
         $this->setTemplate('estados');
    }

                
    public function executeCancelarCarga(sfWebRequest $request)
    {
         $id= $request->getParameter('o_id');
         $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object  does not exist (%s).', $id));
         $this->form= new ObjetoForm($this->objeto);
         $this->setTemplate($request->getParameter('template'));
    }

}
