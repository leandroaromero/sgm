<?php

/**
 * RegistroDetalleObra form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class RegistroDetalleObraAdminForm extends BaseRegistroDetalleObraForm
{
  public function configure()
  {

	  	unset($this['objeto_id']);



	   	$this->widgetSchema['obra_de_arte_id'] = new sfWidgetFormDoctrineChoice(array(
        	'model'   => 'ObraDeArte',
        	'method'       => 'getNombre',
        	'add_empty' => '>> Seleccione <<',
    		));
    	$this->widgetSchema['aspecto_obra_id'] = new sfWidgetFormDoctrineDependentSelect(array(
        	'model'   => 'AspectoObra', 
        	'depends' => 'ObraDeArte',
        	'method'       => 'getNombre',
            'add_empty' => '>> Seleccione <<',
    	));
    	$this->widgetSchema['detalle_obra_id'] = new sfWidgetFormDoctrineDependentSelect(array(
        	'model'   => 'DetalleObra', 
        	'depends' => 'AspectoObra',
        	'method'       => 'getNombre',
            'add_empty' => '>> Seleccione <<',
    	));

    	$this->widgetSchema['obra_de_arte_id']->setAttributes(array('style' =>'width:150px'));
    	$this->widgetSchema['aspecto_obra_id']->setAttributes(array('style' =>'width:150px'));
    	$this->widgetSchema['detalle_obra_id']->setAttributes(array('style' =>'width:150px'));    	    	


        // validadores
            
        $this->validatorSchema['obra_de_arte_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'ObraDeArte',            
            'required' => false,
        ));
                
        $this->validatorSchema['aspecto_obra_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'AspectoObra',
            'required' => false,
        ));          
                
        $this->validatorSchema['detalle_obra_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'DetalleObra',
            'required' => false,
        ));          

        $this->validatorSchema['valor'] = new sfValidatorString(array('max_length' => 45, 'required' => false));
                
        $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
        $this->validatorSchema['delete'] = new sfValidatorPass();            

       // $this->useFields(array('obra_de_arte_id', 'aspecto_obra_id', 'detalle_obra_id', 'valor', 'delete'));            
  }
}
