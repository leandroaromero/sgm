<?php

/**
 * Imagen form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ImagenAdminForm extends BaseImagenForm
{
  public function configure()
  {
      
        unset($this['objeto_id'],$this['updated_at'],$this['created_at'],$this['updated_by'],$this['created_by']);
//           $this->widgetSchema['objeto_id']=  new sfWidgetFormInputHidden();
           $this->widgetSchema['descripcion']=  new sfWidgetFormTextarea();
           $dir= sfContext::getInstance()->getUser()->getFile();
           $uploads= sfConfig::get('sf_upload_dir');


           $this->widgetSchema['archivo'] = new sfWidgetFormInputFileEditable(array(
               'label'     => 'Imagen',
               'file_src'  => '/museo/web/uploads/'.$dir.'/fotos/'.$this->getObject()->getArchivo(),
               'is_image'  => true,
               'edit_mode' => true,
               'template'  => '%input%',
            ));

            $this->validatorSchema['archivo'] = new sfValidatorFile(array(
                'required'    => false,
                 'mime_types' => array(  'application/pdf',
                                         'application/x-pdf',
                                      
                                      'image/png',
                                      'image/jpg',       
                                      'image/jpeg',
                                      'image/pjpeg',
                                      'image/x-png',
                                      'image/gif',
                                      'image/bmp',
                                      'image/tiff',
                                      'image/x-icon',

                                      'video/avi',
                                      'video/divx',
                                      'video/x-flv',
                                      'video/quicktime',
                                      'video/mpeg',
                                      'video/mp4',
                                      'video/ogg',

                                      'audio/mpeg',
                                      'audio/x-realaudio',
                                      'audio/wav',
                                      'audio/ogg',
                                      'audio/midi',
                                      'audio/x-ms-wma',
                                      'audio/x-ms-wax',
                                      'audio/x-matroska'
                                    ), 

                'path' => $uploads.'/'.$dir.'/fotos/',
                'validated_file_class' => 'sfResizedFile',
            ), array(
               // 'max_size' => 'El tama&ntilde;o m&aacute;ximo es 5 MB',
            ));

              $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
              $this->validatorSchema['delete'] = new sfValidatorPass();
  }
     
//  
  public function doUpdateObject($values)
       {
            

            $fail = $this->getObject()->getArchivo();

            if( $values['archivo'] != $fail ){
                $dir= sfContext::getInstance()->getUser()->getFile(); 
                $uploads= sfConfig::get('sf_upload_dir');

                        
                $p1 = $uploads.'/'.$dir.'/fotos/big/'.$fail;
                @unlink($p1);
                
                $p2 = $uploads.'/'.$dir.'/fotos/thumbs/'.$fail;
                @unlink($p2);
                
//                $p  = $uploads.'/'.$dir.'/fotos/'.$fail;
//                @unlink($p);
                
            }
   
           return parent::doUpdateObject($values);
        }
//  
}
