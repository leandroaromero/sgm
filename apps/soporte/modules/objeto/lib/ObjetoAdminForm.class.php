<?php

/**
 * Objeto form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObjetoAdminForm extends BaseObjetoForm
{

    
 public function configure()
  {
     
      unset($this['dimension_list'],$this['updated_at'],$this['created_at'],$this['updated_by'],$this['created_by']);
      sfProjectConfiguration::getActive()->loadHelpers('Url');
      $rango = range(1800, 2040);
      $arreglo_rango = array_combine($rango, $rango);  
      $web = sfConfig::get('app_url_web');       

      $this->widgetSchema['coleccion_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['coleccion_list']->setOption('table_method', 'getOrderNombre');
      $this->widgetSchema['coleccion_list']->setOption('label', 'Colecciones');
      
      $this->widgetSchema['material_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['material_list']->setOption('table_method', 'getOrderNombre');
      $this->widgetSchema['material_list']->setOption('label', 'Materiales');
      
      $this->widgetSchema['categoria_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['categoria_list']->setOption('label', 'Categorias');
      
      $this->widgetSchema['trayectoria_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['trayectoria_list']->setOption('renderer_options', array(
                    'label_unassociated' => 'No Participo de:',
                    'label_associated' => 'Si Participo de:',
                ));
      $this->widgetSchema['trayectoria_list']->setOption('table_method', 'getOrderNombre');
      $this->widgetSchema['trayectoria_list']->setOption('label', 'Eventos');


      $this->widgetSchema['fecha_ingreso'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           'years' => $arreglo_rango,
           ))
        ));
       $this->widgetSchema['fecha_baja'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           'years' => $arreglo_rango,
           ))
        ));
         
       $this->widgetSchema['ubicacion_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Ubicacion ',
              'model'   => 'Ubicacion',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
      
     
    
        //Empotramos al menos un formulario de Imagenes
            $imagenes = $this->getObject()->getImagen();
            if (!$this->getObject()->getImagen()->count()){
                  $imagen = new Imagen();
                  $imagen->setObjeto($this->getObject());
                  $imagenes = array($imagen);
            }

            //Un formulario vacío hará de contenedor para todas las imagenes
            $imagenes_forms = new SfForm();
            $count = 0;
            foreach ($imagenes as $imagen) {
                    $ima_form = new ImagenAdminForm($imagen);
                
                    //Empotramos cada formulario en el contenedor
                    $imagenes_forms->embedForm($count, $ima_form);
                    $count ++;
            }
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('lista_imagenes', $imagenes_forms);
    
    
        //Registro Detalle Obra
            $registros = $this->getObject()->getRegistroDetalleObra();
            if (!$this->getObject()->getRegistroDetalleObra()->count()){
                  $registro = new RegistroDetalleObra();
                  $registro->setObjeto($this->getObject());                  
                  $registros = array($registro);
            }

            //Un formulario vacío hará de contenedor para todas las imagenes
            $registros_forms = new SfForm();
            $count = 0;            
            foreach ($registros as $registro) {

              $registro_form = new RegistroDetalleObraAdminForm($registro);                
              //Empotramos cada formulario en el contenedor
              $registros_forms->embedForm($count, $registro_form);
              $count++;
            }
            
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('detalles_de_obra', $registros_forms);

  }

  
  
}