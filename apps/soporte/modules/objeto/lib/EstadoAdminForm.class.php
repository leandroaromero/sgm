<?php

/**
 * Estado form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EstadoAdminForm extends BaseEstadoForm
{
  public function configure()
  {
	  $this->widgetSchema['objeto_id']= new sfWidgetFormInputHidden();
	  $web = sfConfig::get('app_url_web');       
	  $rango = range(2000, 2025);
      $arreglo_rango = array_combine($rango, $rango);   
	  $this->widgetSchema['fecha_de_cambio'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
         'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           'years' => $arreglo_rango,
           ))
        ));
  }
}
