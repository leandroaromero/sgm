<?php

require_once dirname(__FILE__).'/../lib/intervencion_tipo_imagenGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/intervencion_tipo_imagenGeneratorHelper.class.php';

/**
 * intervencion_tipo_imagen actions.
 *
 * @package    museo
 * @subpackage intervencion_tipo_imagen
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class intervencion_tipo_imagenActions extends autoIntervencion_tipo_imagenActions
{
}
