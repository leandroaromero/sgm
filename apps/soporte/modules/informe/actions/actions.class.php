<?php

/**
 * informe actions.
 *
 * @package    museo
 * @subpackage informe
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class informeActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->form= new IntervencionAdminFormFilter();
  }

  public function executeResult(sfWebRequest $request)
  {
    
    $form= $request->getParameter('result');

    $cambio=array('museo'=> $form['cambio']);  //'todo') 'hombre_chaquenio');   //'todo'   --> por el momento se deja de lado 
    
    $this->search = new SearchBuilder($cambio);
    $this->titulos=array("Diagnosticos", "Propuestas", "Intervenciones", "Recomendaciones" );
    $this->todos= array();       
                

    $anio=$form['fecha'];            
    $meses    = array( "1","2","3","4","5","6","7","8","9","10","11","12");
    $tipos    =array("diagnosticos", "propuestas", "intervenciones","recomendaciones");
    
    foreach ($tipos as $key => $tipo) {              
        $pie = array("TOTAL", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
        $TOTAL= 0;
        $this->result_t= array();
        $this->result_t[]= array('USUARIOS', "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEM.", "OCTUBRE", "NOVIEM.", "DICIEM." , "TOTAL");

      foreach($this->search->getBasesDatos() as $bd)
        {
          $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
               
          $result= array();
          $resultmes= array();
          $result[]=$bd['head']; 
          $total=0;

          foreach ($meses as $key => $mes) 
          {    

            switch ( $tipo ) {
                 case "diagnosticos" :
                    $q = Doctrine_Query::create($conn)->from('Intervencion o');
                    $this->Tipo='Diagnosticos';
                  break;
                 case "propuestas" :
                    $q = Doctrine_Query::create($conn)->from('IntervencionPropuesta o');
                    $this->Tipo= 'Propuestas' ;             
                  break;
                 case "recomendaciones" :
                    $q = Doctrine_Query::create($conn)->from('IntervencionTieneRecomendacion o');
                    $this->Tipo= 'Recomendaciones';              
                  break;      
                  case "intervenciones" :
                    $q = Doctrine_Query::create($conn)->from('IntervencionDetalle o');
                    $this->Tipo= 'Intervenciones';              
                  break;      
               }   


            if( !empty($anio['year']))
             {
                $q->andWhere('YEAR(o.created_at)= ?',  $anio['year']);
                } 
            $q->andWhere('MONTH(o.created_at) = ?',  $mes);            
            $q->fetchArray(); 

            $result[]= $q->count();
            $total= $total + $q->count();
            $pie[ $mes ]=$pie[ $mes ]+$q->count();  
            $TOTAL=$TOTAL +$q->count();  

          }

          $result[]= $total;      
                 
          $this->result_t[]= $result;
       }

       $pie[]= $TOTAL;
       $this->result_t[]= $pie;
       $this->todos[] = $this->result_t;
     }
     $filtro= array();
     $this->fechaD= $anio['year'];
     $filtro[0]= 'Año: '.$this->fechaD;
     $filtro[1]= " ";
      
                   $this->getUser()->setAttribute('tabla_visita', $this->todos);
                   $this->getUser()->setAttribute('titulos_visita', $this->titulos );
                   $this->getUser()->setAttribute('filtros_visita', $filtro);
                   $this->getUser()->setAttribute('archivo', 'Trabajo Realizado');
    }


  public function executeToExcel($request) {        
       
    ini_set("max_execution_time",0);
    $this->setLayout(false);
    sfConfig::set('sf_web_debug', false);    
    
    $nombre = $this->getUser()->getAttribute('archivo');         

    $tmp_dir =   sfConfig::get('sf_cache_dir').'/';           
    if( strlen( $nombre ) > 0){
        $nombre = $nombre.'.xlsx';
    }else{
        $nombre ='Informe General.xlsx';    
    }
    $file_name = $tmp_dir.$nombre;
    $resp = $this->getResponse();
    $resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
    $resp->setHttpHeader('Content-Disposition', ' attachment; filename="'.$nombre.'"');
    $resp->setHttpHeader('Cache-Control', ' maxage=3600');
    $resp->setHttpHeader('Pragma', 'public');
                
    $excel_builder = new ExcelBuilder();
                
                $tabla=   $this->getUser()->getAttribute('tabla_visita');
                $titulos= $this->getUser()->getAttribute('titulos_visita');
                $filtros= $this->getUser()->getAttribute('filtros_visita');
                $objWriter = new PHPExcel_Writer_Excel2007($excel_builder->generateExcelGeneral($titulos, $tabla, $filtros)); 
                    

    $objWriter->save($file_name);
    $this->file = $file_name;

   }    


  public function executeDeterioros(sfWebRequest $request)
  {
    $this->form= new IntervencionAdminFormFilter();
  }


  public function executeResultDeterioros(sfWebRequest $request){
        
    $form= $request->getParameter('result');
    $cambio=array('museo'=> $form['cambio']); 
    if($form['cambio'] == 'todo'){
        $cambio=array('museo'=>'hombre_chaquenio'); 
    }


    $this->search = new SearchBuilder($cambio);
    $this->titulos=array();
    $this->tablas= array();
   

   foreach($this->search->getBasesDatos() as $bd){
        $this->titulos[]= $bd['name']; 
   }
   
   
    $estados = Doctrine_Query::create()
          ->from('IntervencionEstado ie')
          ->orderBy('ie.nombre');
         
    
    $fechaD=$form['fecha_de_diagnostico']['from'];
    $fechaH=$form['fecha_de_diagnostico']['to'];
        
        
    $this->result_t= array();
    $this->result_t[]= array('CAUSA', 'CANTIDAD', 'PORCENTAJE');
    $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
    

    $q = Doctrine_Query::create($conn)
            ->from('Intervencion o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q->andWhere('o.fecha_de_diagnostico >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q->andWhere('o.fecha_de_diagnostico <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      } 
                  $q->fetchArray();
                 
    $total= $q->count();


    foreach($estados->execute() as $estado){
        
        $result= array();
        $result[]=$estado->getNombre();
               
               
        $q = Doctrine_Query::create($conn)
            ->from('Intervencion o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q->andWhere('o.fecha_de_diagnostico >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q->andWhere('o.fecha_de_diagnostico <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      } 
                  $q->innerJoin('o.IntervencionDescripcionEstado ide')
                    ->andWhere('ide.intervencion_estado_id = ?', $estado->getId())    
                    ->fetchArray();
                 
        $result[]= $q->count();
        if($total > 0 ){
            $result[] = (round(( $q->count() / $total), 2)); 
        }else{
            $result[] = $q->count();
        }
        $this->result_t[]= $result;
       }
       $this->result_t[]= array( "TOTAL", $total, "");
       $filtro= array();
       $filtro[]='Desde: '.$this->fechaD= $fechaD['day'].'/'.$fechaD['month'].'/'.$fechaD['year'];
       $filtro[]='Hasta: '.$this->fechaH= $fechaH['day'].'/'.$fechaH['month'].'/'.$fechaH['year'];
      
                   $this->getUser()->setAttribute('tabla_visita', array($this->result_t));
                   $this->getUser()->setAttribute('titulos_visita', array($bd['name']) );
                   $this->getUser()->setAttribute('filtros_visita', $filtro);
                   $this->getUser()->setAttribute('archivo', $bd['name'].' deterioros');
    }

  public function executeEstados(sfWebRequest $request)
  {
    $this->form= new IntervencionAdminFormFilter();
  }


public function executeResultEstados(sfWebRequest $request){
        
    $form= $request->getParameter('result');
    $cambio=array('museo'=> $form['cambio']);   

    $this->search = new SearchBuilder($cambio);
    $this->titulos=array("ESTADOS" );
    $this->todos= array();       
    $tipos    =array("estados");
    
        $pie = array("TOTAL", 0, " " ,0," ", 0 , " " , 0);
        $TOTAL= 0;
        $this->result_t= array();
        $this->result_t[]= array(' USUARIOS ', ' BUENO ', ' % BUENO ' , ' REGULAR ', ' % REGULAR ' , ' MALO ', '% MALO ' ,'NO DEFINIDO' , ' TOTAL ');

    
        foreach($this->search->getBasesDatos() as $bd)
        {
            $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);               
            
            $result= array(); 
            $result[]=$bd['head']; 
            $total=0;

            $q= Doctrine_Query::create($conn)->from('Objeto o')
                ->andWhere('o.fecha_baja is null ')
                ->innerJoin('o.Estado e');

            $q2= Doctrine_Query::create($conn)->from('Objeto o')
                ->andWhere('o.fecha_baja is null ');
 
            $bueno=0;
            $malo=0;
            $regular=0;
            $indefinidos= ( $q2->count() - $q->count() );
            
            foreach ($q->execute() as $key => $objeto) 
            {    
                    switch ( strtoupper( $objeto->getEstadoActual() ) ) {
                        case "BUENO" :
                            $bueno=$bueno+1;
                        break;
                        case "MALO" :
                            $malo=$malo+1;
                        break;
                        case "REGULAR" :
                            $regular=$regular+1;
                        break;      
                        default :
                            $indefinidos =  $indefinidos +1;
                        break;      
                    }
                
            } 

            $total= $bueno + $malo +$regular + $indefinidos;
            if($total > 0 ){
                $result[]= $bueno;
                $result[]=  round((($bueno / $total) * 100 ),2) ;
                $result[]= $regular;
                $result[]= round((($regular / $total) * 100),2);
                $result[]= $malo;
                $result[]= round((($malo / $total)* 100),2);
                $result[]= $indefinidos;
            }else{
                $result[]= $bueno;
                $result[]= $bueno;
                $result[]= $regular;
                $result[]= $regular;
                $result[]= $malo;
                $result[]= $malo;
                $result[]= $indefinidos;
            }
            

            $pie[ 1 ]=$pie[ 1 ] + $bueno;
            $pie[ 3 ]=$pie[ 3 ] + $regular;
            $pie[ 5 ]=$pie[ 5 ] + $malo;
            $pie[ 7 ]=$pie[ 7 ] + $indefinidos;
            $TOTAL=$TOTAL +$total;  
        
            $result[]= $total;                       
            $this->result_t[]= $result;
        }

       $pie[]= $TOTAL;
       $this->result_t[]= $pie;
       $this->todos[] = $this->result_t;
    


     $filtro= array();
     $filtro[0]= '';
     $filtro[1]= '';        
                   $this->getUser()->setAttribute('tabla_visita', $this->todos);
                   $this->getUser()->setAttribute('titulos_visita', $this->titulos );
                   $this->getUser()->setAttribute('filtros_visita', $filtro);
                   $this->getUser()->setAttribute('archivo', 'Estados');
    }

}
