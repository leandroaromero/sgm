<?php

/**
 * Intervencion filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class IntervencionAdminFormFilter extends BaseIntervencionFormFilter
{
  public function configure()
  {

    $format = '%day%/%month%/%year%';
    $web = sfConfig::get('app_url_web');
    $rango = range(2012, 2030);
    $arreglo_rango = array_combine($rango, $rango);
    $this->widgetSchema['fecha']= new sfWidgetFormJQueryDate(array(
      'label' => 'Creado',             
      'culture' => 'es',
      'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                  'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                  buttonText: ['Calendario']}",
                  'date_widget' => new sfWidgetFormDate(array(
                       'format' => '%year%',
                        'years' => $arreglo_rango,
             

            )), ));
        $format = '%day%/%month%/%year%';
        $web = sfConfig::get('app_url_web');
        $rango = range(2012, 2025);
        $arreglo_rango = array_combine($rango, $rango);
        
        $this->widgetSchema['fecha_de_diagnostico']= new sfWidgetFormFilterDate(array(
          'from_date' => new sfWidgetFormJQueryDate(array(
             'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))

            )), 'to_date' => new sfWidgetFormJQueryDate(array(
                 'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))

            )), 'with_empty' => false));



    $parameter = sfConfig::get('app_institucion_data', array(''=>'') );
    $choices    = array('todo' => 'Todas') + $parameter; 
    $this->widgetSchema['cambio'] = new sfWidgetFormChoice(
    array(
        'choices'   => $choices,
        'multiple'  => false,
        'label'     => 'Instituciones',
        'expanded'  => false,
        ),
      array()
    );

    $this->widgetSchema->setNameFormat('result[%s]'); 
  }
}
