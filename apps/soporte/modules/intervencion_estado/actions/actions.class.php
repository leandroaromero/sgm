<?php

require_once dirname(__FILE__).'/../lib/intervencion_estadoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/intervencion_estadoGeneratorHelper.class.php';

/**
 * intervencion_estado actions.
 *
 * @package    museo
 * @subpackage intervencion_estado
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class intervencion_estadoActions extends autoIntervencion_estadoActions
{
}
