<?php

require_once dirname(__FILE__).'/../lib/etapasGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/etapasGeneratorHelper.class.php';

/**
 * pasos actions.
 *
 * @package    museo
 * @subpackage pasos
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class etapasActions extends autoEtapasActions
{

    public function executeNew(sfWebRequest $request)
    {
        $this->form = $this->configuration->getForm();
        $this->intervencion = $this->form->getObject();
        if($objetos =    Doctrine::getTable('objeto')->find($request->getParameter('objeto_id'))){ 
            $this->intervencion->setObjeto($objetos);
            $this->form = new IntervencionAdminForm($this->intervencion);
        }

    }


    public function executeAddImaForm($request)
    {
        $this->forward404unless($request->isXmlHttpRequest());
        $number = intval($request->getParameter("num"));

        if($card =    Doctrine::getTable('Intervencion')->find($request->getParameter('id'))){ 
             $form = new IntervencionAdminForm($card);
            }else{
             $form = new IntervencionAdminForm(null);
         }

        $form->addImagenes($number);
        return $this->renderPartial('addIma',array('form' => $form, 'num' => $number));
    }    
        

    public function executeAddDetallesForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num"));

         if($card =    Doctrine::getTable('Intervencion')->find($request->getParameter('id'))){ 
             $form = new IntervencionAdminForm($card);
             }else{
             $form = new IntervencionAdminForm(null);
         }

         $form->addDetalles($number);
         return $this->renderPartial('addDetalles',array('form' => $form, 'num' => $number));
    }    
        

    public function executeAddRecomendacionesForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num"));

         if($card =    Doctrine::getTable('Intervencion')->find($request->getParameter('id'))){ 
             $form = new IntervencionAdminForm($card);
             }else{
             $form = new IntervencionAdminForm(null);
         }

         $form->addRecomendaciones($number);
         return $this->renderPartial('addRecomendaciones',array('form' => $form, 'num' => $number));
    }    
        

    public function executeAddPropuestasForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num"));

         if($card =    Doctrine::getTable('Intervencion')->find($request->getParameter('id'))){ 
             $form = new IntervencionAdminForm($card);
             }else{
             $form = new IntervencionAdminForm(null);
         }

         $form->addPropuestas($number);
         return $this->renderPartial('addPropuestas',array('form' => $form, 'num' => $number));
    }    
        
  
      public function executeListExport(sfWebRequest $request){
        
            $intervencion =    Doctrine::getTable('Intervencion')->find($request->getParameter('id'));                           
            $objeto = $intervencion->getObjeto();
            $fecha  = "Emitido: ". date("d-m-Y");   ;
            $institution= $this->getUser()->getInstitutionName();
            $subtitle   = $this->getUser()->getInstitutionSubName();
            $file       = $this->getUser()->getDataBase();
                
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'Legal', true, 'UTF-8', false);
                      // settings
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Administrador');
            
            $pdf->SetTitle($institution);
            $pdf->SetSubject('Imforme de visitas');
            $pdf->SetKeywords('Informe de visitas, visitas, patrimonio, sistema, reporte');
                    
            if ( strlen ($subtitle) > 2 ){
                $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, "$subtitle \nInforme del objeto \nficha de conservación");
            }else{
                $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, "Informe del objeto \n$ficha de conservación");
                } 
                    // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                      // init pdf doc
            $pdf->AddPage();
            $pdf->SetFont('helvetica', '', 8);
       
            $pdf->writeHTML($fecha, true, false, false, false, '');

                   
            $formatos_pdf =  array('pdf','x-pdf');
            $pdf->setFormDefaultProp(array('lineWidth'=>1, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));

            $pdf->SetFont('helvetica', 'B', 18);
            $pdf->Cell(0, 5, $objeto->getNombreObjeto(), 0, 1, 'C');
            $pdf->Ln(10);


            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $web = sfConfig::get('sf_upload_dir').'/'.$file.'/fotos/'.$objeto->getFoto()->getArchivo();
            $pdf->Image( "$web", '', '', 50, 50, '', '', 'T', false, 300, 'C', false, false, 1, false, false, false);
            
            $pdf->Ln(55);


            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Nombre:');
            $pdf->SetFont('helvetica', '', 12);
            $pdf->Cell(50, 5, $objeto->getNombreObjeto());
            $pdf->Ln(10);
            
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Tipo de objeto:');
            $pdf->SetFont('helvetica', '', 12);
            $pdf->Cell(50, 5, $objeto->getTipoObjeto());
            $pdf->Ln(10);

            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Autor:');
            $pdf->SetFont('helvetica', '', 12);
            $pdf->Cell(50, 5, $objeto->getAutor());
            $pdf->Ln(10);

            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Año de origen:');
            $pdf->SetFont('helvetica', '', 12);
            $pdf->Cell(50, 5, $objeto->getAnioOrigen());
            $pdf->Ln(10);

            // CATEGORIAS
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Categorias: ');
            $pdf->SetFont('helvetica', '', 10);
            foreach($objeto->getCategoriaObjeto() as $ca):
                $pdf->Cell(50, 5, $ca->getCategoria());
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
            endforeach;
            $pdf->Ln(10);

            //MATERIALES
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Materiales:');             
            $pdf->SetFont('helvetica', '', 10);
            foreach($objeto->getObjetoMaterial() as $ma):
                $pdf->Cell(50, 5, $ma->getMaterial());
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
            endforeach;
            $pdf->Ln(14);

            //Obra de arte
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Obra de Arte:');
            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->Cell(40, 5, 'Lenguaje');
            $pdf->Cell(40, 5, 'Descripción');
            $pdf->Cell(40, 5, 'Detalle');    
            $pdf->Cell(40, 5, 'Valor');             
            $pdf->SetFont('helvetica', '', 10);
            foreach($objeto->getRegistroDetalleObra() as $de):
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->Cell(40, 5, $de->getDetalleObra()->getObraDeArte());
                $pdf->Cell(40, 5, $de->getDetalleObra()->getAspectoObra());
                $pdf->Cell(40, 5, $de->getDetalleObra());
                $pdf->Cell(40, 5, $de->getValor());
            endforeach;
            $pdf->Ln(20);

            //Conservacion
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Conservación:');     
            foreach($objeto->getEstado() as $es):
                $pdf->Ln(6);
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(40, 5, '');
                $pdf->Cell(40, 5, 'Estado');                
                $pdf->SetFont('helvetica', '', 10);
                $pdf->Cell(40, 5, $es->getEstadoDeConservacion());
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(40, 5, 'Fecha');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->Cell(40, 5, $es->getFechaDeCambio());
                $pdf->Ln(5); 
                $pdf->Cell(40, 5, '');
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(40, 5, 'Motivo del cambio');   
                $pdf->Ln(5);                             
                $pdf->Cell(40, 5, '');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->writeHTMLCell(0, 0, '', '', $es->getMotivoDelCambio(), 0, 1, false, true, 'J', false);
            endforeach;
            $pdf->Ln(20);

            //Dimensiones
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Dimensiones:');
            $pdf->Cell(45, 5, 'Dimensión');
            $pdf->Cell(40, 5, 'Valor');
            $pdf->Cell(40, 5, 'Unidad');        
            $pdf->SetFont('helvetica', '', 10);
            foreach ($objeto->getObjetoDimension() as $dim):
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->Cell(45, 5, $dim->getDimension());
                $pdf->Cell(40, 5, $dim->getValor());
                $pdf->Cell(40, 5, $dim->getUnidad());
            endforeach;
            $pdf->Ln(20);


            $pdf->AddPage();
            //Intervenciones
            $pdf->SetFont('helvetica', 'B', 18);
            $pdf->Cell(0, 5, 'Intervencion', 0, 1, 'L');
            $pdf->Ln(10);


            $pdf->SetFont('helvetica', 'B', 14);
            $pdf->Cell(0, 5, 'Diagnóstico', 0, 1, 'L');
            $pdf->Ln(10);


            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Estado:');
            $pdf->SetFont('helvetica', '', 12);
            $pdf->Cell(50, 5, $intervencion->getEstadoGeneral());
            $pdf->Ln(10);


            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Fecha:');
            $pdf->SetFont('helvetica', '', 12);
            $pdf->Cell(50, 5, $intervencion->getFechaDeDiagnostico());
            $pdf->Ln(10);

            // DETERIODOS
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Deterioros: ');
            $pdf->SetFont('helvetica', '', 10);
            foreach($intervencion->getIntervencionDescripcionEstado() as $ide):
                $pdf->Cell(50, 5, $ide->getIntervencionEstado()->getNombre());
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
            endforeach;
            $pdf->Ln(14);

            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Observaciones:');
            $pdf->SetFont('helvetica', '', 12);
            $pdf->writeHTMLCell(0, 0, '', '', $intervencion->getObservacionesEstado(), 0, 1, false, true, 'J', false);
            $pdf->Ln(10);



            //IMAGENES DEL DIAGNOSTICO
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Imagenes: ');
            $pdf->SetFont('helvetica', '', 10);
            foreach($intervencion->getImagenesDiagnostico() as $id):  

                $extension = pathinfo($id->getArchivo(), PATHINFO_EXTENSION);            
                if(in_array($extension, $formatos_pdf) ) {
                    $archivo = sfConfig::get('sf_web_dir').'/images/pdf.png';                
                }else{
                    $archivo = sfConfig::get('sf_upload_dir').'/soporte/'.$file.'/'.$id->getArchivo();    
                }
                
                $pdf->Image( "$archivo", '', '', 50, 50, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
                $pdf->writeHTMLCell(0, 0, '', '', $id->getDescripcion(), 0, 1, false, true, 'J', false);
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->Ln(45);                
                $pdf->Cell(40, 5, '');
            endforeach;
            $pdf->Ln(14);



            $pdf->AddPage();
            //PROPUESTAS
            $pdf->SetFont('helvetica', 'B', 18);
            $pdf->Cell(0, 5, 'Propuestas', 0, 1, 'L');
            $pdf->Ln(10);

            //PROPUESTAS
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Propuesta:');
            
            foreach($intervencion->getIntervencionPropuesta()  as $ip):
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(50, 5, 'Acciones: ');
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->Cell(50, 5, $ip->getIntervencionTipo());
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(50, 5, 'Responsables: ');
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->Cell(50, 5, $ip->getPersona());                
                $pdf->Ln(4);
            endforeach;
            $pdf->Ln(20);

            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Observaciones:');
            $pdf->SetFont('helvetica', '', 12);
            $pdf->writeHTMLCell(0, 0, '', '', $intervencion->getObservacionesPropuestaIntervencion(), 0, 1, false, true, 'J', false);
            $pdf->Ln(10);

            //IMAGENES DEL DETERIORO
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Imagenes: ');
            $pdf->SetFont('helvetica', '', 10);
            foreach($intervencion->getImagenesDeterioros() as $id):

                $extension = pathinfo($id->getArchivo(), PATHINFO_EXTENSION);            
                if(in_array($extension, $formatos_pdf) ) {
                    $archivo = sfConfig::get('sf_web_dir').'/images/pdf.png';                
                }else{
                    $archivo = sfConfig::get('sf_upload_dir').'/soporte/'.$file.'/'.$id->getArchivo();    
                }
                
                $pdf->Image( "$archivo", '', '', 50, 50, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
                $pdf->writeHTMLCell(0, 0, '', '', $id->getDescripcion(), 0, 1, false, true, 'J', false);
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->Ln(45);                      
                $pdf->Cell(40, 5, '');
            endforeach;
            $pdf->Ln(14);


            $pdf->AddPage();
            //INTERVENCIONES
            $pdf->SetFont('helvetica', 'B', 18);
            $pdf->Cell(0, 5, 'Intervenciones', 0, 1, 'L');
            $pdf->Ln(10);
             
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Intervenciones:');
            foreach($intervencion->getIntervencionDetalle() as $d):
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');                            
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(100, 5, 'Tipos: ');
                $pdf->Cell(40, 5, 'Fecha: ');
                $pdf->Ln(6);                                
                $pdf->Cell(40, 5, '');      
                $pdf->SetFont('helvetica', '', 10); 
                $pdf->Cell(100, 5, $d->getIntervencionTipo());
                $pdf->Cell(40, 5, $d->getFecha());
                $pdf->Ln(6);                 
                $pdf->Cell(40, 5, '');                    
                $pdf->SetFont('helvetica', 'B', 10); 
                $pdf->Cell(40, 5, 'Responsables: ');
                $pdf->Ln(6);
                $pdf->SetFont('helvetica', '', 10);
                $pdf->Cell(40, 5, '');
                $pdf->Cell(40, 5, $d->getPersona());                
                $pdf->Ln(6);
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(40, 5, '');
                $pdf->Cell(40, 5, 'Observaciones: ');
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->writeHTMLCell(0, 0, '', '', $d->getObservacionesIntervencionRealizada(), 0, 1, false, true, 'J', false);
                $pdf->Ln(10);
            endforeach;
            $pdf->Ln(4);

            //IMAGENES DE INTERVENCION
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Imagenes: ');
            $pdf->SetFont('helvetica', '', 10);
            foreach($intervencion->getImagenesIntervencion() as $id):

                $extension = pathinfo($id->getArchivo(), PATHINFO_EXTENSION);            
                if(in_array($extension, $formatos_pdf) ) {
                    $archivo = sfConfig::get('sf_web_dir').'/images/pdf.png';                
                }else{
                    $archivo = sfConfig::get('sf_upload_dir').'/soporte/'.$file.'/'.$id->getArchivo();    
                }
                
                $pdf->Image( "$archivo", '', '', 50, 50, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
                $pdf->writeHTMLCell(0, 0, '', '', $id->getDescripcion(), 0, 1, false, true, 'J', false);
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->Ln(45);                      
                $pdf->Cell(40, 5, '');
            endforeach;
            $pdf->Ln(14);


            $pdf->AddPage();
            //RECOMENDACIONES
            $pdf->SetFont('helvetica', 'B', 18);
            $pdf->Cell(0, 5, 'Recomendaciones', 0, 1, 'L');
            $pdf->Ln(10);

            //RECOMENDACIONES
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Recomendacion:');
            
            foreach($intervencion->getIntervencionTieneRecomendacion()  as $itr):
                $pdf->Ln(6);
                $pdf->Cell(40, 5, '');
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(40, 5, 'Acciones');
                $pdf->Ln(5);
                $pdf->Cell(40, 5, '');             
                $pdf->SetFont('helvetica', '', 10);
                $pdf->Cell(40, 5, $itr->getIntervencionRecomendacion());
                $pdf->Ln(5);
                $pdf->Cell(40, 5, '');
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(40, 5, 'Responsables');
                $pdf->Ln(5);
                $pdf->Cell(40, 5, '');                             
                $pdf->SetFont('helvetica', '', 10);
                $pdf->Cell(40, 5, $itr->getPersona());   
                $pdf->Ln(5);                
            endforeach;
            $pdf->Ln(20);

            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->Cell(40, 5, 'Observaciones:');
            $pdf->SetFont('helvetica', '', 12);
            $pdf->writeHTMLCell(0, 0, '', '', $intervencion->getObservacionesRecomendaciones(), 0, 1, false, true, 'J', false);
            $pdf->Ln(10);
            $pdf->Output('Reporte.pdf', 'I');
     
        }
    

}
