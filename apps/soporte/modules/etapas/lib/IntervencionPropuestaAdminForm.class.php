<?php

/**
 * IntervencionPropuesta form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class IntervencionPropuestaAdminForm extends BaseIntervencionPropuestaForm
{
  public function configure()
  {
    unset($this['intervencion_id'],$this['updated_at'],$this['created_at'],$this['updated_by'],$this['created_by']);

    $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
    $this->validatorSchema['delete'] = new sfValidatorPass();
    
    $this->setTitulos();

  }


  protected function setTitulos()
  {
    $this->widgetSchema['intervencion_tipo_id']->setAttributes(array('title' =>
        'Debe seleccionar una acción a realizar sobre el objeto, como propuesta'));

    $this->widgetSchema['persona_id']->setAttributes(array('title' =>'Seleccionar el responsable de la propuesta'));

  }

}
