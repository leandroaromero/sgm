<?php

/**
 * Intervencion form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class IntervencionAdminForm extends BaseIntervencionForm
{
  protected $scheduledForDeletionImagenes = array();
  protected $scheduledForDeletionDetalles = array();
  protected $scheduledForDeletionRecomendaciones = array();
  protected $scheduledForDeletionPropuestas = array();

  public function configure()
  {
    //campos ocultos
     unset($this['updated_at'],$this['created_at'],$this['updated_by'],$this['created_by']);

    //imagenes
    $web = sfConfig::get('app_url_web');  

    //tratamiento de fechas 
    $rango = range(1800, 2040);
    $arreglo_rango = array_combine($rango, $rango);  
    $this->widgetSchema['fecha_de_diagnostico'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           'years' => $arreglo_rango,
           ))
    ));

    //ESTADO GENERAL DEL ACERVO
    $this->widgetSchema['estado_general'] = new sfWidgetFormChoice(array('choices' => array(
      'BUENO' => 'BUENO','REGULAR'=> 'REGULAR', 'MALO' => 'MALO')));

    //tratamiento de doble lista      


    $this->widgetSchema['intervencion_estado_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
    $this->widgetSchema['intervencion_estado_list']->setOption('label', 'Deterioros observados');
    $this->widgetSchema['intervencion_estado_list']->setOption('table_method', 'getOrderNombre');
    $this->widgetSchema['intervencion_estado_list']->setOption('renderer_options', array(
                    'label_unassociated' => 'No contiene:',
                    'label_associated'   => 'Si contiene:',
                ));

    $this->widgetSchema['persona_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
    $this->widgetSchema['persona_list']->setOption('label', 'Personas');      
  #  $this->widgetSchema['persona_list']->setOption('table_method', 'getOrderNombre');
    $this->widgetSchema['persona_list']->setOption('renderer_options', array(
                    'label_unassociated' => 'No involucrado:',
                    'label_associated' => 'Si involucrado:',
                ));



      //tratamiento del objeto a ser tratado
      sfProjectConfiguration::getActive()->loadHelpers('Url');
      $this->widgetSchema['objeto_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['objeto_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
          array(
            'model' => "Objeto",
            'url'   => url_for("@ajax_objeto"),
            'config' => '{ max: 30}'
            ));
   
    
    
    //Empotramos al menos un formulario de Imagenes
            $imagenes = $this->getObject()->getIntervencionImagen();
            if (!$this->getObject()->getIntervencionImagen()->count()){
                  $imagen = new IntervencionImagen();
                  $imagen->setIntervencion($this->getObject());
                  $imagenes = array($imagen);
            }

            //Un formulario vacío hará de contenedor para todas las imagenes
            $imagenes_forms = new SfForm();
            $count = 0;
            foreach ($imagenes as $imagen) {
                    $ima_form = new IntervencionImagenAdminForm($imagen);
                
                    //Empotramos cada formulario en el contenedor
                    $imagenes_forms->embedForm($count, $ima_form);
                    $count ++;
            }
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('lista_de_imagenes', $imagenes_forms);




            $detalles = $this->getObject()->getIntervencionDetalle();
            if (!$this->getObject()->getIntervencionDetalle()->count()){
                  $detalle = new IntervencionDetalle();
                  $detalle->setIntervencion($this->getObject());                  
                  $detalles = array($detalle);
            }

            //Un formulario vacío hará de contenedor para todas las imagenes
            $detalles_forms = new SfForm();
            $count = 0;            
            foreach ($detalles as $detalle) {

              $detalle_form = new IntervencionDetalleAdminForm($detalle);                
              //Empotramos cada formulario en el contenedor
              $detalles_forms->embedForm($count, $detalle_form);
              $count++;
            }
            
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('intervencion_detalles', $detalles_forms);





            $recomendaciones = $this->getObject()->getIntervencionTieneRecomendacion();
            if (!$this->getObject()->getIntervencionTieneRecomendacion()->count()){
                  $recomendacion = new IntervencionTieneRecomendacion();
                  $recomendacion->setIntervencion($this->getObject());                  
                  $recomendaciones = array($recomendacion);
            }

            //Un formulario vacío hará de contenedor para todas las imagenes
            $recomendaciones_forms = new SfForm();
            $count = 0;            
            foreach ($recomendaciones as $recomendacion) {

              $recomendacion_form = new IntervencionTieneRecomendacionAdminForm($recomendacion);                
              //Empotramos cada formulario en el contenedor
              $recomendaciones_forms->embedForm($count, $recomendacion_form);
              $count++;
            }
            
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('recomendaciones', $recomendaciones_forms);





            $propuestas = $this->getObject()->getIntervencionPropuesta();
            if (!$this->getObject()->getIntervencionPropuesta()->count()){
                  $propuesta = new IntervencionPropuesta();
                  $propuesta->setIntervencion($this->getObject());                  
                  $propuestas = array($propuesta);
            }

            //Un formulario vacío hará de contenedor para todas las imagenes
            $propuestas_forms = new SfForm();
            $count = 0;            
            foreach ($propuestas as $propuesta) {

              $propuesta_form = new IntervencionPropuestaAdminForm($propuesta);                
              //Empotramos cada formulario en el contenedor
              $propuestas_forms->embedForm($count, $propuesta_form);
              $count++;
            }
            
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('propuestas', $propuestas_forms);

    //-----------------------------------------------------------------------------

    $this->setTitulos();

  }


  protected function setTitulos()
  {  

    $this->widgetSchema['objeto_id']->setAttributes(array('title' =>
        'Debe seleccionar un objeto de la lista que se despliega a medida que va ingresando el nombre o id del objeto'));
    $this->widgetSchema['estado_general']->setAttributes(array('title' =>
        'Indicar el estado del acervo en la etapa de diagnóstico'));
    $this->widgetSchema['observaciones_estado']->setAttributes(array('title' =>
        'Detallar las observaciones del diagnóstico'));

    //   PROPUESTA    
    $this->widgetSchema['observaciones_propuesta_intervencion']->setAttributes(array('title' =>'Detallar las propuestas realizadas'));

  }

  public function addDetalles($num) 
     {
          $detalle = new IntervencionDetalle();
          
          $detalle->setIntervencion($this->getObject());
          
          $detalle_form = new IntervencionDetalleAdminForm($detalle);

          unset($detalle_form['intervencion_id']);
          //Empotramos
          $this->embeddedForms['intervencion_detalles']->embedForm($num, $detalle_form);
          //Volvemos a empotrar el contenedor
          $this->embedForm('intervencion_detalles', $this->embeddedForms['intervencion_detalles']);

      }


    public function addImagenes($num) 
     {

        $ima = new IntervencionImagen();
          
        $ima->setIntervencion($this->getObject());
          
        $ima_form = new IntervencionImagenAdminForm($ima);

        unset($ima_form['intervencion_id']);
          //Empotramos la nueva pícture en el contenedor
        $this->embeddedForms['lista_de_imagenes']->embedForm($num, $ima_form);
          //Volvemos a empotrar el contenedor
        $this->embedForm('lista_de_imagenes', $this->embeddedForms['lista_de_imagenes']);

    }


    public function addRecomendaciones($num) 
     {

        $recomendacion = new IntervencionTieneRecomendacion();
          
        $recomendacion->setIntervencion($this->getObject());
          
        $recomendacion_form = new IntervencionTieneRecomendacionAdminForm($recomendacion);

        unset($recomendacion_form['intervencion_id']);
          
        $this->embeddedForms['recomendaciones']->embedForm($num, $recomendacion_form);          
        $this->embedForm('recomendaciones', $this->embeddedForms['recomendaciones']);

    }


    public function addPropuestas($num) 
     {

        $propuesta = new IntervencionPropuesta();
          
        $propuesta->setIntervencion($this->getObject());
          
        $propuesta_form = new IntervencionPropuestaAdminForm($propuesta);

        unset($propuesta_form['intervencion_id']);
          
        $this->embeddedForms['propuestas']->embedForm($num, $propuesta_form);          
        $this->embedForm('propuestas', $this->embeddedForms['propuestas']);

    }



    public function bind(array $taintedValues = null, array $taintedFiles = null)
    {
          
          foreach($taintedValues['lista_de_imagenes'] as $key=>$emailValues)
          {
            if (!isset($this['lista_de_imagenes'][$key]) )
            {
          
              $this->addImagenes($key);
          
            }
          }

          foreach($taintedValues['intervencion_detalles'] as $key=>$emailValues)
          {
            if (!isset($this['intervencion_detalles'][$key]) )
            {
          
              $this->addDetalles($key);
          
            }
          }
         
          foreach($taintedValues['recomendaciones'] as $key=>$emailValues)
          {
            if (!isset($this['recomendaciones'][$key]) )
            {
          
              $this->addRecomendaciones($key);
          
            }
          }
         
          foreach($taintedValues['propuestas'] as $key=>$emailValues)
          {
            if (!isset($this['propuestas'][$key]) )
            {
          
              $this->addPropuestas($key);
          
            }
          }

          parent::bind($taintedValues, $taintedFiles);
        
    }
  
    protected function doBind(array $values)
    {

        foreach ($values['lista_de_imagenes'] as $i => $emailValues)
          {
             if ('' === trim($emailValues['archivo']) &&'' === trim($emailValues['descripcion']) )
                {
                  unset($values['lista_de_imagenes'][$i]);
                }
            if (isset($emailValues['delete']) && $emailValues['id'])
            {
              $this->scheduledForDeletionImagenes[$i] = $emailValues['id'];
            }
          }

          foreach ($values['intervencion_detalles'] as $i => $emailValues)
          {
             if ('' === trim($emailValues['fecha']) )
                {
                  unset($values['intervencion_detalles'][$i]);
                }
            if (isset($emailValues['delete']) && $emailValues['id'])
            {
              $this->scheduledForDeletionDetalles[$i] = $emailValues['id'];
            }
          }


        foreach ($values['recomendaciones'] as $i => $emailValues)
          {
             if ('' === trim($emailValues['intervencion_recomendacion_id']) &&'' === trim($emailValues['persona_id']) )
                {
                  unset($values['recomendaciones'][$i]);
                }
            if (isset($emailValues['delete']) && $emailValues['id'])
            {
              $this->scheduledForDeletionRecomendaciones[$i] = $emailValues['id'];
            }
          }

        foreach ($values['propuestas'] as $i => $emailValues)
          {
             if ('' === trim($emailValues['intervencion_tipo_id']) &&'' === trim($emailValues['persona_id']) )
                {
                  unset($values['propuestas'][$i]);
                }
            if (isset($emailValues['delete']) && $emailValues['id'])
            {
              $this->scheduledForDeletionPropuestas[$i] = $emailValues['id'];
            }
          }

        parent::doBind($values);
    
      }


      protected function doUpdateObject($values)
      {

        if (count($this->scheduledForDeletionImagenes))
        {
          foreach ($this->scheduledForDeletionImagenes as $index => $id)
          {

            unset($values['lista_de_imagenes'][$index]);
            Doctrine::getTable('IntervencionImagen')->findOneById($id)->delete();

          }

        }
        if (count($this->scheduledForDeletionDetalles))
        {
          foreach ($this->scheduledForDeletionDetalles as $index => $id)
          {

            unset($values['intervencion_detalles'][$index]);
            Doctrine::getTable('IntervencionDetalle')->findOneById($id)->delete();

          }

        }


        if (count($this->scheduledForDeletionRecomendaciones))
        {
          foreach ($this->scheduledForDeletionRecomendaciones as $index => $id)
          {

            unset($values['recomendaciones'][$index]);
            Doctrine::getTable('IntervencionTieneRecomendacion')->findOneById($id)->delete();

          }

        }

        if (count($this->scheduledForDeletionPropuestas))
        {
          foreach ($this->scheduledForDeletionPropuestas as $index => $id)
          {

            unset($values['propuestas'][$index]);
            Doctrine::getTable('IntervencionPropuesta')->findOneById($id)->delete();

          }

        }

        $this->getObject()->fromArray($values);

      }
  

}
