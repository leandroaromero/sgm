<?php

/**
 * IntervencionDetalle form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class IntervencionDetalleAdminForm extends BaseIntervencionDetalleForm
{
  public function configure()
  {
    unset($this['intervencion_id'],$this['updated_at'],$this['created_at'],$this['updated_by'],$this['created_by']);

    //imagenes
    $web = sfConfig::get('app_url_web');  

    //tratamiento de fechas 
    $rango = range(2020, 2040);
    $arreglo_rango = array_combine($rango, $rango);  
    $this->widgetSchema['fecha'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           'years' => $arreglo_rango,
           ))
    ));

        $this->validatorSchema['fecha'] = new sfValidatorDate( 
            array(
                'required' => false
                 )       
            );    

        $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
        $this->validatorSchema['delete'] = new sfValidatorPass();            


        $this->setTitulos();    
    }


  protected function setTitulos()
  {
    $this->widgetSchema['intervencion_tipo_id']->setAttributes(array('title' =>
        'Debe seleccionar una acción realizada sobre el objeto'));
    $this->widgetSchema['fecha']->setAttributes(array('title' =>
        'Fecha que se llevo acabo la intervención'));
    $this->widgetSchema['persona_id']->setAttributes(array('title' =>
        'Responsabe que llevo acabo la intervencion'));

    $this->widgetSchema['observaciones_intervencion_realizada']->setAttributes(array('title' =>
        'Detallar de la intervención la accion realizada'));
  }

}
