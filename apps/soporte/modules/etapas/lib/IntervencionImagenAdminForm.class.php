<?php

/**
 * IntervencionImagen form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class IntervencionImagenAdminForm extends BaseIntervencionImagenForm
{
  public function configure()
  {
    unset($this['intervencion_id'],$this['updated_at'],$this['created_at'],$this['updated_by'],$this['created_by']);


      $this->widgetSchema['intervencion_tipo_imagen_id'] = new sfWidgetFormDoctrineChoice(array(
          'model'   => 'IntervencionTipoImagen',
          'method'       => 'getNombre',
          'add_empty' => '>> Seleccione <<',
        ));
      $this->widgetSchema['intervencion_tipo_fotografia_id'] = new sfWidgetFormDoctrineDependentSelect(array(
          'model'   => 'IntervencionTipoFotografia', 
          'depends' => 'IntervencionTipoImagen',
          'method'       => 'getNombre',
            'add_empty' => '>> Seleccione <<',
      ));
      
        // validadores
            
        $this->validatorSchema['intervencion_tipo_imagen_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'IntervencionTipoImagen',            
            'required' => false,
        ));
                
        $this->validatorSchema['intervencion_tipo_fotografia_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'IntervencionTipoFotografia',
            'required' => false,
        ));     

        $dir= sfContext::getInstance()->getUser()->getFile();
        $uploads= sfConfig::get('sf_upload_dir');

        $this->widgetSchema['archivo'] = new sfWidgetFormInputFileEditable(array(
            'label'     => 'Imagen',
            'file_src'  => '/museo/web/uploads/soporte/'.$dir.'/'.$this->getObject()->getArchivo(),
            'is_image'  => true,
            'edit_mode' => true,
            'template'  => '%input%',
        ));

            $this->validatorSchema['archivo'] = new sfValidatorFile(array(
                'required'    => false,
                'mime_types' => array(  'application/pdf',
                                         'application/x-pdf',
                                      
                                      'image/png',
                                      'image/jpg',       
                                      'image/jpeg',
                                      'image/pjpeg',
                                      'image/x-png',
                                      'image/gif',
                                      'image/bmp',
                                      'image/tiff',
                                      'image/x-icon'
                                    ), 

                'path' => $uploads.'/soporte/'.$dir.'/',
           //     'max_size' => '4M' ,
               // 'validated_file_class' => 'sfResizedFile',
            ), array(
              //  'max_size' => 'El tama&ntilde;o m&aacute;ximo es 5 MB',
            ));

              $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
              //$this->widgetSchema['delete']->setAttribute('style', 'width: 35px');
              $this->validatorSchema['delete'] = new sfValidatorPass();
           // Se anula por el momento   
          //    $this->validatorSchema['descripcion']=  new sfValidatorString(array('max_length' => 255, 'required' => true));
              
    $this->setTitulos();

    }


    protected function setTitulos()
    {  

        $this->widgetSchema['intervencion_tipo_imagen_id']->setAttributes(array('title' =>
            '
            DIAGNÓSTICO:
                Se deben poder subir 5 imágenes.
            MAPA DE DETERIOROS:
                Posibilidad de cargar hasta 5 imágenes en JPG o PDF.  
            INTERVENCIÓN REALIZADA:
                Se deben poder subir 5 imágenes como mínimo.
                '));
        $this->widgetSchema['descripcion']->setAttributes(array('title' =>'Describir la imágen con simple palabras'));

    }


  public function doUpdateObject($values)
       {
            

            $fail = $this->getObject()->getArchivo();

            if( $values['archivo'] != $fail ){
                $dir= sfContext::getInstance()->getUser()->getFile(); 
                $uploads= sfConfig::get('sf_upload_dir');

                /*
                $p1 = $uploads.'/'.$dir.'/fotos/big/'.$fail;
                @unlink($p1);
                
                $p2 = $uploads.'/'.$dir.'/fotos/thumbs/'.$fail;
                @unlink($p2);*/
                
            }
   
           return parent::doUpdateObject($values);
        }

}
