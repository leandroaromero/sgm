<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style type="text/css">
    #sf_admin_container .imagenes input {
    min-width: 35px;
}
    #sf_admin_container .lista_de_imagenes-group .content{
        padding-left: 2px;
    } 
</style>
<div class="lista_de_imagenes-group sf_admin_form_row sf_admin_text sf_admin_form_field_estado_general">

 
 <div class="content" style="padding-left: 2em;">
    <table style="width: 100%" class="imagenes">
    <tr>            
        <td>Borrar</td>
        <td></td>
        <td></td>
        <td></td>        
        <td></td>    
        
    </tr>
     <?php foreach ($form['lista_de_imagenes'] as $key=>$r ):?>
       <tr>              
         <td>
            <?php echo $r['id']->render(); ?>
            <?php echo $r['delete']->render(); ?></td>
         <td>
              <?php include_partial('etapas/digital', array( 'key' => $r['id']->getValue())) ?>
         </td>    
         <td> 
            ETAPA: </br>
             <?php echo $r['intervencion_tipo_imagen_id']->renderError() ?>
             <?php echo $r['intervencion_tipo_imagen_id']->render(); ?></br></br>
            FOTOGRAFIA: </br>         
             <?php echo $r['intervencion_tipo_fotografia_id']->renderError() ?>
             <?php echo $r['intervencion_tipo_fotografia_id']->render(); ?></td>
         <td>
            ARCHIVO: </br>
             <?php echo $r['archivo']->renderError() ?>
             <?php echo $r['archivo']->render(); ?></br></br>
            DESCRIPCION* : </br>
             <?php echo $r['descripcion']->renderError() ?>
             <?php echo $r['descripcion']->render(); ?></td>
         
    </tr>    
    


    
 <?php endforeach;?>

    <tr><td></td><td></td></tr>

<tr>	
    <td colspan="2">
         <center>
              <button id="add_imagenes" type="button"><?php echo "Añadir otra imagen"?></button>
         </center> 	
    </td>
</tr>

</table>
     
   <tr>    
        <div id="extra_imagenes"></div>    
    </tr>
  
    </div>
</div>
<div>
    <div>
        <p>
            <b>Etapa de diagnóstico: </b>
            Tipos de fotografías que se pueden cargar
            <ul>
                <li>Plano general</li>
                <li>Detalles de deterioros</li>
                <li>Opcional: luz rasante, luz transmitida, luz UV, microfotografía,macrofotografía.</li>
            </ul>


        </p>
        <p>Se deben poder subir 5 imágenes como mínimo en buena calidad: formato TIFF, 300 DPI. Se debe poner descripcion a las imágenes cargadas, por ejemplo: <i> hongos iluminados con luz UV; detalle del desprendimiento de capa pictórica, etc.1.4. </i>
        </p>

        <p>
            <b>Mapa de deterioros: </b>
            Pueden ser
            <ul>
                <li>Fotografía</li>
                <li>Dibujo</li>
            </ul>
        </p>
        <p> Se podrá cargar hasta 5 imágenes en JPG o PDF. A partir de una fotografía o un dibujo del objeto, se debe indicar con colores y/o grafismos la ubicación y extensión de los deterioros. Se debe incluir en la imagen las referencias, es decir, qué color/grafismo corresponde a cada tipo de deterioro. Según el tipo de objeto y deterioro puede ser necesario más de una mapa.
        </p>

        <p>
            <b>Intervención realizada: </b>
            <ul>
                <li>Procesos (detalles)</li>
                <li>Resultados (detalles y plano general)</li>
            </ul>
        </p>    
        <p>Se deben poder subir 5 imágenes como mínimo, en buena calidad: formato TIFF, 300 DPI. Debe ser posible poner título a las imágenes cargadas, por ejemplo: <i>proceso de limpieza con x producto, retoque pictórico, etc.</i>
        </p>

    </div>    
</div>