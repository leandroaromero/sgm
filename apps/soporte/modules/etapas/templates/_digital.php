 <?php 
      if($key < 1):
        echo image_tag('imagenDisponible.jpg');
      else:
        $uploads= sfConfig::get('sf_upload_dir');
          $q = Doctrine_Query::create()
            ->from('IntervencionImagen od')
            ->where('od.id = ?', $key)
            ->fetchOne();
          
          
          $file= $sf_user->getFile();
          $dir =  (string)  '/museo/web/uploads/soporte/'.$file.'/'.$q->getArchivo(); 
          $formatos_pdf =  array('pdf','x-pdf');
          $formatos_imagen =  array('png','jpg','jpeg','pjpeg','x-png','gif','bmp','x-icon');
          $formatos_tiff =  array('tiff','x-icon', 'tif');
          
          $extension = pathinfo($dir, PATHINFO_EXTENSION);
      ?>      

        <a href='<?php echo $dir ?>' target="_blank">    
            
            <?php if(in_array($extension, $formatos_pdf) ): ?>
                <embed src="<?php echo $dir?>" type="application/pdf" width="200" height="200"></embed>
                
            <?php endif; ?>

            <?php if(in_array($extension, $formatos_imagen) ) :
                echo image_tag('../uploads/soporte/'.$file.'/'.$q->getArchivo() , 'size=300x300' );
              endif; ?>

            
            <?php if(in_array($extension, $formatos_tiff) ) :?>
              <embed width=400 height=400  src="<?php echo $dir?>" type="image/tiff" negative="true" >
            <?php  endif; ?>  
          <p>Ver</p>  
        </a>    

<?php endif?>       
        