<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if($form->getObject()->getIntervencionPropuesta()->count()):
?>
<style type="text/css">
    #sf_admin_container .detalles input {
    min-width: 35px;
}

</style>

<div class="intervencion_detalles-group sf_admin_form_row sf_admin_text">

    
 <label class="intervencion_detalles-label" for="member_id"></label>
 <div class="content">
<table style="width: 100%" class="detalles">
    <tr>
        <td>Borrar</td>
        <td><b>Tipo</b></td>
        <td><b>Fecha</b></td>
        <td><b>Responsable</b></td>        
        <td>Observación</td>        
        
        
    </tr>
     <?php foreach ($form['intervencion_detalles'] as $key=>$r ):?>
       <tr>              
         <td>
            <?php echo $r['id']->render(); ?>
            <?php echo $r['delete']->render(); ?></td>
         <td> 
             <?php echo $r['intervencion_tipo_id']->renderError() ?>
             <?php echo $r['intervencion_tipo_id']->render(); ?></td>
         <td>
             <?php echo $r['fecha']->renderError() ?>
             <?php echo $r['fecha']->render(); ?></td>
         <td>
             <?php echo $r['persona_id']->renderError() ?>
             <?php echo $r['persona_id']->render(); ?></td>
         <td>
             <?php echo $r['observaciones_intervencion_realizada']->renderError() ?>
             <?php echo $r['observaciones_intervencion_realizada']->render(); ?></td>
        </tr>    
    

    
 <?php endforeach;?>

    <tr><td></td><td></td></tr>

<tr>	
    <td colspan="2">
         <center>
              <button id="add_detalles" type="button"><?php echo "Añadir otro detalle"?></button>
         </center> 	
    </td>
</tr>


 
    
    
</table>
     
     
	
    <div id="extra_detalles"></div>
    
    

    </div>
  </div>
<?php else: ?>    
    <div id="notificaciones_detalle">
            <p style="color:red">DEBERÁ CARGAR UNA PROPUESTA PRIMERO</p>
    </div>
<?php endif ?>  