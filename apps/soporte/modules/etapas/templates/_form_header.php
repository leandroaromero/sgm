<script type="text/javascript">
var pars = <?php print_r($form['lista_de_imagenes']->count())?>;

function addIma(num) {
  var r = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('etapas/addImaForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num=':'&num=')?>'+num,
    async: false
  }).responseText;
  return r;
}
$().ready(function() {
  $('button#add_imagenes').click(function() {
    $("#extra_imagenes").append(addIma(pars));
    pars = pars + 1;
  });
});
</script>



<script type="text/javascript">
  var detalles = <?php print_r($form['intervencion_detalles']->count())?>;

  function addDetalles(num) {
    var r = $.ajax({
      type: 'GET',
      url: '<?php echo url_for('etapas/addDetallesForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num=':'&num=')?>'+num,
      async: false
    }).responseText;
    return r;
  }
  $().ready(function() {
    $('button#add_detalles').click(function() {
      $("#extra_detalles").append(addDetalles(detalles));
      detalles = detalles + 1;
    });
  });
</script>


<script type="text/javascript">
  var recomendaciones = <?php print_r($form['recomendaciones']->count())?>;

  function addRecomendaciones(num) {
    var r = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('etapas/addRecomendacionesForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num=':'&num=')?>'+num,
    async: false
    }).responseText;
      return r;
  }
  $().ready(function() {
    $('button#add_recomendaciones').click(function() {
      $("#extra_recomendaciones").append(addRecomendaciones(recomendaciones));
      recomendaciones = recomendaciones + 1;
    });
  });
</script>


<script type="text/javascript">
  var propuestas = <?php print_r($form['propuestas']->count())?>;

  function addPropuestas(num) {
    var r = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('etapas/addPropuestasForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num=':'&num=')?>'+num,
    async: false
    }).responseText;
      return r;
  }
  $().ready(function() {
    $('button#add_propuestas').click(function() {
      $("#extra_propuestas").append(addPropuestas(propuestas));
      propuestas = propuestas + 1;
    });
  });
</script>
