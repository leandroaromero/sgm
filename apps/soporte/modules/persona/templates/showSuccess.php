<?php use_stylesheet('main.css'); ?>
<div id="formulario_show">
    <center>

 <table >
  <tbody>
     
    <tr>
      <th>Nombre :</th>
      <td><?php echo $persona->getNombre() ?></td>
    </tr>
    <tr>
      <th>Apellido:</th>
      <td><?php echo $persona->getApellido() ?></td>
    </tr>
    <tr>
      <th>Nro documento:</th>
      <td><?php echo $persona->getNroDocumento() ?></td>
    </tr>
    <tr>
      <th>Dirección:</th>
      <td><?php echo $persona->getDireccion() ?></td>
    </tr>
    <tr>
      <th>País:</th>
      <td><?php echo $persona->getLocalidad()->getProvincia()->getPais() ?></td>
    </tr>
    <tr>
      <th>Provincia:</th>
      <td><?php echo $persona->getLocalidad()->getProvincia()?></td>
    </tr>
    <tr>
      <th>Localidad:</th>
      <td><?php echo $persona->getLocalidad() ?></td>
    </tr>
  </tbody>
</table>
<br/>
DIRECCIONES DE CORREOS ELECTRONICOS: <br/>
<?php if( count ($persona->getEmail()) == 0 ):?>
Esta persona no posee correo registrado<br/>
<?php  else: ?>
<table  class="dentro">
                    <tr>
                        <th class="dentros">Dirrección del correo</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($persona->getEmail() as $ema): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                      <td><?php echo $ema->getDireccionEmail()?></td>
                   </tr>
                <?php endforeach; ?>

              </table>
<?php endif?>
<br/>
TELEFONOS: <br/>
<?php if( count ($persona->getTelefono()) == 0 ):?>
Esta persona no posee telefono registrado<br/>
<?php  else: ?>
<table  class="dentro">
                    <tr>
                        <th class="dentros">Tipo de telefono</th>
                        <th class="dentros">Número</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($persona->getTelefono() as $tel): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                      <td><?php echo $tel->getTipoTelefono()?></td>
                      <td><?php echo $tel->getNumeroTelefono()?></td>
                       </tr>
                <?php endforeach; ?>

</table>
<?php endif ?>
<br/>

    </center>
</div>
<a href="<?php echo url_for('persona/index') ?>">Volver a la lista</a>

