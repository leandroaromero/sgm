<?php use_stylesheet('main.css'); ?>
<div id="formulario_show">
    <center>
 <table >
  <tbody>
      <tr style="background: #F2F2F2">
        <td colspan="4" ><b> DATOS</b>  </td>
    </tr>
    <tr>
      <th>Nombre y apellido:</th>
      <td><?php echo $persona->getNombreYApellido() ?></td>
    </tr>
    <tr>
      <th>Nro documento:</th>
      <td><?php echo $persona->getNroDocumento() ?></td>
    </tr>
    <tr>
      <th>Direccion:</th>
      <td><?php echo $persona->getDireccion() ?></td>
    </tr>
    <tr>
      <th>Localidad:</th>
      <td><?php echo $persona->getLocalidadId() ?></td>
    </tr>
    <tr style="background: #F2F2F2">
        <td colspan="4"><b>TELEFONOS</b> </td>
    </tr>
    <?php foreach( $persona->getTelefono() as $tel ):?>
    <tr>
       <th>tipo:</th>
          <td><?php echo $tel->getTipoTelefono() ?></td>

       <th>Numero:</th>
        <td><?php echo $tel->getNumeroTelefono() ?></td>
    </tr>
    <?php endforeach ?>
      <tr style="background: #F2F2F2">
        <td colspan="4"><b>EMAILS</b> </td>
    </tr>
    <?php foreach( $persona->getEmail() as $email ):?>
    <tr>
       <th>Direccion:</th>
       <td><?php echo $email->getDireccionEmail() ?></td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>

<hr />
</center>
</div>
<a id="volver" href="<?php echo url_for('persona/index') ?>">volver a la lista</a>
