<?php

require_once dirname(__FILE__).'/../lib/personaGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/personaGeneratorHelper.class.php';

/**
 * persona actions.
 *
 * @package    museo
 * @subpackage persona
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class personaActions extends autoPersonaActions
{
	public function executeListShow($request){
		$this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($request->getParameter('id'))), sprintf('Esta persona no existe (%s).', $request->getParameter('id')));
		$this->setTemplate('show');
		}
        
        
        
        public function executeAddAutor($request){
           $id= $request->getParameter('a_id');
           $a= new Autor();
           $a->setId($id);
           $a->save();
           $this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
           $this->form= new PersonaForm($this->persona);
           $this->setTemplate('tipo');
        }
        public function executeDeleteAutor($request){
            $id= $request->getParameter('a_id');
            $autor = Doctrine::getTable('Autor')->find($id);
            $autor->delete();
            //$this->redirect('persona/edit?id='.$id);
            $this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
            $this->form= new PersonaForm($this->persona);
            $this->setTemplate('tipo');
        }
        public function executeAddDonante($request){
           $id= $request->getParameter('d_id');
           $a= new Donante();
           $a->setId($id);
           $a->save();
           //$this->redirect('persona/edit?id='.$id);
            $this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
            $this->form= new PersonaForm($this->persona);
            $this->setTemplate('tipo');
        }
        public function executeDeleteDonante($request){
            $id= $request->getParameter('d_id');
            $don = Doctrine::getTable('Donante')->find($id);
            $don->delete();
          // $this->redirect('persona/edit?id='.$id);
            $this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
            $this->form= new PersonaForm($this->persona);
            $this->setTemplate('tipo');
            
        }

    public function executeGetOrigen($request)
    {
        $this->getResponse()->setContentType('application/json');
        $donacion = Doctrine::getTable('Localidad')->retrieveForSelect(
                    $request->getParameter('q'),
                    $request->getParameter('limit')
        );

        return $this->renderText(json_encode($donacion));
    }
}
