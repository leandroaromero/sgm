<?php

require_once dirname(__FILE__).'/../lib/intervencion_tipo_fotografiaGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/intervencion_tipo_fotografiaGeneratorHelper.class.php';

/**
 * intervencion_tipo_fotografia actions.
 *
 * @package    museo
 * @subpackage intervencion_tipo_fotografia
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class intervencion_tipo_fotografiaActions extends autoIntervencion_tipo_fotografiaActions
{
}
