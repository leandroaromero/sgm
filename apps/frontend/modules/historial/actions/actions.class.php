<?php

/**
 * componentes actions.
 *
 * @package    museo
 * @subpackage componentes
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class historialActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
   public function executeUso(){
        
        $this->form= new ObjetoAdminFormFilter();
    }
    public function executeResult(sfWebRequest $request){
        
      $form= $request->getParameter('result');
      $cambio=array('museo'=>'todo');
      $this->search = new SearchBuilder($cambio);
      $this->titulos=array();
      $this->tablas= array();
      // visitas mensuales
      $this->titulos[]= "Informe por localidad";
   //   $this->tablas[]= $this->getExportRecordRowMes( $form ) ;
        
        
        
        $q = Doctrine_Query::create()
                   ->from('sfGuardUser s')
                   ->orderBy('s.last_name');
        
        
        $fechaD=$form['fecha']['from'];
        $fechaH=$form['fecha']['to'];
        
        
        
        $this->result_t= array();
        $this->result_t[]= array('USUARIOS', 'Acervos Creados', 'Acervos Modificados', 'Imagenes Creadas', 'Imagenes Modificadas', 'Actividades Creadas', 'Actividades Modificadas');
          foreach($this->search->getBasesDatos() as $bd){
                          $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
               $result= array();
               $result[]=$bd['head'];
               
               //Acervos
        $q = Doctrine_Query::create($conn)
                   ->from('Objeto o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q->andWhere('o.created_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q->andWhere('o.created_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                 //$q->andWhere('o.created_by =?', $user->getId())
                  $q ->fetchArray();
                 
        $result[]= $q->count();
                
        $q_u = Doctrine_Query::create($conn)
                   ->from('Objeto o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q_u->andWhere('o.updated_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q_u->andWhere('o.updated_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                  //$q_u->andWhere('o.updated_by =?', $user->getId())
                  $q_u->  fetchArray();
                 
        $result[]= $q_u->count();
        
                //Imagenes
        $q = Doctrine_Query::create($conn)
                   ->from('Imagen o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q->andWhere('o.created_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q->andWhere('o.created_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
              //   $q->andWhere('o.created_by =?', $user->getId())
                   $q->fetchArray();
                 
        $result[]= $q->count();
                
        $q_u = Doctrine_Query::create($conn)
                   ->from('Imagen o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q_u->andWhere('o.updated_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q_u->andWhere('o.updated_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                  //$q_u->andWhere('o.updated_by =?', $user->getId())
                   $q_u->fetchArray();
                 
        $result[]= $q_u->count();
        
                
        $q = Doctrine_Query::create($conn)
                   ->from('Actividad o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q->andWhere('o.created_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q->andWhere('o.created_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                // $q->andWhere('o.created_by =?', $user->getId())
                  $q ->fetchArray();
                 
        $result[]= $q->count();
                
        $q_u = Doctrine_Query::create($conn)
                   ->from('Actividad o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q_u->andWhere('o.updated_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q_u->andWhere('o.updated_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                  //$q_u->andWhere('o.updated_by =?', $user->getId())
                  $q_u->fetchArray();
                 
        $result[]= $q_u->count();
        
        $this->result_t[]= $result;
       }
       $filtro= array();
       $filtro[]='Desde: '.$this->fechaD= $fechaD['day'].'/'.$fechaD['month'].'/'.$fechaD['year'];
       $filtro[]='Hasta: '.$this->fechaH= $fechaH['day'].'/'.$fechaH['month'].'/'.$fechaH['year'];
      
                   $this->getUser()->setAttribute('tabla_visita', array($this->result_t));
                   $this->getUser()->setAttribute('titulos_visita', array('Informe de Uso') );
                   $this->getUser()->setAttribute('filtros_visita', $filtro);
    }
    
 public function executeToExcel($request) { 
       
       
    ini_set("max_execution_time",0);
		$this->setLayout(false);
		sfConfig::set('sf_web_debug', false);

		$resp = $this->getResponse();
		$resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
		$resp->setHttpHeader('Content-Disposition', ' attachment; filename="Informe General.xlsx"');
		$resp->setHttpHeader('Cache-Control', ' maxage=3600');
		$resp->setHttpHeader('Pragma', 'public');
                
		$excel_builder = new ExcelBuilder();
                
                $tabla=   $this->getUser()->getAttribute('tabla_visita');
                $titulos= $this->getUser()->getAttribute('titulos_visita');
                $filtros= $this->getUser()->getAttribute('filtros_visita');
                   
                 $objWriter = new PHPExcel_Writer_Excel2007($excel_builder->generateExcelGeneral($titulos, $tabla, $filtros)); 

		$tmp_dir =   sfConfig::get('sf_cache_dir').'/';           
		$file_name = $tmp_dir.'Informe General.xlsx';                

		$objWriter->save($file_name);
		$this->file = $file_name;

   }    

   public function executeUsoAnio(){
        
        $this->form= new ObjetoAnioAdminFormFilter();
    }   

    public function executeResultAnio(sfWebRequest $request)
    {       

      $form= $request->getParameter('result');
      $cambio=array('museo'=>'todo');
     
      $this->search = new SearchBuilder($cambio);
      $this->titulos=array("Acervos", "Imagenes", "Visitas");
      $this->todos= array();       
                

      $anio=$form['fecha'];
      $this->Tipo='';
        
      $meses    = array( "1","2","3","4","5","6","7","8","9","10","11","12");
      $tipos    =array("acervos", "imagenes", "visitas");
      foreach ($tipos as $key => $tipo) {        
      
        $pie = array("TOTAL", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
        $TOTAL= 0;
        $this->result_t= array();
        $this->result_t[]= array('USUARIOS', "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEM.", "OCTUBRE", "NOVIEM.", "DICIEM." , "TOTAL");


        foreach($this->search->getBasesDatos() as $bd)
        {
          $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
               
          $result= array();
          $resultmes= array();
          $result[]=$bd['head']; 
          $total=0;

          foreach ($meses as $key => $mes) 
          {    

            switch ( $tipo ) {
                 case "imagenes" :
                  $q = Doctrine_Query::create($conn)->from('Imagen o');
                  $this->Tipo='Imágenes';
                   break;
                 case "acervos" :
                  $q = Doctrine_Query::create($conn)->from('Objeto o');
                  $this->Tipo= 'Acervos' ;             
                   break;
                 case "visitas" :
                  $q = Doctrine_Query::create($conn)->from('Actividad o');
                  $this->Tipo= 'Visítas';              
                   break;      

               }   

            if( !empty($anio['year']))
             {
                $q->andWhere('YEAR(o.created_at)= ?',  $anio['year']);
                } 

            $q->andWhere('MONTH(o.created_at) = ?',  $mes);            
            $q->fetchArray(); 

            $result[]= $q->count();
            $total= $total + $q->count();
            $pie[ $mes ]=$pie[ $mes ]+$q->count();  
            $TOTAL=$TOTAL +$q->count();  

          }

          $result[]= $total;      
                 
          $this->result_t[]= $result;
       }

       $pie[]= $TOTAL;
       $this->result_t[]= $pie;
       $this->todos[] = $this->result_t;
     }
     $filtro= array();
     $this->fechaD= $anio['year'];
     $filtro[0]= 'Año: '.$this->fechaD;
      
                   $this->getUser()->setAttribute('tabla_visita', $this->todos);
                   $this->getUser()->setAttribute('titulos_visita', $this->titulos );
                   $this->getUser()->setAttribute('filtros_visita', $filtro);
    }





}


