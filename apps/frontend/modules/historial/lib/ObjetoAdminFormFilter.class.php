<?php

/**
 * Objeto filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObjetoAdminFormFilter extends BaseObjetoFormFilter
{
  public function configure()
  {
       $format = '%day%/%month%/%year%';
        $web = sfConfig::get('app_url_web');
        $rango = range(2012, 2030);
        $arreglo_rango = array_combine($rango, $rango);
        
        $this->widgetSchema['fecha']= new sfWidgetFormFilterDate(array(
           'label'    => 'Fecha',
          'from_date' => new sfWidgetFormJQueryDate(array(
                 'label' => 'Creado',
                'image'  => '/museo/'.$web.'images/calendar_icon.gif',
               'culture' => 'es',
               'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                buttonText: ['Calendario']}",
               'date_widget' => new sfWidgetFormDate(array(
                   'format' => '%day%/%month%/%year%',
                   'years' => $arreglo_rango,
               ))




            )), 'to_date' => new sfWidgetFormJQueryDate(array(
                 'label' => 'Creado',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))

            )), 'with_empty' => false));
 

        
        $this->widgetSchema->setNameFormat('result[%s]'); 
  }

}
