<?php

require_once dirname(__FILE__).'/../lib/objeto_informeGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/objeto_informeGeneratorHelper.class.php';

/**
 * objeto actions.
 *
 * @package    museo
 * @subpackage objeto
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class objeto_informeActions extends autoObjeto_informeActions
{
    public function executeListShow($request){
  	 $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($request->getParameter('id'))), sprintf('Esta persona no existe (%s).', $request->getParameter('id')));		
    }

  public function executeCambiarVista($request)
  {
      if($this->getUser()->getAttribute('listado_cuadrado', false)== true){
          
            $this->getUser()->setAttribute('listado_cuadrado', false );
      }else{
          $this->getUser()->setAttribute('listado_cuadrado', true );
      }
         $this->forward('objeto_informe', 'index');

   }
public function executeListExportar(sfWebRequest $request)
      {
         
        ini_set("max_execution_time",0);
        $this->setLayout(false);
        sfConfig::set('sf_web_debug', false);

        $resp = $this->getResponse();
        $resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
        $resp->setHttpHeader('Content-Disposition', ' attachment; filename="objetos.xls"');
        $resp->setHttpHeader('Cache-Control', ' maxage=3600');
        $resp->setHttpHeader('Pragma', 'public');
                  
        $excel_builder = new ExcelBuilder();
                
        $records = $this->getPager2(); 
                
        $titulo="ACERVOS";
        $cabecera= array ('NRO', 'TITULO', 'TIPO','DESCRIPCION', 'AÑO', 'TEXTO','AUTOR', 'COLECCIONES','CATEGORIAS','MATERIALES', 'DONACION','UBICACION');
        
        $rows= array();
        $rows = $this->getExportRecordRowObjetos($records);
        
                
        $objWriter = new PHPExcel_Writer_Excel5($excel_builder->generateExcel($titulo, $cabecera, $rows));
                
        $tmp_dir =   sfConfig::get('sf_cache_dir').'/';
        $file_name = $tmp_dir.'Objetos.xls';                

        $objWriter->setTempDir($tmp_dir);        
        $objWriter->save($file_name);

        $this->file = $file_name;
        $this->setTemplate('toExcel'); 
      
    }

    protected function getPager2()
    {
        $pager = $this->configuration->getPager('Objeto');
        $pager->setQuery($this->buildQuery());
        $pager->setMaxPerPage(10000);
        $pager->init();
        return $pager;
    }  
    public function getExportRecordRowObjetos($records){
       
        $rows= array();
        $cant=0;
        foreach($records->getResults() as $objeto){
            $row= array();
            $row[]=$objeto->getId();
            $row[]=$objeto->getNombreObjeto();            
            $row[]=$objeto->getTipo();
            $row[]=$objeto->getDescripcionGenerica();
            $row[]=$objeto->getAnioOrigen();
            $row[]=$objeto->getTextoAntiguedad();
            $row[]=$objeto->getAutores();
            $row[]=$objeto->getColecciones();
            $row[]=$objeto->getCategorias();
            $row[]=$objeto->getMateriales() ;
            $row[]=$objeto->getDonaciones() ;
            $row[]=$objeto->getUbicaciones();
            $cant++;
            $rows[]=$row;
        }
        $row= array();
        $row[]="TOTAL";
        $row[]=$cant;
        $rows[]=$row;   
        return $rows;
      
   } 
}
