<?php use_stylesheet('main.css'); ?>
<div id="formulario_show">
    <center>
 <table>
  <tbody>
  
    <tr>
      <th>N inventario:</th>
      <td><?php echo $objeto->getId() ?></td>
    </tr>
    <tr>
      <th>Otros Inventarios:</th>
      <td>
          
          <table>
              <thead>
                  <tr>
                    <th>Tipo de Inventario</th>
                    <th>Numero</th>
                  </tr>  
              </thead>
              <tbody>
                  <?php foreach($objeto->getOtroInventario() as $inv): ?>
                  <tr>
                      <td><?php echo $inv->getTipoInventario()?></td>
                      <td><?php echo $inv->getNumero()?></td>
                  </tr>
                  <?php endforeach;?>
              </tbody>
          </table>
      
      </td>
    </tr>
    
    <tr>
      <th>Nombre objeto:</th>
      <td><?php echo $objeto->getNombreObjeto() ?></td>
    </tr>
    <tr>
      <th>Tipo de objeto:</th>
      <td><?php echo $objeto->getTipoObjeto() ?></td>
    </tr>
       <tr>
      <th>Categorias:</th>
      <td><?php foreach($objeto->getCategoriaObjeto() as $ca):?>
              <?php echo $ca->getCategoria(); ?><br/>
          <?php endforeach?>
      </td>
    </tr>    
      <tr>
      <th>Obra de Arte:</th>
      <td>
        <table>
              <thead>
                  <tr>
                    <th>Lenguaje</th>
                    <th>Descripción</th>
                    <th>Detalles</th>
                    <th>Especificación</th>
                  </tr>  
              </thead>
              <tbody>
                <?php foreach($objeto->getRegistroDetalleObra() as $de):?>
                  <tr>
                    <td><?php echo $de->getDetalleObra()->getObraDeArte()?></td>
                    <td><?php echo $de->getDetalleObra()->getAspectoObra()?></td>
                    <td><?php echo $de->getDetalleObra()?></td>
                    <td><?php echo $de->getValor()?></td>
                  </tr>
                <?php endforeach?>  
              </tbody>
        </table>

          
      </td>
    </tr>
    <tr>
      <th>Conservación:</th>
      <td>
                 
          <table>
              <thead>
                  <tr>
                    <th>Estado</th>
                    <th>Fecha</th>
                    <th>Motivo del cambio</th>
                  </tr>  
              </thead>
              <tbody>
                  <?php foreach($objeto->getEstado() as $es): ?>
                  <tr>
                       <td><?php echo $es->getEstadoDeConservacion() ?></td>
                       <td><?php echo $es->getFechaDeCambio() ?></td>
                       <td><?php echo $es->getMotivoDelCambio()?></td>
                  </tr>
                  <?php endforeach;?>
              </tbody>
          </table>
      
      </td>
    </tr>
    <tr>
      <th>Autor:</th>
      <td><?php echo $objeto->getAutor() ?></td>
    </tr>
    <tr>
      <th>Ubicación:</th>
      <td><?php echo $objeto->getUbicacion() ?></td>
    </tr>
    <tr>
      <th>Descripción genérica:</th>
      <td><?php echo $objeto->getDescripcionGenerica() ?></td>
    </tr>
    
    <tr>
      <th>Fecha ingreso:</th>
      <td><?php echo $objeto->getFechaIngreso() ?></td>
    </tr>
    <tr>
      <th>Forma ingreso:</th>
      <td><?php echo $objeto->getFormaIngreso() ?></td>
    </tr> 
    <tr>
      <th>Donación:</th>
      <td><?php echo $objeto->getDonacion() ?></td>
    </tr>
    
    <tr>
      <th>Fecha baja:</th>
      <td><?php echo $objeto->getFechaBaja() ?></td>
    </tr>
    <tr>
      <th>Motivo baja:</th>
      <td><?php echo $objeto->getMotivoBaja() ?></td>
    </tr>
    
    <tr>
      <th>Año origen:</th>
      <td><?php echo $objeto->getAnioOrigen() ?></td>
    </tr>
    <tr>
      <th>Texto antigüedad:</th>
      <td><?php echo $objeto->getTextoAntiguedad() ?></td>
    </tr>
      <tr>
      <th>Historia del objeto:</th>
      <td><?php echo $objeto->getHistoriaDelObjeto() ?></td>
    </tr>
    <tr>
      <th>País:</th>
      <td><?php echo $objeto->getLocalidad()->getProvincia()->getPais() ?></td>
    </tr>
    <tr>
      <th>Provincia:</th>
      <td><?php echo $objeto->getLocalidad()->getProvincia()?></td>
    </tr>
    <tr>
      <th>Localidad:</th>
      <td><?php echo $objeto->getLocalidad() ?></td>
    </tr>
    
    <tr>
      <th>Materiales:</th>
      <td><?php foreach($objeto->getObjetoMaterial() as $ma):?>
              <?php echo $ma->getMaterial(); ?><br/>
          <?php endforeach?>
      </td>
    </tr>
   
    
   <tr>
      <th>Colección:</th>
      <td><?php foreach($objeto->getObjetoColeccion() as $co):?>
              <?php echo $co->getColeccion(); ?><br/>
          <?php endforeach?>
      </td>
    </tr>
  </tbody>
</table>
DIMENSIONES: <br/>
<?php if( count ($objeto->getObjetoDimension()) == 0 ):?>
Este objeto no posee dimensiones<br/>
<?php  else: ?>
<table class="dentro">
                    <tr>
                        <th class="dentros">Dimensión</th>
                        <th class="dentros">valor</th>
                        <th class="dentros">unidad</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($objeto->getObjetoDimension() as $dim): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                       <td><?php echo $dim->getDimension() ?></td>
                       <td><?php echo $dim->getValor()?></td>
                      <td><?php echo $dim->getUnidad()?></td>
                     </tr>
                <?php endforeach; ?>

</table>
 <?php endif ?>
<br/>

EVENTOS: <br/>
<?php if(count($objeto->getTrayectoria()) == 0): ?>
Este objeto no posee eventos<br/>
<?php  else: ?>
<table class="dentro">
                    <tr>
                        <th class="dentros">Nombre del evento</th>
                        <th class="dentros">Fecha</th>
                        <th class="dentros">Lugar</th>
                        <th class="dentros">Relato</th>
                        <th class="dentros">tipo de evento</th>
                        <th class="dentros">Acción</th>
                    </tr>
                <?php  $con=0; $i=1; foreach($objeto->getTrayectoria() as $tra): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                      <td><?php echo $tra->getNombreEvento() ?></td>
                      <td><?php echo $tra->getFecha('d/m/Y')?></td>
                      <td><?php echo $tra->getLugar()?></td>
                      <td><?php echo $tra->getRelato()?></td>
                      <td><?php echo $tra->getTipoEvento()?></td>
                      <td>
                              <?php if($tra->getGaleriaDeImagenes()):?>
                                     <a onclick="window.open(this.href);return false;" href="<?php echo $tra->getGaleriaDeImagenes() ;?>" style="color: red" ><?php echo image_tag('inbox.png', array('alt' => 'Agregar', 'title' =>'Ir a la galería'));?></a>
                              <?php endif?>
                      </td>

                   </tr>
                <?php endforeach; ?>

</table>
<?php endif ?>
<br/>

GALERÍA:<br/>
<?php if($objeto->getGaleriaDeImagenes()):?>
<center><p style="color: red"> <a onclick="window.open(this.href);return false;" href="<?php echo $objeto->getGaleriaDeImagenes() ;?>" style="color: red" > ir a la galeria</a></p></center>
<?php else: ?>
Este objeto no esta relacionado a ninguna carpeta en el gallery
<?php endif?>
<br><br>
<?php if(! count($objeto->getImagen())==0 ):?>
    


  <table class="dentro">
    <tr>
      <th class="dentros">Imagen</th>
      <th class="dentros">Título</th>
      <th class="dentros">Descripción</th>
    </tr>

      <?php  $con=0; $i=1; foreach ($objeto->getImagen() as $picture): $odd = fmod(++$i, 2) ;$band=false; ?>
        <tr class="sf_admin_row_<?php echo $odd ?>">
          <td>   

            <?php if($picture->getArchivo()):                  
          
                $file= $sf_user->getFile();
                $dir =  (string)  '/museo/web/uploads/'.$file.'/fotos/'.$picture->getArchivo(); 
                $formatos_pdf =  array('pdf','x-pdf');
                $formatos_imagen =  array('png','jpg','jpeg','pjpeg','x-png','gif','bmp','tiff','x-icon');
                $formatos_videos = array( 'avi','divx','x-flv','quicktime','mpeg','mp4','ogg');
                $formatos_audios = array('mpeg','x-realaudio','wav','ogg','midi','x-ms-wma','mp3');
          
                $extension = pathinfo($dir, PATHINFO_EXTENSION);
            
                if(in_array($extension, $formatos_pdf) ): ?>
                  <embed src="<?php echo $dir?>" type="application/pdf" width="400" height="400"></embed>
            <?php endif;

                if(in_array($extension, $formatos_imagen) ) :
                  echo image_tag('../uploads/'.$file.'/fotos/thumbs/'.$picture->getArchivo() );
                endif;
            
              if(in_array($extension, $formatos_videos) ) : ?>
                <video class="bordered-feature-image" width="300" height="160" controls >
                   <source src="<?php echo $dir?>" type="video/mp4">
                  Your browser does not support the video tag.
                </video>
            <?php 
              endif;

              if(in_array($extension, $formatos_audios) ):?>
                <audio controls>
                  <source src="<?php echo $dir?>" type="audio/mpeg">
                    Your browser does not support the audio element.
                  </audio>
            <?php 
              endif; 

        endif?>       
                





          </td>
          <td><?php echo $picture->getTitulo()?></td>
          <td><?php echo $picture->getDescripcion()?></td>
        </tr>
      <?php endforeach; ?>











    </table >
<?php endif?>
<br />
INVESTIGACIONES:<br />
<?php if( count($objeto->getInvestigacion())==0 ):?>
Este objeto no posee investigaciones
<?php else: ?>
<table class="dentro">
                    <tr>
                        <th class="dentros">Fecha</th>
                        <th class="dentros">Relato</th>
                        <th class="dentros">Fuente B.</th>
                        <th class="dentros">Investigador</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($objeto->getInvestigacion() as $inves): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                       <td><?php echo $inves->getFecha() ?></td>
                       <td><?php echo $inves->getRelato()?></td>
                       <td><?php echo $inves->getFuenteBibliografica()?></td>
                       <td><?php foreach($inves->getInvestigadores() as $inve):?> 
                           <?php echo $inve; ?> <br />
                           <?php endforeach ?>
                       </td>
                    </tr>
                <?php endforeach; ?>

</table>
<?php endif ?>
</center>


&nbsp;
<div id="volver"> 
  <a href="<?php echo url_for('objeto_informe/index') ?>">Volver a la lista</a>
</div>
</div>
<br/>
