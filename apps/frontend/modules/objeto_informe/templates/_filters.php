<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
    <script>
    $(function() {
            $( "#tabs_f" ).tabs();
    });
    </script>
        
    <style>

#filtros form label {
	float: left;	
	display: inline-block;
	width: 150px;
	text-align: right;
	margin-right: 10px;
	font-size: 14px;	
}


#datos {
margin-left: 160px;
	
}
    </style>        
<div class="sf_admin_filter">
  <?php if ($form->hasGlobalErrors()): ?>
    <?php echo $form->renderGlobalErrors() ?>
  <?php endif; ?>
      <div id="filtros">
            <form  action="<?php echo url_for('objeto_informe_collection', array('action' => 'filter')) ?>" method="post">
            <table cellspacing="0">
              <tfoot>
                <tr>
                  <td colspan="2">
                    <?php echo $form->renderHiddenFields() ?>
                    <?php echo link_to(__('Reset', array(), 'sf_admin'), 'objeto_informe_collection', array('action' => 'filter'), array('query_string' => '_reset', 'method' => 'post')) ?>
                    <input type="submit" value="<?php echo __('Filter', array(), 'sf_admin') ?>" />
                  </td>
                </tr>
              </tfoot>
              <tbody>
                <div id="tabs_f">
                       <ul>  
                           <?php $cont=0;
                                 $os = sfConfig::get('app_campos_objetos') ;?>
                                         
                                <li><a href="#tabs-<?php echo $cont?>">Datos</a></li>
                                        <?php foreach ($configuration->getFormFilterFields($form) as $name => $field): ?>

                                                <?php if (in_array($name, $os)): ?>
                                                        <?php $cont++; ?>                    
                                                         <li><a href="#tabs-<?php echo $cont?>"><?php echo $field->getConfig('label')?></a></li>
                                             <?php endif; ?>            
                                       <?php endforeach; ?>
                       </ul> 


                 <?php $cont1=0;?>

                 <div id="tabs-<?php echo $cont1?>">
                <?php foreach ($configuration->getFormFilterFields($form) as $name => $field): ?>
                        

                <?php if ((isset($form[$name]) && $form[$name]->isHidden()) || (!isset($form[$name]) && $field->isReal())) continue ?>  

                    <?php if ( in_array($name, $os)): ?>
                         <?php $cont1++;?> 
                            </div>
                        <div id="tabs-<?php echo $cont1?>">
                    <?php endif ?> 

                  <?php include_partial('objeto_informe/filters_field', array(
                    'name'       => $name,
                    'attributes' => $field->getConfig('attributes', array()),
                    'label'      => $field->getConfig('label'),
                    'help'       => $field->getConfig('help'),
                    'form'       => $form,
                    'field'      => $field,
                    'class'      => 'sf_admin_form_row sf_admin_'.strtolower($field->getType()).' sf_admin_filter_field_'.$name,
                  )) ?>

                    <br/>        

                        
                <?php endforeach; ?>    
                    </div>
                   </div>   
              </tbody>
            </table>
          </form>
    </div>    
</div>
