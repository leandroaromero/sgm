<?php

/**
 * Objeto form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObjetoAdminForm extends BaseObjetoForm
{
    
 public function configure()
  {
      unset($this['dimension_list']);
      sfProjectConfiguration::getActive()->loadHelpers('Url');
      $rango = range(1800, 2040);
      $arreglo_rango = array_combine($rango, $rango);  
      $web = sfConfig::get('app_url_web');       
      $user= sfContext::getInstance()->getUser()->getGuardUser()->getId();
      $this->validatorSchema['created_at']= new sfValidatorDateTime(array('required'=>false) );
      $this->validatorSchema['updated_at']= new sfValidatorDateTime(array('required'=>false) );
      $this->widgetSchema['coleccion_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['coleccion_list']->setOption('table_method', 'getOrderNombre');
      $this->widgetSchema['material_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['material_list']->setOption('table_method', 'getOrderNombre');
     // $this->widgetSchema['categoria_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      //$this->widgetSchema['material_list']->setOption('table_method', 'getOrderNombre');
      $this->widgetSchema['trayectoria_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['trayectoria_list']->setOption('table_method', 'getOrderNombre');
      $this->widgetSchema['trayectoria_list']->setOption('label', 'Eventos');
      unset($this->widgetSchema['created_at']);
      unset($this->widgetSchema['updated_at']);
      $this->widgetSchema['created_by']= new sfWidgetFormInputHidden();
      $this->getObject()->setCreatedBy( $user);
      $this->widgetSchema['autor_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['autor_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
      array(
        'model' => "Autor",
        'url'   => url_for("@ajax_autor"),
        'config' => '{ max: 30}'
        ));
        
      $this->widgetSchema['donacion_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['donacion_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
    array(
      'model' => "Donacion",
      'url'   => url_for("@ajax_donacion"),
      'config' => '{ max: 30}'
    ));




      $this->widgetSchema['fecha_ingreso'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
         'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           'years' => $arreglo_rango,
           ))
        ));
       $this->widgetSchema['fecha_baja'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
       monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
       buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           'years' => $arreglo_rango,
           ))
        ));
         
       $this->widgetSchema['ubicacion_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Ubicacion ',
              'model'   => 'Ubicacion',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
   sfProjectConfiguration::getActive()->loadHelpers('Url');
      $this->widgetSchema['localidad_id'] =  new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true));
            ////new sfValidatorChoice(array('choices' => array($this->getObject()->get('localidad_id')), 'empty_value' => $this->getObject()->get('localidad_id'), 'required' => false));
      $this->widgetSchema['localidad_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['localidad_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
      array(
        'model' => "Localidad",
        'url'   => url_for("@ajax_origen"),
        'config' => '{ max: 30}',
        'method' => 'getProcedencia'
        ));
     
    $this->validatorSchema['localidad_id']=  new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'required' => false));


  }

}
