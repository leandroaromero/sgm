<?php

require_once dirname(__FILE__).'/../lib/tipo_inventarioGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/tipo_inventarioGeneratorHelper.class.php';

/**
 * tipo_inventario actions.
 *
 * @package    museo
 * @subpackage tipo_inventario
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class tipo_inventarioActions extends autoTipo_inventarioActions
{
}
