<?php

require_once dirname(__FILE__).'/../lib/pictureGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/pictureGeneratorHelper.class.php';

/**
 * picture actions.
 *
 * @package    museo
 * @subpackage picture
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class pictureActions extends autoPictureActions
{
   public function executeNew(sfWebRequest $request)
  {
    $this->objeto_id=$request->getParameter('acervo_id');
    $this->imagen = new Imagen();
    $this->imagen->setObjetoId($this->objeto_id);
    $this->form = new ImagenAdminForm($this->imagen);
  }


  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
    $dir= $this->getUser()->getFile();
    $this->forward404Unless($picture = Doctrine::getTable('Imagen')->find(array($request->getParameter('id'))), sprintf('Object picture does not exist (%s).', $request->getParameter('id')));
    $OId=$picture->getObjetoId();
    $filename = $picture->getArchivo(); 
    $uploads= sfConfig::get('sf_upload_dir');
    
    $filepath =  $uploads.'/'.$dir.'/fotos/'.$filename;
                 @unlink($filepath);
    $filepath1 = $uploads.'/'.$dir.'/fotos/thumbs/'.$filename;
                 @unlink($filepath1);
    $filepath2 = $uploads.'/'.$dir.'/fotos/big/'.$filename;
                 @unlink($filepath2);
    $picture->delete();

     $this->redirect('objeto/edit?id='.$OId);
  }
  public function executeList_cancel($request){
    //$this->forward404Unless($picture = Doctrine::getTable('Imagen')->find(array($request->getParameter('id'))), sprintf('Object picture does not exist (%s).', $request->getParameter('id')));

    $OId=$request->getParameter('o_id');
    $this->redirect('objeto/edit?id='.$OId);
  }
  public function executeUpdate(sfWebRequest $request)
  {

    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($picture = Doctrine::getTable('Imagen')->find(array($request->getParameter('id'))), sprintf('Object picture does not exist (%s).', $request->getParameter('id')));
          ini_set("max_execution_time",0);
          ini_set('memory_limit', '-1');
    $a= array();
    $a=$request->getParameter('imagen');
    
    $this->form = new ImagenAdminForm($picture);
    
    $this->processForm($request, $this->form);
    $this->redirect('picture/edit?id='.$picture->getId());
  }

  public function executeDownload(sfwebRequest $request)
  {
    $picture = Doctrine_Core::getTable('Imagen')->find($request->getParameter('i_id'));
    $this->forward404Unless($picture);
    $dir= $this->getUser()->getFile(); 
    
    $filename= $picture->getArchivo();
    $uploads=  sfConfig::get('sf_upload_dir');

    $formatos_imagen =  array('png','jpg','jpeg','pjpeg','x-png','gif','bmp','tiff','x-icon');
    $extension = pathinfo($filename, PATHINFO_EXTENSION);
    if(in_array($extension, $formatos_imagen) )
    {      
      
      $path =    $uploads.'/'.$dir.'/fotos/big/'.$filename;

    }else{
      
      $path =    $uploads.'/'.$dir.'/fotos/'.$filename;

    }


    $name= $picture->getObjeto()->getNombreObjeto(); 
    if($picture->getTitulo()){
		$name= $name.' '.$picture->getTitulo(); 
		}  
    
    $type = '';

    if (is_file($path)) {
        $size = filesize($path);
        if (function_exists('mime_content_type')) {
            $type = mime_content_type($path);
         } else if (function_exists('finfo_file')) {
                   $info = finfo_open(FILEINFO_MIME);
                   $type = finfo_file($info, $path);
                   finfo_close($info);
                }
         if ($type == '') {
             $type = "application/force-download";
            }
    $file = basename($name); 
    header("Content-Type: $type");
    header("Content-Disposition: attachment; filename=\"$file\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . $size);
    // descargar achivo
    readfile($path);
    } else {
         die("File not exist !!");
       }
  }

}
