<ul class="sf_admin_actions">
<?php if ($form->isNew()): ?>   
  <li class="sf_admin_action_cancel">
  <?php echo link_to(__('Cancel', array(), 'messages'), 'picture/List_cancel?o_id='.$form->getObject()->getObjetoId(), array()) ?>
  </li>
  <?php echo $helper->linkToSave($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Save',)) ?>
<?php else: ?>
  <?php echo link_to(image_tag('cross.png', array('alt_title' => 'Eliminar imagen', 'height' => 16)).'Delete', 'picture/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <li class="sf_admin_action_cancel">
  <?php echo link_to(__('Terminar', array(), 'messages'), 'picture/List_cancel?o_id='.$form->getObject()->getObjetoId(), array()) ?>
  </li>
  <li>
    <?php echo link_to(__('Descargar', array(), 'messages'), 'picture/download?i_id='.$form->getObject()->getId(), array()) ?>
  </li>  
  <?php echo $helper->linkToSave($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Save',)) ?>

<?php endif; ?>
</ul>
