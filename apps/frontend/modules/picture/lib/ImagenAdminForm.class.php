<?php

/**
 * Imagen form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ImagenAdminForm extends BaseImagenForm
{
      public function configure()
      {

           unset($this['updated_at'],$this['created_at'],$this['updated_by'],$this['created_by']);
           $this->widgetSchema['objeto_id']=  new sfWidgetFormInputHidden();
           $this->widgetSchema['descripcion']=  new sfWidgetFormTextarea();
           $dir= sfContext::getInstance()->getUser()->getFile();
           $uploads= sfConfig::get('sf_upload_dir');
           $this->widgetSchema['archivo'] = new sfWidgetFormInputFileEditable(array(
               'label'     => 'Imagen *',
               'file_src'  => $uploads.'/'.$dir.'/fotos/'.$this->getObject()->getArchivo(),
               'is_image'  => true,
               'edit_mode' => false,
               'template'  => '<div>%file%<br /><label></label>%input%<br /><label></label>%delete% Eliminar imagen actual</div>',
            ));

            $this->validatorSchema['archivo'] = new sfValidatorFile(array(
               'required'   => false,               
                'mime_types' => array(  'application/pdf',
                                         'application/x-pdf',
                                      
                                      'image/png',
                                      'image/jpg',       
                                      'image/jpeg',
                                      'image/pjpeg',
                                      'image/x-png',
                                      'image/gif',
                                      'image/bmp',
                                      'image/tiff',
                                      'image/x-icon',

                                      'video/avi',
                                      'video/divx',
                                      'video/x-flv',
                                      'video/quicktime',
                                      'video/mpeg',
                                      'video/mp4',
                                      'video/ogg',

                                      'audio/mpeg',
                                      'audio/x-realaudio',
                                      'audio/wav',
                                      'audio/ogg',
                                      'audio/midi',
                                      'audio/x-ms-wma',
                                      'audio/x-ms-wax',
                                      'audio/x-matroska'
                                    ), 

               'path' => $uploads.'/'.$dir.'/fotos/',
               'validated_file_class' => 'sfResizedFile',
            ));

      }


      public function doSave($con = null)
       {
            $foto= $this->getValue('archivo');

            $fail = $this->getObject()->getArchivo();

            if($foto){
                $dir= sfContext::getInstance()->getUser()->getFile(); 
                $uploads= sfConfig::get('sf_upload_dir');

                $p  = $uploads.'/'.$dir.'/fotos/'.$fail;
                $p1 = $uploads.'/'.$dir.'/fotos/big/'.$fail;
                $p2 = $uploads.'/'.$dir.'/fotos/thumbs/'.$fail;

                @unlink($p);
                @unlink($p1);
                @unlink($p2);
            }
   
           return parent::doSave($con);
        }  


}
