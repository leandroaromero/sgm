<?php

/**
 * Pictures form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PicturesAdminForm extends BasePicturesForm
{
  public function configure()
  {
   //   unset($this['institucion_id']);
           $dir= sfContext::getInstance()->getUser()->getFile();    
           $this->widgetSchema['institucion_id']=  new sfWidgetFormInputHidden();
           $this->widgetSchema['file'] = new sfWidgetFormInputFileEditable(array(
           'label'     => 'Imagen *',
           'file_src'  => sfConfig::get('sf_upload_dir').'/museo/'.$this->getObject()->getFile(),
           'is_image'  => true,
           'edit_mode' => false,
           'template'  => '<div>%file%<br /><label></label>%input%<br /><label></label>%delete% Eliminar imagen actual</div>',
        ));

//        $this->validatorSchema['file'] = new sfValidatorFile(array(
//           'required'   => false,
//           'mime_types' => 'web_images',
//           'path' => sfConfig::get('sf_upload_dir').'/museo/',
//           'validated_file_class' => 'sfResizedFile',
//        ));
           
           
           
           
    $this->validatorSchema['file'] = new sfValidatorFile(array(
    'required'   => false,
    'max_size'  => '4202304', 
    'mime_types' => 'web_images',
    'path' => sfConfig::get('sf_upload_dir').'/museo/',
    'validated_file_class' => 'sfResizedFile',
), array(
       'required' => 'Ten&eacute;s que seleccionar una imagen.',
       'max_size' => 'El tama&ntilde;o m&aacute;ximo es 4 MB',
       'mime_types' => 'S&oacute;lo se permiten im&aacute;genes para web (jpg, png, gif)',
 
//        'max_width' => 'El ancho de "%value%" es muy chico (mínimo %min_width% pixels).',
//      'min_height' => 'El alto de "%value%" es muy chico (mínimo %min_height% pixels).',
    )
        
        );
	  
  }
 /** public function doSave($con = null)
{
	if(!$this->isNew()){
    $picture = Doctrine::getTable('Pictures')->find($this->getObject()->getId());
    $fail=$picture->getFile();
    if(isset($pic['file'])){
 
    $p  = sfConfig::get('sf_upload_dir').'/museo/'.$fail;
    $p1 = sfConfig::get('sf_upload_dir').'/museo/big/'.$fail;
    $p2 = sfConfig::get('sf_upload_dir').'/museo/thumbs/'.$fail;

      @unlink($p);
      @unlink($p1);
      @unlink($p2);
  }

}
  return parent::doSave($con);
} **/
  
  
    public function doUpdateObject($values)
       {
            

       $fail = $this->getObject()->getFile();

            if( $values['file'] != $fail ){
                $dir= sfContext::getInstance()->getUser()->getFile(); 
                $uploads= sfConfig::get('sf_upload_dir');

                        
                $p1 = $uploads.'/museo/big/'.$fail;
                @unlink($p1);
                
                $p2 = $uploads.'/museo/thumbs/'.$fail;
                @unlink($p2);
                
//                $p  = $uploads.'/'.$dir.'/fotos/'.$fail;
//                @unlink($p);
                
            }
   
           return parent::doUpdateObject($values);
        }
}
