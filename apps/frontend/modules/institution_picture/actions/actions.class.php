<?php

require_once dirname(__FILE__).'/../lib/institution_pictureGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/institution_pictureGeneratorHelper.class.php';

/**
 * institution_picture actions.
 *
 * @package    museo
 * @subpackage institution_picture
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class institution_pictureActions extends autoInstitution_pictureActions
{
      public function executeNew(sfWebRequest $request)
      {
        $this->museo_id=$request->getParameter('museo_id');
        $this->pictures = new Pictures();
        $this->pictures->setInstitucionId($this->museo_id);
        $this->getUser()->setAttribute('institution_id', $this->museo_id );
        $this->form = new PicturesAdminForm($this->pictures);
      }

      public function executeEdit(sfWebRequest $request)
      {
        $this->pictures = $this->getRoute()->getObject();
        $this->form =  $this->form = new PicturesAdminForm($this->pictures);
        
        $this->getUser()->setAttribute('institution_id', $this->pictures->getInstitucionId());
      }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
//    $dir= $this->getUser()->getFile();
    
    $this->forward404Unless($picture = Doctrine::getTable('Pictures')->find(array($request->getParameter('id'))), sprintf('Object picture does not exist (%s).', $request->getParameter('id')));
//    $p= new Pictures();
//    $p->getInstitucionId();
            
    $OId=$picture->getInstitucionId();
    
    $filename = $picture->getFile();
    $filepath = sfConfig::get('sf_upload_dir').'/museo/'.$filename;
    @unlink($filepath);
    $filepath1 = sfConfig::get('sf_upload_dir').'/museo/thumbs/'.$filename;
    unlink($filepath1);
    $filepath2 = sfConfig::get('sf_upload_dir').'/museo/big/'.$filename;
    @unlink($filepath2);
    $picture->delete();

     $this->redirect('institution/edit?id='.$OId);
  }
  public function executeList_cancel($request){
    //$this->forward404Unless($picture = Doctrine::getTable('Imagen')->find(array($request->getParameter('id'))), sprintf('Object picture does not exist (%s).', $request->getParameter('id')));

    $OId=  $this->getUser()->getAttribute('institution_id');
    $this->redirect('institution/edit?id='.$OId);
  }
  public function executeUpdate(sfWebRequest $request)
  {

    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($picture = Doctrine::getTable('Pictures')->find(array($request->getParameter('id'))), sprintf('Object picture does not exist (%s).', $request->getParameter('id')));
    $a= array();
    $a=$request->getParameter('pictures');
    
//    if($a['file']){
//    $filename = $picture->getFile();
//    $filepath = sfConfig::get('sf_upload_dir').'/museo/'.$filename;
//    @unlink($filepath);
//    $filepath1 = sfConfig::get('sf_upload_dir').'/museo/thumbs/'.$filename;
//    unlink($filepath1);
//    $filepath2 = sfConfig::get('sf_upload_dir').'/museo/big/'.$filename;
//    @unlink($filepath2);
//    }
    $this->form = new PicturesAdminForm($picture);
    $this->pictures = $this->getRoute()->getObject(); 
    $this->processForm($request, $this->form);
    
//    $this->redirect('institution_picture/edit?id='.$picture->getId());
  $this->setTemplate('edit');
  }

  public function executeDownload(sfwebRequest $request)
  {
    $picture = Doctrine_Core::getTable('Imagen')->find($request->getParameter('i_id'));
    $this->forward404Unless($picture);
     $dir= $this->getUser()->getFile(); 
    $filename = $picture->getFile();
    $path = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/big/'.$filename;

    $name= $picture->getObjeto()->getNombreObjeto(); 
    if($picture->getTitulo()){
		$name= $name.' '.$picture->getTitulo(); 
		}  
    
    $type = '';

    if (is_file($path)) {
        $size = filesize($path);
        if (function_exists('mime_content_type')) {
            $type = mime_content_type($path);
         } else if (function_exists('finfo_file')) {
                   $info = finfo_open(FILEINFO_MIME);
                   $type = finfo_file($info, $path);
                   finfo_close($info);
                }
         if ($type == '') {
             $type = "application/force-download";
            }
    $file = basename($name); 
    header("Content-Type: $type");
    header("Content-Disposition: attachment; filename=\"$file\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . $size);
    // descargar achivo
    readfile($path);
    } else {
         die("File not exist !!");
       }
  }

}
