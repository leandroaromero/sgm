<?php

require_once dirname(__FILE__).'/../lib/tipo_objetoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/tipo_objetoGeneratorHelper.class.php';

/**
 * tipo_objeto actions.
 *
 * @package    museo
 * @subpackage tipo_objeto
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class tipo_objetoActions extends autoTipo_objetoActions
{
}
