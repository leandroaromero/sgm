<?php use_helper('jQuery') ?>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<div id="dato_dimem">
<form action="<?php echo url_for('obj_dimension/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;
           &nbsp;
          <?php echo jq_link_to_remote('Cancelar', array(
                                        'update' => 'dato_dimension',
                                        'url' => 'objeto/cancelarDimensiones?o_id='.$objeto_id,
                                        'script' => 'true',
                                                  )); ?>

          <?php  echo jq_submit_to_remote('agregar','Grabar', array(
          'update' => 'dato_dimension',
          'url'    => 'objeto/grabarDimensiones?d_id='.$form->getObject()->getId(),
           ))
            ?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['dimension_id']->renderLabel('Dimensión *') ?></th>
        <td>
          <?php echo $form['dimension_id']->renderError() ?>
          <?php echo $form['dimension_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['valor']->renderLabel('Valor *') ?></th>
        <td>
          <?php echo $form['valor']->renderError() ?>
          <?php echo $form['valor'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['unidad']->renderLabel() ?></th>
        <td>
          <?php echo $form['unidad']->renderError() ?>
          <?php echo $form['unidad'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
</div>