
<?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_imagen" style="display:<?php echo $sf_user->getAttribute('imagen')?>" >
  <?php echo __('Imagen', array(), 'messages') ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_id" style="display:<?php echo $sf_user->getAttribute('o_N_inv')?>">
  <?php if ('id' == $sort[0]): ?>
    <?php echo link_to(__('N. Inv', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=id&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('N. Inv', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=id&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th id="nombre_objeto"  class="sf_admin_text sf_admin_list_th_nombre_objeto" style="display:<?php echo $sf_user->getAttribute('o_nombre')?>">
  <?php if ('nombre_objeto' == $sort[0]): ?>
    <?php echo link_to(__('Nombre', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=nombre_objeto&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Nombre', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=nombre_objeto&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_foreignkey sf_admin_list_th_autor_id" style="display:<?php echo $sf_user->getAttribute('autor')?>">
  <?php if ('autor_id' == $sort[0]): ?>
    <?php echo link_to(__('Autor', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=autor_id&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Autor', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=autor_id&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_foreignkey sf_admin_list_th_ubicacion_id" style="display:<?php echo $sf_user->getAttribute('ubicacion')?>">
  <?php if ('ubicacion_id' == $sort[0]): ?>
    <?php echo link_to(__('Ubicación', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=ubicacion_id&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Ubicación', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=ubicacion_id&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_descripcion_generica" style="display:<?php echo $sf_user->getAttribute('descripcion')?>">
  <?php if ('descripcion_generica' == $sort[0]): ?>
    <?php echo link_to(__('Descripción genércia', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=descripcion_generica&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Descripción genércia', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=descripcion_generica&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_forma_ingreso" style="display:<?php echo $sf_user->getAttribute('f_ingreso')?>">
  <?php if ('forma_ingreso' == $sort[0]): ?>
    <?php echo link_to(__('Forma ingreso', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=forma_ingreso&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Forma ingreso', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=forma_ingreso&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_date sf_admin_list_th_fecha_ingreso" style="display:<?php echo $sf_user->getAttribute('d_ingreso')?>">
  <?php if ('fecha_ingreso' == $sort[0]): ?>
    <?php echo link_to(__('F ingreso', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=fecha_ingreso&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('F ingreso', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=fecha_ingreso&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_foreignkey sf_admin_list_th_donacion_id" style="display:<?php echo $sf_user->getAttribute('donacion')?>">
  <?php if ('donacion_id' == $sort[0]): ?>
    <?php echo link_to(__('Donacion', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=donacion_id&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Donacion', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=donacion_id&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_anio_origen" style="display:<?php echo $sf_user->getAttribute('a_origen')?>" >
  <?php if ('anio_origen' == $sort[0]): ?>
    <?php echo link_to(__('Año de origen', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=anio_origen&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Año de origen', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=anio_origen&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_texto_antiguedad" style="display:<?php echo $sf_user->getAttribute('t_antiguedad')?>">
  <?php if ('texto_antiguedad' == $sort[0]): ?>
    <?php echo link_to(__('texto de antigüedad', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=texto_antiguedad&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('texto de antigüedad', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=texto_antiguedad&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_historia_del_objeto" style="display:<?php echo $sf_user->getAttribute('historia')?>" >
  <?php if ('historia_del_objeto' == $sort[0]): ?>
    <?php echo link_to(__('Historia del objeto', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=historia_del_objeto&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Historia del objeto', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=historia_del_objeto&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_foreignkey sf_admin_list_th_localidad_id" style="display:<?php echo $sf_user->getAttribute('localidad')?>">
  <?php if ('localidad_id' == $sort[0]): ?>
    <?php echo link_to(__('Localidad', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=localidad_id&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Localidad', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=localidad_id&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_imagen" style="display:<?php echo $sf_user->getAttribute('colecciones')?>" >
  <?php echo __('Coleccion', array(), 'messages') ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_imagen" style="display:<?php echo $sf_user->getAttribute('materiales')?>" >
  <?php echo __('Materiales', array(), 'messages') ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_tasacion_valor" style="display:<?php echo $sf_user->getAttribute('tasacion')?>">
  <?php if ('tasacion_valor' == $sort[0]): ?>
    <?php echo link_to(__('Tasación valor', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=tasacion_valor&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Tasación valor', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=tasacion_valor&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_tasacion_unidades" style="display:<?php echo $sf_user->getAttribute('t_unidad')?>">
  <?php if ('tasacion_unidades' == $sort[0]): ?>
    <?php echo link_to(__('Tasación por unidad', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=tasacion_unidades&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Tasación por unidad', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=tasacion_unidades&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_tasacion_fuente" style="display:<?php echo $sf_user->getAttribute('t_fuente')?>">
  <?php if ('tasacion_fuente' == $sort[0]): ?>
    <?php echo link_to(__('Tasación fuente', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=tasacion_fuente&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Tasación fuente', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=tasacion_fuente&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_estado_de_conservacion" style="display:<?php echo $sf_user->getAttribute('estados')?>">
  <?php if ('estado_de_conservacion' == $sort[0]): ?>
    <?php echo link_to(__('Estado de conservación', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=estado_de_conservacion&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Estado de conservación', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=estado_de_conservacion&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>

<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_date sf_admin_list_th_fecha_baja" style="display:<?php echo $sf_user->getAttribute('f_baja')?>">
  <?php if ('fecha_baja' == $sort[0]): ?>
    <?php echo link_to(__('Fecha baja', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=fecha_baja&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Fecha baja', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=fecha_baja&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_motivo_baja" style="display:<?php echo $sf_user->getAttribute('m_baja')?>">
  <?php if ('motivo_baja' == $sort[0]): ?>
    <?php echo link_to(__('Motivo baja', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=motivo_baja&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Motivo baja', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=motivo_baja&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?><?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_galeria_de_imagenes" style="display:<?php echo $sf_user->getAttribute('g_imagenes')?>">
  <?php if ('galeria_de_imagenes' == $sort[0]): ?>
    <?php echo link_to(__('Galería de imagenes', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=galeria_de_imagenes&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Galería de imagenes', array(), 'messages'), '@total_objeto', array('query_string' => 'sort=galeria_de_imagenes&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?>
