<?php use_helper('I18N', 'Date') ?>
<?php include_partial('total_objeto/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Lista de acervos', array(), 'messages') ?></h1>

  <?php include_partial('total_objeto/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('total_objeto/list_header', array('pager' => $pager)) ?>
  </div>

  <div id="sf_admin_bar">

  </div>
<button id="column" style="background:#F1F3F5;color:#000000;"><?php echo __('Columnas')?></button>
<div id="datos_columnas" style="display: none">
   <form name="busquedas" action="<?php echo url_for('total_objeto/objectView')?>" method="post">
    <table>
          <thead>
            <th>
                <label style="width: auto"> N inv
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('o_N_inv') == 'none' ? '' : "checked='1'" )  ?> name="o_N_inv">
                </label>
            </th>
            <th>
                <label style="width: auto">Nombre
                <input  type="checkbox"  <?php echo ( $sf_user->getAttribute('o_nombre') == 'none' ? '' : "checked='1'" )  ?> name="o_nombre" >
                </label>
            </th>
            <th>
                <label style="width: auto">Autor
                <input  type="checkbox" <?php echo ( $sf_user->getAttribute('autor') == 'none' ? '' : "checked='1'" )  ?> name="autor">
                </label>
            </th>
            <th>
                <label style="width: auto">Ubicación
                <input  type="checkbox" <?php echo ( $sf_user->getAttribute('ubicacion') == 'none' ? '' : "checked='1'" )  ?> name="ubicacion">
                </label>
            </th>
            <th>
                <label style="width: auto">Descripción g.
                <input  type="checkbox" <?php echo ( $sf_user->getAttribute('descripcion') == 'none' ? '' : "checked='1'" )  ?> name="descripcion">
                </label>
            </th>
            <th>
                <label style="width: auto">Forma ingreso
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('f_ingreso') == 'none' ? '' : "checked='1'" )  ?> name="f_ingreso">
                </label>
            </th>
            <th>
                <label style="width: auto">F ingreso
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('d_ingreso') == 'none' ? '' : "checked='1'" )  ?> name="d_ingreso">
                </label>
            </th>
            <th>
                <label style="width: auto">Donacion
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('donacion') == 'none' ? '' : "checked='1'" )  ?> name="donacion">
                </label>
            </th>
            <th>
                <label style="width: auto">Año de origen
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('a_origen') == 'none' ? '' : "checked='1'" )  ?> name="a_origen">
                </label>
            </th>
            <th>
                <label style="width: auto">Texto
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('t_antiguedad') == 'none' ? '' : "checked='1'" )  ?> name="t_antiguedad">
                </label>
            </th>
            <th>
                <a id="firts_line_1" >Marcar</a>
                 /
                <a id="firts_line_0" >Desmarcar</a>
            </th>
          </thead>
          <thead>
            <th>
                <label style="width: auto">Historia
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('historia') == 'none' ? '' : "checked='1'" )  ?> name="historia">
                </label>
            </th>
            <th>
                <label style="width: auto">Localidad
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('localidad') == 'none' ? '' : "checked='1'" )  ?> name="localidad">
                </label>
            </th>
            <th>
                <label style="width: auto">Colecciones
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('colecciones') == 'none' ? '' : "checked='1'" )  ?> name="colecciones">
                </label>
            </th>
            <th>
                <label style="width: auto">Materiales
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('materiales') == 'none' ? '' : "checked='1'" )  ?> name="materiales">
                </label>
            </th>
            <th>
                <label style="width: auto">Tasación
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('tasacion') == 'none' ? '' : "checked='1'" )  ?> name="tasacion">
                </label>
            </th>
            <th>
                <label style="width: auto"> Tasación por u.
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('t_unidad') == 'none' ? '' : "checked='1'" )  ?> name="t_unidad">
                </label>
            </th>
            <th>
                <label style="width: auto"> Tasación fuente
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('t_fuente') == 'none' ? '' : "checked='1'" )  ?> name="t_fuente">
                </label>
            </th>
            <th>
                <label style="width: auto">Fecha de baja
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('f_baja') == 'none' ? '' : "checked='1'" )  ?> name="f_baja">
                </label>
            </th>
            <th>
                <label style="width: auto">Motivo de baja
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('m_baja') == 'none' ? '' : "checked='1'" )  ?> name="m_baja">
                </label>
            </th>
            <th>
                <label style="width: auto">Galeria de imagenes
                    <input  type="checkbox" <?php echo ( $sf_user->getAttribute('g_imagenes') == 'none' ? '' : "checked='1'" )  ?> name="g_imagenes">
                </label>
            </th>
            <th>
                <a id="second_line_1" >Marcar</a>
                 /
                <a id="second_line_0" >Desmarcar</a>
            </th>
          </thead>
        <tfoot>
            <tr>
                <td colspan="9"> <button><?php echo __('Save')?></button>
                </td>
            </tr>
        </tfoot>
    </table>
 </form>  
</div>

  <div id="buscador">

      <center>
          <button id="show" style="background:#F1F3F5;color:#000000;font-size: 16px;width:600px"><?php echo __('Busqueda Avanzada')?></button>
          <br/>
          <br/>
          <div id="serach_bui" style="display: none">
               <?php include_partial('total_objeto/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
            </div>
      </center>
  </div>

  <div id="sf_admin_content">
    <form action="<?php echo url_for('total_objeto_collection', array('action' => 'batch')) ?>" method="post">
    <?php include_partial('total_objeto/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('total_objeto/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('total_objeto/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('total_objeto/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
  <script type="text/javascript">
      $("#show").click(function () {
        if ($('#serach_bui').css('display') == "none") {
          $('#serach_bui').slideToggle(800);
          $("#show").html("<?php echo __('Ocultar Busqueda')?>");
        } else {
          $("#show").html("<?php echo __('Busqueda Avanzada')?>");
          $('#serach_bui').hide(800);
        }
      });

      $("#column").click(function () {
        if ($('#datos_columnas').css('display') == "none") {
          $('#datos_columnas').slideToggle(800);
          $("#column").html("<?php echo __('Ocultar')?>");
        } else {
          $("#column").html("<?php echo __('Configurar')?>");
          $('#datos_columnas').hide(800);
        }

      });
</script>
<script type="text/javascript">
      $('#third_line_0').click( function () {
        });
      $('#third_line_1').click(function () {
        });

      $('#firts_line_0').click( function () {
         document.busquedas.elements['o_N_inv'].checked=0
         document.busquedas.elements['o_nombre'].checked=0
         document.busquedas.elements['autor'].checked=0
         document.busquedas.elements['ubicacion'].checked=0
         document.busquedas.elements['descripcion'].checked=0
         document.busquedas.elements['f_ingreso'].checked=0
         document.busquedas.elements['d_ingreso'].checked=0
         document.busquedas.elements['donacion'].checked=0
         document.busquedas.elements['a_origen'].checked=0
         document.busquedas.elements['t_antiguedad'].checked=0
        });
      $('#firts_line_1').click(function () {
         document.busquedas.elements['o_N_inv'].checked=1
         document.busquedas.elements['o_nombre'].checked=1
         document.busquedas.elements['autor'].checked=1
         document.busquedas.elements['ubicacion'].checked=1
         document.busquedas.elements['descripcion'].checked=1
         document.busquedas.elements['f_ingreso'].checked=1
         document.busquedas.elements['d_ingreso'].checked=1
         document.busquedas.elements['donacion'].checked=1
         document.busquedas.elements['a_origen'].checked=1
         document.busquedas.elements['t_antiguedad'].checked=1
        });
      $('#second_line_0').click( function () {
         document.busquedas.elements['historia'].checked=0
         document.busquedas.elements['localidad'].checked=0
         document.busquedas.elements['colecciones'].checked=0
         document.busquedas.elements['materiales'].checked=0
         document.busquedas.elements['tasacion'].checked=0
         document.busquedas.elements['t_unidad'].checked=0
         document.busquedas.elements['t_fuente'].checked=0
         document.busquedas.elements['f_baja'].checked=0
         document.busquedas.elements['m_baja'].checked=0
         document.busquedas.elements['g_imagenes'].checked=0
        });
      $('#second_line_1').click(function () {
         document.busquedas.elements['historia'].checked=1
         document.busquedas.elements['localidad'].checked=1
         document.busquedas.elements['colecciones'].checked=1
         document.busquedas.elements['materiales'].checked=1
         document.busquedas.elements['tasacion'].checked=1
         document.busquedas.elements['t_unidad'].checked=1
         document.busquedas.elements['t_fuente'].checked=1
         document.busquedas.elements['f_baja'].checked=1
         document.busquedas.elements['m_baja'].checked=1
         document.busquedas.elements['g_imagenes'].checked=1
        });

</script>