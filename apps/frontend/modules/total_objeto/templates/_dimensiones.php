<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_imagenes2">
<div id="dato_dimension">
 <?php if (!$form->isNew()):?>
 
<table  class="sf_admin_list" width="100%">
                    <tr>
                        <th>Dimensión</th>
                        <th>valor</th>
                        <th>unidad</th>
                        <th>Acción</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($form->getObject()->getObjetoDimension() as $dim): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                       <td><?php echo $dim->getDimension() ?></td>
                       <td><?php echo $dim->getValor()?></td>
                      <td><?php echo $dim->getUnidad()?></td>
                      <td>
                            <?php echo jq_link_to_remote(image_tag('edit16.png', array('alt' => 'Agregar', 'title' =>'Modificar')), array(
                                        'update' => 'dato_dimension',
                                        'url' => 'objeto/cargarDimension?dimension_id='.$dim->getId().'&objeto_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  )); ?>
                         <?php echo jq_link_to_remote(image_tag('cross.png', array('alt_title' => 'Eliminar dimension', 'height' => 20)), array(
                                        'update' => 'dato_dimension',
                                        'url' => 'objeto/deleteDimension?id='.$dim->getId(),
                                        'script' => 'true',
                                        'confirm' => '¿Esta usted seguro que desea eliminar esta dimension?'
                                                  )); ?>
                      </td>

                   </tr>
                <?php endforeach; ?>

</table>
    <center>
    <?php echo jq_link_to_remote('Nueva Dimensión', array(
                                        'update' => 'dato_dimension',
                                        'url' => 'objeto/cargarDimension?objeto_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  ),
                                    array('class'=> 'button')); ?>



    </center>
<?php endif?>
</div>

</div>