

<td class="sf_admin_text sf_admin_list_td_imagen" style="display:<?php echo $sf_user->getAttribute('imagen')?>">
  <?php echo get_partial('total_objeto/imagen', array('type' => 'list', 'objeto' => $objeto)) ?>
</td>


<td class="sf_admin_text sf_admin_list_td_id" style="display:<?php echo $sf_user->getAttribute('o_N_inv')?>">
  <?php echo link_to($objeto->getId(), 'total_objeto_edit', $objeto) ?>
</td>
<td id="nombre_objeto" class="sf_admin_text sf_admin_list_td_nombre_objeto" style="display:<?php echo $sf_user->getAttribute('o_nombre')?>" >
  <?php echo link_to($objeto->getNombreObjeto(), 'total_objeto_edit', $objeto) ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_autor_id" style="display:<?php echo $sf_user->getAttribute('autor')?>">
  <?php echo $objeto->getAutor() ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_ubicacion_id" style="display:<?php echo $sf_user->getAttribute('ubicacion')?>">
  <?php echo $objeto->getUbicacion() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_descripcion_generica" style="display:<?php echo $sf_user->getAttribute('descripcion')?>">
  <?php echo $objeto->getDescripcionGenerica() ?>
</td>


<td class="sf_admin_text sf_admin_list_td_forma_ingreso" style="display:<?php echo $sf_user->getAttribute('f_ingreso')?>" >
  <?php echo $objeto->getFormaIngreso() ?>
</td>
<td class="sf_admin_date sf_admin_list_td_fecha_ingreso" style="display:<?php echo $sf_user->getAttribute('d_ingreso')?>" >
  <?php echo false !== strtotime($objeto->getFechaIngreso()) ? format_date($objeto->getFechaIngreso(), "d/m/y") : '&nbsp;' ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_donacion_id" style="display:<?php echo $sf_user->getAttribute('donacion')?>">
  <?php echo $objeto->getDonacionId() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_anio_origen" style="display:<?php echo $sf_user->getAttribute('a_origen')?>">
  <?php echo $objeto->getAnioOrigen() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_texto_antiguedad" style="display:<?php echo $sf_user->getAttribute('t_antiguedad')?>">
  <?php echo $objeto->getTextoAntiguedad() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_historia_del_objeto"style="display:<?php echo $sf_user->getAttribute('historia')?>">
  <?php echo $objeto->getHistoriaDelObjeto() ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_localidad_id" style="display:<?php echo $sf_user->getAttribute('localidad')?>">
  <?php echo $objeto->getLocalidad()->getProcedencia() ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_coleccion" style="display:<?php echo $sf_user->getAttribute('colecciones')?>">
   <?php echo get_partial('total_objeto/colecciones', array('type' => 'list', 'objeto' => $objeto)) ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_coleccion" style="display:<?php echo $sf_user->getAttribute('materiales')?>">
   <?php echo get_partial('total_objeto/materiales', array('type' => 'list', 'objeto' => $objeto)) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_tasacion_valor" style="display:<?php echo $sf_user->getAttribute('tasacion')?>">
  <?php echo $objeto->getTasacionValor() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_tasacion_unidades" style="display:<?php echo $sf_user->getAttribute('t_unidad')?>">
  <?php echo $objeto->getTasacionUnidades() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_tasacion_fuente" style="display:<?php echo $sf_user->getAttribute('t_fuente')?>">
  <?php echo $objeto->getTasacionFuente() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_estado_de_conservacion"style="display:<?php echo $sf_user->getAttribute('estado')?>" >
  <?php echo $objeto->getEstadoDeConservacion() ?>
</td>

<td class="sf_admin_date sf_admin_list_td_fecha_baja" style="display:<?php echo $sf_user->getAttribute('f_baja')?>" >
  <?php echo false !== strtotime($objeto->getFechaBaja()) ? format_date($objeto->getFechaBaja(), "f") : '&nbsp;' ?>
</td>
<td class="sf_admin_text sf_admin_list_td_motivo_baja" style="display:<?php echo $sf_user->getAttribute('m_baja')?>">
  <?php echo $objeto->getMotivoBaja() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_galeria_de_imagenes" style="display:<?php echo $sf_user->getAttribute('g_imagenes')?>">
  <?php echo $objeto->getGaleriaDeImagenes() ?>
</td>

