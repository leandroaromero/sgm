<?php

require_once dirname(__FILE__).'/../lib/total_objetoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/total_objetoGeneratorHelper.class.php';
//require_once sfConfig::get('sf_plugins_dir').'/sfTCPDFPlugin/lib//tcpdf/tcpdf.php';
//require_once sfConfig::get('sf_plugins_dir').'/sfTCPDFPlugin/lib/tcpdf/config/lang/eng.php';
/**
 * total_objeto actions.
 *
 * @package    museo
 * @subpackage total_objeto
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class total_objetoActions extends autoTotal_objetoActions
{

 public function executeListShow($request){
		$this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($request->getParameter('id'))), sprintf('Esta persona no existe (%s).', $request->getParameter('id')));
		
		}
    public function executeListRColecciones(){
        $this->reportes();
        $this->redirect('@total_objeto');
    }
    public function executeListRMateriales(){
        $this->reportesMateriales();
        $this->redirect('@total_objeto');
    }
public function executeListRIngreso(){
        $this->reportesIngresos();
        $this->redirect('@total_objeto');
    }
  protected function getPagerDos()
  {
    $pager = $this->configuration->getPager('Objeto');
    $pager->setQuery($this->buildQuery());
    $pager->setPage($this->getPage());
   // $pager->init();

    return $pager;
  }

       // Load table data from file
    public function LoadData() {    
    $data = array();
    $pagers = $this->getPagerDos();
        foreach($pagers->getResults() as $Objeto) {

            $fecha= date("d/m/Y", strtotime($Objeto->getFechaIngreso()));
            $line=$Objeto->getNombreObjeto().';'.$Objeto->getUbicacion().';'.$Objeto->getEstadoDeConservacion().';'.$fecha;
            $data[] = explode(';', chop($line));
        }
        return $data;
    }

public function reportes(){

    $array= array();
    $array= $this->getFilters();//$request->getRequestParameter('objeto_filters[coleccion_list]'); //$this->configuration->getFilterForm($this->getFilters());// $this->getRequestParameter('objeto_filters_material_list');
    if(!$this->getFilters() || !$array['coleccion_list']){
      $this->getUser()->setFlash('error', 'Debe filtar por COLECCION primero, para generar su reporte.'); 
      $this->redirect('@total_objeto');
    }
    $subtitulo='';
    if($array['coleccion_list']){
    $subtitulo=' ( ';
    foreach ($array['coleccion_list'] as $ar){
        $cole = Doctrine::getTable('Coleccion')->find($ar);
     $subtitulo=$subtitulo.' '.$cole.'  ';
    }$subtitulo=$subtitulo.' )';

    }

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$fechaE=date('d/m/Y');
  // settings
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('administrador');
$pdf->SetTitle('Museo del Hombre Chaqueño');
$pdf->SetSubject('Lista de Acervo');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING."Discriminados por colección$subtitulo\nLista emitida el $fechaE");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  // init pdf doc

  $pdf->AddPage();

  $pdf->SetFont('helvetica', '', 8);

// add a page

//Column titles
$header = array('Nombre', 'Ubicación', 'Estado', 'Altas del periodo' );

//Data loading
$data= $this->LoadData();
// print colored table
$pdf->ColoredTable($header, $data);

  $pdf->Output();

 }
public function reportesMateriales(){

    $array= array();
    $array= $this->getFilters();//$request->getRequestParameter('objeto_filters[coleccion_list]'); //$this->configuration->getFilterForm($this->getFilters());// $this->getRequestParameter('objeto_filters_material_list');
    if(!$this->getFilters() || !$array['material_list']){
      $this->getUser()->setFlash('error', 'Debe filtar por MATERIALES primero, para generar su reporte.'); 
      $this->redirect('@total_objeto');
    }
    $subtitulo='';
    if($array['material_list']){
        $subtitulo=' ( ';
    foreach ($array['material_list'] as $ar){
        $cole = Doctrine::getTable('Material')->find($ar);
     $subtitulo=$subtitulo.' '.$cole.' ';
     } $subtitulo=$subtitulo.' ) ';
    }

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  // settings
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('administrador');
$pdf->SetTitle('Museo del Hombre Chaqueño');
$pdf->SetSubject('Lista de Acervo');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$fechaE=date('d/m/Y');
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING."Discriminados por material$subtitulo\nLista emitida el $fechaE");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  // init pdf doc

  $pdf->AddPage();

  $pdf->SetFont('helvetica', '', 8);

// add a page

//Column titles
$header = array('Nombre', 'Ubicación', 'Estado', 'Altas del periodo' );

//Data loading
$data= $this->LoadData();
// print colored table
$pdf->ColoredTable($header, $data);

  $pdf->Output();

 }

 /////////////////////////////////////////
 public function reportesIngresos(){

    $array= array();
    $array= $this->getFilters();//$request->getRequestParameter('objeto_filters[coleccion_list]'); //$this->configuration->getFilterForm($this->getFilters());// $this->getRequestParameter('objeto_filters_material_list');

    $subtitulo='  (  ';
    $value2 ='';
    $value3 ='';
       if ($array['fecha_ingreso']['from'])
      {
           $value2 =$fecha= date("d/m/Y", strtotime($array['fecha_ingreso']['from']));
      }
      if ($array['fecha_ingreso']['to'])
      {
           $value3 =$fecha= date("d/m/Y", strtotime($array['fecha_ingreso']['to']));
      }


    if($array['fecha_ingreso']['from']){
        $subtitulo=$subtitulo.''.$value2;
    if($array['fecha_ingreso']['to']){
          $subtitulo=$subtitulo.'  hasta  '.$value3;
    }
    }$subtitulo=$subtitulo.' )';

    

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  // settings
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('administrador');
$pdf->SetTitle('Museo del Hombre ChaqueÃ±o');
$pdf->SetSubject('Lista de Acervo');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$fechaE=date('d/m/Y');
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE,  PDF_HEADER_STRING."Discriminado por el periodo en el cual ingreso al museo$subtitulo\nLista emitida el $fechaE");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  // init pdf doc

  $pdf->AddPage();

  $pdf->SetFont('helvetica', '', 8);

// add a page

//Column titles
$header = array('Nombre', 'Ubicación', 'Estado', 'Altas del periodo' );

//Data loading
$data= $this->LoadData();
// print colored table
$pdf->ColoredTable($header, $data);


  $pdf->Output();

 }

 public function executeObjectView($resquest){


    if($resquest->getParameter('imagen')){
        $this->getUser()->setAttribute('imagen', '');
    }else{
     $this->getUser()->setAttribute('imagen', 'none');
    }
    if($resquest->getParameter('o_N_inv')){
        $this->getUser()->setAttribute('o_N_inv', '');
    }else{
     $this->getUser()->setAttribute('o_N_inv', 'none');
    }

    if($resquest->getParameter('o_N_inv_p')){
        $this->getUser()->setAttribute('o_N_inv_p', '');
    }else{
     $this->getUser()->setAttribute('o_N_inv_p', 'none');
    }
    
    if($resquest->getParameter('Nantiguo')){
        $this->getUser()->setAttribute('Nantiguo', '');
    }else{
     $this->getUser()->setAttribute('Nantiguo', 'none');
    }
   if($resquest->getParameter('o_nombre')){
        $this->getUser()->setAttribute('o_nombre', '');
    }else{
     $this->getUser()->setAttribute('o_nombre', 'none');
    }
 
    if($resquest->getParameter('autor')){
        $this->getUser()->setAttribute('autor', '');
    }else{
     $this->getUser()->setAttribute('autor', 'none');
    }

    if($resquest->getParameter('ubicacion')){
        $this->getUser()->setAttribute('ubicacion', '');
    }else{
     $this->getUser()->setAttribute('ubicacion', 'none');
    }

    if($resquest->getParameter('descripcion')){
        $this->getUser()->setAttribute('descripcion', '');
    }else{
     $this->getUser()->setAttribute('descripcion', 'none');
    }


     if($resquest->getParameter('f_ingreso')){
        $this->getUser()->setAttribute('f_ingreso', '');
    }else{
     $this->getUser()->setAttribute('f_ingreso', 'none');
    }

    if($resquest->getParameter('d_ingreso')){
        $this->getUser()->setAttribute('d_ingreso', '');
    }else{
     $this->getUser()->setAttribute('d_ingreso', 'none');
    }

    if($resquest->getParameter('donacion')){
        $this->getUser()->setAttribute('donacion', '');
    }else{
     $this->getUser()->setAttribute('donacion', 'none');
    }
   if($resquest->getParameter('a_origen')){
        $this->getUser()->setAttribute('a_origen', '');
    }else{
     $this->getUser()->setAttribute('a_origen', 'none');
    }

    if($resquest->getParameter('t_antiguedad')){
        $this->getUser()->setAttribute('t_antiguedad', '');
    }else{
     $this->getUser()->setAttribute('t_antiguedad', 'none');
    }

    if($resquest->getParameter('historia')){
        $this->getUser()->setAttribute('historia', '');
    }else{
     $this->getUser()->setAttribute('historia', 'none');
    }

    if($resquest->getParameter('localidad')){
        $this->getUser()->setAttribute('localidad', '');
    }else{
     $this->getUser()->setAttribute('localidad', 'none');
    }


     if($resquest->getParameter('colecciones')){
        $this->getUser()->setAttribute('colecciones', '');
    }else{
     $this->getUser()->setAttribute('colecciones', 'none');
    }

    if($resquest->getParameter('materiales')){
        $this->getUser()->setAttribute('materiales', '');
    }else{
     $this->getUser()->setAttribute('materiales', 'none');
    }

    if($resquest->getParameter('tasacion')){
        $this->getUser()->setAttribute('tasacion', '');
    }else{
     $this->getUser()->setAttribute('tasacion', 'none');
    }
   if($resquest->getParameter('t_unidad')){
        $this->getUser()->setAttribute('t_unidad', '');
    }else{
     $this->getUser()->setAttribute('t_unidad', 'none');
    }

    if($resquest->getParameter('t_fuente')){
        $this->getUser()->setAttribute('t_fuente', '');
    }else{
     $this->getUser()->setAttribute('t_fuente', 'none');
    }

    if($resquest->getParameter('f_baja')){
        $this->getUser()->setAttribute('f_baja', '');
    }else{
     $this->getUser()->setAttribute('f_baja', 'none');
    }
    if($resquest->getParameter('m_baja')){
        $this->getUser()->setAttribute('m_baja', '');
    }else{
     $this->getUser()->setAttribute('m_baja', 'none');
    }
    if($resquest->getParameter('g_imagenes')){
        $this->getUser()->setAttribute('g_imagenes', '');
    }else{
     $this->getUser()->setAttribute('g_imagenes', 'none');
    }

     $this->redirect('@total_objeto');
 }

}