<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_imagenes">
<div id="tipo_persona_1">
<?php
$id=$form->getObject()->getId();
$autor = Doctrine::getTable('Autor')->find($id);
$donante = Doctrine::getTable('Donante')->find($id);

?>
<center>
<table class="sf_admin_list" width="70%">
    <thead>
    <th>TIPO</th>
    <th>ESTADO</th>
    <th>ACCIÓN</th>
    </thead>
    <tr>
        <td>Autor</td>
        <td><?php if( $autor){
                    echo image_tag('tick');
              }?>
        </td>
        <td><?php if( $autor){
                    echo jq_link_to_remote(image_tag('cross.png', array('alt' => 'Delete', 'title' =>'Eliminar como autor')), array(
                                        'update' => 'tipo_persona_1',
                                        'url'    => 'persona/deleteAutor?a_id='.$id,
                                        'script' => 'true',
                                        'confirm'=> '¿Esta usted seguro que desea eliminarlo como autor?'
                                                  ));

              }else{
                   echo jq_link_to_remote(image_tag('success.png', array('alt' => 'Agregar', 'title' =>'Agregar como autor')), array(
                                        'update' => 'tipo_persona_1',
                                        'url'    => 'persona/addAutor?a_id='.$id,
                                        'script' => 'true',
                                                  ));
              }?>

        </td>
    </tr>
    <tr>
        <td>Donante</td>
        <td><?php if( $donante ){
              echo image_tag('tick');
             } ?>
        </td>
        <td><?php if( $donante){
             echo jq_link_to_remote(image_tag('cross.png', array('alt' => 'Delete', 'title' =>'Eliminar como donante')), array(
                                        'update' => 'tipo_persona_1',
                                        'url'    => 'persona/deleteDonante?d_id='.$id,
                                        'script' => 'true',
                                        'confirm'=> '¿Esta usted seguro que desea eliminarlo como donante?'
                                                  ));

              }else{
                   echo jq_link_to_remote(image_tag('success.png', array('alt' => 'Agregar', 'title' =>'Agregar como donante')), array(
                                        'update' => 'tipo_persona_1',
                                        'url'    => 'persona/addDonante?d_id='.$id,
                                        'script' => 'true',
                                                  ));
              }?></td>
    </tr>
</table>
</center>
</div>
</div>