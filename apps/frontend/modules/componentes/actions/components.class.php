<?php

/**
 * localizaciones actions.
 *
 * @package    ruv
 * @subpackage localizaciones
 * @author     'Juan Carlos Fernández'
 * @version    SVN: $Id: actions.class.php 9301 2008-05-27 01:08:46Z dwhittle $
 */
class componentesComponents extends sfComponents
{

/*
 * 
 * Nombre: executeTelefonos ;
 * Tipo: public;
 * Desarrollador: Leandro Romero <leandroaromero@hotmail.com> - RUV;
 * Fecha creación: 2009-10-28;
 * @Parámetros: $persona;(ya sea un desaprecido, un exdetenido)
 * @Resultados: devuelve una plantilla de telefonos;
 * @Descripción: es un elto componente que puede ser usado para mostrar los telefonos de una persona;
 */

  public function executeTelefonos()
  {  
	 $this->form=$this->form;
  	 //$this->arreglo = strtolower(str_ireplace('lib.model.', '',$this->objeto->getPeer()->getOMClass()));
  }
  
  /*
 * 
 * Nombre: executeEmail ;
 * Tipo: public;
 * Desarrollador: Leandro Romero <leandroaromero@hotmail.com> - RUV;
 * Fecha creación: 2011-06-03;
 * @Parámetros: 
 * @Resultados: 
 * @Descripción:
 */
  public function executeEmail()
  {
     $this->form=$this->objeto;
  }
}
