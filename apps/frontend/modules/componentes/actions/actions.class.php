<?php

/**
 * componentes actions.
 *
 * @package    museo
 * @subpackage componentes
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class componentesActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeCambiar(sfWebRequest $request)
  {   
    $cambio= $request->getParameter('institucion');
    $this->getUser()->setAttribute('base_dato', $cambio['cambio']);
    $this->redirect('@homepage');
    
  }
  public function executeCambiarLogo(sfWebRequest $request)
  {   
    $cambio= $request->getParameter('institucion');
    $this->getUser()->setAttribute('base_dato', $cambio['cambio']);
    
  }
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }
  public function  executeDeleteTelefono($request) {
        $this->forward404Unless($telefono = Doctrine::getTable('Telefono')->find(array($request->getParameter('id'))), sprintf('Este telefono no exite (%s).', $request->getParameter('id')));
        $id= $telefono->getPersonaId();
        $telefono->delete();
        $this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
        $this->form= new PersonaForm($this->persona);
        $this->setTemplate('telefonos');


    }
  public function executeCargarTelefono($request)
  {
      $t_id = $this->getRequestParameter('telefono[id]');
      $this->p_id = $this->getRequestParameter('persona_id');
      if($t_id >0){
          $this->forward404Unless($this->telefono = Doctrine::getTable('Telefono')->find(array($t_id)), sprintf('Este telefono no exite (%s).', $t_id));
      }else{
             $this->telefono= new Telefono();
             $this->telefono->setPersonaId($this->p_id);
           }
      $this->form= new TelefonoForm($this->telefono);
  }
 public function executeGrabarTelefono(sfWebRequest $request)
  {    
         $t_id= $request->getParameter('t_id');
         $arr=array();
         $arr=$request->getParameter('telefono');
     if( $t_id > 0){
         $this->forward404Unless($telefono = Doctrine::getTable('Telefono')->find(array($t_id)), sprintf('Object telefono does not exist (%s).', $t_id));
     }else{
         $telefono = new Telefono();
         $telefono->setPersonaId($arr['persona_id']);
     }
     $telefono->setTipoTelefono($arr['tipo_telefono']);
     $telefono->setNumeroTelefono($arr['numero_telefono']);
     $telefono->save();
     $id= $telefono->getPersonaId();
      $this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
      $this->form= new PersonaForm($this->persona);
      $this->setTemplate('telefonos');


  }

 public function executeCancelarTelefono(sfWebRequest $request)
  {
         $id= $request->getParameter('id');
         $this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
         $this->form= new PersonaForm($this->persona);
         $this->setTemplate('telefonos');
  }
  public function executeCancelarEmail(sfWebRequest $request)
  {
         $id= $request->getParameter('id');
         $this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
         $this->form= new PersonaForm($this->persona);
         $this->setTemplate('email');
  }
 public function executeCargarEmail($request)
  {
      $e_id = $this->getRequestParameter('email[id]');
      $this->p_id = $this->getRequestParameter('persona_id');
      if($e_id >0){
          $this->forward404Unless($this->email = Doctrine::getTable('Email')->find(array($e_id)), sprintf('Este telefono no exite (%s).', $e_id));
      }else{
          $this->email= new Email();
          $this->email->setPersonaId($this->p_id);
           }
        $this->form= new EmailForm($this->email);
  }
 public function executeGrabarEmail(sfWebRequest $request)
  {
         $e_id= $request->getParameter('e_id');
         $arr=array();
         $arr=$request->getParameter('email');
     if( $e_id > 0){
         $this->forward404Unless($email = Doctrine::getTable('Email')->find(array($e_id)), sprintf('Object email does not exist (%s).', $e_id));
     }else{
         $email = new Email();
         $email->setPersonaId($arr['persona_id']);
     }
     $email->setDireccionEmail($arr['direccion_email']);
     $email->save();
     $id= $email->getPersonaId();
      $this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
      $this->form= new PersonaForm($this->persona);
      $this->setTemplate('email');
 }
  public function  executeDeleteEmail($request) {
        $ema=$request->getParameter('id');
        $this->forward404Unless($email = Doctrine::getTable('Email')->find(array($ema)), sprintf('Este email no exite (%s).', $ema));
        $id= $email->getPersonaId();
        $email->delete();
       $this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
        $this->form= new PersonaForm($this->persona);
        $this->setTemplate('email');
    }

}
