<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_imagenes">
<div id="dato_imagen">
 <?php if ($form->isNew()):?>
 <br/>
 <p style="color:red"> Las imagenes no estará habilitado hasta que grabe los cambios</p>
 <br>
 <?php else: ?>
<br/>
<table  class="sf_admin_list" width="100%">
                    <tr>
                        <th>Imagen</th>
                        <th>Título</th>
                        <th>Descripción</th>
                        <th>Acción</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($form->getObject()->getPictures() as $picture): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                       <td>   <?php
           if($picture->getFile()){
                
          echo image_tag('../uploads//museo/thumbs/'.$picture->getFile());
          }else{
			  echo image_tag('../images/noDisponible.jpg');
         } ?> </td>
                       <td><?php echo $picture->getTitle()?></td>
                      <td><?php echo $picture->getDescription()?></td>
                      <td>
                           <a href="<?php echo url_for('institution_picture/edit?id='.$picture->getId()) ?>"><?php echo image_tag('../images/edit16.png',array('title'=>'Editar la imagen','border'=>'0'));?></a>
                         <?php  link_to(image_tag('cross.png', array('alt_title' => 'Eliminar imagen', 'height' => 20)), 'institution_picture/delete?id='.$picture->getId(), array('method' => 'delete', 'confirm' => '¿Esta usted seguro que desea eliminar esta imagen?')) ?>
                          
                      </td>

                   </tr>
                <?php endforeach; ?>

</table>

<center><a class="button" href="<?php echo url_for('institution_picture/new?museo_id='.$form->getObject()->getId()) ?>"> Nueva Imagen </a></center>
<?php endif?>
</div>

</div>