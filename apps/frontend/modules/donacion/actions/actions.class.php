<?php

require_once dirname(__FILE__).'/../lib/donacionGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/donacionGeneratorHelper.class.php';

/**
 * donacion actions.
 *
 * @package    museo
 * @subpackage donacion
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class donacionActions extends autoDonacionActions
{
    public function executeGetDonantes($request)
	{
	    $this->getResponse()->setContentType('application/json');

	    $donante = Doctrine::getTable('Donante')->retrieveForSelect(
	                $request->getParameter('q'),
	                $request->getParameter('limit')
	    );

	    return $this->renderText(json_encode($donante));
	}
     public function executeListShow($request){
		$this->forward404Unless($this->donacion = Doctrine::getTable('Donacion')->find(array($request->getParameter('id'))), sprintf('Esta persona no existe (%s).', $request->getParameter('id')));
}
}
