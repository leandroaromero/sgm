<ul class="sf_admin_actions">
<?php if ($form->isNew()): ?>
  <li class="sf_admin_action_terminar">

  <?php echo link_to(__('Cancelar', array(), 'messages'), 'investigacion_object/ListTerminar?objeto_id='.$investigacion->getObjetoId()) ?>

  </li>
  <?php echo $helper->linkToSave($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Save',)) ?>
<?php else: ?>
  <?php echo $helper->linkToDelete($form->getObject(), array(  'params' =>   array(  ),  'confirm' => 'Are you sure?',  'class_suffix' => 'delete',  'label' => 'Delete',)) ?>
  <li class="sf_admin_action_terminar">
<?php if (method_exists($helper, 'linkToTerminar')): ?>
  <?php echo $helper->linkToTerminar($form->getObject(), array(  'label' => 'Terminar',  'params' =>   array(  ),  'class_suffix' => 'terminar',)) ?>
<?php else: ?>
  <?php echo link_to(__('Terminar', array(), 'messages'), 'investigacion_object/ListTerminar?id='.$investigacion->getId(), array()) ?>
<?php endif; ?>
  </li>
  <?php echo $helper->linkToSave($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Save',)) ?>
<?php endif; ?>
</ul>
