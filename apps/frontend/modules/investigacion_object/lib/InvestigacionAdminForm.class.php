<?php

/**
 * Investigacion form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvestigacionAdminForm extends BaseInvestigacionForm
{
  public function configure()
  {
     $this->validatorSchema['created_at']= new sfValidatorDateTime(array('required'=>false) );
      $this->validatorSchema['updated_at']= new sfValidatorDateTime(array('required'=>false) );
      sfProjectConfiguration::getActive()->loadHelpers('Url');
      $rango = range(1800, 2030);
      $arreglo_rango = array_combine($rango, $rango);
      $user= sfContext::getInstance()->getUser()->getGuardUser()->getId();
      $this->widgetSchema['created_by']= new sfWidgetFormInputHidden();
      if ($this->isNew()){
        $this->getObject()->setCreatedBy( $user);
      }
      $this->widgetSchema['objeto_id']= new sfWidgetFormInputHidden();
      $this->widgetSchema['investigadores_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['investigadores_list']->setOption('table_method', 'getOrderNombre');
        
      $web = sfConfig::get('app_url_web');  
      $this->widgetSchema['fecha'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
       monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
       buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           'years' => $arreglo_rango,
           ))
        ));
  }
}
