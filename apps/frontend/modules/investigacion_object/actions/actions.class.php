<?php

require_once dirname(__FILE__).'/../lib/investigacion_objectGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/investigacion_objectGeneratorHelper.class.php';

/**
 * investigacion_object actions.
 *
 * @package    museo
 * @subpackage investigacion_object
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class investigacion_objectActions extends autoInvestigacion_objectActions
{
  public function executeNew(sfWebRequest $request)
  {
    $this->investigacion = new Investigacion();
    $this->investigacion->setObjetoId($request->getParameter('obj_id'));
    $this->form = new InvestigacionAdminForm($this->investigacion);
  }
   public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));
    $obj_id= $this->getRoute()->getObject()->getObjetoId();
    if ($this->getRoute()->getObject()->delete())
    {
      $this->getUser()->setFlash('notice', 'Se ha eliminado la investigacion');
    }

    $this->redirect('objeto/edit?id='.$obj_id);
  }
  public function executeEditObject(sfWebRequest $request)
  {
    $obj_id= $this->getRoute()->getObject()->getObjetoId();
    $this->redirect('objeto/edit?id='.$obj_id);
  }
   public function executeListTerminar(sfWebRequest $request)
  {
    $obj_id = $request->getParameter('objeto_id');
    if (! $obj_id > 0){
         $obj_id= $this->getRoute()->getObject()->getObjetoId();
    }
    $this->redirect('objeto/edit?id='.$obj_id);
  }
  


}
