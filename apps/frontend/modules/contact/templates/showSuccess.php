
<?php use_helper('jQuery'); ?>

<table>
    <tr>
    	<td colspan="1">Correo</td>
		<td colspan="4"></td>
    </tr>
    <tr>
    	<td colspan="3"></td>
		<td>Fecha</td>
		<td><?php echo $contact->getCreated_at(); ?></td>
		
    </tr>
	<tr>
		<td> </td>
		<td>Nombre: </td>
		<td><?php echo $contact->getName(); ?></td>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td></td>
		<td>Correo: </td>
		<td><?php echo $contact->getEmail(); ?></td>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td></td>
		<td>Institucion: </td>
		<td><?php echo $contact->getInstitution(); ?></td>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td></td>
		<td>Mensaje: </td>
		<td><?php echo $contact->getComment(); ?></td>
		<td colspan="2"></td>
	</tr>
</table>

<div id="formulario">
<form>

          <?php echo $form->renderHiddenFields(false) ?>                 
<table>
	<tr>
	    <td></td>
		<td colspan="2">Detallar observaciones del contacto</td>
	</tr>
	<tr>
		<td></td>
		<td>
		     <?php echo $form['detail']->renderError() ?>
             <?php echo $form['detail'] ?>                  	
    	</td>		
    	<td> <?php echo jq_submit_to_remote(  '' , 'Enviar',
            $options = array(
              'update' => 'formulario',
              'url'    => 'contact/observation?c_id='.$contact->getId(),
                  ), 
        $options_html = array(
        ))?></td>
	</tr>
</table>
</form>
<table>
	<?php foreach ($observations->execute() as $key => $value): ?>
		<tr>
			<td><?php echo $value->getCreatedAt() ?></td>
			<td><?php echo $user = Doctrine::getTable('sfGuardUser')->find( $value->getCreatedBy())?></td>
			<td><?php echo $value->getDetail() ?></td>
		</tr>
	<?php endforeach?>
</table>


</div>