<?php

/**
 * Observation form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObservationAdminForm extends BaseObservationForm
{
  public function configure()
  {
  	unset($this['updated_at'],$this['updated_by']);
	$this->widgetSchema['contact_id'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['detail']->setAttributes(array(
          'style'=> 'width:550px; height: 100px'
      )); 


  	if(!$this->getObject()->isNew()){
      $this->widgetSchema['detail']->setAttribute('readonly', 'readonly'); 
      $this->widgetSchema['created_at']->setAttribute('readonly', 'readonly');  		
      $this->widgetSchema['created_by']->setAttribute('readonly', 'readonly');  		
  	  $this->widgetSchema['created_at']->setOption('label', 'Creado');
  	  $this->widgetSchema['created_by']->setOption('label', 'Autor');
  	}else{
  		unset($this['created_at'],$this['created_by']);
  	}
  	$this->widgetSchema['detail']->setOption('label', 'Detalle');

  }
}
