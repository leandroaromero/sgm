<?php

/**
 * Contact form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ContactAdminForm extends BaseContactForm
{
  public function configure()
  {
      unset($this['ip_address'],$this['updated_at'],$this['ip_report'],$this['is_spam_actived']);
       $user = sfContext::getInstance()->getUser();
        
        if (!$user->hasCredential('gerente')){    
          unset($this['institucion_id']);
        }
//      $this->widgetSchema['institucion_id']->setAttribute('readonly', 'readonly');
      $this->widgetSchema['email']->setAttribute('readonly', 'readonly');
      $this->widgetSchema['name']->setAttribute('readonly', 'readonly');
      $this->widgetSchema['comment']->setAttribute('readonly', 'readonly');
      $this->widgetSchema['created_at']->setAttribute('readonly', 'readonly');

    




      //Empotramos al menos un formulario de participantes
      $observations = $this->getObject()->getObservation();

      //Un formulario vacío hará de contenedor para todas las observaciones
      $observations_forms = new SfForm();
      $count = 0;
      foreach ($observations as $observation) {
              $obs_form = new ObservationAdminForm($observation);
              //Empotramos cada formulario en el contenedor
              $observations_forms->embedForm($count, $obs_form);
              $count ++;
            }

      $observation = new Observation();
      $observation->setContact($this->getObject());
      $obs_form = new ObservationAdminForm($observation);
      //Empotramos cada formulario en el contenedor
      $observations_forms->embedForm($count, $obs_form);
      //Empotramos el contenedor en el formulario principal
      $this->embedForm('lista_de_observaciones', $observations_forms);
  }
    public function saveEmbeddedForms($con = null, $forms = null)
    {
      if (null === $forms)
    {
      $observations = $this->getValue('lista_de_observaciones');
      $forms = $this->embeddedForms;
      foreach ($this->embeddedForms['lista_de_observaciones'] as $name => $form)
      {
          if (!isset($observations[$name]['detail']) || '' === trim($observations[$name]['detail']) )
          {
              unset($forms['lista_de_observaciones'][$name]);
          }
        }
      }
 
          return parent::saveEmbeddedForms($con, $forms);
      }

}
