<?php

require_once dirname(__FILE__).'/../lib/contactGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/contactGeneratorHelper.class.php';

/**
 * contact actions.
 *
 * @package    museo
 * @subpackage contact
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class contactActions extends autoContactActions
{
	public function executeShow($request)
  	{
  	 $this->forward404Unless($this->contact = Doctrine::getTable('Contact')->find(array($request->getParameter('id'))), sprintf('Esta contacto no existe (%s).', $request->getParameter('id')));
  	 $this->contact->setIsActive(1);
  	 $this->contact->save();	
  	 $observation = new Observation();
  	 $observation->setContact($this->contact);
  	 $this->form= new ObservationAdminForm($observation);

  	  $this->observations = Doctrine_Core::getTable('Observation')
      ->createQuery('a')
      ->orderBy('a.created_at Desc')
      ->where('a.contact_id = ?', $request->getParameter('id'));
   	}

   	public function executeObservation($request){
	   $this->form = new ObservationAdminForm();
	   $this->contact = Doctrine::getTable('Contact')->find($request->getParameter('c_id'));
 	   $this->processFormObservation($request, $this->form);
  	 
  	 	$observation = new Observation();
  	 	$observation->setContact($this->contact);
  	 	$this->form= new ObservationAdminForm($observation);
  	 	$this->observations = Doctrine_Core::getTable('Observation')
      						->createQuery('a')
					      	->orderBy('a.created_at Desc')
      						->where('a.contact_id = ?', $request->getParameter('c_id'));

   	}

  protected function processFormObservation(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $contact = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

      }
    }
  }
}
