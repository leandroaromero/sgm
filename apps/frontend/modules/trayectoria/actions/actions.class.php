<?php

require_once dirname(__FILE__).'/../lib/trayectoriaGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/trayectoriaGeneratorHelper.class.php';

/**
 * trayectoria actions.
 *
 * @package    museo
 * @subpackage trayectoria
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class trayectoriaActions extends autoTrayectoriaActions
{
     public function executeListShow($request){
		$this->forward404Unless($this->trayectoria = Doctrine::getTable('Trayectoria')->find(array($request->getParameter('id'))), sprintf('Esta persona no existe (%s).', $request->getParameter('id')));
		}
}
