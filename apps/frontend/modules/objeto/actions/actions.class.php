<?php

require_once dirname(__FILE__).'/../lib/objetoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/objetoGeneratorHelper.class.php';

/**
 * objeto actions.
 *
 * @package    museo
 * @subpackage objeto
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class objetoActions extends autoObjetoActions
{
  public function executeListShow($request)
  {
  	 $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($request->getParameter('id'))), sprintf('Esta persona no existe (%s).', $request->getParameter('id')));
		
   }
  public function executeCambiarVista($request)
  {
      if($this->getUser()->getAttribute('listado_cuadrado', false)== true){
          
            $this->getUser()->setAttribute('listado_cuadrado', false );
      }else{
          $this->getUser()->setAttribute('listado_cuadrado', true );
      }
         $this->forward('objeto', 'index');

   }
      
                
  public function executeCancelarCarga(sfWebRequest $request)
  {
         $id= $request->getParameter('o_id');
         $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object  does not exist (%s).', $id));
         $this->form= new ObjetoForm($this->objeto);
         $this->setTemplate($request->getParameter('template'));
   }

  public function executeCargarEstado($request)
  {
         $d_id = $this->getRequestParameter('estado_id');
         $this->objeto_id = $this->getRequestParameter('objeto_id');
         if($d_id > 0){
              $this->forward404Unless($this->estado = Doctrine::getTable('Estado')->find(array($d_id)), sprintf('Este estado no exite (%s).', $d_id));
          }else{
              $this->estado= new Estado();
              $this->estado->setObjetoId($this->objeto_id);
               }
        $this->form= new EstadoAdminForm($this->estado);
  }
  public function executeCargarInventario($request)
  {
      $d_id = $this->getRequestParameter('inventario_id');
      $this->objeto_id = $this->getRequestParameter('objeto_id');
      if($d_id > 0){
          $this->forward404Unless($this->otro_inventario = Doctrine::getTable('OtroInventario')->find(array($d_id)), sprintf('Este Inventario no exite (%s).', $d_id));
      }else{
          $this->otro_inventario= new OtroInventario();
          $this->otro_inventario->setObjetoId($this->objeto_id);
           }
        $this->form= new OtroInventarioAdminForm($this->otro_inventario);
  }
/*  
 public function executeCargarEvento($request)
  {
      $d_id = $this->getRequestParameter('evento_id');
      $this->objeto_id = $this->getRequestParameter('objeto_id');
      if($d_id > 0){
          $this->forward404Unless($this->trayectoria = Doctrine::getTable('Trayectoria')->find(array($d_id)), sprintf('Esta trayectoria no exite (%s).', $d_id));
      }else{
          $this->trayectoria= new Trayectoria();
  //        $this->trayectoria->setObjetoId($this->objeto_id);
           }
        $this->form= new TrayectoriaForm($this->trayectoria);
  }
 public function executeCancelarEvento(sfWebRequest $request)
  {
         $id= $request->getParameter('o_id');
         $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object  does not exist (%s).', $id));
         $this->form= new ObjetoForm($this->objeto);
         $this->setTemplate('evento');
  }
  public function executeGrabarEvento(sfWebRequest $request)
  {
         $eve_id= $request->getParameter('eve_id');
         $arr=array();
         $arr=$request->getParameter('trayectoria');
     if( $eve_id > 0){
         $this->forward404Unless($odim = Doctrine::getTable('Trayectoria')->find(array($eve_id)), sprintf('Object email does not exist (%s).', $eve_id));
     }else{
         $odim = new Trayectoria();
        

     }
     $id= $request->getParameter('o_id');
     $odim->setObjetoId($id);
     $odim->setNombreEvento($arr['nombre_evento']);

      $dateFormat = new sfDateFormat($this->getUser()->getCulture());
                              if (!is_array($arr['fecha']))
          {
            $value = $dateFormat->format($arr['fechadesde'], 'i', $dateFormat->getInputPattern('d'));
          }
          else
          {
            $value_array = $arr['fecha'];
            $value = $value_array['year'].'-'.$value_array['month'].'-'.$value_array['day'].(isset($value_array['hour']) ? ' '.$value_array['hour'].':'.$value_array['minute'].(isset($value_array['second']) ? ':'.$value_array['second'] : '') : '');
          }
          $odim->setFecha($value);

     $odim->setLugar($arr['lugar']);
     $odim->setRelato($arr['relato']);
     $odim->setTipoEventoId($arr['tipo_evento_id']);
     $odim->setGaleriaDeImagenes($arr['galeria_de_imagenes']);
     $odim->save();
     
      $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
      $this->form= new ObjetoForm($this->objeto);
      $this->setTemplate('evento');
 }
  public function  executeDeleteEvento($request) {
        $this->forward404Unless($evento = Doctrine::getTable('Trayectoria')->find(array($request->getParameter('tray_id'))), sprintf('Object objeto_dimension does not exist (%s).', $request->getParameter('tray_id')));
        $id= $evento->getObjetoId();
        $evento->delete();
        $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
        $this->form= new ObjetoForm($this->objeto);
        $this->setTemplate('evento');
   }*/
  public function  executeDeleteEstado($request) {
        $this->forward404Unless($estado = Doctrine::getTable('Estado')->find(array($request->getParameter('e_id'))), sprintf('Object estado does not exist (%s).', $request->getParameter('id')));
        $id= $estado->getObjetoId();
        $estado->delete();
        $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
        $this->form= new ObjetoForm($this->objeto);
        $this->setTemplate('Estados');
   }
  public function  executeDeleteInventario($request) {
        $this->forward404Unless($otro_inventario = Doctrine::getTable('OtroInventario')->find(array($request->getParameter('e_id'))), sprintf('Object inventario does not exist (%s).', $request->getParameter('id')));
        $id= $otro_inventario->getObjetoId();
        $otro_inventario->delete();
        $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
        $this->form= new ObjetoForm($this->objeto);
        $this->setTemplate('inventarios');
   }

 public function executeGrabarEstado(sfWebRequest $request)
  {
         $e_id= $request->getParameter('e_id');
         $arr=array();
         $arr=$request->getParameter('estado');
         if( $e_id > 0){
             $this->forward404Unless($odim = Doctrine::getTable('Estado')->find(array($e_id)), sprintf('Object estado does not exist (%s).', $e_id));
         }else{
             $odim = new Estado();
             $odim->setObjetoId($arr['objeto_id']);

         }
         $fecha = $arr['fecha_de_cambio']['year'].'-'.$arr['fecha_de_cambio']['month'].'-'.$arr['fecha_de_cambio']['day'];
         $odim->setEstadoDeConservacion($arr['estado_de_conservacion']);
         $odim->setFechaDeCambio( $fecha );
         $odim->setMotivoDelCambio($arr['motivo_del_cambio']);
         $odim->save();
     
         $id= $odim->getObjetoId();
         $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
         $this->form= new ObjetoForm($this->objeto);
         $this->setTemplate('estados');
 }
 public function executeGrabarInventario(sfWebRequest $request)
  {
         $e_id= $request->getParameter('d_id');
         $arr=array();
         $arr=$request->getParameter('otro_inventario');
         if( $e_id > 0){
             $this->forward404Unless($odim = Doctrine::getTable('OtroInventario')->find(array($e_id)), sprintf('Object inventario does not exist (%s).', $e_id));
         }else{
             $odim = new OtroInventario();
             $odim->setObjetoId($arr['objeto_id']);

         }
         $odim->setTipoInventarioId($arr['tipo_inventario_id']);
         $odim->setNumero($arr['numero']);
         $odim->save();
         $id= $odim->getObjetoId();
         $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($id)), sprintf('Object telefono does not exist (%s).', $id));
         $this->form= new ObjetoForm($this->objeto);
         $this->setTemplate('inventarios');
 }

     public function executeGetAutores($request)
	{
	    $this->getResponse()->setContentType('application/json');

	    $autores = Doctrine::getTable('Autor')->retrieveForSelect(
	                $request->getParameter('q'),
	                $request->getParameter('limit')
	    );

	    return $this->renderText(json_encode($autores));
	}
    public function executeGetDonaciones($request)
	{
	    $this->getResponse()->setContentType('application/json');

	    $donacion = Doctrine::getTable('Donacion')->retrieveForSelect(
	                $request->getParameter('q'),
	                $request->getParameter('limit')
	    );

	    return $this->renderText(json_encode($donacion));
	}

    public function executeAddImaForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num"));

         if($card =    Doctrine::getTable('Objeto')->find($request->getParameter('id'))){ 
             $form = new ObjetoAdminForm($card);
             }else{
             $form = new ObjetoAdminForm(null);
         }

         $form->addImagenes($number);
         return $this->renderPartial('addIma',array('form' => $form, 'num' => $number));
      }    
        

    public function executeAddDetallesForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num"));

         if($card =    Doctrine::getTable('Objeto')->find($request->getParameter('id'))){ 
             $form = new ObjetoAdminForm($card);
             }else{
             $form = new ObjetoAdminForm(null);
         }

         $form->addDetalles($number);
         return $this->renderPartial('addDetalles',array('form' => $form, 'num' => $number));
      }    
        

    public function executeAddDimensionesForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num"));

         if($card =    Doctrine::getTable('Objeto')->find($request->getParameter('id'))){ 
             $form = new ObjetoAdminForm($card);
             }else{
             $form = new ObjetoAdminForm(null);
         }

         $form->addDimensiones($number);
         return $this->renderPartial('addDimensiones',array('form' => $form, 'num' => $number));
      }    
        

        
}
