<?php

/**
 * Objeto form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObjetoAdminForm extends BaseObjetoForm
{
   protected $scheduledForDeletion = array();
   protected $scheduledForDeletionDetalles = array();
   protected $scheduledForDeletionDimension = array();

    
 public function configure()
  {
     
      unset($this['dimension_list'],$this['updated_at'],$this['created_at'],$this['updated_by'],$this['created_by']);
      sfProjectConfiguration::getActive()->loadHelpers('Url');
      $rango = range(1800, 2040);
      $arreglo_rango = array_combine($rango, $rango);  
      $web = sfConfig::get('app_url_web');       

      $this->widgetSchema['coleccion_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['coleccion_list']->setOption('table_method', 'getOrderNombre');
      
      $this->widgetSchema['material_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['material_list']->setOption('table_method', 'getOrderNombre');
      
      $this->widgetSchema['categoria_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      //$this->widgetSchema['material_list']->setOption('table_method', 'getOrderNombre');
      
      $this->widgetSchema['trayectoria_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      $this->widgetSchema['trayectoria_list']->setOption('renderer_options', array(
                    'label_unassociated' => 'No Participo de:',
                    'label_associated' => 'Si Participo de:',
                ));
      $this->widgetSchema['trayectoria_list']->setOption('table_method', 'getOrderNombre');
      $this->widgetSchema['trayectoria_list']->setOption('label', 'Eventos');
     # $this->widgetSchema['trayectoria_list']->setAtribute('label_unassociated', 'No Participo');
      #$this->widgetSchema['trayectoria_list']->setOption('label_associated', 'Participo');

      $this->widgetSchema['autor_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['autor_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
          array(
            'model' => "Autor",
            'url'   => url_for("@ajax_autor"),
            'config' => '{ max: 30}'
            ));
        
      $this->widgetSchema['donacion_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['donacion_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
         array(
           'model' => "Donacion",
           'url'   => url_for("@ajax_donacion"),
           'config' => '{ max: 30}'
         ));


      $this->widgetSchema['fecha_ingreso'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           'years' => $arreglo_rango,
           ))
        ));
       $this->widgetSchema['fecha_baja'] = new sfWidgetFormJQueryDate(array(
       'image'  => '/museo/'.$web.'images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%day%/%month%/%year%',
           'years' => $arreglo_rango,
           ))
        ));
         
       $this->widgetSchema['ubicacion_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Ubicacion ',
              'model'   => 'Ubicacion',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
      
      $this->widgetSchema['localidad_id'] =  new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true));
      $this->widgetSchema['localidad_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['localidad_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
      array(
        'model' => "Localidad",
        'url'   => url_for("@ajax_origen"),
        'config' => '{ max: 30}',
        'method' => 'getProcedencia'
        ));
     
    $this->validatorSchema['localidad_id']=  new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'required' => false));



    //---------------------------------------------
    
        //Empotramos al menos un formulario de Imagenes
            $imagenes = $this->getObject()->getImagen();
            if (!$this->getObject()->getImagen()->count()){
                  $imagen = new Imagen();
                  $imagen->setObjeto($this->getObject());
                  $imagenes = array($imagen);
            }

            //Un formulario vacío hará de contenedor para todas las imagenes
            $imagenes_forms = new SfForm();
            $count = 0;
            foreach ($imagenes as $imagen) {
                    $ima_form = new ImagenAdminForm($imagen);
                
                    //Empotramos cada formulario en el contenedor
                    $imagenes_forms->embedForm($count, $ima_form);
                    $count ++;
            }
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('lista_imagenes', $imagenes_forms);
    //-----------------------------------------------------------------------------



    //---------------------------------------------
    
        //Registro Detalle Obra
            $registros = $this->getObject()->getRegistroDetalleObra();
            if (!$this->getObject()->getRegistroDetalleObra()->count()){
                  $registro = new RegistroDetalleObra();
                  $registro->setObjeto($this->getObject());                  
                  $registros = array($registro);
            }

            //Un formulario vacío hará de contenedor para todas las imagenes
            $registros_forms = new SfForm();
            $count = 0;            
            foreach ($registros as $registro) {

              $registro_form = new RegistroDetalleObraAdminForm($registro);                
              //Empotramos cada formulario en el contenedor
              $registros_forms->embedForm($count, $registro_form);
              $count++;
            }
            
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('detalles_de_obra', $registros_forms);
    //-----------------------------------------------------------------------------
    
        //Registro Dimensiones
            $objeto_dimensiones = $this->getObject()->getObjetoDimension();
            if (!$this->getObject()->getObjetoDimension()->count()){
                  $objeto_dimension = new ObjetoDimension();
                  $objeto_dimension->setObjeto($this->getObject());                  
                  $objeto_dimensiones = array($objeto_dimension);
            }

            //Un formulario vacío hará de contenedor para todos los objetos dimension
            $dimension_forms = new SfForm();
            $count = 0;            
            foreach ($objeto_dimensiones as $obj_dimension) {
              $dimension_form = new ObjetoDimensionAdminForm($obj_dimension);                
              //Empotramos cada formulario en el contenedor
              $dimension_forms->embedForm($count, $dimension_form);
              $count++;
            }
            
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('dimensiones', $dimension_forms);
    //-----------------------------------------------------------------------------



  }

    public function addDimensiones($num) 
     {
          $dimension = new ObjetoDimension();
          
          $dimension->setObjeto($this->getObject());
          
          $dimension_form = new ObjetoDimensionAdminForm($dimension);

          unset($dimension_form['objeto_id']);
          //Empotramos la nueva pícture en el contenedor
          $this->embeddedForms['dimensiones']->embedForm($num, $dimension_form);
          //Volvemos a empotrar el contenedor
          $this->embedForm('dimensiones', $this->embeddedForms['dimensiones']);

      }


  public function addDetalles($num) 
     {
          $detalle = new RegistroDetalleObra();
          
          $detalle->setObjeto($this->getObject());
          
          $detalle_form = new RegistroDetalleObraAdminForm($detalle);

          unset($detalle_form['objeto_id']);
          //Empotramos la nueva pícture en el contenedor
          $this->embeddedForms['detalles_de_obra']->embedForm($num, $detalle_form);
          //Volvemos a empotrar el contenedor
          $this->embedForm('detalles_de_obra', $this->embeddedForms['detalles_de_obra']);

      }


  public function addImagenes($num) 
     {

          $ima = new Imagen();
          
          $ima->setObjeto($this->getObject());
          
          $ima_form = new ImagenAdminForm($ima);

          unset($ima_form['objeto_id']);
          //Empotramos la nueva pícture en el contenedor
          $this->embeddedForms['lista_imagenes']->embedForm($num, $ima_form);
          //Volvemos a empotrar el contenedor
          $this->embedForm('lista_imagenes', $this->embeddedForms['lista_imagenes']);

         //---------------------------------------------------------------------
      }
     public function bind(array $taintedValues = null, array $taintedFiles = null)
        {
          
          foreach($taintedValues['lista_imagenes'] as $key=>$emailValues)
          {
            if (!isset($this['lista_imagenes'][$key]) )
            {
          
              $this->addImagenes($key);
          
            }
          }

          foreach($taintedValues['detalles_de_obra'] as $key=>$emailValues)
          {
            if (!isset($this['detalles_de_obra'][$key]) )
            {
          
              $this->addDetalles($key);
          
            }
          }

          foreach($taintedValues['dimensiones'] as $key=>$dimensionValues)
          {
            if (!isset($this['dimensiones'][$key]) )
            {
          
              $this->addDimensiones($key);
          
            }
          }
         
          parent::bind($taintedValues, $taintedFiles);
        
        }
  
     protected function doBind(array $values)
      {

          foreach ($values['lista_imagenes'] as $i => $emailValues)
          {
             if ('' === trim($emailValues['archivo']) &&'' === trim($emailValues['descripcion'])&&'' === trim($emailValues['titulo']) )
                {
                  unset($values['lista_imagenes'][$i]);
                }
            if (isset($emailValues['delete']) && $emailValues['id'])
            {
              $this->scheduledForDeletion[$i] = $emailValues['id'];
            }
          }

          foreach ($values['detalles_de_obra'] as $i => $emailValues)
          {
             if ('' === trim($emailValues['detalle_obra_id']) )
                {
                  unset($values['detalles_de_obra'][$i]);
                }
            if (isset($emailValues['delete']) && $emailValues['id'])
            {
              $this->scheduledForDeletionDetalles[$i] = $emailValues['id'];
            }
          }

          foreach ($values['dimensiones'] as $i => $dimensionValues)
          {
             if ('' === trim($dimensionValues['valor'])  && '' === trim($dimensionValues['id']))
                {
                  unset($values['dimensiones'][$i]);

                }
            if (isset($dimensionValues['delete']) && $dimensionValues['id'])
            {
              $this->scheduledForDeletionDimension[$i] = $dimensionValues['id'];
            }
          }

        parent::doBind($values);
    
      }


      protected function doUpdateObject($values)
      {

        if (count($this->scheduledForDeletion))
        {
          foreach ($this->scheduledForDeletion as $index => $id)
          {

            unset($values['lista_imagenes'][$index]);
            Doctrine::getTable('Imagen')->findOneById($id)->delete();

          }

        }
        if (count($this->scheduledForDeletionDetalles))
        {
          foreach ($this->scheduledForDeletionDetalles as $index => $id)
          {

            unset($values['detalles_de_obra'][$index]);
            Doctrine::getTable('RegistroDetalleObra')->findOneById($id)->delete();

          }

        }

        if (count($this->scheduledForDeletionDimension))
        {
          foreach ($this->scheduledForDeletionDimension as $index => $id)
          {

            unset($values['dimensiones'][$index]);
            Doctrine::getTable('ObjetoDimension')->findOneById($id)->delete();

          }

        }


        $this->getObject()->fromArray($values);

      }
  
  

//algo de basura
//      if($this->getObject()->isNew()){    
//          unset($this->widgetSchema['created_at']);
//          
//          $this->getObject()->setCreatedBy( $user);
//      }  else {
//          $this->widgetSchema['created_by']= new sfWidgetFormInputHidden();
//          $this->widgetSchema['created_at']= new sfWidgetFormInputHidden();
//      }
      
//      $this->validatorSchema['created_at']= new sfValidatorDateTime(array('required'=>false) );
//      $this->validatorSchema['updated_at']= new sfValidatorDateTime(array('required'=>false) );
}