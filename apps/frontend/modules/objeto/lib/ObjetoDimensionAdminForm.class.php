<?php

/**
 * ObjetoDimension form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObjetoDimensionAdminForm extends BaseObjetoDimensionForm
{
  public function configure()
  {
    unset($this['objeto_id']);

          $this->widgetSchema['dimension_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Dimensión',
              'model'   => 'Dimension',
              'table_method'=> 'getOrderNombre',
              'add_empty' => false,
            ));
                  
        $this->validatorSchema['valor'] = new sfValidatorPass();      
        //$this->validatorSchema['objeto_id'] = new sfValidatorPass();  
        $this->validatorSchema['dimension_id'] = new sfValidatorPass();     

        $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
        $this->validatorSchema['delete'] = new sfValidatorPass();         
            
  }
}
