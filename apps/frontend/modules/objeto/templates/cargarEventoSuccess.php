<?php use_helper('jQuery') ?>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('evento/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
                   &nbsp;
          <?php echo jq_link_to_remote('Cancelar', array(
                                        'update' => 'dato_evento',
                                        'url' => 'objeto/cancelarEvento?o_id='.$objeto_id,
                                        'script' => 'true',
                                                  )); ?>
         <?php  echo jq_submit_to_remote('agregar','Grabar', array(
          'update' => 'dato_evento',
          'url'    => 'objeto/grabarEvento?eve_id='.$form->getObject()->getId().'&o_id='.$objeto_id,
           ))
            ?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['nombre_evento']->renderLabel('Nombre del evento') ?></th>
        <td>
          <?php echo $form['nombre_evento']->renderError() ?>
          <?php echo $form['nombre_evento'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['fecha']->renderLabel('Fecha *') ?></th>
        <td>
          <?php echo $form['fecha']->renderError() ?>
          <?php echo $form['fecha'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['lugar']->renderLabel() ?></th>
        <td>
          <?php echo $form['lugar']->renderError() ?>
          <?php echo $form['lugar'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['relato']->renderLabel() ?></th>
        <td>
          <?php echo $form['relato']->renderError() ?>
          <?php echo $form['relato'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['tipo_evento_id']->renderLabel('Tipo de evento *') ?></th>
        <td>
          <?php echo $form['tipo_evento_id']->renderError() ?>
          <?php echo $form['tipo_evento_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['galeria_de_imagenes']->renderLabel('Galería de imagenes') ?></th>
        <td>
          <?php echo $form['galeria_de_imagenes']->renderError() ?>
          <?php echo $form['galeria_de_imagenes'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
