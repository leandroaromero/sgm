<?php use_helper('jQuery') ?>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<div id="dato_inventarios">
<form action="<?php echo url_for('objeto/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
          <td colspan="1" align="left">
          <?php echo $form->renderHiddenFields(false) ?>

            <?php 
                if(!$form->getObject()->isNew()){
                       echo jq_link_to_remote('Borrar', array(
                                        'update' => 'dato_inventarios',
                                        'url' => 'objeto/deleteInventario?e_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                        'confirm' => '¿Esta usted seguro que desea eliminar este Inventario?'
                                                  ));
                }  ?>
           &nbsp;
           &nbsp;
        </td>
        <td>
        

          <?php echo jq_link_to_remote('Cancelar', array(
                                        'update' => 'dato_inventarios',
                                        'url' => 'objeto/cancelarCarga?o_id='.$objeto_id.'&template=inventarios',
                                        'script' => 'true',
                                                  )); ?>

          <?php  echo jq_submit_to_remote('agregar','Grabar', array(
          'update' => 'dato_inventarios',
          'url'    => 'objeto/grabarInventario?d_id='.$form->getObject()->getId(),
           ))
            ?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['tipo_inventario_id']->renderLabel() ?>*</th>
        <td>
          <?php echo $form['tipo_inventario_id']->renderError() ?>
          <?php echo $form['tipo_inventario_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['numero']->renderLabel() ?>*</th>
        <td>
          <?php echo $form['numero']->renderError() ?>
          <?php echo $form['numero'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
</div>
