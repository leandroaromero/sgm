<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_estados">
<div id="dato_estado">
<?php if (!$form->isNew()):?>
 
<table  class="sf_admin_list" width="100%">
                    <tr>
                        <th>Estado de conservacion</th>
                        <th>Fecha</th>
                        <th>Motivo del cambio</th>
                        <th>Acción</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($form->getObject()->getEstado() as $es): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                       <td><?php echo $es->getEstadoDeConservacion() ?></td>
                       <td><?php echo $es->getFechaDeCambio() ?></td>
                       <td><?php echo $es->getMotivoDelCambio()?></td>
                      <td>
                            <?php echo jq_link_to_remote(image_tag('edit16.png', array('alt' => 'Agregar', 'title' =>'Modificar')), array(
                                        'update' => 'dato_estado',
                                        'url' => 'objeto/cargarEstado?estado_id='.$es->getId().'&objeto_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  )); ?>
                      </td>

                   </tr>
                <?php endforeach; ?>

</table>
    <center>
	
    <?php echo jq_link_to_remote('Nuevo Estado' , array(
                                        'update' => 'dato_estado',
                                        'url' => 'objeto/cargarEstado?objeto_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  ), array('class'=>'button')); ?>

    </center>
<?php else:?>
    <p style="color:red" >Los estados del acervos se podran cargar una vez grabado el acervos</p>
<?php endif?>
</div>

</div>
