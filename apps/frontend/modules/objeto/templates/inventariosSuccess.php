<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_estados">
<div id="dato_inventarios">
<?php if (!$form->isNew()):?>
 
<table  class="sf_admin_list" width="100%">
                    <tr>
                        <th>Inventario</th>
                        <th>Numero</th>
                        <th>Acción</th>
                    </tr>
                    
                <?php  $con=0; $i=1; foreach ($form->getObject()->getOtroInventario() as $in): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                       <td><?php echo $in->getTipoInventario() ?></td>
                       <td><?php echo $in->getNumero()?></td>
                      <td>
                            <?php echo jq_link_to_remote(image_tag('edit16.png', array('alt' => 'Agregar', 'title' =>'Modificar')), array(
                                        'update' => 'dato_inventarios',
                                        'url' => 'objeto/cargarInventario?inventario_id='.$in->getId().'&objeto_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  )); ?>
                      </td>

                   </tr>
                <?php endforeach; ?>

</table>
    <center>
	
    <?php echo jq_link_to_remote('Nuevo Inventario' , array(
                                        'update' => 'dato_inventarios',
                                        'url' => 'objeto/cargarInventario?objeto_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  ), array('class'=>'button')); ?>

    </center>
<?php else:?>
    <p style="color:red" >Los inventarios del acervos se podran cargar una vez grabado el acervos</p>
<?php endif?>
    <br/>
</div>

</div>
