<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="personas-group sf_admin_form_row sf_admin_text">
 <label class="imagenes-label" for="member_id"></label>
 <div class="controls">
<table style="width: 100%">
    <tr>
        <td>Borrar</td>
        <td>Imagen</td>        
        <td>Archivo</td>        
        <td>Descripción</td>
    </tr>
     <?php foreach ($form['lista_imagenes'] as $key=>$r ):?>
       <tr>              
         <td>
            <?php echo $r['id']->render(); ?>
             <?php echo $r['delete']->render(); ?></td>
         <td>
              <?php include_partial('objeto/digital', array( 'key' => $r['id']->getValue())) ?>
         </td>
         <td> 
            <?php if( $r['id']->getValue() > 0):?>
                <a onclick="window.open(this.href);return false;" href="<?php echo url_for('picture/edit?id='.$r['id']->getValue()) ?>">
                <?php echo image_tag('inbox.png');?> Ver objeto digital </a> </br></br>
            <?php endif?>    

             <?php echo $r['archivo']->renderError() ?>
             <?php echo $r['archivo']->render(); ?></td>
         <td>
             Título:<br/>
             <?php echo $r['titulo']->renderError() ?>
             <?php echo $r['titulo']->render(); ?>
         <br/>
             Descripción: <br/>  
             <?php echo $r['descripcion']->renderError() ?>
             <?php echo $r['descripcion']->render(); ?></td>
         
    </tr>    
    
    
 <?php endforeach;?>

    <tr><td></td><td></td></tr>

<tr>	
    <td colspan="2">
         <center>
              <button id="add_imagenes" type="button"><?php echo "Añadir otra imagen"?></button>
         </center> 	
    </td>
</tr>


 
    
    
</table>
     
     <tr>
	
    <div id="extra_imagenes"></div>
    
    
</tr>
    </div>
  </div>