<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_imagenes2">
<div id="dato_evento">
 <?php if (!$form->isNew()):?>
 
<table  class="sf_admin_list" width="100%">
                    <tr>
                        <th>Nombre del evento</th>
                        <th>Fecha</th>
                        <th>Lugar</th>
                        <th>Relato</th>
                        <th>tipo de evento</th>
                        <th>Acción</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($form->getObject()->getTrayectoria() as $tra): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                      <td><?php echo $tra->getNombreEvento() ?></td>
                      <td><?php echo $tra->getFecha('d/m/Y')?></td>
                      <td><?php echo $tra->getLugar()?></td>
                      <td><?php echo $tra->getRelato()?></td>
                      <td><?php echo $tra->getTipoEvento()?></td>
                      <td>
                            <?php echo jq_link_to_remote(image_tag('edit16.png', array('alt' => 'Agregar', 'title' =>'Modificar')), array(
                                        'update' => 'dato_evento',
                                        'url' => 'objeto/cargarEvento?evento_id='.$tra->getId().'&objeto_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  )); ?>
                         <?php echo jq_link_to_remote(image_tag('cross.png', array('alt_title' => 'Eliminar evento', 'height' => 20)), array(
                                        'update' => 'dato_evento',
                                        'url' => 'objeto/deleteEvento?tray_id='.$tra->getId(),
                                        'script' => 'true',
                                        'confirm' => '¿Esta usted seguro que desea eliminar este evento?'
                                                  )); ?>
                               <?php if($tra->getGaleriaDeImagenes()):?>
                                     <a onclick="window.open(this.href);return false;" href="<?php echo $tra->getGaleriaDeImagenes() ;?>" style="color: red" ><?php echo image_tag('inbox.png', array('alt' => 'Agregar', 'title' =>'Ir a la galeria'));?></a>
                              <?php endif?>
                      </td>

                   </tr>
                <?php endforeach; ?>

</table>
    <center>
    <?php echo jq_link_to_remote(image_tag('new.png', array('alt' =>'Agregar', 'title' =>'Agregar un evento')), array(
                                        'update' => 'dato_evento',
                                        'url' => 'objeto/cargarEvento?objeto_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  )); ?>



    </center>
<?php endif?>
</div>

</div>