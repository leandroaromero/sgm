<?php

require_once dirname(__FILE__).'/../lib/dimensionGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/dimensionGeneratorHelper.class.php';

/**
 * dimension actions.
 *
 * @package    museo
 * @subpackage dimension
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class dimensionActions extends autoDimensionActions
{
}
