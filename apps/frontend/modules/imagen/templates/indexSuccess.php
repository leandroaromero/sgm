<h1>Imagens List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Archivo</th>
      <th>Objeto</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($imagens as $imagen): ?>
    <tr>
      <td><a href="<?php echo url_for('imagen/show?id='.$imagen->getId()) ?>"><?php echo $imagen->getId() ?></a></td>
      <td><?php echo $imagen->getArchivo() ?></td>
      <td><?php echo $imagen->getObjetoId() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('imagen/new') ?>">New</a>
