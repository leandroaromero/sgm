<?php

require_once dirname(__FILE__).'/../lib/donanteGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/donanteGeneratorHelper.class.php';

/**
 * donante actions.
 *
 * @package    museo
 * @subpackage donante
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class donanteActions extends autoDonanteActions
{
 public function executeListShow($request){
		$this->forward404Unless($this->persona = Doctrine::getTable('Persona')->find(array($request->getParameter('id'))), sprintf('Esta persona no existe (%s).', $request->getParameter('id')));

		}


 protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $persona = $form->save();
        $donante = Doctrine::getTable('Donante')->find($persona->getId());
        if (! $donante){
        $d= new Donante();
        $d->setId($persona->getId());
        $d->save();
        }
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $persona)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@donante_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect(array('sf_route' => 'donante_edit', 'sf_subject' => $persona));
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }


  public function LoadData($id) {
    

    $donante = Doctrine::getTable('Donante')->find($id);
    $data= array();
        foreach($donante->getDonacion() as $donas) {
        foreach($donas->getObjeto() as $Objeto) {

            $fecha= date("d/m/Y", strtotime($Objeto->getFechaIngreso()));
            $mates='';
            foreach( $Objeto->getMaterial() as $material) {
                $mates=$mates.' '.$material->getNombreMaterial();
            }
            $line=$donas->getNroActa().';'.$Objeto->getNombreObjeto().';'.$mates.';'.$fecha;
            $data[] = explode(';', chop($line));
        }
        }
        return $data;
    }
public function executeReportes($request){
    $id=$request->getParameter('id');
    $donante =  Doctrine::getTable('Persona')->find($id);
$don= $donante->getApellido().'; '.$donante->getNombre().' ( '.$donante->getNroDocumento().' ) ';
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  // settings
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('administrador');
$pdf->SetTitle('Museo del Hombre Chaqueño');
$pdf->SetSubject('Lista de Acervo');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$fechaE=date('d/m/Y');
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING."Donados por $don\nLista emitida el $fechaE");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  // init pdf doc

  $pdf->AddPage();

  $pdf->SetFont('helvetica', '', 8);

// add a page

//Column titles
$header = array('Nro Acta', 'Nombre', 'Materiales', 'Ingreso' );

//Data loading
$data= $this->LoadData($id);
// print colored table
$pdf->ColoredTable2($header, $data);

  $pdf->Output();

 }




}
