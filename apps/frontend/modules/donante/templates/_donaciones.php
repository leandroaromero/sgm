<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_imagenes">
<div id="dato_telefono">
 <?php if (! $form->isNew()):?>
    <?php
$id=$form->getObject()->getId();
$donante = Doctrine::getTable('Donante')->find($id);

?>
<br/>
<center>
<table  class="sf_admin_list" width="70%">
                    <tr>
                        <th>N° de acta</th>
                        <th>Acervos ( Nombre - Materiales)</th>

                        <th>Acción</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($donante->getDonacion() as $don): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                      <td><?php echo $don->getNroActa()?></td>
                      <td>
                          <?php if($don->getObjeto()):?>
                          <table>
                             <?php foreach( $don->getObjeto() as $obj ):?>
                              <tr>
                                  <td width="30%" align="left">
                                <?php echo $obj->getNombreObjeto()?>
                              </td>
                              <td width="70%" align="left">
                                <?php $mats='';foreach( $obj->getMaterial() as $mat ):?>
                                        <?php   $mats = $mats.' '. $mat;?>
                                <?php endforeach;?>
                                        <?php echo $mats ?>
                              </td>
                             </tr>
                             <?php endforeach;?>
                          </table>
                           <?php endif ?>
                      <td>
                            <a onclick="window.open(this.href);return false;" href="<?php echo url_for('donacion/edit?id='.$don->getId())?>">
                              <?php echo image_tag('open.png', array('alt' => 'Delete', 'title' =>'Editar la obra'))?></a>

                      </td>

                   </tr>
                <?php endforeach; ?>

</table>
</center>
<center>
    </center>
<div align="right">
    <a onclick="window.open(this.href);return false;" href="<?php echo url_for('donante/reportes?id='.$id)?>"> Emitir Reporte</a>
</div>


<?php endif?>
</div>

</div>