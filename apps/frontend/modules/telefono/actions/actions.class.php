<?php

/**
 * telefono actions.
 *
 * @package    museo
 * @subpackage telefono
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class telefonoActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->telefonos = Doctrine::getTable('Telefono')
      ->createQuery('a')
      ->execute();
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new TelefonoForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new TelefonoForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($telefono = Doctrine::getTable('Telefono')->find(array($request->getParameter('id'))), sprintf('Object telefono does not exist (%s).', $request->getParameter('id')));
    $this->form = new TelefonoForm($telefono);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($telefono = Doctrine::getTable('Telefono')->find(array($request->getParameter('id'))), sprintf('Object telefono does not exist (%s).', $request->getParameter('id')));
    $this->form = new TelefonoForm($telefono);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($telefono = Doctrine::getTable('Telefono')->find(array($request->getParameter('id'))), sprintf('Object telefono does not exist (%s).', $request->getParameter('id')));
    $telefono->delete();

    $this->redirect('telefono/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $telefono = $form->save();

      $this->redirect('telefono/edit?id='.$telefono->getId());
    }
  }
}
