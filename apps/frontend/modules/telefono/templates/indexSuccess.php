<h1>Telefonos List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Numero telefono</th>
      <th>Tipo telefono</th>
      <th>Persona</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($telefonos as $telefono): ?>
    <tr>
      <td><a href="<?php echo url_for('telefono/edit?id='.$telefono->getId()) ?>"><?php echo $telefono->getId() ?></a></td>
      <td><?php echo $telefono->getNumeroTelefono() ?></td>
      <td><?php echo $telefono->getTipoTelefono() ?></td>
      <td><?php echo $telefono->getPersonaId() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('telefono/new') ?>">New</a>
