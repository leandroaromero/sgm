<?php

/**
 * sfGuardUser filter form.
 *
 * @package    propiedades-nea
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardUserAdminFormFilter extends PluginsfGuardUserFormFilter
{
   public function configure()
  {
       
      $this->widgetSchema['permissions_list']->setLabel('Permisos');
      $this->widgetSchema['permissions_list']->setOption('table_method', 'getOrderNombre');
      $this->setWidget('tipo_sub', new sfWidgetFormDoctrineChoice(array('model' => 'SubscriptionType', 'add_empty' => true)));
        //$this->validatorSchema['tipo_sub']= new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false)));
      $this->validatorSchema['tipo_sub']= new sfValidatorDoctrineChoice(array('required' => false, 'model' =>'SubscriptionType', 'column' => 'id'));
  }
   public function addTipoSubColumnQuery(Doctrine_Query $query, $field, $value) {
         $valor=$value;
          if (null !== $valor['text'] )
           {
               $query->leftJoin($query->getRootAlias().'.Profile u')->andWhere('u.subscription_type_id = ?',$valor['text']);
           }
   }

  public function getFields()
  {
    $fields = parent::getFields();
    $fields['tipo_sub'] = 'custom';
    return $fields;
  }
}
