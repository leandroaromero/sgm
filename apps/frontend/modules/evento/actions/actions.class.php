<?php

require_once dirname(__FILE__).'/../lib/eventoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/eventoGeneratorHelper.class.php';

/**
 * evento actions.
 *
 * @package    museo
 * @subpackage evento
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class eventoActions extends autoEventoActions
{
    public function executeGetObjetosTrayectoria($request)
	{
	    $this->getResponse()->setContentType('application/json');

	     $objetoss = Doctrine::getTable('Objeto')->retrieveForSelect(
	                $request->getParameter('q'),
	                $request->getParameter('limit')
	    );

	    return $this->renderText(json_encode($objetoss));
	}
}
