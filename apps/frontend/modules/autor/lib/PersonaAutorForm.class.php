<?php

/**
 * Persona form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PersonaAutorForm extends BasePersonaForm
{
protected $scheduledForDeletion = array();  	
protected $scheduledForDeletionTel = array();  	

  public function configure()
  {
       $this->validatorSchema['created_at']= new sfValidatorDateTime(array('required'=>false) );
       $this->validatorSchema['updated_at']= new sfValidatorDateTime(array('required'=>false) );
      sfProjectConfiguration::getActive()->loadHelpers('Url');
      $this->widgetSchema['localidad_id'] =  new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true));
            ////new sfValidatorChoice(array('choices' => array($this->getObject()->get('localidad_id')), 'empty_value' => $this->getObject()->get('localidad_id'), 'required' => false));
      $this->widgetSchema['localidad_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['localidad_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
      array(
        'model' => "Localidad",
        'url'   => url_for("@ajax_origen"),
        'config' => '{ max: 30}',
        'method' => 'getProcedencia'
        ));
     
      $this->validatorSchema['localidad_id']=  new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'required' => false));

      if(! $this->isNew()){
        $autor = $this->getObject()->getAutor();        
        $form_autor = new AutorAdminForm($autor[0]); 
        $this->embedForm('biografia', $form_autor);
      }
  }
  
 /* public function agregarTel($num){
	  $tel = new Telefono();
	  $tel->setPersona($this->getObject());
	  $tel_form = new TelefonoForm($tel);
	  $this->embeddedForms['telefonos']->embedForm($num, $tel_form);
	  $this->embedForm('telefonos', $this->embeddedForms['telefonos']);
	}
  public function agregarEmail($num){
	  $mail = new Email();
	  $mail->setPersona($this->getObject());
	  $mail_form = new EmailForm($mail);
	  $this->embeddedForms['emails']->embedForm($num, $mail_form);
	  $this->embedForm('emails', $this->embeddedForms['emails']);
	}	
	
  public function bind(array $taintedValues = null, array $taintedFiles = null)
	{
	  foreach($taintedValues['telefonos'] as $key=>$newPic)
	  {
	    if (!isset($this['telefonos'][$key]))
	    {
	      $this->agregarTel($key);
	    }
	  }
	  foreach($taintedValues['emails'] as $key=>$email)
	  {
	    if (!isset($this['emails'][$key]))
	    {
	      $this->agregarEmail($key);
	    }
	  }
	  parent::bind($taintedValues, $taintedFiles);
	}
	

 protected function doBind(array $values)
  {

      foreach ($values['emails'] as $i => $emailValues)
      {
        if (isset($emailValues['delete']) && $emailValues['id'])
        {
          $this->scheduledForDeletion[$i] = $emailValues['id'];
        }
      }
       foreach ($values['telefonos'] as $i => $telValues)
      {
        if (isset($telValues['delete']) && $telValues['id'])
        {
          $this->scheduledForDeletionTel[$i] = $telValues['id'];
        }
      }
    parent::doBind($values);
  }
  
  protected function doUpdateObject($values)
  {
    if (count($this->scheduledForDeletion))
    {
      foreach ($this->scheduledForDeletion as $index => $id)
      {
        unset($values['emails'][$index]);
        Doctrine::getTable('email')->findOneById($id)->delete();
      }
    }
    if (count($this->scheduledForDeletionTel))
    {
      foreach ($this->scheduledForDeletionTel as $index => $id)
      {
        unset($values['telefonos'][$index]);
        Doctrine::getTable('telefono')->findOneById($id)->delete();
      }
    }
    $this->getObject()->fromArray($values);
  }

*/


}
