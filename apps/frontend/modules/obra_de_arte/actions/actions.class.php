<?php

require_once dirname(__FILE__).'/../lib/obra_de_arteGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/obra_de_arteGeneratorHelper.class.php';

/**
 * obra_de_arte actions.
 *
 * @package    museo
 * @subpackage obra_de_arte
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class obra_de_arteActions extends autoObra_de_arteActions
{
}
