<?php

require_once dirname(__FILE__).'/../lib/localidadGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/localidadGeneratorHelper.class.php';

/**
 * localidad actions.
 *
 * @package    museo
 * @subpackage localidad
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class localidadActions extends autoLocalidadActions
{
    public function executeGetOrigen($request)
	{
	    $this->getResponse()->setContentType('application/json');
	    $donacion = Doctrine::getTable('Localidad')->retrieveForSelect(
	                $request->getParameter('q'),
	                $request->getParameter('limit')
	    );

	    return $this->renderText(json_encode($donacion));
	}	
}
