<?php

require_once dirname(__FILE__).'/../lib/materialGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/materialGeneratorHelper.class.php';

/**
 * material actions.
 *
 * @package    museo
 * @subpackage material
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class materialActions extends autoMaterialActions
{
}
