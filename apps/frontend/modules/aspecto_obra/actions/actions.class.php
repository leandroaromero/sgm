<?php

require_once dirname(__FILE__).'/../lib/aspecto_obraGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/aspecto_obraGeneratorHelper.class.php';

/**
 * aspecto_obra actions.
 *
 * @package    museo
 * @subpackage aspecto_obra
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class aspecto_obraActions extends autoAspecto_obraActions
{
}
