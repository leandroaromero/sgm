<?php use_stylesheet('main.css'); ?>
<div id="formulario_show">
    <center>
<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $investigacion->getId() ?></td>
    </tr>
    <tr>
      <th>Fecha:</th>
      <td><?php echo $investigacion->getFecha() ?></td>
    </tr>
    <tr>
      <th>Relato:</th>
      <td><?php echo $investigacion->getRelato() ?></td>
    </tr>
    <tr>
      <th>Fuente bibliografica:</th>
      <td><?php echo $investigacion->getFuenteBibliografica() ?></td>
    </tr>
    <tr>
      <th>Objeto:</th>
      <td><?php echo $investigacion->getObjeto() ?></td>
    </tr>
    <tr>
      <th>Investigador:</th>
      <td><?php foreach($investigacion->getInvestigadores() as $inve):?> 
            <?php echo $inve; ?> <br />
            <?php endforeach ?>
      </td>
    </tr>
  </tbody>
</table>

<hr />

    </center>


<a href="<?php echo url_for('investigacion/index') ?>">Volver a la lista</a>
</div>
<br/>
