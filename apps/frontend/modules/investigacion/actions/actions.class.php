<?php

require_once dirname(__FILE__).'/../lib/investigacionGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/investigacionGeneratorHelper.class.php';

/**
 * investigacion actions.
 *
 * @package    museo
 * @subpackage investigacion
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class investigacionActions extends autoInvestigacionActions
{
     public function executeListShow($request){
		$this->forward404Unless($this->investigacion = Doctrine::getTable('Investigacion')->find(array($request->getParameter('id'))), sprintf('Esta persona no existe (%s).', $request->getParameter('id')));
}
   
}
