<?php

/**
 * DetalleObra form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class DetalleObraAdminForm extends BaseDetalleObraForm
{
  public function configure()
  {

  		$this->widgetSchema['obra_de_arte_id'] = new sfWidgetFormDoctrineChoice(array(
        	'model'   => 'ObraDeArte',
        	'method'       => 'getNombre',
        	'add_empty' => '>> Seleccione <<',
    		));
    	$this->widgetSchema['aspecto_obra_id'] = new sfWidgetFormDoctrineDependentSelect(array(
        	'model'   => 'AspectoObra', 
        	'depends' => 'ObraDeArte',
        	'method'       => 'getNombre',
            'add_empty' => '>> Seleccione <<',
    	));

    	    // validadores
            
        $this->validatorSchema['obra_de_arte_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'ObraDeArte',            
           # 'required' => false,
        ));
                
        $this->validatorSchema['aspecto_obra_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'AspectoObra',
            #'required' => false,
        ));  
  }
}
