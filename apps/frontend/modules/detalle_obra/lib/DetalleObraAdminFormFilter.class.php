<?php

/**
 * DetalleObra filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class DetalleObraAdminFormFilter extends BaseDetalleObraFormFilter
{
  public function configure()
  {
  		$this->widgetSchema['obra_de_arte_id'] = new sfWidgetFormDoctrineChoice(array(
        	'model'   => 'ObraDeArte',
        	'method'       => 'getNombre',
        	'add_empty' => '>> Seleccione <<',
    		));
    	$this->widgetSchema['aspecto_obra_id'] = new sfWidgetFormDoctrineDependentSelect(array(
        	'model'   => 'AspectoObra', 
        	'depends' => 'ObraDeArte',
        	'method'       => 'getNombre',
            'add_empty' => '>> Seleccione <<',
    	));

    	    // validadores
            
        $this->validatorSchema['obra_de_arte_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'ObraDeArte',            
            'required' => false,
        ));
               
        $this->validatorSchema['aspecto_obra_id'] = new sfValidatorDoctrineChoice(array(
            'model' => 'AspectoObra',
            'required' => false,
        ));          
                
  }

    public function addObraDeArteIdColumnQuery(Doctrine_Query $query, $field, $value) 
        {

               if ( isset($value) && '' != $value)
               {
                        $query->leftJoin($query->getRootAlias().'.AspectoObra ao' )
                              ->leftJoin('ao.ObraDeArte oda')
                              ->andWhere('oda.id = ?', $value);
               }

        }

       public function addAspectoObraIdColumnQuery(Doctrine_Query $query, $field, $value) 
        {

               if ( isset($value) && '' != $value)
               {
                        $query->andWhere($query->getRootAlias().'.aspecto_obra_id = ?', $value);
               }

        }    

}
