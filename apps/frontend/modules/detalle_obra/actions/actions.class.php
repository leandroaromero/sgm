<?php

require_once dirname(__FILE__).'/../lib/detalle_obraGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/detalle_obraGeneratorHelper.class.php';

/**
 * detalle_obra actions.
 *
 * @package    museo
 * @subpackage detalle_obra
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class detalle_obraActions extends autoDetalle_obraActions
{
}
