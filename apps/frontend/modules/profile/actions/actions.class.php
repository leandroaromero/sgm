<?php

/**
 * profile actions.
 *
 * @package    apuntes
 * @subpackage profile
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class profileActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
     $this->forward404Unless($user = Doctrine_Core::getTable('sfGuardUser')->find(array($this->getUser()->getGuardUser()->getId()  )), sprintf('Object Usuario does not exist (%s).', $this->getUser()->getGuardUser()->getId() ));
     $this->form = new ChangeProfileForm($user);
  }
  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($user = Doctrine_Core::getTable('sfGuardUser')->find(array($this->getUser()->getGuardUser()->getId()  )), sprintf('Object Usuario does not exist (%s).', $this->getUser()->getGuardUser()->getId() ));
    $this->form = new ChangeProfileForm($user);

    $this->processForm($request, $this->form);

    $this->setTemplate('index');
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $profile = $form->save();
    //  $this->sendConfirmationEMail($profile);
      $this->redirect('profile/notification?');
    }
  }
  public function executeNotification(){}

  public function executeChangePass(sfWebRequest $request)
  {
     $this->forward404Unless($user = Doctrine_Core::getTable('sfGuardUser')->find(array($this->getUser()->getGuardUser()->getId()  )), sprintf('Object Usuario does not exist (%s).', $this->getUser()->getGuardUser()->getId() ));
     $this->form = new ChangePassForm($user);
  }
 
  public function executeUpdatePass(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($user = Doctrine_Core::getTable('sfGuardUser')->find(array($this->getUser()->getGuardUser()->getId()  )), sprintf('Object Usuario does not exist (%s).', $this->getUser()->getGuardUser()->getId() ));
     $this->form = new ChangePassForm($user);

    $this->processFormPass($request, $this->form);

    $this->setTemplate('changePass');
  }
 protected function processFormPass(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $profile = $form->save();
    //  $this->sendConfirmationEMail($profile);
      $this->redirect('profile/notification?');
    }
  }
}
