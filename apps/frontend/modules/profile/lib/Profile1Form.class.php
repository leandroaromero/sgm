<?php

/**
 * Profile form.
 *
 * @package    apuntes
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class Profile1Form extends BaseProfileForm
{
  public function configure()
  {
      $rango = range(1970, 2020);
      $arreglo_rango = array_combine($rango, $rango); 
      $this->widgetSchema['province_id'] = new sfWidgetFormDoctrineChoice(array(
          'model'   => 'Province',
          'add_empty' => true,
         ));
       $this->widgetSchema['town_id'] = new sfWidgetFormDoctrineDependentSelect(array(
          'model'   => 'Town',
          'depends' => 'Province',
          'add_empty' => true,
         ));
      $this->validatorSchema['province_id'] = new sfValidatorDoctrineChoice(array(
                    'model' => 'Province',
                ));

      $this->validatorSchema['town_id'] = new sfValidatorDoctrineChoice(array(
                    'model' => 'Town',
                ));


      $this->validatorSchema['province_id']->setMessage('required', 'Este campo es obligatorio');
      $this->validatorSchema['town_id']->setMessage('required', 'Este campo es obligatorio');
   $this->widgetSchema['born_date'] = new sfWidgetFormDate(array(
           'format' => '%day% / %month% / %year% ',
           'years' => $arreglo_rango,
           ));
   
   
   /*new sfWidgetFormJQueryDate(array(
       'image'  => '../images/calendar_icon.gif',
       'culture' => 'es',
       'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
         'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: ['Calendario']}",
       'date_widget' => new sfWidgetFormDate(array(
           'format' => '%year% : %month% : %day%',
           'years' => $arreglo_rango,
           ))
        ));*/
  }
}
