<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CompanyProfileForm
 *
 * @author cristian
 */
class ChangePassForm extends sfGuardUserForm {
    public function configure() {

        $this->widgetSchema['password'] = new sfWidgetFormInputPassword();
        $this->widgetSchema['password_old'] = new sfWidgetFormInputPassword();
        $this->validatorSchema['password_old'] =  new sfValidatorPass();//   clone $this->validatorSchema['password'];
        $this->validatorSchema['password']->setOption('required', true);
        $this->widgetSchema['password_confirmation'] = new sfWidgetFormInputPassword();
        $this->validatorSchema['password_confirmation'] = clone $this->validatorSchema['password'];
        $this->widgetSchema->moveField('password_confirmation', 'after', 'password');

        $this->mergePostValidator(new
          sfValidatorSchemaCompare('password',
            sfValidatorSchemaCompare::EQUAL,
            'password_confirmation', array(),
            array('invalid' => 'Las contraseñas no coinciden.')
          )

        );
        $this->mergePostValidator(new
          sfValidatorSchemaCompare('password',
            sfValidatorSchemaCompare::EQUAL,
            'password_confirmation', array(),
            array('invalid' => 'Las contraseñas no coinciden.')
          )

        );

        $this->useFields(array(
          'password_old',
          'password',
          'password_confirmation',
        )); 

    }

 public function checkPassOld($validator, $values)
 {
  $user = Doctrine::getTable('sfGuardUser')->find(6);
  if($user->getPassword() !=  sha1($values['password_old'])){
   throw new sfValidatorErrorSchema($validator, 'No contraseña actual no es correcta');
   }
   throw new sfValidatorErrorSchema($validator, 'No contraseña actual no es correcta feo');
   throw new sfValidatorErrorSchema($validator, array('password_old' => $error));
  return $values;
 }
 public function  doBind(array $values)
    {
     
     $user = Doctrine::getTable('sfGuardUser')->find( $values['id']);
     if( ! $user->checkPassword($values['password_old']) )
        {
            $this->validatorSchema->setPostValidator(
                new sfValidatorSchemaCompare('password_old', sfValidatorSchemaCompare::NOT_EQUAL, 'password_old' , array('throw_global_error' => true), array('invalid' => 'La pass es erronea'))
            );
        }
 
        return parent::doBind($values);
    }


}
