<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CompanyProfileForm
 *
 * @author cristian
 */
class ChangeProfileForm extends sfGuardUserForm {
    public function configure() {
        
// Setup proper password validation with confirmation
        $this->widgetSchema['password'] = new sfWidgetFormInputPassword();
        //$this->validatorSchema['password']->setOption('required', true);
        $this->widgetSchema['password_confirmation'] = new sfWidgetFormInputPassword();
        $this->validatorSchema['password_confirmation'] = clone $this->validatorSchema['password'];
        $this->widgetSchema->moveField('password_confirmation', 'after', 'password');

        $this->validatorSchema['email_address']->setMessage('required', 'Este campo es obligatorio');
        $this->validatorSchema['email_address']->setMessage('invalid', '"%value%" no es una dirección de email válida');

        $this->mergePostValidator(new
          sfValidatorSchemaCompare('password',
            sfValidatorSchemaCompare::EQUAL,
            'password_confirmation', array(),
            array('invalid' => 'Las contraseñas no coinciden.')
          )

        );
        $this->useFields(array(
          'first_name',
          'last_name',
          'email_address',
        ));

      
    }


}
