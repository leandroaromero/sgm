<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<center>
<h1>Hola <?php echo $sf_user->getUsername()?></h1>
<br>
<form class="f-wrap-1" action="<?php echo url_for('profile/'.($form->getObject()->isNew() ? 'updatePass' : 'updatePass').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<table class="table">
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
            <input class="f-submit" type="submit" value="Enviar solicitud" />
        </td>
      </tr>
    </tfoot>
    <tbody>
          <?php echo $form->renderGlobalErrors() ?>
        <tr>
          <td><?php echo $form['password_old']->renderLabel('Actual contraseña') ?></td>
          <td>
              <?php echo $form['password_old']->render() ?>
              <?php echo $form['password_old']->renderError() ?>
          </td>
        </tr>
        <tr>
            <td width="150px"><?php echo $form['password']->renderLabel('Nueva contraseña') ?></td>
          <td>
              <?php echo $form['password']->render() ?>
              <?php echo $form['password']->renderError() ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $form['password_confirmation']->renderLabel('Repetir contraseña') ?></td>
          <td>
              <?php echo $form['password_confirmation']->render() ?>
              <?php echo $form['password_confirmation']->renderError() ?>
          </td>
        </tr>
       
    </tbody>
</table>
</form>

</center>