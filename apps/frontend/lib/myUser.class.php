<?php

class myUser extends sfGuardSecurityUser
{

public function getDataBase(){
$base= $this->getAttribute('base_dato');

$default = 'hombre_chaquenio' ; //sfConfig::get('app_institucion_default');

   if( is_null($base) &&  !$this->isAuthenticated() ){
        $base= $default;
        $this->setAttribute('base_dato', $default);
	}
	return $base;
  }

public function getInstitutionName(){
    
    $valor = $this->getDataBase();
    $name='';
    switch ($valor){
      case 'archivo_historico':
          $name='';
          break;
      case 'bellas_artes':
          $name='MUSEO BELLAS ARTES';
          break;
      case 'ccias_naturales':
          $name='MUSEO CIENCIAS NATURALES';
          break;
      case 'medios_comunicacion':
          $name='MUSEO DE MEDIOS';
          break;
      case 'ichoalay':
          $name='MUSEO ICHOALAY';
          break;
      case 'isla_cerrito':
          $name='';
          break;
      case 'geraldi':
          $name='';
          break;
      case 'jardin_botanico':
          $name='MUSEO CASA JARDIN BOTANICO';
          break;
      case 'casa_meloni':
          $name='MUSEO CASA MELONI';
          break;
      case 'inmaterial':
          $name='P.C.INMATERIAL';
          break;
      case 'hombre_chaquenio':
          $name='MUSEO DEL HOMBRE CHAQUEÑO';
          break;
      case 'so_sotelo':
          $name='MUSEO ARTESANAL';
          break;
      case 'so_urbana':
          $name='MUSEO DE LAS ESCULTURAS';
          break;
      case 'so_fratti':
          $name='MUSEO DE LA EDUCACION';
          break;
      case 'so_malvin':
          $name='MUSEO MALVINAS CHACO';
          break;
      case 'so_quijano':
          $name='MUSEO CASA QUIJANO';
          break;
      case 'so_carnaval':
          $name='MUSEO DEL CARNAVAL';
          break;
      case 'so_palmas':
          $name='MUSEO de S. LAS PALMAS';
          break;
      case 'so_peroni':
          $name='M. H. P. del PERONISMO C.';
          break;
    }
          return $name;
    
         
}

public function getInstitutionSubName(){
    $valor = $this->getDataBase();
    $name='';
    switch ($valor){
      case 'archivo_historico':
          $name='';
          break;
      case 'bellas_artes':
          $name='Rene Brusau';
          break;
      case 'ccias_naturales':
          $name='Agusto Schulz';
          break;
      case 'medios_comunicacion':
          $name='';
          break;
      case 'ichoalay':
          $name='';
          break;
      case 'isla_cerrito':
          $name='';
          break;
      case 'geraldi':
          $name='';
          break;
      case 'jardin_botanico':
          $name='Augusto Schulz';
          break;
      case 'casa_meloni':
          $name='';
          break;
      case 'inmaterial':
          $name='';
          break;
      case 'hombre_chaquenio':
          $name='Profesor Ertivio Acosta';
          break;
      case 'so_sotelo':
          $name='René James Sotelo';
          break;
      case 'so_urbana':
          $name='Urbanas del Mundo';
          break;
      case 'so_fratti':
          $name='Francisco Lázaro Fratti';
          break;
      case 'so_palmas':
          $name='del Chaco Austral';
          break;
    }
          return $name;
}

public function getFile(){
   return $this->getDataBase(); 
   }
}
