<?php

/**
 * Persona filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PersonaOrdenForm extends sfForm
{
	protected static $columnas = array('nombre_y_apellido'=> 'Nombre y  Apellido', 'nro_documento'=>'Nro documento' , 'direccion'=>'Dirección');
	protected static $orden = array('Asc'=> 'Ascendente', 'Desc'=>'Desc');
  public function configure()
  {
	  $this->widgetSchema['columna'] = new sfWidgetFormSelect(array('choices' => self::$columnas));
	  $this->widgetSchema['orden']   = new sfWidgetFormSelect(array('choices' => self::$orden));
  }
}
