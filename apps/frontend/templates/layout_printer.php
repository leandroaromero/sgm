<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php $ico= sfConfig::get('app_url_ico');  ?>
    <link rel="shortcut icon" href="<?php echo $ico ?>" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>

  </head>
<body>
  <div id="title_logo_2" >
	  <?php $file= sfConfig::get('app_institution_file');?>
          <center>
                  <?php echo image_tag('museos/'.$file.'/logo.jpg', 'size=600x100')?>
          </center>
  </div>
      <?php echo $sf_content ?>

</body>
</html>
