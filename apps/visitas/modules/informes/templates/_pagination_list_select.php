﻿<?php
$show = __(' Afficher ');
$perPage = __(' par page');
$nbRecords = array('3','5','10', '20', '50', '100', '∞');
$currentMaxPerPage = $sf_user->getAttribute('informes.max_per_page', $pager->getMaxPerPage(),'admin_module');
?>

| <?php echo $show ?>
 20 
<?php echo $perPage ?>

<script type="text/javascript">
/* <![CDATA[ */
function changePageSize(val){
        var newLocation = "<?php echo url_for('@informes') ?>?maxPerPage="+val;
        window.location=newLocation;
    }
/* ]]> */
</script>
