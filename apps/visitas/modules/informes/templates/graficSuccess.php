        <?php use_javascript('highcharts/js/highcharts.js') ?>
        <?php use_javascript('highcharts/js/modules/exporting.js') ?>


		<script type="text/javascript">
                        var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container1',
						defaultSeriesType: 'column'
					},
					title: {
						text: 'Detalle de Visitantes por mes'
					},
					subtitle: {
						text: '<?php echo $periodo?>'
					},
					xAxis: {
						categories: [
							'Enero',
							'Febrero',
							'Marzo',
							'Abril',
							'Mayo',
							'Junio',
							'Julio',
							'Agosto',
							'Septiembre',
							'Octubre',
							'Noviembre',
							'Diciembre'
						]
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Cantidad'
						}
					},
					legend: {
						layout: 'vertical',
						backgroundColor: '#FFFFFF',
						align: 'left',
						verticalAlign: 'top',
						x: 100,
						y: 70,
						floating: true,
						shadow: true
					},
					tooltip: {
						formatter: function() {
							return ''+
								this.x +': '+ this.y +' ';
						}
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
				        series: [{
						name: 'Participantes',
						data: [
                                                  <?php foreach($row as $t): ?>
                                                       <?php echo $t[2]?>,
                                                  <?php endforeach;?>
                                                ]

					}, {
						name: 'Actividades',
						data: [
                                                    <?php foreach($row as $t): ?>
                                                       <?php echo $t[1]?>,
                                                    <?php endforeach;?>
                                                ]

					}]
				});


			});
				
		</script>
	<script type="text/javascript">

			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container2',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Detalle Visitantes por edades'

					},
					subtitle: {
						text: '<?php echo $periodo?>'
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
								}
							}
						}
					},
				    series: [{
						type: 'pie',
						name: 'Browser share',

						data: [

						  <?php foreach($etareo as $t): ?>

							[  '<?php echo $t[0]?>' , <?php echo $t[2]?> ],

                                             <?php endforeach;?>

						]
					}]
				});
			});

		</script>
	        <script type="text/javascript">

			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container3',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Detalle de actividades por tipo de actividad'

					},
					subtitle: {
						text: '<?php echo $periodo?>'
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
								}
							}
						}
					},
				    series: [{
						type: 'pie',
						name: 'Browser share',

						data: [

						  <?php foreach($servis as $t): ?>

							[  '<?php echo $t[0].' - '.$t[1]?>' , <?php echo $t[3]?> ],

                                             <?php endforeach;?>

						]
					}]
				});
			});

		</script>
                <script type="text/javascript">

			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container4',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Detalle de visitantes por tipo de actividad'

					},
					subtitle: {
						text: '<?php echo $periodo?>'
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
								}
							}
						}
					},
				    series: [{
						type: 'pie',
						name: 'Browser share',

						data: [

						  <?php foreach($servis as $t): ?>

							[  '<?php echo $t[0].' - '.$t[1]?>' , <?php echo $t[5]?> ],

                                             <?php endforeach;?>

						]
					}]
				});
			});

		</script>
                <script type="text/javascript">

			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container5',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Detalle de actividad por procedencia '

					},
					subtitle: {
						text: '<?php echo $periodo?>'
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
								}
							}
						}
					},
				    series: [{
						type: 'pie',
						name: 'Browser share',

						data: [

						  <?php foreach($rowsor as $t): ?>

							[  '<?php echo $t[0] ?>' , <?php echo $t[2]?> ],

                                             <?php endforeach;?>

						]
					}]
				});
			});

		</script>
                <script type="text/javascript">

			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container6',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Detalle de participantes por procedencia '

					},
					subtitle: {
						text: '<?php echo $periodo?>'
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
								}
							}
						}
					},
				    series: [{
						type: 'pie',
						name: 'Browser share',

						data: [

						  <?php foreach($rowsor as $t): ?>

							[  '<?php echo $t[0] ?>' , <?php echo $t[4]?> ],

                                             <?php endforeach;?>

						]
					}]
				});
			});

		</script>
                
                <div id="espacio_blanco" style="height: 90px"></div>                
<div id="containerBig" style="width: 820px; height: 400px; margin: 0 auto">
Numero total de participantes en sala: <?php echo $total_p; ?><br/>
Total de visitas atendidas: <?php echo $total_a; ?>
		<!-- 3. Add the container -->
		<div id="container1" style="width: 800px; height: 400px; margin: 0 auto"></div>
                <br/>
                <div id="container2" style="width: 800px; height: 400px; margin: 0 auto"></div>
                <br/>
                <div id="espacio_blanco" style="height: 270px"></div>
                <div id="container3" style="width: 800px; height: 400px; margin: 0 auto"></div>
                <br/>
                <div id="container4" style="width: 800px; height: 400px; margin: 0 auto"></div>
                <br/>
                <div id="espacio_blanco" style="height: 270px"></div>
                <div id="container5" style="width: 800px; height: 400px; margin: 0 auto"></div>
                <br/>
                <div id="container6" style="width: 800px; height: 400px; margin: 0 auto"></div>

</div>