<?php

require_once dirname(__FILE__).'/../lib/informesGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/informesGeneratorHelper.class.php';

/**
 * informes actions.
 *
 * @package    museo
 * @subpackage informes
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class informesActions extends autoInformesActions
{
    function cambiarFormatoFecha($fecha){
        list($anio,$mes,$dia)=explode("-",$fecha);
        return $dia.'-'.$mes.'-'.$anio;
    }
    function cambiarFormatoFecha2($fecha){
        list($fechas,$hora)=explode(" ",$fecha);
        list($anio,$mes,$dia)=explode("-",$fechas);
        return $dia.'-'.$mes.'-'.$anio;
    }
    public function getTotalActividad($ids = array()){

                   $totalActividad=0;
                    $totalActividad=count($ids);
                    return $totalActividad;
    }
    public function getTotalParticipantes($ids = array()){
        //TOTAL GENERAL de participantes $cant
                  $parts = Doctrine_Query::create()
                        ->select('SUM(p.cantidad)')
                        ->from('Actividad a')
                        ->andWhereIn('a.id', $ids)
                        ->innerJoin('a.Participantes p')
                        ->fetchArray();
                   $totalParticipantes=0;
                   foreach($parts as $part ){
						$totalParticipante= $part['SUM'];
					  }
                   return  $totalParticipante;
    }

    public function setHeaders($t_a, $t_p){

                $blanco = array('', '',);
                $rows = array($this->helper->getExportColumnHeaders($blanco));
                $institution= sfConfig::get('app_institution_name');
                $titulo= array($institution);
                $rows[] = implode("\t", $titulo);
                $periodo= 'Periodo: ';
                $filters = $this->getFilters();
                            if($filters){
                                if($filters['fecha']['from']){
                                    $periodo.= 'desde ';
                                    $periodo.= $this->cambiarFormatoFecha($filters['fecha']['from']);
                                }
                                if($filters['fecha']['to']){
                                    $periodo.= ' hasta ';
                                    $f=$this->cambiarFormatoFecha2($filters['fecha']['to']);
                                    $periodo.= $f;
                                }
                            }
                 $periodo= array($periodo);
                 $rows[] = implode("\t", $periodo);
                 $rows[] = implode("\t", $blanco);
                 $rows[]= implode("\t",  array('NUMERO TOTAL DE VISITANTES EN SALA: '.$t_p));
                 $rows[]= implode("\t",  array('TOTAL DE VISITAS ATENDIDAS: '.$t_a));
                 $rows[] = implode("\t", $blanco);
                return $rows;

    }
    public function executeListListadoGeneral(sfWebRequest $request)
 	  {

                ini_set("max_execution_time",0);
		$this->setLayout(false);
		sfConfig::set('sf_web_debug', false);

		$resp = $this->getResponse();
		$resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
		$resp->setHttpHeader('Content-Disposition', ' attachment; filename="Listado General Museo.xlsx"');
		$resp->setHttpHeader('Cache-Control', ' maxage=3600');
		$resp->setHttpHeader('Pragma', 'public');
                  
		$excel_builder = new ExcelBuilder();
                
            //    $ids = $this->getTotalIds(); 

                $grupos = Doctrine_Query::create()
                        ->from('GruposEtareos g')
                        ->orderBy('g.edad_desde ASC')
                        ->fetchArray();
                     $titulo='Listado General';   
                $records= $this->getExportRecordsCollection2();
                $cabecera= array('Día', 'Fecha', 'Desde', 'Hasta', 'Nombre de la actividad', 'Tipo de Actividad' );
                foreach($grupos as $group){
					
                    if($group['edad_hasta'] >0 ){
		             $cabecera[]= $group['edad_desde'].' - '.$group['edad_hasta'].' años';
					}else{
						$cabecera[]= 'Mayor de '.$group['edad_desde'].' años';
						}
 	           }   
                $rows= array();
 	        foreach ($records as $record)
 	        {
 	         $rows[] = $this->getExportRecordRow($record ,  $grupos);
 	        }
                $cabecera[]='Lugar';
                $cabecera[]='Personal';
                $cabecera[]='Observaciones';
                
                $objWriter = new PHPExcel_Writer_Excel2007($excel_builder->generateExcel($titulo, $cabecera,$rows)); 

		$tmp_dir =   sfConfig::get('sf_cache_dir').'/';           
		$file_name = $tmp_dir.'Listado General Museo.xlsx';           

		$objWriter->save($file_name);
		$this->file = $file_name;
                $this->setTemplate('toExcel'); 
        
  
                
 	      }

 	  protected function getExportRecordsCollection()
 	  {   
                     $this->setMaxPerPage('20'); 
 	    $query = $this->getPager()->getQuery();
 	    return $query->execute();
 	  }

 	  protected function getExportRecordsCollection2()
 	  {
                    $this->setMaxPerPage('10000');
                    $this->setPage(1);
                    $query = $this->getPager()->getQuery();
 	            $result= $query->execute();
                    $this->setMaxPerPage('20'); 
       
 	    return $result;
 	  }

          protected function getTotalIds()
           {
           $records = $this->getExportRecordsCollection2();
                $ids= array();
               //solo los Id que estan en el rango
                foreach($records as $rec){
	 			   $ids[]=$rec['id'];
				   }    
               
                                   return $ids;                    
          }

           

           public function getExportRecordRow($record, $gruposs )
              {
                $row = array();
                //  $headers= array ("id", "tipo_actividad_id", "nombre_actividad", "fecha", "hora_desde", "hora_hasta", "atencion_actividad", "lugar_id");
                  //Dia
                  $fecha = $record['fecha'];
                  $semana = date ( "w", strtotime ($fecha));
                  $dias_semana = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
                  $row[] = $dias_semana[$semana];
                  //fecha
                  $row[] = $record['fecha'];
                  //desde
                  $row[] = $record['hora_desde'];
                  //hasta
                  $row[] = $record['hora_hasta'];
                  //actividad
                  $actividad = Doctrine::getTable('TipoActividad')->find($record['tipo_actividad_id']);
                  $row[] = $actividad->getTipoActividad();
                  //nombre
                  $row[] = $record['nombre_actividad'];

                  
                foreach($gruposs as $grupos ){  
                    if($grupos['edad_hasta'] >0 ){
		        	//participantes
                    $actividades = Doctrine_Query::create()
                        ->select('SUM(p.cantidad) as cantidades')    
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhere('a.id = ?', $record['id'])
                        ->andWhere('p.edad_aproximada >= ? AND p.edad_aproximada <= ?', array($grupos['edad_desde'], $grupos['edad_hasta']))
                        ->fetchOne()  ;
                }else{
                
					
                    $actividades = Doctrine_Query::create()
                        ->select('SUM(p.cantidad) as cantidades')    
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhere('a.id = ?', $record['id'])
                        ->andWhere('p.edad_aproximada >= ?', $grupos['edad_desde'])    
                        ->fetchOne();
                }


                     $cant=$actividades['cantidades'];
                    if(! $cant> 0)
                        $cant =0;
                     $row[]= $cant;
                  
                }
                  if($record['lugar_id'] > 0 && $record['lugar_id']!= NULL ){
                     $lugar = Doctrine::getTable('Lugar')->find($record['lugar_id']);
                     $lugar =$lugar->getNombreLugar();  
                  }else{
                      $lugar ='';
                  }
                  $row[] = $lugar;
//                  //PARTICIPANTES ES LO QUE FALTA
                  $act = Doctrine::getTable('Actividad')->find($record['id']);
                  $users = $act->getUsuarios();
                  $usuarios = '';
                  foreach( $users as $u ){
					   $usuarios .= $u->getFirstName().' ';
					  }
                  
                  //usuarios
                  $row[]= $usuarios;
                  $row[]= $record['atencion_actividad'];
                return  $row;
              }
              
              
     public function executeListListadoAnual(sfWebRequest $request)
 	  {
         
                ini_set("max_execution_time",0);
		$this->setLayout(false);
		sfConfig::set('sf_web_debug', false);

		$resp = $this->getResponse();
		$resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
		$resp->setHttpHeader('Content-Disposition', ' attachment; filename="Listado Anual.xlsx"');
		$resp->setHttpHeader('Cache-Control', ' maxage=3600');
		$resp->setHttpHeader('Pragma', 'public');
                  
		$excel_builder = new ExcelBuilder();
                
               $ids = $this->getTotalIds(); 
 
                $meses    = array( "1","2","3","4","5","6","7","8","9","10","11","12");
                $row      = array();
                $titulo   = 'Visitas mensuales';
                $cabecera = array ('MES', 'NÚMERO DE VISITANTES', 'NÚMERO DE ACCIONES');

                foreach($meses as $mes){
                   $rows[] = $this->getExportRecordRowMes($ids, $mes);   		 
                      }  
                  
                 $objWriter = new PHPExcel_Writer_Excel2007($excel_builder->generateExcel($titulo, $cabecera,$rows)); 

		$tmp_dir =   sfConfig::get('sf_cache_dir').'/';           
		$file_name = $tmp_dir.'Listado Anual.xlsx';                

		$objWriter->save($file_name);
		$this->file = $file_name;
                $this->setTemplate('toExcel'); 
         
 	      }  
 	       
 	public function getExportRecordRowMes($ids, $mes = NULL)
              {
                    
	            $mesName= array("MES", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" );
					//mes 
	            $row[1]= $mesName[$mes];
					
                    
					//participantes
                     $actividades = Doctrine_Query::create()
                        ->select('SUM(p.cantidad) as cantidades')   
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhereIn('a.id', $ids)
                        ->andWhere('MONTH(a.fecha)=?', $mes)
                        ->fetchOne();    
                     $cant=0;
                     if( $actividades['cantidades'] >0)
                       $cant  = $actividades['cantidades'];
                     $row[2]= $cant;  
                     
					//actividades
                    $actividades1 = Doctrine_Query::create()
                        ->select('a.id')   
                        ->from('Actividad a')
                        ->andWhere('MONTH(a.fecha)=?', $mes)
                        ->andWhereIn('a.id', $ids)
                        ->groupBy('a.id')    
                        ->fetchArray();
                     $cant1=0;
                     $cant1  = count($actividades1);     
                     $row[3]= $cant1;
                return $row;
              }
  public function executeListListadoEtareo(sfWebRequest $request)
 	  {
         
                ini_set("max_execution_time",0);
		$this->setLayout(false);
		sfConfig::set('sf_web_debug', false);

		$resp = $this->getResponse();
		$resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
		$resp->setHttpHeader('Content-Disposition', ' attachment; filename="export.xls"');
		$resp->setHttpHeader('Cache-Control', ' maxage=3600');
		$resp->setHttpHeader('Pragma', 'public');
                  
		$excel_builder = new ExcelBuilder();
                
                $ids = $this->getTotalIds(); 
                
                             //REPORTES MEDIANTE GRUPOS ETEREOS
                 $total_p=  $this->getTotalParticipantes($ids);
                 $titulo="GRUPOS ETEREOS";
                 $cabecera= array ('GRUPO ETAREOS', 'NÚMERO DE VISITANTES', 'PORCENTAJE');
                 $rows = array();
                 //TODOS LOS GRUPOS
                 $grupos = Doctrine_Query::create()
                        ->from('GruposEtareos g')
                        ->orderBy('g.edad_desde ASC')
                        ->fetchArray();

			   foreach($grupos as $grup){
			       $rows[] = $this->getExportRecordRowGrupos($ids, $grup, $total_p);
				  }
                
		$objWriter = new PHPExcel_Writer_Excel5($excel_builder->generateExcel($titulo, $cabecera, $rows));
                
		$tmp_dir =   sfConfig::get('sf_cache_dir').'/';
		$file_name = $tmp_dir.'exportar.xls';
                
                $objWriter->setTempDir($tmp_dir);
		
                $objWriter->save($file_name);
		$this->file = $file_name;
                $this->setTemplate('toExcel'); 
      
 	      }

 	public function getExportRecordRowGrupos($ids, $grupos = NULL, $total = 1 )
              {
		
		//nombre del campo
                if($grupos['edad_hasta'] >0 ){
		   $row[]= '"'.$grupos['edad_desde'].' - '.$grupos['edad_hasta'].' años'.'"';

					//participantes
                      $actividades = Doctrine_Query::create()
                        ->select('SUM(p.cantidad) as cant')    
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhereIn('a.id', $ids)
                        ->andWhere('p.edad_aproximada >= ? AND p.edad_aproximada <=?', array($grupos['edad_desde'], $grupos['edad_hasta']))
                        ->fetchOne();
                }else{
                    $row[]='"'. 'Mayor de '.$grupos['edad_desde'].' años'.'"';

		    //participantes
                    $actividades = Doctrine_Query::create()
                        ->select('SUM(p.cantidad) as cant')    
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhereIn('a.id', $ids)
                        ->andWhere('p.edad_aproximada >= ?', $grupos['edad_desde'])
                        ->fetchOne();
                }
                     $cant=0;
                     if ($actividades['cant'] > 0)
                        $cant= $actividades['cant'];
			
                     $row[]= $cant;
                     $porcentaje = ( $cant / $total ) * 100 ;
                     $porcentaje= round($porcentaje,2);
                     $row[]=  $porcentaje;
                return  $row;
              }
              
 public function executeListListadoServicio(sfWebRequest $request)
 	  {
     
     
                ini_set("max_execution_time",0);
		$this->setLayout(false);
		sfConfig::set('sf_web_debug', false);

		$resp = $this->getResponse();
		$resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
		$resp->setHttpHeader('Content-Disposition', ' attachment; filename="export.xls"');
		$resp->setHttpHeader('Cache-Control', ' maxage=3600');
		$resp->setHttpHeader('Pragma', 'public');
                  
		$excel_builder = new ExcelBuilder();
                
                $ids = $this->getTotalIds(); 
                $total_a=  $this->getTotalActividad($ids);
                $total_p=  $this->getTotalParticipantes($ids);
                
                 //REPORTES MEDIANTE SERVICIOS
                 $titulo="TIPOS DE SERVICIOS";
                 $cabecera=array ( 'SERVICIO',  'TIPO DE ACTIVIDAD', 'NÚMERO ACCIONES', 'PORCENTAJE %', 'CANTIDAD DE PERSONA', 'PORCENTAJE %');
                 $rows = array();
                 //TODOS LOS GRUPOS
                 $servicios = Doctrine_Query::create()
                        ->from('Servicio g')
                        ->orderBy('g.nombre_servicio ASC')
                        ->fetchArray();
                 
                 
		   foreach($servicios as $serv){
                      $tipos = Doctrine_Query::create()
                        ->from('TipoActividad g')
                        ->andWhere('g.servicio_id =?', $serv['id'])
                        ->orderBy('g.tipo_actividad ASC')
                        ->fetchArray();

                   foreach($tipos as $tipo){
                                     $rows[] = $this->getExportRecordRowServicios($ids, $tipo, $serv['nombre_servicio'], $total_a, $total_p);
                                          }
                   }

                
		$objWriter = new PHPExcel_Writer_Excel5($excel_builder->generateExcel($titulo, $cabecera, $rows));
                
		$tmp_dir =   sfConfig::get('sf_cache_dir').'/';
		$file_name = $tmp_dir.'exportar.xls';
                
                $objWriter->setTempDir($tmp_dir);
                
		$objWriter->save($file_name);
		$this->file = $file_name;
                $this->setTemplate('toExcel'); 
    
 	      }

 	public function getExportRecordRowServicios($ids, $grupos = NULL, $servicio, $totalA = 1, $totalP = 1 )
              {
		//nombre del servicio            
                 $row[]= $servicio;
                 $row[]= $grupos['tipo_actividad'];
                 $canActi = Doctrine_Query::create()
                        ->from('Actividad a')
                        ->andWhereIn('a.id', $ids)
                         ->andWhere('a.tipo_actividad_id =? ', $grupos['id'])
                        ->fetchArray();
                  $row[]= count($canActi);
                  $t_act= ( count($canActi)/$totalA ) * 100 ;
                  $t_act= round($t_act,2);
                  $row[]=  $t_act;

                //participantes
                 $actividades = Doctrine_Query::create()
                        ->select('SUM(p.cantidad) as cant')  
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhereIn('a.id', $ids)
                        ->andWhere('a.tipo_actividad_id =? ', $grupos['id'])
                        ->fetchOne();
					
                    

                 $cant=0;
                 if($actividades['cant']> 0)
                     $cant= $actividades['cant'];

                 $row[]= $cant;
                 $t_par= ($cant/$totalP ) * 100 ;
                 $t_par= round($t_par,2);
                 $row[]=  $t_par;
                return  $row;
              }
              
              
    public function executeListListadoOrigen(sfWebRequest $request)
 	  {
           
                ini_set("max_execution_time",0);
		$this->setLayout(false);
		sfConfig::set('sf_web_debug', false);

		$resp = $this->getResponse();
		$resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
		$resp->setHttpHeader('Content-Disposition', ' attachment; filename="export.xls"');
		$resp->setHttpHeader('Cache-Control', ' maxage=3600');
		$resp->setHttpHeader('Pragma', 'public');
                  
		$excel_builder = new ExcelBuilder();
                
                $ids = $this->getTotalIds(); 
                $total_a=  $this->getTotalActividad($ids);
                $total_p=  $this->getTotalParticipantes($ids);
                
                 //REPORTES MEDIANTE SERVICIOS
                 $titulo="LUGAR DE ORIGEN";
                 $cabecera= array ( 'PROCEDENCIA', 'NÚMERO ACCIONES', 'PORCENTAJE', 'CANTIDAD DE PERSONA', 'PORCENTAJE');
              
                   //TODOS LOS  TIPOS DE ORIGEN
                 $origenes = Doctrine_Query::create()
                        ->from('TipoOrigen g')
                        ->orderBy('g.orden_importancia ASC')
                        ->fetchArray();
                
                   foreach($origenes as $origen){
                         $noActi = Doctrine_Query::create()
                             ->from('Actividad a')
                             ->andWhereIn('a.id', $ids)
                             ->leftJoin('a.ActividadLocalidad al')
                             ->leftJoin('al.Localidad l')
                             ->leftJoin('l.TipoOrigen to')
                             ->andWhere('to.orden_importancia > ?', $origen['orden_importancia'])
                             ->fetchArray();
                          //solo los Id que estan que no tienen uno de mayor nivel
                         $sacar= array();
                         foreach($noActi as $no){
                             $sacar[]=$no['id'];
                         }
                          foreach($ids as $rec_id){
                                  if (! in_array($rec_id, $sacar)) {
                                                     $ids1[]=$rec_id;
                                     }
                           }
                           if(count ($ids1)>0){
                             $rows[] = $this->getExportRecordRowOrigen($ids1, $origen, $total_a, $total_p);
                            }else{
                                $rows[]=  array($origen['tipo_origen'], '0', '0', '0', '0');
                            }
                               }

                
		$objWriter = new PHPExcel_Writer_Excel5($excel_builder->generateExcel($titulo, $cabecera, $rows));
                
		$tmp_dir =   sfConfig::get('sf_cache_dir').'/';
		$file_name = $tmp_dir.'exportar.xls';
                
                $objWriter->setTempDir($tmp_dir);
                
		$objWriter->save($file_name);
		$this->file = $file_name;
                $this->setTemplate('toExcel'); 
    
 	      }

 	public function getExportRecordRowOrigen($ids, $grupos = NULL, $totalA = 1, $totalP = 1 )
              {
                 
		//nombre del servicio
                 $row[]= $grupos['tipo_origen'];
                 // cantidad de visitas

                 $can_ps = Doctrine_Query::create()
                        ->from('Actividad a')
                        ->andWhereIn('a.id', $ids)
                         ->leftJoin('a.ActividadLocalidad al')
                         ->leftJoin('al.Localidad l')
                         ->innerJoin('l.TipoOrigen to')
                         ->andWhere('to.orden_importancia = ?', $grupos['orden_importancia'])
                         ->groupBy('a.id')
                        ->fetchArray();
                 $idss= array();
                 $canta=0;
                 foreach($can_ps as $cp){
                     $idss[]=$cp['id'];
                          $canta++;
                    }
                 if( count ($idss) >0 ){
                 $can_p = Doctrine_Query::create()
                        ->select('SUM(p.cantidad) AS cant')
                        ->from('Actividad a')
                        ->andWhereIn('a.id', $idss)
                        ->leftJoin('a.Participantes p')
                        ->fetchOne();
                  $cant=0;
                  if($can_p['cant'] > 0)
                	$cant= $can_p['cant'];                 
					
                  $row[]= $canta;
                  $t_act= ( $canta/$totalA ) * 100 ;
                  $t_act= round($t_act,2);
                  $row[]=  $t_act;
                  
                  $row[]= $cant;
                  $t_par= ($cant/$totalP ) * 100 ;
                  $t_par= round($t_par,2);
                  $row[]=  $t_par;
                 }else{
                     $row[]='0';
                     $row[]='0';
                     $row[]='0';
                     $row[]='0';
                 }
                return  $row;
              }

public function executeListListadoExterior(sfWebRequest $request)
 	  {
                $records = $this->getExportRecordsCollection2();
                $ids= array();
                //solo los Id que estan en el rango
                foreach($records as $rec){
	 			   $ids[]=$rec['id'];
				   }
                $total_a=  $this->getTotalActividad($ids);
                $total_p=  $this->getTotalParticipantes($ids);
                $rows = $this->setHeaders($total_a, $total_p);
                //REPORTES MEDIANTE GRUPOS ETEREOS

                $headers= array ( 'PROGRAMA', 'LOCALIDAD','LUGARES');
                $rows[] = implode("\t", $headers);
                 //TODOS LOS GRUPOS
                 $acts = Doctrine_Query::create()
                        ->from('Actividad a')
                        ->andWhereIn('a.id', $ids)
                        ->andWhere('a.lugar_id IS NOT NUll')
                        ->orderBy('a.nombre_actividad ASC')
                        ->fetchArray();

                $ids= array();
                   foreach($acts as $act ){
                         $rows[] = implode("\t",$this->getExportRecordRowFuera( $act, $total_a, $total_p));
                        }


                $response = $this->getContext()->getResponse();
                $response->clearHttpHeaders();
        //        $response->setHttpHeader('Pragma', '');
          //      $response->setHttpHeader('Cache-Control', '');
	        $response->setContentType('text/xls');
	        $response->setHttpHeader('Content-Disposition', 'attachment;filename='.$this->configuration->getExportFilename().'.xls;');
 	        return $this->renderText(implode("\n", $rows));
 	      }

 	public function getExportRecordRowFuera( $grupos = NULL, $totalA = 1, $totalP = 1 )
              {

		//nombre de la actividad
                 $row[0]= $grupos['nombre_actividad'];
                 // cantidad de visitas
                 $lugar = Doctrine::getTable('Lugar')->find($grupos['lugar_id']);
                 $row[1]= '"'.$lugar->getLocalidad()->getNombreLocalidad().'"';
                 $row[2]=  '"'.$lugar->getNombreLugar().'"';
                return  $row;
              }

      public function setHeadersPdf($t_a, $t_p){

                $blanco = array('', '',);
                $rows = array($this->helper->getExportColumnHeaders($blanco));
                 $titulo='NUMERO TOTAL DE VISITANTES EN SALA: '.$t_p.'<br/>';
                 $titulo.='TOTAL DE VISITAS ATENDIDAS: '.$t_a.'<br/><br/><br/>';

                return $titulo;

    }
     public function executeListReportePdf(sfWebRequest $request){

                $periodo=' ';
                $filters = $this->getFilters();
                      if($filters){
                            if($filters['fecha']['from'] || $filters['fecha']['from']){
                                $periodo= 'Periodo: ';
                                if($filters['fecha']['from']){
                                    $periodo.= 'desde ';
                                    $periodo.= $this->cambiarFormatoFecha($filters['fecha']['from']);
                                }
                                if($filters['fecha']['to']){
                                    $periodo.= ' hasta ';
                                    $f=$this->cambiarFormatoFecha2($filters['fecha']['to']);
                                    $periodo.= $f;
                                }
                            }
                      }


               $records = $this->getExportRecordsCollection2();
               $ids= array();
               //solo los Id que estan en el rango
                foreach($records as $rec){
	 			   $ids[]=$rec['id'];
				   }
                $total_a=  $this->getTotalActividad($ids);
                $total_p=  $this->getTotalParticipantes($ids);
                $cabeza = $this->setHeadersPdf($total_a, $total_p);
                
                    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'Legal', true, 'UTF-8', false);
                      // settings
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('Administrador');
                    $institution= $this->getUser()->getInstitutionName();
                    $pdf->SetTitle($institution);
                    $pdf->SetSubject('Imforme de visitas');
                    $pdf->SetKeywords('Informe de visitas, visitas, patrimonio, sistema, reporte');
                    
                   // $subtitulo=
                    
                    // set default header data
                    $subtitle= $this->getUser()->getInstitutionSubName();
                    if ( strlen ($subtitle) > 2 ){
                        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, "$subtitle \nInforme de visitas \n$periodo");
				    }else{
						$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, "Informe de visitas \n$periodo");
						} 
                    // set header and footer fonts
                    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

                    // set default monospaced font
                    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                    //set margins
                    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                      // init pdf doc

                    $pdf->AddPage();

                    $pdf->SetFont('helvetica', '', 8);
       
                    $pdf->writeHTML($cabeza, true, false, false, false, '');

                    $mes='<h3><b>Detalle de visitantes por mes</b></h3>';
                    $pdf->writeHTML($mes, true, false, false, false, '');
                    $data=$this->getMensualidad();
                    $header = array ('MES', 'NÚMERO DE VISITANTES', 'NÚMERO DE ACCIONES');
                    $pdf->ColoredTableVisitas($header, $data);

                    $edad='<br><h3><b>Detalle Visitantes por edades</b></h3>';
                    $pdf->writeHTML($edad, true, false, false, false, '');
                    $headers2= array ('GRUPO ETAREOS', 'NÚMERO DE VISITANTES', 'PORCENTAJE');
                    $data2= $this->getEdades($ids,$total_p);
                    $pdf->ColoredTableVisitas($headers2, $data2);

 
                    $tipo='<br><h3><b>Detalle de visitantes por tipo de actividad</b></h3>';
                    $pdf->writeHTML($tipo, true, false, false, false, '');
                    $headers4= array ( 'SERVICIO',  'TIPO DE ACTIVIDAD', 'N° ACCIONES', '%', 'C. DE PERSONA', '%');
                    $data4= $this->getServicios($ids,$total_p, $total_a );
                    $pdf->ColoredTableVisitas6($headers4, $data4);

                    $proce='<br><h3><b>Detalle de visitantes por procedencia</b></h3>';
                    $pdf->writeHTML($proce, true, false, false, false, '');
                    $headersP= array (  'PROCEDENCIA', 'N° ACCIONES', '%', 'C. DE PERSONA', '%');
                    $dataP= $this->getProcedencia($ids,$total_p, $total_a );
                    $pdf->ColoredTableVisitas5($headersP, $dataP);

//                    $fuera='<br><h3><b>Actividad fuera del museo</b></h3><br>';
//                    $pdf->writeHTML($fuera, true, false, false, false, '');
//                    $headers5= array ( 'PROGRAMA', 'LOCALIDAD', 'LUGARES');
//                    $data5= $this->getFuera($ids,$total_p, $total_a );
//                    $pdf->ColoredTableVisitas3($headers5, $data5);

                    $pdf->Output('Reporte.pdf', 'I');
         
     }
     public function getMensualidad(){
         
         $records = $this->getExportRecordsCollection2();
                $ids= array();
               //solo los Id que estan en el rango
                foreach($records as $rec){
	 			   $ids[]=$rec['id'];
				   }
                $total_a=  $this->getTotalActividad($ids);
                $total_p=  $this->getTotalParticipantes($ids);
                $meses= array( "1","2","3","4","5","6","7","8","9","10","11","12");
                $headers= 

                $header = array ('MES', 'NÚMERO DE VISITANTES', 'NÚMERO DE ACCIONES');

                 $data = array();

			   foreach($meses as $mes){
			       $data[]= $this->getExportRecordRowMesPdf($ids, $mes);
				  }
                
                       $data[]=  array('TOTAL', $total_p, $total_a);

                return $data;
     }
     public function getExportRecordRowMesPdf($ids, $mes = NULL)
              {
	            $mesName= array("MES", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" );
					//mes
	            $row[0]=$mesName[$mes];

					//participantes
					 $actividades = Doctrine_Query::create()
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhereIn('a.id', $ids)
                        ->andWhere('MONTH(a.fecha)=?', $mes)
                        ->fetchArray();
					
					
                     $cant=0;
                     foreach($actividades as $act ){
						$cant+= $act['cantidad'];
					  }
                     $row[1]= $cant;

					//actividades
		     $actividades = Doctrine_Query::create()
                        ->from('Actividad a')
                        ->where('MONTH(a.fecha)=?', $mes)
                        ->andWhereIn('a.id', $ids)
                        ->fetchArray();

                    $row[2]=count($actividades);


                return $row;
              }
              public function getEdades($ids,$total_p = 1){

                 $grupos = Doctrine_Query::create()
                        ->from('GruposEtareos g')
                        ->orderBy('g.edad_desde ASC')
                        ->fetchArray();
                        $rows= array();
			   foreach($grupos as $grup){
			       $rows[] = $this->getExportRecordRowGrupos($ids, $grup, $total_p);
				  }
                         $total= array('TOTAL', $total_p, '100%');
                          $rows[] =  $total;

                      return $rows;
              }

              public function getServicios($ids, $total_p = 1, $total_a ){

                 //INICIALIZADOR DE TOTALES
                 $totales= array('TOTAL',' - ', 0, 0, 0,0 );  
                 $servicios = Doctrine_Query::create()
                        ->from('Servicio g')
                        ->orderBy('g.nombre_servicio ASC')
                        ->fetchArray();
                   
                   $rows=array();
		   foreach($servicios as $serv){
                      $tipos = Doctrine_Query::create()
                        ->from('TipoActividad g')
                        ->andWhere('g.servicio_id =?', $serv['id'])
                        ->orderBy('g.tipo_actividad ASC')
                        ->fetchArray();

                        
                         foreach($tipos as $tipo){
                            $row = $this->getExportRecordRowServicios($ids, $tipo, $serv['nombre_servicio'], $total_a, $total_p);

                             $rows[] =$row;
                             $totales[2]=$totales[2]+ $row[2];
                             $totales[3]=$totales[3]+ $row[3];
                             $totales[4]=$totales[4]+ $row[4];
                             $totales[5]=$totales[5]+ $row[5];
                         }
                   }
                         
                          $rows[] =$totales;   
                          return $rows;
              }

              public function getOrigen($ids, $total_p = 1, $total_a = 1){


                $rows = array();
                 //TODOS LOS GRUPOS
                 $origenes = Doctrine_Query::create()
                        ->from('TipoOrigen g')
                        ->orderBy('g.orden_importancia ASC')
                        ->fetchArray();

                   foreach($origenes as $origen){
                         $ids1= array();
                         $noActi = Doctrine_Query::create()
                             ->from('Actividad a')
                             ->andWhereIn('a.id', $ids)
                             ->leftJoin('a.Origenes l')
                             ->leftJoin('l.TipoOrigen to')
                             ->andWhere('to.orden_importancia > ?', $origen['orden_importancia'])
                             ->fetchArray();
                          //solo los Id que estan que no tienen uno de mayor nivel
                         $sacar= array();
                         foreach($noActi as $no){
                             $sacar[]=$no['id'];
                         }
                          foreach($ids as $rec){
                                  if (! in_array($rec['id'], $sacar)) {
                                                     $ids1[]=$rec;
                                                     //sfContext::getInstance()->getLogger()->debug('ESTOSON '.$origen['orden_importancia'].'    EL ID: '.$rec['id'] );
                                     }
                           }
                           if(count ($ids1)>0){
                              $rows[] =  $this->getExportRecordRowOrigen($ids1, $origen, $total_a, $total_p);
                             }else{
                                   $rows[]=  array($origen['tipo_origen'] , '0', '0%', '0', '0%' );
                                 }
                           }
                  return $rows;
              }


           public function getFuera($ids, $total_p=1, $total_a = 1 ){


                $rows = array();
                 //TODOS LOS GRUPOS
                 $acts = Doctrine_Query::create()
                        ->from('Actividad a')
                        ->andWhereIn('a.id', $ids)
                        ->andWhere('a.lugar_id IS NOT NUll')
                        ->orderBy('a.nombre_actividad ASC')
                        ->fetchArray();

                $ids= array();
                   foreach($acts as $act ){
                         $rows[] = $this->getExportRecordRowFuera( $act, $total_a, $total_p );
                        }
              return $rows;
              }
    public function getProcedencia( $ids,$total_p, $total_a ){

                 //INICIALIZADOR DE TOTALES
                 $totales= array('TOTAL', 0, 0, 0,0 );  

                 //TODOS LOS GRUPOS
                 $origenes = Doctrine_Query::create()
                        ->from('TipoOrigen g')
                        ->orderBy('g.orden_importancia ASC')
                        ->fetchArray();
                   $ids_directo= $this->getTotalIds();
                   foreach($origenes as $origen){
                         $ids1= array();
                         $noActi = Doctrine_Query::create()
                             ->from('Actividad a')
                             ->andWhereIn('a.id', $ids_directo)
                             ->leftJoin('a.ActividadLocalidad al')
                             ->leftJoin('al.Localidad l')
                             ->leftJoin('l.TipoOrigen to')
                             ->andWhere('to.orden_importancia > ?', $origen['orden_importancia'])
                             ->fetchArray();
                          //solo los Id que estan que no tienen uno de mayor nivel
                         $sacar= array();
                         foreach($noActi as $no){
                             $sacar[]=$no['id'];
                         }
                          foreach($ids_directo as $rec){
                                  if (! in_array($rec, $sacar)) {
                                                     $ids1[]=$rec;             
                                     }
                           }
                           if(count ($ids1)>0){
                             $row =  $this->getExportRecordRowOrigen($ids1, $origen, $total_a, $total_p);
                             $rows[] =$row;
                             $totales[1]=$totales[1]+ $row[1];
                             $totales[2]=$totales[2]+ $row[2];
                             $totales[3]=$totales[3]+ $row[3];
                             $totales[4]=$totales[4]+ $row[4];

                            }else{
                                $rows[]=  array('"'.$origen['tipo_origen'].'"', '0', '0', '0', '0');
                            }
                               }

                          $row =  $this->getExportRecordRowSinOrigen($ids_directo, $origen, $total_a, $total_p);
                          $rows[] =$row;
                          $totales[1]=$totales[1]+ $row[1];
                          $totales[2]=$totales[2]+ $row[2];
                          $totales[3]=$totales[3]+ $row[3];
                          $totales[4]=$totales[4]+ $row[4];  
                          $rows[] =$totales;   
                          return $rows;

    }

public function getExportRecordRowSinOrigen($ids, $grupos = NULL, $totalA = 1, $totalP = 1 )
          {
             $row      = array();
             $row = array ('Sin Origen');
             
                   $total=0;
                    
                 $acts = Doctrine_Query::create()
                    ->from('Actividad a')
                     ->andWhereIn('a.id', $ids)
                     ->leftJoin('a.ActividadLocalidad al')     
                     ->leftJoin('al.Localidad l')
                     ->leftJoin('l.TipoOrigen to')
                     ->andWhere('to.orden_importancia > ?', 0)
                     ->groupBy('a.id')    
                     ->fetchArray();    
                 $idsCon= array();
                 $canta=0;

                 foreach ($acts as $a){
                     $idsCon[]=$a['id'];
                     $canta++;
                    }


                         
              $can_p = Doctrine_Query::create()
                    ->select('SUM(p.cantidad) AS cantidades') 
                    ->from('Participantes p')
                    ->leftJoin('p.Actividad a' )
                    ->andWhereIn('a.id', $ids) 
                    ->andWhereNotIn('a.id', $idsCon)
                    ->fetchOne(); 

                  $cant=0;
                  if($can_p['cantidades'] > 0){
                  $cant= $can_p['cantidades'];                 
          
                  $row[]= ($totalA - $canta) ;
                  $t_act= ( $canta/$totalA ) * 100 ;
                  $t_act= round($t_act,2);
                  $row[]=  $t_act;
                  
                  $row[]= $cant;
                  $t_par= ($cant/$totalP ) * 100 ;
                  $t_par= round($t_par,2);
                  $row[]=  $t_par;
                 }else{
                     $row[]='0';
                     $row[]='0';
                     $row[]='0';
                     $row[]='0';
                 }
                return  $row;
      
   }




    public function executeListReporteGrafico(sfWebRequest $request ){


             $this->periodo=' ';
                $filters = $this->getFilters();
                      if($filters){
                            if($filters['fecha']['from'] || $filters['fecha']['from']){
                                $this->periodo= 'Periodo: ';
                                if($filters['fecha']['from']){
                                    $this->periodo.= 'desde ';
                                    $this->periodo.= $this->cambiarFormatoFecha($filters['fecha']['from']);
                                }
                                if($filters['fecha']['to']){
                                    $this->periodo.= ' hasta ';
                                    $f=$this->cambiarFormatoFecha2($filters['fecha']['to']);
                                    $this->periodo.= $f;
                                }
                            }
                      }

           $records = $this->getExportRecordsCollection2();




                $ids= array();
               //solo los Id que estan en el rango
                foreach($records as $rec){
	 			   $ids[]=$rec['id'];
				   }
                $this->total_a=  $this->getTotalActividad($ids);
                $this->total_p=  $this->getTotalParticipantes($ids);

                $records = $this->getExportRecordsCollection2();

                $ids= array();
               //solo los Id que estan en el rango
                foreach($records as $rec){
	 			          $ids[]=$rec['id'];
				         }
//-----------------------ANUAL-------------------------------------------------------------------------------
                $meses= array( "1","2","3","4","5","6","7","8","9","10","11","12");
                $this->row = array();
                $headers= array ('MES', '"'.'NÚMERO DE VISITANTES'.'"', '"'.'NÚMERO DE ACCIONES'.'"');
                

			   foreach($meses as $mes){
			       $this->row[] = $this->getExportRecordRowMesGrafic($ids, $mes);
				  }

//-------------------------ETAREO----------------------------------------------------------------------------
              $grupos = Doctrine_Query::create()
                        ->from('GruposEtareos g')
                        ->orderBy('g.edad_desde ASC')
                        ->fetchArray();
                        $this->etareo= array();
			   foreach($grupos as $grup){
			       $this->etareo[] = $this->getExportRecordRowGruposGrafic($ids, $grup, $this->total_p);
				  }
//-----------------Servicios--------------------------------------------------------------------------------------
           $servicios = Doctrine_Query::create()
                        ->from('Servicio g')
                        ->orderBy('g.nombre_servicio ASC')
                        ->fetchArray();

                   $this->servis= array();

		   foreach($servicios as $serv){
                      $tipos = Doctrine_Query::create()
                        ->from('TipoActividad g')
                        ->andWhere('g.servicio_id =?', $serv['id'])
                        ->orderBy('g.tipo_actividad ASC')
                        ->fetchArray();

                           foreach($tipos as $tipo){
                                       $this->servis[] = $this->getExportRecordRowServiciosGrafic($ids, $tipo, $serv['nombre_servicio'], $this->total_a, $this->total_p);
                                          }
                     }


                     
//------------------------Procedencia-----------------------------------------------------------------------------------
            $origenes = Doctrine_Query::create()
                        ->from('TipoOrigen g')
                        ->orderBy('g.orden_importancia ASC')
                        ->fetchArray();
                    $this->rowsor= array();
                   foreach($origenes as $origen){
                         $ids1= array();
                         $noActi = Doctrine_Query::create()
                             ->from('Actividad a')
                             ->andWhereIn('a.id', $ids)
                             ->leftJoin('a.ActividadLocalidad al')
                             ->leftJoin('al.Localidad l')
                             ->leftJoin('l.TipoOrigen to')
                             ->andWhere('to.orden_importancia > ?', $origen['orden_importancia'])
                             ->fetchArray();
                          //solo los Id que estan que no tienen uno de mayor nivel
                         $sacar= array();
                         
                         foreach($noActi as $no){
                             $sacar[]=$no['id'];
                         }
                          foreach($records as $rec){
                                  if (! in_array($rec['id'], $sacar)) {
                                                     $ids1[]=$rec['id'];
                                     }
                           }
                           if(count ($ids1)>0){
                             $this->rowsor[] = $this->getExportRecordRowOrigenGrafic($ids1, $origen, $this->total_a, $this->total_p);
                            }else{
                                $this->rowsor[]= array( $origen['tipo_origen'], '0', '0%', '0', '0%');
                            }
                               }


//----------------------------------------------------------
        $this->setTemplate('grafic');
    }
        public function getExportRecordRowMesGrafic($ids, $mes = NULL)  {
	             $mesName= array("MES", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" );					//mes
	             $row[0]= $mesName[$mes];
                     $actividades = Doctrine_Query::create()
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhereIn('a.id', $ids)
                        ->andWhere('MONTH(a.fecha)=?', $mes)
                        ->fetchArray();
                     $cant=0;
                     foreach($actividades as $act ){
						$cant+= $act['cantidad'];
					  }
                     $row[2]= $cant;
		     $actividades = Doctrine_Query::create()
                        ->from('Actividad a')
                        ->andWhere('MONTH(a.fecha)=?', $mes)
                        ->andWhereIn('a.id', $ids)
                        ->fetchArray();

                     $row[1]= count($actividades);
                return  $row;
              }
 	public function getExportRecordRowGruposGrafic($ids, $grupos = NULL, $total = 1 ){
		$band= false;

		//nombre del campo
                if($grupos['edad_hasta'] >0 ){
		   $row[0]= $grupos['edad_desde'].' - '.$grupos['edad_hasta'].' años';

					//participantes
                      $actividades = Doctrine_Query::create()
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhereIn('a.id ', $ids)
                        ->andWhere('p.edad_aproximada >= ? AND p.edad_aproximada < ?', array($grupos['edad_desde'], $grupos['edad_hasta']))
                        ->fetchArray();
                }else{
                    $row[0]= 'Mayor de '.$grupos['edad_desde'].' años';

					//participantes
                    $actividades = Doctrine_Query::create()
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhereIn('a.id ', $ids)
                        ->andWhere('p.edad_aproximada >= ?', $grupos['edad_desde'])
                        ->fetchArray();
                }


                     $cant=0;
                     foreach($actividades as $act ){
						$cant+= $act['cantidad'];
					  }
                     $row[1]= $cant;
                     $porcentaje = ( $cant / $total ) * 100 ;
                     $porcentaje= round($porcentaje,2);
                     $row[2]=  $porcentaje;
                return  $row;
              }
        public function getExportRecordRowServiciosGrafic($ids, $grupos = NULL, $servicio, $totalA = 1, $totalP = 1 ) {

		//nombre del servicio
                 $row[0]= $servicio;
                 $row[1]= $grupos['tipo_actividad'];
                 $canActi = Doctrine_Query::create()
                        ->from('Actividad a')
                        ->andWhereIn('a.id', $ids)
                         ->andWhere('a.tipo_actividad_id =? ', $grupos['id'])
                        ->fetchArray();
                  $row[2]= count($canActi);
                  $t_act= ( count($canActi)/$totalA ) * 100 ;
                  $t_act= round($t_act,2);
                  $row[3]=  $t_act;

					//participantes
					 $actividades = Doctrine_Query::create()
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhereIn('a.id', $ids)
                        ->andWhere('a.tipo_actividad_id =? ', $grupos['id'])
                        ->fetchArray();
					
                    

                     $cant=0;
                     foreach($actividades as $act ){
						$cant+= $act['cantidad'];
					  }
                     $row[4]= $cant;
                     $t_par= ($cant/$totalP ) * 100 ;
                     $t_par= round($t_par,2);
                     $row[5]=  $t_par;
                return  $row;
              }
function getExportRecordRowOrigenGrafic($ids, $grupos = NULL, $totalA = 1, $totalP = 1 )
              {

		//nombre del servicio
                 $row[0]= $grupos['tipo_origen'];
                 // cantidad de visitas

                 $can_ps = Doctrine_Query::create()
                        ->from('Actividad a')
                        ->andWhereIn('a.id', $ids)
                         ->leftJoin('a.ActividadLocalidad al')
                         ->leftJoin('al.Localidad l')
                         ->innerJoin('l.TipoOrigen to')
                         ->andWhere('to.orden_importancia = ?', $grupos['orden_importancia'])
                         ->groupBy('a.id')
                        ->fetchArray();
                 $idss= array();
                 $canta=0;
                 foreach($can_ps as $cp){
                     $idss[]=$cp['id'];
                          $canta++;

                 }
                 if( count ($idss) >0 ){
                 $can_p = Doctrine_Query::create()
                        ->select('SUM(p.cantidad)')
                        ->from('Actividad a')
                        ->andWhereIn('a.id', $idss)
                        ->leftJoin('a.Participantes p')
                        ->fetchArray();
                  $cant=0;
                     foreach($can_p as $act ){
						$cant= $act['SUM'];
					  }
                  $row[1]= $canta;
                  $t_act= ( count($can_p)/$totalA ) * 100 ;
                  $t_act= round($t_act,2);
                  $row[2]=  $t_act;

                  $row[3]= $cant;
                  $t_par= ($cant/$totalP ) * 100 ;
                  $t_par= round($t_par,2);
                  $row[4]=  $t_par;
                 }else{
                     $row[1]=0;
                     $row[2]=0;
                     $row[3]=0;
                     $row[4]=0;
                 }
                return  $row;
              }
              
              
              public function executeListListadoTotal(){
            
                ini_set("max_execution_time",0);
		$this->setLayout(false);
		sfConfig::set('sf_web_debug', false);

		$resp = $this->getResponse();
		$resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
		$resp->setHttpHeader('Content-Disposition', ' attachment; filename="export.xls"');
		$resp->setHttpHeader('Cache-Control', ' maxage=3600');
		$resp->setHttpHeader('Pragma', 'public');
                  
		$excel_builder = new ExcelBuilder();
                
                $ids = $this->getTotalIds(); 
                $total_a=  $this->getTotalActividad($ids);
                $total_p=  $this->getTotalParticipantes($ids);
                
                 //REPORTES MEDIANTE SERVICIOS
                ini_set("max_execution_time",0);
		$this->setLayout(false);
		sfConfig::set('sf_web_debug', false);

		$resp = $this->getResponse();
		$resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
		$resp->setHttpHeader('Content-Disposition', ' attachment; filename="export.xls"');
		$resp->setHttpHeader('Cache-Control', ' maxage=3600');
		$resp->setHttpHeader('Pragma', 'public');
                  
		$excel_builder = new ExcelBuilder();
                
               $ids = $this->getTotalIds(); 
 
                $meses    = array( "1","2","3","4","5","6","7","8","9","10","11","12");
                $row      = array();
                $titulo   = 'Visitas mensuales';
                $cabecera = array ('MES', 'NÚMERO DE VISITANTES', 'NÚMERO DE ACCIONES');

                foreach($meses as $mes){
                   $rows[] = $this->getExportRecordRowMes($ids, $mes);   		 
                      }  
                
		$objWriter = new PHPExcel_Writer_Excel5($excel_builder->generateExcel($titulo, $cabecera,$rows));
                
		$tmp_dir =   sfConfig::get('sf_cache_dir').'/';
		$file_name = $tmp_dir.'exportar.xls';
		$objWriter->save($file_name);
		$this->file = $file_name;
                $this->setTemplate('toExcel'); 
                
		$objWriter = new PHPExcel_Writer_Excel5($excel_builder->generateExcel($titulo, $cabecera, $rows));
                
		$tmp_dir =   sfConfig::get('sf_cache_dir').'/';
		$file_name = $tmp_dir.'exportar.xls';
		$objWriter->save($file_name);
		$this->file = $file_name;
                $this->setTemplate('toExcel'); 
                  
                  
                  
              }

}
