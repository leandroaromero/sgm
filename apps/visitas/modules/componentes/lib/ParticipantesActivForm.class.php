<?php

/**
 * Participantes form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ParticipantesActivForm extends BaseParticipantesForm
{
  public function configure()
  {
      //  $this->widgetSchema['origenes_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
   /*    $this->widgetSchema['pais_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'País ',
              'model'   => 'Pais',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
        $this->widgetSchema['provincia_id'] = sfWidgetFormDoctrineDependentSelect(array(
              'label' => 'Provincia ',
              'model'   => 'Provincia',
              'table_method' => 'getOrderNombre',
              'depends' => 'Pais'));
        $this->widgetSchema['localidad_id'] = new sfWidgetFormDoctrineDependentSelect(array(
              'label' => 'Localidad ',
              'model'   => 'Localidad',
              'table_method' => 'getOrderNombre',
              'depends' => 'Provincia'));
       $this->widgetSchema['lugar_id'] = new sfWidgetFormDoctrineDependentSelect(array(
              'label' => 'Lugar ',
              'model'   => 'Lugar',
              'table_method' => 'getOrderNombre',
              'depends' => 'Localidad'));*/
        $this->widgetSchema['origenes_list']->setOption('label', 'Origenes');
        $this->widgetSchema['origenes_list']->setOption('table_method', 'getOrderNombre');
    /*    $this->widgetSchema['origenes_list']=
                new sfWidgetFormDoctrineDependentSelect(array(
              'label' => 'Origenes ',
              'model'   => 'PrticipantesLocalidad',
              'table_method' => 'getOrderNombre',
              'depends' => 'Lugar'));*/
        $this->widgetSchema['actividad_id']= new sfWidgetFormInputHidden();
  }
}
