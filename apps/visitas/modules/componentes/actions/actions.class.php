<?php

/**
 * componentes actions.
 *
 * @package    museo
 * @subpackage componentes
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class componentesActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeCambiar(sfWebRequest $request)
  {   
    $cambio= $request->getParameter('institucion');
    $this->getUser()->setAttribute('base_dato', $cambio['cambio']);
    $this->redirect('@homepage');
    
  }
  public function executeCambiarLogo(sfWebRequest $request)
  {   
    $cambio= $request->getParameter('institucion');
    $this->getUser()->setAttribute('base_dato', $cambio['cambio']);
    
  }
  
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }

  public function executeCargarParticipantes($request)
  {
      $p_id = $this->getRequestParameter('participante[id]');
      $this->a_id = $this->getRequestParameter('actividad_id');
      if($p_id >0){
          $this->forward404Unless($this->participante = Doctrine::getTable('Participantes')->find(array($p_id)), sprintf('Este participante no exite (%s).', $p_id));
      }else{
             $this->participante= new Participantes();
             $this->participante->setActividadId($this->a_id);
           }
      $this->form= new ParticipantesActivForm($this->participante);
      
  }
  public function executeCancelarParticipantes(sfWebRequest $request)
  {
         $id= $request->getParameter('id');
         $this->forward404Unless($this->actividad = Doctrine::getTable('Actividad')->find(array($id)), sprintf('Actividad does not exist (%s).', $id));
         $this->form= new ActividadForm($this->actividad);
         $this->setTemplate('participantes');
  }

 public function executeGrabarParticipantes(sfWebRequest $request)
  {
         $p_id= $request->getParameter('p_id');
     if( $p_id > 0){
         $this->forward404Unless($participante = Doctrine::getTable('Participantes')->find(array($p_id)), sprintf('participante does not exist (%s).', $p_id));
     }else{
         $participante = new Participantes();
     }
      $this->form= new ParticipantesActivForm($participante);
      $this->processForm($request, $this->form);
     
  }

 protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $band= false;
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $participante = $form->save();
      $band= true;
    }
    if($band){
      $this->actividad = Doctrine::getTable('Actividad')->find($participante->getActividadId());
      $this->form= new ActividadForm($this->actividad);
       $this->setTemplate('participantes');
    }else{
        $this->a_id=  $form->getObject()->getActividadId();
         $this->setTemplate('cargarParticipantes');
    }
  }
 public function  executeDeleteParticipantes($request) {
        $this->forward404Unless($participante = Doctrine::getTable('Participantes')->find(array($request->getParameter('id'))), sprintf('Este participantes no exite (%s).', $request->getParameter('id')));
        $id= $participante->getActividadId();
        $participante->delete();
        $this->forward404Unless($this->actividad = Doctrine::getTable('Actividad')->find(array($id)), sprintf('Actividad does not exist (%s).', $id));
        $this->form= new ActividadForm($this->actividad);
        $this->setTemplate('participantes');


    }
    public function executeGetOrigen($request)
	{
	    $this->getResponse()->setContentType('application/json');
	    $donacion = Doctrine::getTable('Localidad')->retrieveForSelect(
	                $request->getParameter('q'),
	                $request->getParameter('limit')
	    );

	    return $this->renderText(json_encode($donacion));
	}
}
