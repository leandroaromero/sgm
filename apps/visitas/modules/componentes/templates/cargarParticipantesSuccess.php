<?php use_helper('jQuery') ?>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<div id="dato_participante1">
<form action="<?php echo url_for('componentes/'.($form->getObject()->isNew() ? 'grabarTelefono' : 'grabarTelefono').(!$form->getObject()->isNew() ? '?t_id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td>
          <?php echo $form->renderHiddenFields(false) ?>
            <?php if (! $form->getObject()->isNew() ):?>
            <?php echo jq_link_to_remote( 'Borrar ', array(
                                        'update' => 'dato_participante',
                                        'url' => 'componentes/deleteParticipantes?id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                        'confirm' => '¿Esta usted seguro que desea eliminar este participante?'
                                                  )); ?>

            <?php endif ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td><td>
          &nbsp;
          <?php echo jq_link_to_remote('Cancelar', array(
                                        'update' => 'dato_participante',
                                        'url' => 'componentes/cancelarParticipantes?id='.$a_id,
                                        'script' => 'true',
                                                  )); ?>

          <?php  echo jq_submit_to_remote('agregar','Grabar', array(
          'update' => 'dato_participante',
          'url'    => 'componentes/grabarParticipantes?p_id='.$form->getObject()->getId(),
           ))
            ?>



        </td>
      </tr>
    </tfoot>
    <tbody>
        <tr><?php echo $form?></tr>
        
        
    </tbody>
  </table>
</form>
</div>