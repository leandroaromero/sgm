<?php use_helper('jQuery') ?>
<div class="sf_admin_form_row sf_admin_text sf_admin_form_field_telefonos">
<label>Lista de participantes </label>
<div id="dato_participante">

<center>
<table  class="sf_admin_list" width="70%">
                    <tr>
                        <th>Edad aproximada</th>
                        <th>Cantidad</th>
                        <th>Acción</th>
                    </tr>
                <?php  $con=0; $i=1; foreach ($form->getObject()->getParticipantes() as $par): $odd = fmod(++$i, 2) ;$band=false; ?>
                   <tr class="sf_admin_row_<?php echo $odd ?>">
                      <td><?php echo $par->getEdadAproximada()?></td>
                      <td><?php echo $par->getCantidad()?></td>
                      <td>
                          <?php echo jq_link_to_remote(image_tag('edit16.png', array('alt' => 'Agregar', 'title' =>'Modificar')), array(
                                        'update' => 'dato_participante',
                                        'url' => 'componentes/cargarParticipantes?participante[id]='.$par->getId().'&actividad_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  )); ?>
                      </td>

                   </tr>
                <?php endforeach; ?>

</table>

    <?php echo jq_link_to_remote(image_tag('new.png', array('alt' => __('Agregar'), 'title' => __('Agregar un participante'))), array(
                                        'update' => 'dato_participante',
                                        'url' => 'componentes/cargarParticipantes?actividad_id='.$form->getObject()->getId(),
                                        'script' => 'true',
                                                  )); ?>



    </center>
</div>

</div>