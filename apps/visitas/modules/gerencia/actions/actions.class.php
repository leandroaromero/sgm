<?php

/**
 * gerencia actions.
 *
 * @package    museo
 * @subpackage gerencia
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class gerenciaActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
      $this->form= new ActividadGFormFilter();
  }

  public function executeInforme(sfWebRequest $request)
  {
          ini_set("max_execution_time",0);
          ini_set('memory_limit', '-1');                      
          ini_set('post_max_size','10M');
          ini_set('max_execution_time','1000');
          
      
      
      
      
      $form= $request->getParameter('actividad_filters');
 
      $this->titulos=array();
      $cambio=array('museo'=>'todo');
      $this->search = new SearchBuilder($cambio);
      $this->tablas= array();
      // visitas mensuales
      $this->titulos[]= "Informe por mes";
      $this->tablas[]= $this->getExportRecordRowMes( $form ) ;
      
//      //visitas grupos
      $this->titulos[]= "Informe por grupo etáreo";
      $this->tablas[]= $this->getExportRecordRowGrupos($form );
//      //visitas servicios
      $this->titulos[]= "Informe por servicios";
      $this->tablas[]= $this->getExportRecordRowServicios($form );
      //actividades
      $this->titulos[]= "Informe por tipo de actividad";
      $this->tablas[]=$this->getExportRecordRowActividades($form );
//      //visitas origen
      $this->titulos[]= "Informe por origen";
      $this->tablas[]=$this->getExportRecordRowOrigen($form);
//
//      $this->titulos[]= "Informe actualmente sin origen";
//      $this->tablas[]= $this->getExportRecordRowSinOrigen($form);
//      
  
      
            
      $fechaD=$form['fecha']['from'];
      $fechaH=$form['fecha']['to'];
      $this->fechaD= $fechaD['day'].'/'.$fechaD['month'].'/'.$fechaD['year'];
      $this->fechaH= $fechaH['day'].'/'.$fechaH['month'].'/'.$fechaH['year'];
      $tipo_actividad= "";
      if($form['tipo_actividad_id'] > 0){
      $tipo_actividad= Doctrine_Core::getTable('TipoActividad')->find( $form['tipo_actividad_id']);
      $tipo_actividad=$tipo_actividad->getTipoActividad();
      }
      $this->Filtros=array($this->fechaD, $this->fechaH, $tipo_actividad); 
      
      $this->getUser()->setAttribute('tabla_visita',   $this->tablas);
      $this->getUser()->setAttribute('titulos_visita', $this->titulos);
      $this->getUser()->setAttribute('filtros_visita', $this->Filtros);
   }
   
   public function executeToExcel($request) { 
       
       
                ini_set("max_execution_time",0);
		$this->setLayout(false);
		sfConfig::set('sf_web_debug', false);

		$resp = $this->getResponse();
		$resp->setHttpHeader('Content-type', 'application/x-excel; charset=UTF-8');
		$resp->setHttpHeader('Content-Disposition', ' attachment; filename="Informe General.xlsx"');
		$resp->setHttpHeader('Cache-Control', ' maxage=3600');
		$resp->setHttpHeader('Pragma', 'public');
                
		$excel_builder = new ExcelBuilder();
                
                $tabla=   $this->getUser()->getAttribute('tabla_visita');
                $titulos= $this->getUser()->getAttribute('titulos_visita');
                $filtros= $this->getUser()->getAttribute('filtros_visita');
                   
                 $objWriter = new PHPExcel_Writer_Excel2007($excel_builder->generateExcelGeneral($titulos, $tabla, $filtros)); 

		$tmp_dir =   sfConfig::get('sf_cache_dir').'/';           
		$file_name = $tmp_dir.'Informe General.xlsx';                

		$objWriter->save($file_name);
		$this->file = $file_name;

   }

   public function getExportRecordRowSinOrigen($form)
          {
             $row      = array();
             $tabla    = array();
             $cabecera = array ('');
             $row = array ('Sin Origen');
             foreach($this->search->getBasesDatos() as $bd){
                 $cabecera[]=$bd['head'];
             } 
             $cabecera[]='TOTAL';
             $tabla[]=$cabecera;
             //FECHAS 
             $fechaD=$form['fecha']['from'];
             $fechaH=$form['fecha']['to'];
             
                   $total=0;
                    
                 foreach($this->search->getBasesDatos() as $bd){
                 $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
                      
                 $acts = Doctrine_Query::create($conn)
                 //  ->select('a.id as id') 
                    ->from('Actividad a')
                     ->leftJoin('a.ActividadLocalidad al')     
                     ->leftJoin('al.Localidad l')
                     ->leftJoin('l.TipoOrigen to')
                     ->andWhere('to.orden_importancia > ?', 0)
                     ->groupBy('a.id')    
                     ->fetchArray();    
                 $ids= array();
                 foreach ($acts as $a)
                     $ids[]=$a['id'];
                         
                  $q = Doctrine_Query::create($conn)
                    ->select('SUM(p.cantidad) AS cantidades') 
                    ->from('Participantes p')
                    ->leftJoin('p.Actividad a' );
                  
                  if( ($form['tipo_actividad_id'] > 0 ) && !empty($form['tipo_actividad_id']) ){
                      $q->andWhere('a.tipo_actividad_id = ?', $form['tipo_actividad_id'] );
                 }
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q->andWhere('a.fecha >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q->andWhere('a.fecha <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                          
                   $q ->andWhereNotIn('a.id', $ids);
                  
                  
                    $result=  $q->fetchArray(); 

                           if($result[0]['cantidades']>0){
                               $row[$bd['file']]= $result[0]['cantidades'];
                               //total por fila
                               $total=$total + $result[0]['cantidades'];
                           }else{
                               $row[$bd['file']]=0;
                           }
                      }
                  $row[]=$total;
                  $tabla[]=$row;
               
               return $row;//$tabla;
      
   }
  
   public function getExportRecordRowOrigen($form)
          {
             $row      = array();
             $tabla    = array();
             $cabecera = array ('PROCEDENCIA');
             $total_pie = array ('TOTAL');
             $total_final= 0;
             foreach($this->search->getBasesDatos() as $bd){
                 $cabecera[]=$bd['head'];
                 $total_pie[$bd['file']]=0;
             } 
             $cabecera[]='TOTAL';
             $tabla[]=$cabecera;
             //FECHAS 
             $fechaD=$form['fecha']['from'];
             $fechaH=$form['fecha']['to'];
             
             $q = Doctrine_Query::create()
                        ->from('TipoOrigen g')
                        ->orderBy('g.orden_importancia ASC');
             $origenes=$q->fetchArray();
             
             //La siguiente linea comentada saco del informe los registros que no tienen origenes
             //ya que se implemento la obligatoriedad del origen en los informes
             //$row_sin_o= $this->getExportRecordRowSinOrigen($form);
             
             
             foreach($origenes as $origen){
                   $total=0;
                 
                 $row[]=$origen['tipo_origen'];
                 
                 foreach($this->search->getBasesDatos() as $bd){
                     
                 $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
                      
                 $no_acts = Doctrine_Query::create($conn)
                             ->select('a.id as id') 
                             ->from('Actividad a')
                             ->leftJoin('a.ActividadLocalidad al')
                             ->leftJoin('al.Localidad l')
                             ->leftJoin('l.TipoOrigen to')
                             ->where('to.orden_importancia > ?', $origen['orden_importancia'])
                             ->addGroupBy('a.id')
                             ->fetchArray();  
                 $sacar= array();
                 foreach ($no_acts as $n_a){
                     $sacar[]=$n_a['id'];
                 }     
                 $acts = Doctrine_Query::create($conn)
                             ->select('a.id as id') 
                             ->from('Actividad a')
                             ->leftJoin('a.ActividadLocalidad al')
                             ->leftJoin('al.Localidad l')
                             ->where('l.tipo_origen_id = ? ', $origen['id'])
                             ->addGroupBy('a.id')
                             ->fetchArray();  
                 
                 
                 $ids= array(0);
                 foreach($acts as $rec){
                                  if (! in_array($rec['id'], $sacar)) {
                                                     $ids[]=$rec['id'];
                                                   
                                     }
                           }
                 
                  $q = Doctrine_Query::create($conn)
                    ->select('SUM(p.cantidad) AS cantidades') 
                    ->from('Participantes p')
                    ->leftJoin('p.Actividad a' )
                    ->whereIn('a.id', $ids);     
                  if( ($form['tipo_actividad_id'] > 0 ) && !empty($form['tipo_actividad_id']) ){
                      $q->andWhere('a.tipo_actividad_id = ?', $form['tipo_actividad_id'] );
                 }
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q->andWhere('a.fecha >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q->andWhere('a.fecha <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                          
                  
                    $result=  $q->fetchArray(); 

                           if($result[0]['cantidades']>0){
                               $row[]= $result[0]['cantidades'];
                               //total por fila
                               $total=$total + $result[0]['cantidades'];
                               //total por pie
                               $total_pie[$bd['file']]=$total_pie[$bd['file']]+ $result[0]['cantidades'];
                           }else{
                               $row[]=0;
                           }
                      }
                 
                      
                  $row[]=$total;
                  $total_final= $total_final+ $total;
                  $tabla[]=$row;
                  $row  = array();
               }  
//               Ya no se considera los que no tienen origen 
//               ///caso especial por los sin origenes
//               
//               foreach($this->search->getBasesDatos() as $bd){
//                       $total_pie[$bd['file']]= $total_pie[$bd['file']] + $row_sin_o[$bd['file']];              
//               }
//               $tabla[]=  $row_sin_o;//$this->getExportRecordRowSinOrigen($form);
               
               $total_pie[]=$total_final;
               $tabla[]=$total_pie;
               
//                $tabla[]= $this->getExportRecordRowSinOrigen($form);   
//               $total_pie[]='';
//               $tabla[]=$total_pie;
               
               return $tabla;
      
      
   }
              
    public function getExportRecordRowServicios($form)
    {
             $row      = array();
             $tabla    = array();
             $cabecera = array ('SERVICIO');
             $total_pie = array ('TOTAL');
             $total_final= 0;
             foreach($this->search->getBasesDatos() as $bd){
                 $cabecera[]=$bd['head'];
                 $total_pie[$bd['file']]=0;
             } 
             $cabecera[]='TOTAL';
             $tabla[]=$cabecera;
             //FECHAS 
             $fechaD=$form['fecha']['from'];
             $fechaH=$form['fecha']['to'];
             
              $t= Doctrine_Query::create()
                ->select('s.nombre_servicio as servicio' )      
                ->from('Servicio s')           
                ->orderBy('s.nombre_servicio ASC');

                $tipos=$t->fetchArray();

                 foreach($tipos as $tipo ){  
                   $total=0;
                   $row[]= $tipo['servicio'];
                    
                      foreach($this->search->getBasesDatos() as $bd){
                      $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
                        //participantes
                         $q = Doctrine_Query::create($conn)
                                ->select('SUM(p.cantidad) as cantidades')  
                                ->from('Participantes p')
                                ->leftJoin('p.Actividad a' );
                  if(($form['tipo_actividad_id'] > 0)&& !empty($form['tipo_actividad_id']) ){
                      $q->andWhere('a.tipo_actividad_id = ?', $form['tipo_actividad_id'] );
                 }
                      
                                  if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                                         $q->andWhere('a.fecha >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                                  }
                                  if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                                        $q->andWhere('a.fecha <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                                  }
                                  $q->leftJoin('a.TipoActividad ta' )
                                  ->leftJoin('ta.Servicio s' )        
                                  ->andWhere('s.nombre_servicio = ? ', $tipo['servicio']);
				
                          $result=  $q->fetchArray(); 

                           if($result[0]['cantidades']>0){
                               $row[]= $result[0]['cantidades'];
                               //total por fila
                               $total=$total + $result[0]['cantidades'];
                               //total por pie
                               $total_pie[$bd['file']]=$total_pie[$bd['file']]+ $result[0]['cantidades'];
                           }else{
                               $row[]=0;
                           }
                      }
                  $row[]=$total;
                  $total_final= $total_final+ $total;
                  $tabla[]=$row;
                  $row  = array();
               }  
               $total_pie[]=$total_final;
               $tabla[]=$total_pie;
               
               return $tabla;
      
   }            
   public function getExportRecordRowActividades($form)
    {
             $row      = array();
             $tabla    = array();
             $cabecera = array ('SERVICIO', 'TIPO DE ACTIVIDAD');
             $total_pie = array ('TOTAL', '');
             $total_final= 0;
             foreach($this->search->getBasesDatos() as $bd){
                 $cabecera[]=$bd['head'];
                 $total_pie[$bd['file']]=0;
             } 
             $cabecera[]='TOTAL';
             $tabla[]=$cabecera;
             //FECHAS 
             $fechaD=$form['fecha']['from'];
             $fechaH=$form['fecha']['to'];
             
              $t= Doctrine_Query::create()
                ->select('ta.tipo_actividad as tipo, s.nombre_servicio as servicio' )      
                ->from('TipoActividad ta')
                ->leftJoin('ta.Servicio s')           
                ->orderBy('ta.tipo_actividad ASC')
                ->orderBy('s.nombre_servicio ASC');

                $tipos=$t->fetchArray();

                 foreach($tipos as $tipo ){  
                   $total=0;
                   $row[]= $tipo['servicio'];
                   $row[]= $tipo['tipo'];
                    
                      foreach($this->search->getBasesDatos() as $bd){
                      $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
                        //participantes
                         $q = Doctrine_Query::create($conn)
                                ->select('SUM(p.cantidad) as cantidades')  
                                ->from('Participantes p')
                                ->leftJoin('p.Actividad a' );
                  if( ($form['tipo_actividad_id']> 0 )&& !empty($form['tipo_actividad_id']) ){
                      $q->andWhere('a.tipo_actividad_id = ?', $form['tipo_actividad_id'] );
                 }
                      
                                  if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                                         $q->andWhere('a.fecha >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                                  }
                                  if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                                        $q->andWhere('a.fecha <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                                  }
                                  $q->leftJoin('a.TipoActividad ta' )
                                  ->andWhere('ta.tipo_actividad = ? ', $tipo['tipo']);
				
                          $result=  $q->fetchArray(); 

                           if($result[0]['cantidades']>0){
                               $row[]= $result[0]['cantidades'];
                               //total por fila
                               $total=$total + $result[0]['cantidades'];
                               //total por pie
                               $total_pie[$bd['file']]=$total_pie[$bd['file']]+ $result[0]['cantidades'];
                           }else{
                               $row[]=0;
                           }
                      }
                  $row[]=$total;
                  $total_final= $total_final + $total;
                  $tabla[]=$row;
                  $row  = array();
               } 
                $total_pie[]=$total_final;
               $tabla[]=$total_pie;
               
               return $tabla;
      
   }
              

  public function getExportRecordRowGrupos($form)
  {	
             $row      = array();
             $tabla    = array();
             $cabecera = array ('GRUPO');
             $total_pie = array ('TOTAL');
             $total_final= 0;
             foreach($this->search->getBasesDatos() as $bd){
                 $cabecera[]=$bd['head'];
                 $total_pie[$bd['file']]=0;
             } 
             $cabecera[]='TOTAL';
             $tabla[]=$cabecera;
             //FECHAS 
             $fechaD=$form['fecha']['from'];
             $fechaH=$form['fecha']['to'];
             $grupos = Doctrine_Query::create()
                    ->from('GruposEtareos g')
                    ->orderBy('g.edad_desde ASC')
                    ->fetchArray();

             foreach($grupos as $grup){  
                      $total=0;
                      
                if($grup['edad_hasta'] >0 ){
		   $row[]= '"'.$grup['edad_desde'].' - '.$grup['edad_hasta'].' años'.'"';
                 }else{
                       $row[]='"'. 'Mayor de '.$grup['edad_desde'].' años'.'"';
                 }
                  
                 foreach($this->search->getBasesDatos() as $bd){
                      $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
			       
                        //participantes
                      $q = Doctrine_Query::create($conn)
                        ->select('SUM(p.cantidad) as cantidades')    
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' );
                  if( ($form['tipo_actividad_id'] > 0 )&& !empty($form['tipo_actividad_id']) ){
                      $q->andWhere('a.tipo_actividad_id = ?', $form['tipo_actividad_id'] );
                 }
                          if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                                 $q->andWhere('a.fecha >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                          }
                          if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                                $q->andWhere('a.fecha <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                          }
                          if($grup['edad_hasta'] >0 ){
                                $q->andWhere('p.edad_aproximada >= ? AND p.edad_aproximada <=?', array($grup['edad_desde'], $grup['edad_hasta']));
                          }else{
                                $q->andWhere('p.edad_aproximada >= ?', $grup['edad_desde']);
                               }                        
               
                      $result=  $q->fetchArray(); 

                           if($result[0]['cantidades']>0){
                           $row[]= $result[0]['cantidades'];
                           //total por fila
                           $total=$total + $result[0]['cantidades'];
                           //total por pie
                           $total_pie[$bd['file']]=$total_pie[$bd['file']]+ $result[0]['cantidades'];
                           }else{
                               $row[]=0;
                           }
                      }
                  $row[]=$total;
                  $total_final= $total_final+ $total;
                  $tabla[]=$row;
                  $row  = array();
               } 
               $total_pie[]=$total_final;
               $tabla[]=$total_pie;
               return $tabla;
        
  }
   public function getExportRecordRowMes($form)
   {
             $meses    = array( "1","2","3","4","5","6","7","8","9","10","11","12");
             $mesName  = array("MES", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" );
             $row      = array();
             $tabla    = array();
             $cabecera = array ('MES');
             $total_pie = array ('TOTAL');
             $total_final= 0;
             foreach($this->search->getBasesDatos() as $bd){
                 $cabecera[]=$bd['head'];
                 $total_pie[$bd['file']]=0;
             } 
             $cabecera[]='TOTAL';
             $tabla[]=$cabecera;
              
             $fechaD=$form['fecha']['from'];
             $fechaH=$form['fecha']['to'];
               
                  foreach($meses as $mes){   
                      $total=0;
                      $row[]= $mesName[$mes];
                      foreach($this->search->getBasesDatos() as $bd){
                          $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
                          
                          $q = Doctrine_Query::create($conn)
                                ->select('SUM(p.cantidad) as cantidades')   
                                ->from('Participantes p')
                                ->leftJoin('p.Actividad a' );
                                $q->andWhere('MONTH(a.fecha)=?', $mes);
                          if( ($form['tipo_actividad_id'] > 0)&& !empty($form['tipo_actividad_id']) ){
                              $q->andWhere('a.tipo_actividad_id = ?', $form['tipo_actividad_id'] );
                         }
                          if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                                 $q->andWhere('a.fecha >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                          }
                          if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                                $q->andWhere('a.fecha <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                          }
                              $result=  $q->fetchArray(); 

 
                           if($result[0]['cantidades']>0){
                           $row[]= $result[0]['cantidades'];
                           //total por fila
                           $total=$total + $result[0]['cantidades'];
                           //total por pie
                           $total_pie[$bd['file']]=$total_pie[$bd['file']]+ $result[0]['cantidades'];
                           }else{
                               $row[]=0;
                           }
                      }

                  $row[]=$total;
                  $total_final= $total_final+ $total;
                  $tabla[]=$row;
                  $row  = array();
               } 
                $total_pie[]=$total_final;
               $tabla[]=$total_pie;
               return $tabla;
       }   
   
}
