<?php

/**
 * grafic actions.
 *
 * @package    museo
 * @subpackage grafic
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class graficActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
                 $gruposs = Doctrine_Query::create()
                        ->from('GruposEtareos g')
                        ->orderBy('g.edad_desde ASC')
                        ->fetchArray();

                 $parts = Doctrine_Query::create()
                        ->select('SUM(p.cantidad)')
                        ->from('Actividad a')
                        ->innerJoin('a.Participantes p')
                        ->fetchArray();
                   $total=0;
                   foreach($parts as $part ){
						$total= $part['SUM'];
					  }
                     $total;

         $this->total= array();
         $totaSum=0;
         $band= false;
           foreach($gruposs as $grupos){
               $row= array();
		            if($grupos['edad_hasta'] >0 ){
		   $row[0]= $grupos['edad_desde'].' - '.$grupos['edad_hasta'].' años';

					//participantes
                      $actividades = Doctrine_Query::create()
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhere('p.edad_aproximada >= ? AND p.edad_aproximada < ?', array($grupos['edad_desde'], $grupos['edad_hasta']))
                        ->fetchArray();
                }else{
                    $row[0]= 'Mayor de '.$grupos['edad_desde'].' años';

					//participantes
                     $actividades = Doctrine_Query::create()
                        ->from('Participantes p')
                        ->leftJoin('p.Actividad a' )
                        ->andWhere('p.edad_aproximada >= ?', $grupos['edad_desde'])
                        ->fetchArray();
                     $band= true;
                }
                 $cant=0;
                     foreach($actividades as $act ){
						$cant+= $act['cantidad'];
					  }
                     $row[1]= $cant;
                     $porcentaje = ( $cant / $total ) * 100 ;
                     $porcentaje= round($porcentaje,1);

                     if($band){
                       $row[2]=  100 - $totaSum ;
                     }else{
                       $row[2]=  $porcentaje;
                       $totaSum = $totaSum + $porcentaje ;
                     }
             //  $row[2]=25.0;
               $this->total[]= $row;
				  }
  }
}
