<?php use_javascript('highcharts/js/highcharts.js') ?>
<?php use_javascript('highcharts/js/modules/exporting.js') ?>

            <div id="container" style="width: 820px; margin: 0 auto">

	<script type="text/javascript">

			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container1',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Browser market shares at a specific website, 2010'
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
								}
							}
						}
					},
				    series: [{
						type: 'pie',
						name: 'Browser share',

						data: [

						  <?php foreach($total as $t): ?>

							[  '<?php echo $t[0]?>' , <?php echo $t[2]?> ],

                                             <?php endforeach;?>

						]
					}]
				});
			});

		</script>




        <script type="text/javascript">


			Highcharts.setOptions({
				global: {
					useUTC: false
				}
			});

			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container2',
						defaultSeriesType: 'spline',
						marginRight: 10,
						events: {
							load: function() {

								// set up the updating of the chart each second
								var series = this.series[0];
								setInterval(function() {
									var x = (new Date()).getTime(), // current time
										y = Math.random();
									series.addPoint([x, y], true, true);
								}, 1000);
							}
						}
					},
					title: {
						text: 'Live random data'
					},
					xAxis: {
						type: 'datetime',
						tickPixelInterval: 150
					},
					yAxis: {
						title: {
							text: 'Value'
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}]
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) +'<br/>'+
								Highcharts.numberFormat(this.y, 2);
						}
					},
					legend: {
						enabled: false
					},
					exporting: {
						enabled: false
					},
					series: [{
						name: 'Random data',
						data: (function() {
							// generate an array of random data
							var data = [],
								time = (new Date()).getTime(),
								i;

							for (i = -19; i <= 0; i++) {
								data.push({
									x: time + i * 1000,
									y: Math.random()
								});
							}
							return data;
						})()
					}]
				});


			});

		</script>



                 <div id="container1" style="width: 800px; height: 400px; margin: 0 auto"></div>
		<!-- 3. Add the container -->
		<div id="container2" style="width: 800px; height: 400px; margin: 0 auto"></div>
           </div>

