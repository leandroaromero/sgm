<?php

require_once dirname(__FILE__).'/../lib/publicacionGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/publicacionGeneratorHelper.class.php';

/**
 * publicacion actions.
 *
 * @package    museo
 * @subpackage publicacion
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class publicacionActions extends autoPublicacionActions
{
}
