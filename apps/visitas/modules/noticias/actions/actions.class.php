<?php

require_once dirname(__FILE__).'/../lib/noticiasGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/noticiasGeneratorHelper.class.php';

/**
 * noticias actions.
 *
 * @package    museo
 * @subpackage noticias
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class noticiasActions extends autoNoticiasActions
{

public function executeEdit(sfWebRequest $request)
  {
    $this->news = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->news);
    $file_ins = $this->form->getObject()->getInstitution()->getFile();
    $file_user = $this->getUser()->getFile();
    if ($file_user != $file_ins){
    	$this->redirect('@noticias');
    }
  }

}
