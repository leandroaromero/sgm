<?php

/**
 * news form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsAdminForm extends BaseNewsForm
{
  public function configure()
  {
  	 unset($this['updated_at'],$this['created_at'],$this['updated_by'],$this['created_by'], $this['institucion_id'] );
    
    if($this->getObject()->isNew()){
  	  $qrs = Doctrine_Query::create()
        ->from('Institution i')
        ->where('i.file = ? ', sfContext::getInstance()->getUser()->getFile())
        ->fetchOne();

  	   $this->getObject()->setInstitucionId($qrs->getId());
    }

  	$parameter = sfConfig::get('app_noticias_tipo', array(''=>'') );
    sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));

    $this->widgetSchema['tipo'] = new sfWidgetFormChoice(
                                            array(
                                                  'choices'   => $parameter,
                                                  'multiple'  => false,
                                                  'expanded'  => false,
                                                ),
                                            array( 
                                                  
                                                  )
                                          );
  }
}
