<?php

/**
 * Actividad filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ActividadGFormFilter extends BaseActividadFormFilter
{
  public function configure()
  {
	  
          
        $web = sfConfig::get('app_url_web');   
        $this->widgetSchema['tipo_actividad_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Tipo de actividad',
              'model'   => 'TipoActividad',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));

        $format = '%day%/%month%/%year%';
        $web = sfConfig::get('app_url_web');
        $rango = range(2015, 2026);
        $arreglo_rango = array_combine($rango, $rango);
        
        $this->widgetSchema['fecha']= new sfWidgetFormFilterDate(array(
          'from_date' => new sfWidgetFormJQueryDate(array(
             'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))




            )), 'to_date' => new sfWidgetFormJQueryDate(array(
                 'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))

            )), 'with_empty' => false));

          unset ($this['origenes_list'] ,$this['nombre_actividad'], $this['usuarios_list'],
                 $this['created_by']    ,$this['updated_by'],       $this['created_at'], 
                 $this['updated_at']    ,$this['categorias_list'],  $this['tipo_objetos_list'], 
                 $this['colecciones_list'],$this['lugar_id'],       $this['atencion_actividad'], 
                 $this['hora_hasta']    , $this['hora_desde']  );
  }
}
