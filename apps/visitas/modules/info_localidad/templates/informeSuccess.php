<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div id="sf_admin_container" style="width: 100%" >
    
     <div style="display: inline; float: right; margin-right: 40px ">
             <a href="<?php echo url_for('gerencia/toExcel')?>" title="Generar informe"><?php echo image_tag('public/img/mono-icons/linedpaper32.png')?></a>
             <a href="<?php echo url_for('@uso')?>" title="Volver al filtro"><?php echo image_tag('public/img/mono-icons/search32.png')?></a>       
      </div>
    <h1> INFORME GENERAL  </h1>
    <h3><?php echo 'Desde: '.$Filtros[0].' Hasta: '.$Filtros[1]?></h3>
    <h3><?php echo 'Tipo de actividad: '.$Filtros[2]?></h3>
    <div id="sf_admin_content"  style="width: 100%">
        
       <?php foreach ($tablas as $key=>$tabla):?>
        <h2> <?php echo $titulos[$key]?>    </h2>
                <div class="sf_admin_list" style="padding-right: 25px;">

                    <table cellspacing="0" style="width: 100%"> 

                           <?php $firts=true;?>   
                          <?php  foreach ($tabla as $row):?>
                              <?php if($firts):?>
                                  <thead>
                              <?php endif?>  
                                      <tr>
                                      <?php foreach ($row as $r):?>

                                              <?php if($firts):?>
                                                  <th class="sf_admin_date sf_admin_list_th_fecha">  <?php echo $r?> </th>
                                              <?php else:?>  
                                               <td>  <?php echo $r?> </td>
                                              <?php endif ?> 

                                      <?php endforeach;?>
                                     </tr>              
                                       <?php if($firts):?>
                                          </thead>
                                          <tbody>
                                      <?php endif ?> 
                                    <?php $firts=false;?>

                          <?php  endforeach;?>
                          </tbody>
                      </table>
                    <br/><br/>
                  </div>
      <?php endforeach; ?>  
     <div style=" margin-right: 40px ">
             <a href="<?php echo url_for('gerencia/toExcel')?>" title="Generar informe"><?php echo image_tag('public/img/mono-icons/linedpaper32.png')?></a>
             <a href="<?php echo url_for('@uso')?>" title="Volver al filtro"><?php echo image_tag('public/img/mono-icons/search32.png')?></a>       
      </div>
    </div>
    
</div>
