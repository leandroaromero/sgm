<?php

/**
 * info_localidad actions.
 *
 * @package    museo
 * @subpackage info_localidad
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class info_localidadActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
   $this->form= new ActividadGFormFilter(); 
  }
  
   public function executeInforme(sfWebRequest $request)
  {
      $form= $request->getParameter('actividad_filters');
 
      $this->titulos=array();
      $cambio=array('museo'=>'todo');
      $this->search = new SearchBuilder($cambio);
      $this->tablas= array();
      // visitas mensuales
      $this->titulos[]= "Informe por localidad";
      $this->tablas[]= $this->getExportRecordRowMes( $form ) ;
      
////      //visitas grupos
//      $this->titulos[]= "Informe por grupo etáreo";
//      $this->tablas[]= $this->getExportRecordRowGrupos($form );
////      //visitas servicios
//      $this->titulos[]= "Informe por servicios";
//      $this->tablas[]= $this->getExportRecordRowServicios($form );
//      //actividades
//      $this->titulos[]= "Informe por tipo de actividad";
//      $this->tablas[]=$this->getExportRecordRowActividades($form );
////      //visitas origen
//      $this->titulos[]= "Informe por origen";
//      $this->tablas[]=$this->getExportRecordRowOrigen($form);
      
                  
      $fechaD=$form['fecha']['from'];
      $fechaH=$form['fecha']['to'];
      $this->fechaD= $fechaD['day'].'/'.$fechaD['month'].'/'.$fechaD['year'];
      $this->fechaH= $fechaH['day'].'/'.$fechaH['month'].'/'.$fechaH['year'];
      $tipo_actividad= "";
      if($form['tipo_actividad_id'] > 0){
      $tipo_actividad= Doctrine_Core::getTable('TipoActividad')->find( $form['tipo_actividad_id']);
      $tipo_actividad=$tipo_actividad->getTipoActividad();
      }
      $this->Filtros=array($this->fechaD, $this->fechaH, $tipo_actividad); 
      
      $this->getUser()->setAttribute('tabla_visita',   $this->tablas);
      $this->getUser()->setAttribute('titulos_visita', $this->titulos);
      $this->getUser()->setAttribute('filtros_visita', $this->Filtros);
      
    }

   public function getExportRecordRowMes($form)
   {
          
             $row      = array();
             $tabla    = array();
             $cabecera = array ('LOCALIDAD');
             $total_pie = array ('TOTAL');
             $total_final= 0;
             foreach($this->search->getBasesDatos() as $bd){
                 $cabecera[]=$bd['head'];
                 $total_pie[$bd['file']]=0;
             } 
             $cabecera[]='TOTAL';
             $tabla[]=$cabecera;
              
             $fechaD=$form['fecha']['from'];
             $fechaH=$form['fecha']['to'];
               
               $q = Doctrine_Query::create()
                                ->select('l.nombre_localidad as nombre')   
                                ->from('Localidad l')
                                ->leftJoin('l.Provincia p' )
                                ->where('p.nombre_provincia= ?', 'CHACO'); 
             
             
                  foreach($q->execute() as $localidad){   
                      $total=0;
                      $row[]= $localidad['nombre'];
                      foreach($this->search->getBasesDatos() as $bd){
                          $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
                          
                          $q = Doctrine_Query::create($conn)
                               // ->select('SUM(p.cantidad) as cantidades')   
                                ->from('Participantes p')
                                ->leftJoin('p.Actividad a' )
                                ->leftJoin('a.ActividadLocalidad o' )
                                ->leftJoin('o.Localidad l' )  
                                ->andWhere('l.nombre_localidad=?', $localidad['nombre']);
                          if( ($form['tipo_actividad_id'] > 0)&& !empty($form['tipo_actividad_id']) ){
                              $q->andWhere('a.tipo_actividad_id = ?', $form['tipo_actividad_id'] );
                         }
                          if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                                 $q->andWhere('a.fecha >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                          }
                          if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                                $q->andWhere('a.fecha <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                          }
                              $result=  $q->count(); //fetchArray(); 

 
                         //  if($result[0]['cantidades']>0){
                           $row[]= $result;//[0]['cantidades'];
                           //total por fila
                           $total=$total + $result;//[0]['cantidades'];
                           //total por pie
                           $total_pie[$bd['file']]=$total_pie[$bd['file']]+ $result;//[0]['cantidades'];
//                           }else{
//                               $row[]=0;
//                           }
                      }
                  if($total > 0){     
                    $row[]=$total;
                    $total_final= $total_final+ $total;
                    $tabla[]=$row;
                  }
                  $row  = array();
               } 
                $total_pie[]=$total_final;
               $tabla[]=$total_pie;
               return $tabla;
       }   

  }