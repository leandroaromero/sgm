<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<center>
<h1>Hola <?php echo $sf_user->getUsername()?></h1>

<form class="f-wrap-1" action="<?php echo url_for('profile/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<table class="table">
    <tfoot>
      <tr>
        <td colspan="2">
            <?php echo $form->renderHiddenFields(false) ?>
            <input class="f-submit" type="submit" value="Grabar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
          <?php echo $form->renderGlobalErrors() ?>
        <tr>
            <td width="150px"><?php echo $form['first_name']->renderLabel('Nombre') ?></td>
          <td>
              <?php echo $form['first_name']->render() ?>
              <?php echo $form['first_name']->renderError() ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $form['last_name']->renderLabel('Apellido') ?></td>
          <td>
              <?php echo $form['last_name']->render() ?>
              <?php echo $form['last_name']->renderError() ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $form['email_address']->renderLabel('Correo') ?></td>
          <td>
              <?php echo $form['email_address']->render(array('class' => 'text')) ?>
              <?php echo $form['email_address']->renderError() ?>
          </td>
        </tr>
    </tbody>
</table>
</form>

</center>
