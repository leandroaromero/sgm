<?php

require_once dirname(__FILE__).'/../lib/cs_settingGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/cs_settingGeneratorHelper.class.php';

/**
 * cs_setting actions.
 *
 * @package    museo
 * @subpackage cs_setting
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class cs_settingActions extends autoCs_settingActions
{
}
