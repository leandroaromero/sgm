<?php

/**
 * Participantes form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ParticipantesActForm extends BaseParticipantesForm
{

 public function configure()
  {

    $this->useFields(array('tipo_participantes',
                            'cantidad',
                            'edad_aproximada',

                        ));
   $this->widgetSchema['tipo_participantes']->setOption('label', csSettings::get('actividad_lista_participantes_tipo_participantes', 'Tipo de participante'));
   $this->widgetSchema['cantidad']->setOption('label', csSettings::get('actividad_lista_participantes_cantidad', 'Cantidad'));
   $this->widgetSchema['edad_aproximada']->setOption('label', csSettings::get('actividad_lista_participantes_edad_aproximada', 'Edad aproximada'));
   
        $this->widgetSchema['actividad_id']= new sfWidgetFormInputHidden();
        unset ($this['actividad_id']);
   if ($this->object->exists())
    {
      $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
      $this->validatorSchema['delete'] = new sfValidatorPass();
    }
  }
  public function  save($con = null) {

      //parent::doSave($con);
    }
}
