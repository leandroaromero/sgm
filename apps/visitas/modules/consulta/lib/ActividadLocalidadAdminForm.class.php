<?php

/**
 * ActividadLocalidad form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ActividadLocalidadAdminForm extends BaseActividadLocalidadForm
{
  public function configure()
  {
    unset ($this['actividad_id']);
   sfProjectConfiguration::getActive()->loadHelpers('Url');
      $this->widgetSchema['localidad_id'] =  new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true));
            ////new sfValidatorChoice(array('choices' => array($this->getObject()->get('localidad_id')), 'empty_value' => $this->getObject()->get('localidad_id'), 'required' => false));
      $this->widgetSchema['localidad_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
      $this->widgetSchema['localidad_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
      array(
        'model' => "Localidad",
        'url'   => url_for("@ajax_origen"),
        'config' => '{ max: 30}',
        'method' => 'getProcedencia'
        ));
     
    $this->validatorSchema['localidad_id']=  new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'required' => false));
   if ($this->object->exists())
    {
      $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
      $this->validatorSchema['delete'] = new sfValidatorPass();
    }
  
  }
}
