<?php

/**
 * ActividadObjeto form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ActividadObjetoAdminForm extends BaseActividadObjetoForm
{
  public function configure()
  {
      unset($this['actividad_id']);      
      sfProjectConfiguration::getActive()->loadHelpers('Url');
      
//      $this->widgetSchema['objeto_id'] =  new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Objeto'), 'add_empty' => true));
//      $this->widgetSchema['objeto_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
//      $this->widgetSchema['objeto_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
//      array(
//        'model' => "Objeto",
//        'url'   => url_for("@ajax_objetos"),
//        'config' => '{ max: 30}',
//        'method' => 'getTrabajoVisita'
//        ));
     
   $this->validatorSchema['objeto_id'] = new sfValidatorPass();
   if ($this->object->exists())
    {
      $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
      $this->validatorSchema['delete'] = new sfValidatorPass();
    }
  }
}
