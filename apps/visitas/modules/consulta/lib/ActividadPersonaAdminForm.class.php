<?php

/**
 * ActividadPersona form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ActividadPersonaAdminForm extends BaseActividadPersonaForm
{
  public function configure()
  {
       unset ($this['actividad_id']);
       sfProjectConfiguration::getActive()->loadHelpers('Url');
       $this->widgetSchema['persona_id'] =  new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Persona'), 'add_empty' => true));
       $this->widgetSchema['persona_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
       $this->widgetSchema['persona_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
       array(
            'model' => "Persona",
            'label'=> csSettings::get('actividad_lista_de_usuarios_persona_id', 'Usuarios'),
            'url'   => url_for("@ajax_personas"),
            'config' => '{ max: 30}',
            'method' => 'getUsuario'
        ));
     
       $this->validatorSchema['persona_id']=  new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Persona'), 'required' => false));
       if ($this->object->exists())
        {
          $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
          $this->validatorSchema['delete'] = new sfValidatorPass();
        }
  
  }
}