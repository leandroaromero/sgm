
<div id="lista_de_objeto_o">
<?php use_helper('jQuery');?>

<table width="100%" class="sf_admin_list">
    <thead>
        <tr class="sf_admin_row_1">
            <td width="5%"> <b>N° in</b></td>    
            <td width="10%"><b>Nombre</b></td>
            <td width="79%"><b>Descripcion</b></td>
            <td width="6%"><b>Acción</b></td>
        </tr>
    </thead>
  <tbody>
    
    <?php foreach ($form['lista_objetos'] as $index=> $obj):?> 
    <?php echo $obj->renderHiddenFields(false) ?>
    
        <tr>
            <?php  $id =  $obj['objeto_id']->getValue();
                   $oj =Doctrine::getTable('Objeto')->find($id);
            ?>
                <td>
                    <?php 
                          echo $obj['objeto_id']->renderError();
                          $obj['objeto_id']->render();
                          echo $oj->getId()
                        ?></td>
                <td><?php echo $oj->getNombreObjeto()?></td>
                <td><?php echo $oj->getDescripcionGenerica()?></td>
                <td>
                    <?php echo jq_link_to_remote(image_tag('delete.png').'Borrar', array(
                      'update'    => 'lista_de_objeto_o',
                      'url'       => 'consulta/procesarBorrado?actividad_id='.$form->getObject()->getId().'&objeto_id='.$id,
                      'script' => true,
                          ));?>
                </td>
        </tr>        
    <?php endforeach;?>
            
  </tbody>

</table>

<div class="content" style="border-bottom: 1px solid #DDDDDD;border-color: #DDDDDD" >

	   <center>
                   <?php echo link_to( image_tag('open.png').' Lista de documentos',
                              'list_object/index?actividad_id='.$form->getObject()->getId(),
                               array('popup'=>array('popupWindow','status=no,scrollbars=yes,location=yes,resizable=yes,width=960,height=550,left=320,top=0')))
                     ?>
                  <?php echo jq_link_to_remote(image_tag('tick.png').'procesar', array(
                      'update'    => 'lista_de_objeto_o',
                      'url'       => 'consulta/procesar?actividad_id='.$form->getObject()->getId(),
                      'script' => true,
                          ));?>
	   </center> 	

<br>
</div>
</div> 
 
