<?php

require_once dirname(__FILE__).'/../lib/consultaGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/consultaGeneratorHelper.class.php';

/**
 * consulta actions.
 *
 * @package    museo
 * @subpackage consulta
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class consultaActions extends autoConsultaActions
{
    
     public function executeAddParForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num"));

         if($card =    Doctrine::getTable('Actividad')->find($request->getParameter('id'))){  //CardPeer::retrieveByPk($request->getParameter('id'))){
             $form = new ActividadAdminForm($card);
             }else{
             $form = new ActividadAdminForm(null);
         }

         $form->addParticipantes($number);
         return $this->renderPartial('addPar',array('form' => $form, 'num' => $number));
      }
        public function executeAddOrigForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num1"));

         if($orig =    Doctrine::getTable('Actividad')->find($request->getParameter('id'))){  //CardPeer::retrieveByPk($request->getParameter('id'))){
             $form = new ActividadAdminForm($orig);
             }else{
             $form = new ActividadAdminForm(null);
         }

         $form->addOrigen($number);
         return $this->renderPartial('addOrig',array('form' => $form, 'num' => $number));
      }
      
        public function executeAddPerForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num1"));

         if($orig =    Doctrine::getTable('Actividad')->find($request->getParameter('id'))){  //CardPeer::retrieveByPk($request->getParameter('id'))){
             $form = new ActividadAdminForm($orig);
             }else{
             $form = new ActividadAdminForm(null);
         }

         $form->addPersona($number);
         return $this->renderPartial('addPer',array('form' => $form, 'num' => $number));
      }
      
 public function executeProcesar($request){
         
         $listados = $this->getUser()->getAttribute('lista_de_objetos');
         
         if($orig =    Doctrine::getTable('Actividad')->find($request->getParameter('actividad_id'))){  //CardPeer::retrieveByPk($request->getParameter('id'))){
             $this->form = new ActividadAdminForm($orig);
             }else{
             $this->form = new ActividadAdminForm(null);
         }
         $this->getUser()->setAttribute('lista_de_objetos', $listados);
         $number=0;
         foreach ($listados as $l){
            $this->form->addObjeto($number, $l);
            $number++;
           }
        
     }
 
     public function executeProcesarBorrado($request){
         
         $listados = $this->getUser()->getAttribute('lista_de_objetos');
         
         $obj_id= $request->getParameter('objeto_id');
         if($request->getParameter('actividad_id') > 0){
             $act_obj = Doctrine_Query::create()
                      ->from('ActividadObjeto')
                      ->where('objeto_id = ?', $obj_id)
                      ->andWhere('actividad_id =?', $request->getParameter('actividad_id')) 
                      ->fetchOne();

              if($act_obj){
                  $act_obj->delete();
              }
         }
         unset($listados[$obj_id]);
      
        if($orig =    Doctrine::getTable('Actividad')->find($request->getParameter('actividad_id'))){          
             $this->form = new ActividadAdminForm($orig);
             }else{
             $this->form = new ActividadAdminForm(null);
         }
         $number=0;
         $list = array();
         foreach ($listados as $l){
             if($obj_id != $l){
                    $this->form->addObjeto($number, $l);
                    $number++;
                    $list[$l]=$l;
             }
           }
        $this->getUser()->setAttribute('lista_de_objetos', $list);   
        $this->setTemplate('procesar');
     }     
}
