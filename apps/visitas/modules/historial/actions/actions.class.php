<?php

require_once dirname(__FILE__).'/../lib/historialGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/historialGeneratorHelper.class.php';

/**
 * historial actions.
 *
 * @package    museo
 * @subpackage historial
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class historialActions extends autoHistorialActions
{
    public function executeUso(){
        
        $this->form= new ObjetoAdminFormFilter();
    }
    public function executeResult(sfWebRequest $request){
        
        $form= $request->getParameter('result');
        $q = Doctrine_Query::create()
                   ->from('sfGuardUser s')
                   ->orderBy('s.last_name');
        
        
        $fechaD=$form['fecha']['from'];
        $fechaH=$form['fecha']['to'];
        
        
        
        $this->result_t= array();
        $this->result_t[]= array('USUARIOS', 'Acervos Creados', 'Acervos Modificados', 'Imagenes Creadas', 'Imagenes Modificadas', 'Actividades Creadas', 'Actividades Modificadas');
           foreach ($q->execute() as $user){
               $result= array();
               $result[]=$user->__toString();
               
               //Acervos
        $q = Doctrine_Query::create()
                   ->from('Objeto o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q->andWhere('o.created_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q->andWhere('o.created_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                 $q->andWhere('o.created_by =?', $user->getId())
                   ->fetchArray();
                 
        $result[]= $q->count();
                
        $q_u = Doctrine_Query::create()
                   ->from('Objeto o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q_u->andWhere('o.updated_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q_u->andWhere('o.updated_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                  $q_u->andWhere('o.updated_by =?', $user->getId())
                    ->fetchArray();
                 
        $result[]= $q_u->count();
        
                //Imagenes
        $q = Doctrine_Query::create()
                   ->from('Imagen o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q->andWhere('o.created_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q->andWhere('o.created_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                 $q->andWhere('o.created_by =?', $user->getId())
                   ->fetchArray();
                 
        $result[]= $q->count();
                
        $q_u = Doctrine_Query::create()
                   ->from('Imagen o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q_u->andWhere('o.updated_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q_u->andWhere('o.updated_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                  $q_u->andWhere('o.updated_by =?', $user->getId())
                    ->fetchArray();
                 
        $result[]= $q_u->count();
        
                
        $q = Doctrine_Query::create()
                   ->from('Actividad o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q->andWhere('o.created_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q->andWhere('o.created_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                 $q->andWhere('o.created_by =?', $user->getId())
                   ->fetchArray();
                 
        $result[]= $q->count();
                
        $q_u = Doctrine_Query::create()
                   ->from('Actividad o');
                    if( !empty($fechaD['year']) && !empty($fechaD['month']) && !empty($fechaD['day'])){
                             $q_u->andWhere('o.updated_at >= ?',  $fechaD['year'].'-'.$fechaD['month'].'-'.$fechaD['day']);
                      }
                      if( !empty($fechaH['year']) && !empty($fechaH['month']) && !empty($fechaH['day'])){
                            $q_u->andWhere('o.updated_at <= ?',  $fechaH['year'].'-'.$fechaH['month'].'-'.$fechaH['day']); 
                      }      
                  $q_u->andWhere('o.updated_by =?', $user->getId())
                    ->fetchArray();
                 
        $result[]= $q_u->count();
        
        $this->result_t[]= $result;
       }
       $filtro= array();
       $filtro[]=$this->fechaD= $fechaD['day'].'/'.$fechaD['month'].'/'.$fechaD['year'];
       $filtro[]=$this->fechaH= $fechaH['day'].'/'.$fechaH['month'].'/'.$fechaH['year'];
      
                   $this->getUser()->setAttribute('tabla_visita', array($this->result_t));
                   $this->getUser()->setAttribute('titulos_visita', array('Informe de Uso') );
                   $this->getUser()->setAttribute('filtros_visita', $filtro);
    }
}


