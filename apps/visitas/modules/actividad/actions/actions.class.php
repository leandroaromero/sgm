<?php

require_once dirname(__FILE__).'/../lib/actividadGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/actividadGeneratorHelper.class.php';

/**
 * actividad actions.
 *
 * @package    museo
 * @subpackage actividad
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class actividadActions extends autoActividadActions
{
    public function executeAddParForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num"));

         if($card =    Doctrine::getTable('Actividad')->find($request->getParameter('id'))){  //CardPeer::retrieveByPk($request->getParameter('id'))){
             $form = new ActividadAdminForm($card);
             }else{
             $form = new ActividadAdminForm(null);
         }

         $form->addParticipantes($number);
         return $this->renderPartial('addPar',array('form' => $form, 'num' => $number));
      }
        public function executeAddOrigForm($request)
        {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num1"));

         if($orig =    Doctrine::getTable('Actividad')->find($request->getParameter('id'))){  //CardPeer::retrieveByPk($request->getParameter('id'))){
             $form = new ActividadAdminForm($orig);
             }else{
             $form = new ActividadAdminForm(null);
         }

         $form->addOrigen($number);
         return $this->renderPartial('addOrig',array('form' => $form, 'num' => $number));
      }
}
