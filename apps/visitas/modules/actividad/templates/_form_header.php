<script type="text/javascript">
var pars = <?php print_r($form['lista_participantes']->count())?>;

function addPar(num) {
  var r = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('actividad/addParForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num=':'&num=')?>'+num,
    async: false
  }).responseText;
  return r;
}
$().ready(function() {
  $('button#add_participante').click(function() {
    $("#extra_participante").append(addPar(pars));
    pars = pars + 1;
  });
});
</script>


<script type="text/javascript">
var oris = <?php print_r($form['lista_origenes']->count())?>;

function addOrig(num1) {
  var r2 = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('actividad/addOrigForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num1=':'&num1=')?>'+num1,
    async: false
  }).responseText;
  return r2;
}
$().ready(function() {
  $('button#add_origen').click(function() {
    $("#extra_origen").append(addOrig(oris));
    oris = oris + 1;
  });
});
</script>