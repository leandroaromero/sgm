<?php use_helper('I18N', 'Date') ?>
<?php include_partial('actividad/assets') ?>

<script type="text/javascript">
			$(function(){

				// Dialog			
				$('#dialog').dialog({
					autoOpen: false,
					width: 800,
                    backgroundColor: '#000',
                    color: '#000',
                    
					buttons: {
						"Cancel": function() { 
							$(this).dialog("close"); 
						} 
					}
				});
				
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});

				//hover states on the static widgets
				$('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); }, 
					function() { $(this).removeClass('ui-state-hover'); }
				);
				
			});
		</script>
		<style type="text/css">
			/*demo page css*/
			#dialog_link { font: 80.5% "Trebuchet MS", sans-serif; margin: 50px;}
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
		</style>	

<div id="sf_admin_container">
  <h1><?php echo __('Lista de actividades', array(), 'messages') ?></h1>

  <?php include_partial('actividad/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('actividad/list_header', array('pager' => $pager)) ?>
  </div>


  <div id="sf_admin_bar">
  </div>
  <div style="margin-right: 40px; float: right"  >
        <p ><a href="#" id="dialog_link" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-newwin"></span>Busqueda avanzada</a></p>
 </div>       
  <div id="dialog" title="Busqueda avanzada" style="background: #444"> 
          <div id="serach_bui" style="border:silver; background:#FFF" >
    <?php include_partial('actividad/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
          </div>  
       </div>
  <br/><br/>

  <div id="sf_admin_content">
    <form action="<?php echo url_for('actividad_collection', array('action' => 'batch')) ?>" method="post">
    <?php include_partial('actividad/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('actividad/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('actividad/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('actividad/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
