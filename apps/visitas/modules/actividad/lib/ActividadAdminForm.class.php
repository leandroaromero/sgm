<?php

/**
 * Actividad form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ActividadAdminForm extends BaseActividadForm
{
 protected $scheduledForDeletion = array();
 protected $scheduledForDeletionO = array();


 public function configure()
  {
            
          unset($this['updated_at'],$this['created_at'],$this['updated_by'],$this['created_by']);
          $this->widgetSchema['tipo_actividad_id'] = new sfWidgetFormDoctrineChoice(array(
                  'label' => 'Tipo de actividad *',
                  'model'   => 'TipoActividad',
                  'table_method' => 'getOrderNombre',
                  'add_empty' => true,
                ));
          $web = sfConfig::get('app_url_web');     
          
          $this->widgetSchema['hora_desde']->setOption('label', 'Hora desde *');
          $this->widgetSchema['nombre_actividad']->setOption('label', 'Nombre de la actividad *');
          $rango = range(2012, 2025);
          $arreglo_rango = array_combine($rango, $rango);
          $this->widgetSchema['fecha'] = new sfWidgetFormJQueryDate(array(
           'label' => 'Fecha *',
           'image'  => '/museo/'.$web.'images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))
            ));
          
          $minute= array(0, 15, 30, 45);
          $this->widgetSchema['hora_hasta']= new sfWidgetFormTime(array(
                                              'minutes'      => array_combine($minute, $minute),
                                            ));
         $date= date('d-m-Y');
         $this->widgetSchema['fecha']->setAttribute('value' ,$date);
         $this->widgetSchema['fecha']->setDefault($date);
         $hora= date('H:i');
         $this->widgetSchema['hora_desde']->setDefault($hora);
         $this->widgetSchema['hora_hasta']->setDefault($hora);
         
         $this->widgetSchema['usuarios_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
         $this->widgetSchema['usuarios_list']->setOption('label', 'Responsables');
         $this->widgetSchema['usuarios_list']->setOption('table_method', 'getOrderNombre');
        

          
            //Empotramos al menos un formulario de participantes
            $participantes = $this->getObject()->getParticipantes();
            if (!$this->getObject()->getParticipantes()->count()){
                  $participante = new Participantes();
                  $participante->setActividad($this->getObject());
                  $participantes = array($participante);
            }

            //Un formulario vacío hará de contenedor para todas los participantes
            $participantes_forms = new SfForm();
            $count = 0;
            foreach ($participantes as $participante) {
                    $par_form = new ParticipantesActForm($participante);
                /*    $par_form->useFields(array(
                            'cantidad',
                            'edad_aproximada',
                  //          'delete'
                        ));*/
                //    unset($par_form['actividad_id']);
                    //Empotramos cada formulario en el contenedor
                    $participantes_forms->embedForm($count, $par_form);
                    $count ++;
            }
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('lista_participantes', $participantes_forms);
    //-----------------------------------------------------------------------------


            //Empotramos al menos un formulario de participantes
            $origenes = $this->getObject()->getActividadLocalidad();
            if (!$this->getObject()->getActividadLocalidad()->count()){
                  $origen = new ActividadLocalidad();
                  $origen->setActividad($this->getObject());
                  $origenes = array($origen);
            }

            //Un formulario vacío hará de contenedor para todas los participantes
            $origenes_forms = new SfForm();
            $count = 0;
            foreach ($origenes as $origen) {
                    $or_form = new ActividadLocalidadAdminForm($origen);
                
                    $origenes_forms->embedForm($count, $or_form);
                    $count ++;
            }
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('lista_origenes', $origenes_forms);
     
            
            $this->validatorSchema['lista_origenes']->setMessage('required', 'Debe cargar al menos una procedencia');

            $this->mergePostValidator(new sfValidatorCallback(array(
              'callback' => array($this, 'checkLocalidad'),
            ), array(
              'required'=> 'Debe cargar al menos una procedencia' 
            )));

     }


     // add this method to the same form class
     public function checkLocalidad(sfValidatorBase $validator, array $values, array $arguments)
     {
            foreach ($values['lista_origenes'] as $valu){
                if($valu['localidad_id']> 0){
                      return $values;            
                    }
             }

            throw new sfValidatorErrorSchema($validator, array(
                  'lista_origenes' => new sfValidatorError($validator, 'required')
                ));
            return $values;
       }

  
     public function addParticipantes($num) 
     {

          $par = new Participantes();
          $par->setActividad($this->getObject());
          $par_form = new ParticipantesActForm($par);

          unset($par_form['actividad_id']);
          //Empotramos la nueva pícture en el contenedor
          $this->embeddedForms['lista_participantes']->embedForm($num, $par_form);
          //Volvemos a empotrar el contenedor
          $this->embedForm('lista_participantes', $this->embeddedForms['lista_participantes']);

         //---------------------------------------------------------------------
      }

      public function bind(array $taintedValues = null, array $taintedFiles = null)
        {
          
          foreach($taintedValues['lista_participantes'] as $key=>$emailValues)
          {
            if (!isset($this['lista_participantes'][$key]) )
            {
             if ('' === trim($emailValues['edad_aproximada']) && '' === trim($emailValues['cantidad']))
            {
              unset($values['lista_participantes'][$i]);
            } else{
              $this->addParticipantes($key);
            }
            }
          }
          foreach($taintedValues['lista_origenes'] as $key=>$origenlValues)
          {
            if (!isset($this['lista_origenes'][$key]) )
            {
             if ('' === trim($origenlValues['localidad_id']))
            {
              unset($values['lista_participantes'][$i]);
            } else{
              $this->addOrigen($key);
            }
            }
          }
          parent::bind($taintedValues, $taintedFiles);
        // die('malo 3');
        }


     protected function doBind(array $values)
      {

          foreach ($values['lista_participantes'] as $i => $emailValues)
          {
             if ('' === trim($emailValues['edad_aproximada']) && '' === trim($emailValues['cantidad']))
                {
                  unset($values['lista_participantes'][$i]);
                }
            if (isset($emailValues['delete']) && $emailValues['id'])
            {
              $this->scheduledForDeletion[$i] = $emailValues['id'];
            }
          }
          foreach ($values['lista_origenes'] as $i => $oriValues)
          {
             if ('' === trim($oriValues['localidad_id']))
                {
                  unset($values['lista_origenes'][$i]);
                }
            if (isset($oriValues['delete']))
            {
              $this->scheduledForDeletionO[$i] = $oriValues['localidad_id'];
                unset($values['lista_origenes'][$i]);
            }
          }

        parent::doBind($values);
    //die('malo 2');
      }



      protected function doUpdateObject($values)
      {

        if (count($this->scheduledForDeletion))
        {
          foreach ($this->scheduledForDeletion as $index => $id)
          {

            unset($values['lista_participantes'][$index]);
            Doctrine::getTable('Participantes')->findOneById($id)->delete();

          }

        }
         foreach ($this->scheduledForDeletionO as $index => $id)
          {
            //unset($values['lista_origenes'][$index]);
            $q = Doctrine_Query::create()
                ->from('ActividadLocalidad a')
                ->where('a.localidad_id = ?', $id)
                ->andWhere('a.actividad_id =?', $this->getObject()->getId() )
                ->fetchOne();
                 $q->delete();

          }

        $this->getObject()->fromArray($values);

      }
/*creo que no funciona esta parte
  26/10/21
     public function saveEmbeddedForms($con = null, $forms = null)
      {

        if (null === $con)
        {
          $con = $this->getConnection();
        }

        if (null === $forms)
        {
          $forms = $this->embeddedForms;
        }

        foreach ($forms as $form)
        {
          if ($form instanceof sfFormObject)
          {
           if($form->getModelName() == 'Participantes'){
                if (!in_array($form->getObject()->getId(), $this->scheduledForDeletion))
                {
                  $form->saveEmbeddedForms($con);
                  $form->getObject()->save($con);

                }
          }else{
              try {
                      $form->saveEmbeddedForms($con);
                      $form->getObject()->save($con);
                  } catch (Exception $exc) {

              }
               }
          }
          else
          {
            $this->saveEmbeddedForms($con, $form->getEmbeddedForms());
          }
        }
      }*/



//-----------------------------lo de origenes--------------------------------------------------

      public function addOrigen($num){

          $or = new ActividadLocalidad();
          $or->setActividad($this->getObject());
          $or_form = new ActividadLocalidadAdminForm($or);

          unset($or_form['actividad_id']);
          //Empotramos la nueva pícture en el contenedor
          $this->embeddedForms['lista_origenes']->embedForm($num, $or_form);
          //Volvemos a empotrar el contenedor
          $this->embedForm('lista_origenes', $this->embeddedForms['lista_origenes']);

         //---------------------------------------------------------------------
      }
}

//basura
         /*$par_form->useFields(array(
                                'cantidad',
                                'edad_aproximada',

                            ));*/