<?php

/**
 * Participantes form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ParticipantesActForm extends BaseParticipantesForm
{

 public function configure()
  {

    unset($this['created_by'],$this['updated_by'], $this['updated_at'],$this['created_at']);

    $this->useFields(array('tipo_participantes',
                            'cantidad',
                            'edad_aproximada',

                        ));
   $this->widgetSchema['tipo_participantes']->setOption('label', 'Tipo de participante');
        $this->widgetSchema['actividad_id']= new sfWidgetFormInputHidden();
        unset ($this['actividad_id']);
   if ($this->object->exists())
    {
      $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
      $this->validatorSchema['delete'] = new sfValidatorPass();
    }
  }

}
