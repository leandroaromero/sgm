<?php

/**
 * list_object module configuration.
 *
 * @package    museo
 * @subpackage list_object
 * @author     LeandroARomero
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class list_objectGeneratorConfiguration extends BaseList_objectGeneratorConfiguration
{
}
