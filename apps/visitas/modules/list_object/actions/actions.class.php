<?php

require_once dirname(__FILE__).'/../lib/list_objectGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/list_objectGeneratorHelper.class.php';

/**
 * list_object actions.
 *
 * @package    museo
 * @subpackage list_object
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class list_objectActions extends autoList_objectActions
{
        public function executeBatchSeleccionar(sfWebRequest $request)
  {
//        $ids = $request->getParameter('ids');
//
//        $records = Doctrine_Query::create()
//          ->from('Objeto')
//          ->whereIn('id', $ids)
//          ->execute();
//        
        $ids = $request->getParameter('ids');    
        $cont=0;
        $array= array();
        $array = $this->getUser()->getAttribute('lista_de_objetos');
        
        foreach ($ids as $record)
        {
          $cont++;
          $array[$record]=$record;
        }
        $this->getUser()->setAttribute('lista_de_objetos', $array);
        
        $this->getUser()->setFlash('notice', 'Se adjuntaron los '.$cont. 'elementos seleccionados');
        $this->redirect('@list_object');

    }
     public function executeList_Seleccionar(sfWebRequest $request)
      {
        $id = $request->getParameter('id');
         
        $array= array();
        $array = $this->getUser()->getAttribute('lista_de_objetos');
        
          $array[$id]=$id;
        
        $this->getUser()->setAttribute('lista_de_objetos', $array);
        
        $this->getUser()->setFlash('notice', 'Se adjunto el elementos seleccionados');
        $this->redirect('@list_object');

    }
      public function executeIndex(sfWebRequest $request)
      {
        // sorting
        if ($request->getParameter('sort') && $this->isValidSortColumn($request->getParameter('sort')))
        {
          $this->setSort(array($request->getParameter('sort'), $request->getParameter('sort_type')));
        }

        // pager
        if ($request->getParameter('page'))
        {
          $this->setPage($request->getParameter('page'));
        }

        $this->pager = $this->getPager();
        $this->sort = $this->getSort();
        $this->setLayout('layout_printer');
      }

}
