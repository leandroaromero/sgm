<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php $ico= sfConfig::get('app_url_ico');  ?>
    <link rel="shortcut icon" href="<?php echo $ico ?>" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>

          <!-- CSS -->

		<!--[if IE 8]>
			<link rel="stylesheet" type="text/css" media="screen" href="/museo/css/css/css/ie8-hacks.css" />
		<![endif]-->
		<!-- ENDS CSS -->	
		
		<!-- GOOGLE FONTS 
		<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>-->
		
		<!-- JS -->

               <?php $web= sfConfig::get('app_url_web');  ?>
                <script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/easing.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/custom.js"></script>
		
		<!-- Isotope -->
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery.isotope.min.js"></script>
		
		<!--[if IE]>
			<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<!--[if IE 6]>
			<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/DD_belatedPNG.js"></script>
			<script type="text/javascript"  >
	      		/* EXAMPLE */
	      		//DD_belatedPNG.fix('*');
	    	</script>
		<![endif]-->
		<!-- ENDS JS -->
		
		
		<!-- Nivo slider -->
		
		<script src="/museo/<?php echo $web?>js/js/js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		
		<script src="/museo/<?php echo $web?>js/js/js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
               
		
</head>
     


    <body>                             
	                      
<script type="text/javascript" >
function setMenu(valor){

     var   valores = $.ajax({
                   type: 'GET',
                   url: '<?php echo url_for('componentes/setMenu')?>'+'?menu='+valor,
                   async: false
                }).responseText;
     
}

</script>                                       
<script language="javascript" type="text/javascript" >
window.onload = function(){
     document.getElementById('li_museum').className='';
     document.getElementById('li_object').className='';
     document.getElementById('li_contact').className='';
     
     <?php if ( $sf_user->getAttribute('menu') ) :?>
            document.getElementById('li_home').className='';
            document.getElementById('<?php echo $sf_user->getAttribute('menu')?>').className= 'current-menu-item';
     <?php endif;?>       
            
};
</script>      	
        
   
        
        <!-- HEADER -->
			<div id="header">
                            
                             
                            
				<!-- wrapper-header -->
				<div class="wrapper">
					<a href="<?php echo url_for('@homepage')?>"><img id="logo" src="/museo/<?php echo $web?>images/public/logo.png" alt="Museos De La Provincia del Chaco" /></a>
					<!-- search -->
					<div class="top-search">
						<form  method="get" id="searchform" action="#">
							<div>
								
							</div>
						</form>
					</div>
					<!-- ENDS search -->
				</div>
				<!-- ENDS wrapper-header -->					
			</div>
			<!-- ENDS HEADER -->
			
			
			<!-- Menu -->
			<div id="menu">
			
			
			
				<!-- ENDS menu-holder -->
				<div id="menu-holder">
					<!-- wrapper-menu -->
                                        
                                            
					<div class="wrapper">
                                            
     
                                            
						<!-- Navigation -->
						<ul id="nav" class="sf-menu">
                                                    <li id="li_home"    onclick="setMenu('li_home')" class="" ><a href="<?php echo url_for('@homepage')?>"><?php echo __('Home')?><span class="subheader"><?php echo __('Welcome')?></span></a></li>
				                    <li id="li_museum"  onclick="setMenu('li_museum')" ><a href="<?php echo url_for('@museos')?>"><?php echo __('Our Museums')?><span class="subheader"><?php echo __('Information')?></span></a>
                                                            <?php $museos =Doctrine_Query::create()->from('Institution')
                                                                    ->orderBy('name')
                                                                    ->execute();?>
                                                            
								<ul>
                                                                     <?php foreach ($museos as $museo):?>
                                                                    <li onclick="setMenu('li_museum')" >
                                                                        <a  href="<?php echo url_for('museos/museum?id='.$museo->getId());?>"> 
                                                                            <span>
                                                                                  <?php echo $museo->getName()?> 
                                                                                  <?php if($museo->getSubtitle()):?>
                                                                                      " <?php echo $museo->getSubtitle()?> " 
                                                                                  <?php endif?>
                                                                            </span>
                                                                        </a>
                                                                    </li>  
                                                                     <?php endforeach;?> 
                                                                      

                                                                </ul>
						     </li>
                                                     <li id="li_object" onclick="setMenu('li_object')"  ><a href="<?php echo url_for('@search')?>"><?php echo __('Objects')?> <span class="subheader"><?php echo __('Showcase work')?></span></a>
                                                            <ul>   
                                                                <li><a onclick="setMenu('li_object')" href="<?php echo url_for('@search')?>"> <span><?php echo __('Search')?></span></a></li>
                                                                <li><a onclick="setMenu('li_object')" href="<?php echo url_for('@search_avan')?>"> <span><?php echo __('Advanced')?></span></a></li>
                                                            </ul>
						     </li>
                                                     
						     <li id="li_contact" onclick="setMenu('li_contact')"><a href="<?php echo url_for('@contact')?>"><?php echo __('Contact')?><span class="subheader"><?php echo __('Get in touch')?></span></a>
                                                            <ul>   
                                                                <li><a onclick="setMenu('li_contact')" href="<?php echo url_for('@contact')?>"> <span><?php echo __('Contact us')?></span></a></li>
                                                                <li><a onclick="setMenu('li_contact')" href="<?php echo url_for('@comment')?>"> <span><?php echo __('Comments')?></span></a></li>
                                                            </ul>
							</li>
						</ul>
						<!-- Navigation -->
					</div>
					<!-- wrapper-menu -->
				</div>
				<!-- ENDS menu-holder -->
			</div>
			<!-- ENDS Menu -->
			
			
			

                    <!-- MAIN -->
	             <div id="main">    
                         <!-- wrapper-main -->
                            <div class="wrapper">						
                                <!-- content -->
                                <div id="content">
                                    <div style="display: inline; float: right">
                                               <?php $user = sfContext::getInstance()->getUser(); $form = new sfFormLanguage($user, array('languages' => array('en', 'es')));?>
                                               <form action="<?php echo url_for('change_language') ?>">
                                               <?php echo $form ?><input type="submit" value="<?php echo __('go')?>" />
                                               </form>
                                    </div>
                                 <!-- ####################################################################################################### -->
                                   <?php echo $sf_content ?>
                                 <!-- ####################################################################################################### -->
		
                                 </div>
			        <!-- ENDS CONTENT -->  
			    </div>
			<!-- ENDS WRAPPER-->  
                     </div>
			<!-- ENDS MAIN -->   
                         
                         
                         
			<!-- Twitter -->
			<div id="twitter" style="height: 95px;">
                <div class="wrapper" >
				      	<br/>
                                 <center>
                                          <div class="fb-like" data-href="http://patrimonio.chaco.gov.ar" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div> 
			                     </center>  
			       
				</div>
			<!-- 	</div>-->
			</div>
			<!-- ENDS Twitter -->

			
			<!-- FOOTER -->
			<div id="footer">
				<!-- wrapper-footer -->
				<div class="wrapper">
					<!-- footer-cols -->
					<ul id="footer-cols">
						<li class="col">
							<h6><?php echo __('Pages')?></h6>
							<ul>
								<li class="page_item"><a href="<?php echo url_for('@homepage')?>"><?php echo __('Home')?></a></li>
                                                                <li class="page_item"><a href="<?php echo url_for('@museos')?>"><?php echo __('Our Museums')?></a></li>
                                                                <li class="page_item"><a href="<?php echo url_for('@search')?>"><?php echo __('Objects')?></a></li>
                                                                <li class="page_item"><a href="<?php echo url_for('@contact')?>"><?php echo __('Contact')?></a></li>
								
							</ul>
						</li>
						
						<li class="col">
							<h6><?php echo __('Our Pages in facebook')?></h6>
							<ul>
                                                                <?php foreach ($museos as $museo):?>
                                                                   <?php if($museo->getFacebook()): ?> 
                                                                        <li>
                                                                            <a onclick="window.open(this.href);return false;" href="<?php echo $museo->getFacebook();?>"> 
                                                                                <span>
                                                                                      <?php echo $museo->getName()?>  
                                                                                      <?php if($museo->getSubtitle()):?>
                                                                                          " <?php echo $museo->getSubtitle()?> " 
                                                                                      <?php endif?>
                                                                                </span>
                                                                            </a>
                                                                        </li>  
                                                                    <?php endif?>
                                                                <?php endforeach;?> 
							</ul>
						</li>
                                            
      
          
						<li class="col">
							  <h6><?php echo __('About Us')?></h6>
                                                          <p align="justify"><?php echo __('The culture is not an expense, it is a strategic investment for the development and incrase of towns; the culture is a inalienable social right and the State is its guarantor delegated...')?></p>
                                                          <p align="justify"><?php echo __('The doers of the culture, tha peoples and citizens of this province are the same, although governments change')?>.</p>
						</li>
						
					</ul>
					<!-- ENDS footer-cols -->
				</div>
				<!-- ENDS wrapper-footer -->
			</div>
			<!-- ENDS FOOTER -->
		
		
			<!-- Bottom -->
			<div id="bottom">
				<!-- wrapper-bottom -->
				<div class="wrapper">
                                    <div id="bottom-text"><a href="<?php echo url_for('@nosotros')?>">Desarrollado por...</a></div>
					<!-- Social -->
					<ul class="social ">
                                            <li></li>
                                            2011 Nova all rights reserved (Template) http://www.luiszuno.com
					</ul>
					<!-- ENDS Social -->
					<div id="to-top" class="poshytip" title="<?php echo __('To top')?>"></div>
				</div>
				<!-- ENDS wrapper-bottom -->
			</div>
			<!-- ENDS Bottom -->
                        
</body>

</html>

