<?php

/**
 * museos actions.
 *
 * @package    museo
 * @subpackage museos
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class noticiasActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->museums = Doctrine_Core::getTable('News')
      ->createQuery('a')
      ->orderBy('a.created_at Desc');
   //   ->execute();
    $this->pager = new sfDoctrinePager( 'News', 6);
    $this->pager->setQuery($this->museums);
    $this->pager->setPage($request->getParameter('page', 1));
    $this->pager->init(); 
  }

  public function executeSearchResult(sfWebRequest $request){

    $cambio = $request->getParameter('news_filters');      
    
    if( $request->getParameter('first') == 'false'){
      $cambio = $this->getUser()->getAttribute('search_news');  
    }else{
      $this->getUser()->setAttribute('search_news', $cambio);
    }

    $this->news = $this->getResult($cambio);

    $this->pager = new sfDoctrinePager( 'News', 6);
    $this->pager->setQuery($this->news);
    $this->pager->setPage($request->getParameter('page', 1));
    $this->pager->init();
      
  }

  protected function getResult($search ){

    $news = Doctrine_Query::create()
            ->from('News n')
            ->orderBy('n.created_at Desc');

    if($search['tipo'] != 'todos'){                       
      $news->andWhere('n.tipo like ?', '%'.$search['tipo'].'%'); 
    
    }

    if($search['institucion_id'] > 0 ){                       
     $news->andWhere('n.institucion_id = ?', $search['institucion_id']);
    }                      

    if( isset($search['descripcion']['text']) ){                       
      $news->andWhere('n.descripcion like ?', '%'.$search['descripcion']['text'].'%'); 
    }   

    return $news;

  }
}
