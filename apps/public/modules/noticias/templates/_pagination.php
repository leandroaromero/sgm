<?php use_helper('jQuery'); ?>
<!-- pagination --> 




<ul class='pager'>
    <li class='first-page' >
        <?php echo jq_link_to_remote('<<', 
            $options = array(
                'update' => 'CargarResultado',
                'url'    => 'noticias/searchResult?page=1&first=false',     
                
            ), array(
                 'href' => '#posts'   
            )
        );
            
        ?>
    </li>
    <li>            
        <?php echo jq_link_to_remote('<', 
            $options = array(
            'update' => 'CargarResultado',
            'url'    => 'noticias/searchResult?page='.$pager->getPreviousPage().'&first=false'),
             array(
                 'href' => '#posts',   
            )

        );
            ?>
    </li>
    <?php foreach ($pager->getLinks() as $page): ?>
        <?php if ($page == $pager->getPage()): ?>
            <li class='active'> <a> <?php echo $page ?></a></li>
        <?php else: ?>
            <li>
                <?php echo jq_link_to_remote( $page, 
                    $options = array(
                       'update' => 'CargarResultado',
                       'url'    => 'noticias/searchResult?page='. $page .'&first=false'),
                array(
                    'href' => '#posts',   
            ));
                ?>   
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
    <li>
        <?php echo jq_link_to_remote( '>', 
            $options = array(
               'update' => 'CargarResultado',
               'url'    => 'noticias/searchResult?page='. $pager->getNextPage() .'&first=false'),
             array(
                 'href' => '#posts',   
            ));
        ?>  
    </li>
    <li class='last-page'>
        <?php echo jq_link_to_remote( '>>', 
            $options = array(
               'update' => 'CargarResultado',
               'url'    => 'noticias/searchResult?page='. $pager->getLastPage() .'&first=false'),
             array(
                 'href' => '#posts',   
            ));
        ?>  
    </li>
</ul>
                        <!-- ENDS pagination -->
                  

                    
                    
<script type="text/javascript">
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top-$('#header').height()
                }, 1000);
            return false;
            }
        }
    });
</script>
