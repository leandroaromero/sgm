
<?php echo use_stylesheet('ui-lightness/jquery-ui-1.8.11.custom.css')?>
<?php echo use_stylesheet('jquery-ui-1.8.7.custom.css')?>
<?php echo use_stylesheet('layout.css')?>
<?php echo use_stylesheet('main.css')?>
<?php echo use_javascript('jquery-1.4.4.min.js')?>	
<?php echo use_javascript('jquery-ui-1.8.11.custom.min.js')?>        
<?php use_helper('jQuery'); ?>        
        





						
    <!-- title -->
    <div id="page-title">
            <span class="title"><?php echo __('News and Events')?></span>
            <span class="subtitle"><?php echo __('in the Province of Chaco')?></span>          
    </div>
    <div id="posts">
      <?php $form = new NewsPublicFormFilter();?>
    <form action="javascript:void(0);" method="post" >
          <center>
            <table class="table">
              <tbody>
                <tr>

                  <td >
                    <?php echo $form['institucion_id']->renderLabel() ?>: </br>
                    <?php echo $form['institucion_id']->renderError() ?>
                    <?php echo $form['institucion_id']->render(array('class' => 'number')) ?>
                  </td>

                  <td>
                    <?php echo $form['descripcion']->renderLabel() ?>: </br>
                    <?php echo $form['descripcion']->renderError() ?>
                    <?php echo $form['descripcion']->render(array('class' => 'number')) ?>
                  </td>

                  <td>
                    <?php echo $form['tipo']->renderLabel() ?>: </br>
                    <?php echo $form['tipo']->renderError() ?>
                    <?php echo $form['tipo']->render(array('class' => 'number')) ?>
                  </td>

                  <td colspan="">
                    <br/>
                    <?php echo jq_submit_to_remote( ' __()'   , __('Send'),
                        $options = array(
                        'update' => 'CargarResultado',
                        'url'    => 'noticias/searchResult?',
                      ), 
                      $options_html = array()
                    )?>
                  </td>   

                </tr>                
              </tbody>
            </table>
          </center>
        </form>
    </div>
    <!-- ENDS title -->
    
    <div id="CargarResultado">
      <div id="resultado">
      <?php include_partial('noticias/list', array('news' => $pager->getResults())) ?>

      <div class="clear"></div>
      <div id="to-top">
        <?php if ($pager->haveToPaginate()): ?>
          <?php include_partial('noticias/pagination', array('pager' => $pager)) ?>
        <?php endif; ?>
      </div>  
      <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?>
      <?php if ($pager->haveToPaginate()): ?>
        <?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
      <?php endif; ?>
      </div>
    </div>