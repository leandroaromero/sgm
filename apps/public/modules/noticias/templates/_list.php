
<style type="text/css">
#portfolio{
    display:block;
    width:100%;
    line-height:1.6em;
    }

#portfolio {
    display:block;
    width:100%;
    margin-bottom:2px;
    padding:0;
    border-bottom:0px solid #ffffff;
    }



#portfolio ul, #portfolio h2, #portfolio p, #portfolio img{
    margin:0;
    padding:0;
    list-style:none;
    }

#portfolio  li{
    float:left;
    margin:0 20px 20px 0;
    }

#portfolio  li.last{
    margin-right:0;
    }

#portfolio  li img{
    border:5px solid #DFDFDF;
    }

#portfolio .fl_right p.name{
    font-weight:bold;
    }

#portfolio .fl_right p.readmore{
    text-transform:uppercase;
    }
</style>


<div class="sf_admin_list">
  <div id="portfolio">
    <ul>
      <?php foreach ($news as $i => $new): ?>
        <li style="list-style:none"  >
          <div align="center" style="background-color: #DFDFDF; min-height: 450px; min-width: 460px">

                  <?php 
                  
                          $texto = (String) htmlspecialchars_decode ($new->getEnlace() );
                          $busqueda = array ('@width=".*?"@','@height=".*?"@'); 
                          $reemplazar=array('width="450"','height="750"');
                       
                          $busqueda2 = array ('@width=.*?"@'); 
                          $reemplazar2=array('width=450"');
                  ?>
                  <?php   
                          
                          $texto = preg_replace($busqueda, $reemplazar, $texto, 1);      
                          $texto = preg_replace($busqueda2, $reemplazar2, $texto, 1); 
                         
                          
                  ?>        
                          
                    <?php echo htmlspecialchars_decode ( $texto )?>
          </div>
          <p class="name"></p>
          <p class="readmore"></p>              
          <ul class="sf_admin_td_actions">
              <li class="sf_admin_action_show">            
              </li>
          </ul>
              <p></p>
        </li>
              
      <?php endforeach; ?>          
    </ul>
  </div>

</div>





















