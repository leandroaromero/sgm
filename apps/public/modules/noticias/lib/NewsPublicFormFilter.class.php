<?php

/**
 * news filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsPublicFormFilter extends BaseNewsFormFilter
{
  public function configure()
  {
    sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));

  	unset($this['created_by'],$this['updated_by'],$this['enlace'],$this['updated_at']);
    $format = '%day%/%month%/%year%';
    $web = sfConfig::get('app_url_web');

    $rango = range(2018, 2018);
    $arreglo_rango = array_combine($rango, $rango);
        

   $this->widgetSchema['created_at']= new sfWidgetFormFilterDate(array(
          'from_date' => new sfWidgetFormJQueryDate(array(
             'label' => 'Fecha *',
           'image'  => '/museo/web/images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))
              )), 

          'to_date' => new sfWidgetFormJQueryDate(array(
                 'label' => 'Fecha *',
                 'image'  => '/museo/'.$web.'images/calendar_icon.gif',
                 'culture' => 'es',
                 'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))

            )), 'with_empty' => false));






  	$this->widgetSchema['descripcion']->setOption('with_empty', false);
  	$parameter = sfConfig::get('app_noticias_tipo', array(''=>'') );
    $this->widgetSchema['tipo'] = new sfWidgetFormChoice(
                                            array(
                                                  'choices'   => $parameter,
                                                  'multiple'  => false,
                                                  'expanded'  => false,
                                                ),
                                            array( 
                                                  
                                                  )
                                          );
  }
}
