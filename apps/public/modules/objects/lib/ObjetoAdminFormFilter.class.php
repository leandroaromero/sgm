<?php

/**
 * Objeto filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObjetoAdminFormFilter extends BaseObjetoFormFilter
{
  public function configure()
  {
      $format = '%day%/%month%/%year%';

        $this->widgetSchema['fecha_ingreso'] = new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(array('format' => $format, 'label'=> 'Desde')) , 'to_date' => new sfWidgetFormDate(array('format' => $format))));
        $this->widgetSchema['fecha_baja'] = new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(array('format' => $format)) , 'to_date' => new sfWidgetFormDate(array('format' => $format))));

     $this->widgetSchema['autor_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Autor ',
              'model'   => 'Autor',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
     $this->widgetSchema['ubicacion_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Ubicación ',
              'model'   => 'Ubicacion',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
     $this->widgetSchema['donacion_id'] = new sfWidgetFormDoctrineChoice(array(
              'label' => 'Donacion ',
              'model'   => 'Donacion',
              'table_method' => 'getOrderNombre',
              'add_empty' => true,
            ));
     $this->widgetSchema['coleccion_list']->setOption('table_method', 'getOrderNombre');
     $this->widgetSchema['material_list']->setOption('table_method', 'getOrderNombre');
     sfProjectConfiguration::getActive()->loadHelpers('I18N');  
     $this->widgetSchema['material_list']->setOption('label', __('Materials') );
     $this->widgetSchema['nombre_objeto']->setOption('label', __('Name') );
     $this->widgetSchema['material_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
     $this->widgetSchema['material_list']->setOption('table_method', 'getOrderNombre');
     $this->widgetSchema['material_list']->setOption('renderer_options', array('label_associated' => __('Selected'),
                                                                             'label_unassociated' => __('Unselected'), 
                                                                          ));
 //                                                                         'unassociate' => 'Asociar',
 //                                                                         'associate' => 'No Asociar', ));

  }
 /*  public function addColeccionListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }
    $q = Doctrine_Query::create()
    ->from('Objeto o');
    $res=$q->fetchArray();



       $arr2= array();
        foreach ($res as $re2){

        $cont2=0;
       $qq2 = Doctrine_Query::create();
       $qq2->from('ObjetoColeccion c');
       foreach ($values as $va){
        $qq2->where('c.coleccion_id = ?', $va );
        $qq2->AndWhere('c.objeto_id = ?', $re2['id']);
        $objetos2 = $qq2->fetchArray();
        if( count($objetos2) > 0 ){
            $cont2++;
        }

   }

   if( $cont2  == count($values)){
    $arr2[]= $re2['id'];
    }
   }
    if(count( $arr2 ) > 0){
    $query->whereIn($query->getRootAlias().'.id ', $arr2);
}

  }*/

  /////////////////////////////////////////////////////////
    public function addMaterialListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }
     $q = Doctrine_Query::create()
          ->from('Objeto o');
     $res=$q->fetchArray();



       $arr= array();
        foreach ($res as $re){

        $cont=0;
       $qq = Doctrine_Query::create();
       $qq->from('ObjetoMaterial m');
       foreach ($values as $va){
        $qq->where('m.material_id = ?', $va );
        $qq->AndWhere('m.objeto_id = ?', $re['id']);
        $objetos = $qq->fetchArray();
        if( count($objetos) > 0 ){
            $cont++;
        }

   }

   if( $cont  == count($values)){
    $arr[]= $re['id'];
    }

    }

    if(count( $arr ) > 0){

    $query->whereIn($query->getRootAlias().'.id ', $arr);
    
}


  }
  ////////////////////////////////////////////



}
