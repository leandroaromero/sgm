<style type="text/css">
#portfolio{
	display:block;
	width:100%;
	line-height:1.6em;
	}

#portfolio {
	display:block;
	width:100%;
	margin-bottom:20px;
	padding:0;
	border-bottom:0.0px solid #ffffff;
	}



#portfolio ul, #portfolio h2, #portfolio p, #portfolio img{
	margin:0;
	padding:0;
	list-style:none;
	}

#portfolio  li{
	float:left;
	margin:0 20px 20px 0;
	}

#portfolio  li.last{
	margin-right:0;
	}

#portfolio  li img{
	border:5px solid #444;
	}

#portfolio .fl_right p.name{
	font-weight:bold;
	}

#portfolio .fl_right p.readmore{
	text-transform:uppercase;
	}
</style>
<br>
<div class="" >
  <?php if (!$pager->getNbResults()): ?>
     <p><?php echo __('No result', array(), 'sf_admin') ?></p>
  <?php else: ?>
     
<div id="portfolio">

          <ul >
        <?php foreach ($pager->getResults() as $i => $objeto): $odd = fmod(++$i, 2) ? 'odd' : 'even' ?>
         <a href="<?php echo url_for('@object_show?id='.$objeto->getId())?>">
          <li style="list-style:none">
                  <div align="center" style="background-color: #444; min-height: 150px; width: 211px">
               <?php
                    if($foto = $objeto->getFoto()){
                $dir= $sf_user->getFile();
          
                         echo image_tag('../uploads/'.$dir.'/fotos/'.$foto['archivo'], 'style=" height: 150px; max-width: 210px"');
                      }else{
                                      echo image_tag('images/demo/210x150.gif');
                     }
                 ?>
              </div>  
              <p class="name"> <?php echo substr($objeto->getNombreObjeto(),0,32); ?></p>
              <p class="readmore">
                  <a title=" <?php echo $objeto->getDescripcionGenerica() ?>" href="<?php echo url_for('object/index?id='.$objeto->getId())?>"> 
                      <?php echo $objeto->getDescription() ?> &raquo;
                  </a>
              </p>
           </li>
         </a>
        <?php endforeach; ?>
         
         </ul>
    </div>   
    
                
                
      
     
            <?php if ($pager->haveToPaginate()): ?>
                 <?php include_partial('objects/pagination', array('pager' => $pager)) ?>
            <?php endif; ?>

            <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?>
            <?php if ($pager->haveToPaginate()): ?>
                <?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
            <?php endif; ?>
 
          
        
  <?php endif; ?>
</div>
<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.className == 'sf_admin_batch_checkbox') box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked } return true;
}
/* ]]> */
</script>
