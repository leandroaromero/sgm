<?php use_helper('I18N', 'Date') ?>
<?php include_partial('objects/assets') ?>

<div class="wrapper col2">
    <div id="container" class="clear">
<div id="sf_admin_container">
  <h1><?php echo __('Object list', array(), 'messages') ?></h1>

  <?php include_partial('objects/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('objects/list_header', array('pager' => $pager)) ?>
  </div>

    <div id="showt">
   <button id="show" style="background:#2DAEBF;color:#FFFFFF;font-weight:bold;font-size: 9px"><?php echo __('Search')?></button>
   <br/>
   <br/>
    </div>
   <div class="serach_bui" id="serach_bui" style="display: none; background: #EFFBFB" >
    <?php include_partial('objects/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>




  <div id="sf_admin_content">
    <form action="<?php echo url_for('objeto_collection', array('action' => 'batch')) ?>" method="post">
    <?php include_partial('objects/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('objects/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
</div>
</div>
  <script type="text/javascript">
      $("#show").click(function () {
        if ($('#serach_bui').css('display') == "none") {
          $('#serach_bui').slideToggle(800);
          $("#show").html("<?php echo __('Hide browse')?>");
        } else {
          $("#show").html("<?php echo __('Search')?>");
          $('#serach_bui').hide(800);
        }
      });

</script>

