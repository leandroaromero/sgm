<?php

require_once dirname(__FILE__).'/../lib/objectsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/objectsGeneratorHelper.class.php';

/**
 * objects actions.
 *
 * @package    museo
 * @subpackage objects
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class objectsActions extends autoObjectsActions
{
 public function executeIndexPagina(sfWebRequest $request)
  {
   $this->getUser()->setAttribute('pagina_actual', 'Objects');
   $this->redirect("@objeto");
  }
  
  public function executeMilagro(sfWebRequest $request){
      
      $arr= array( "hombre_chaquenio", "hombre_chaquenio", "ccias_naturales","hombre_chaquenio", "hombre_chaquenio", "ccias_naturales","hombre_chaquenio", "hombre_chaquenio", "ccias_naturales");// "bellas_artes", "ccias_naturales", "medios_comunicacion", "hombre_chaquenio");
      $this->respuesta= array();
      foreach ($arr as $museo) {
          $this->Cambiar2($museo);
          
          $q = Doctrine_Query::create()
               ->from('Objeto o')
               ->execute();
          
       foreach ($q as $obj){
           $oo= new Publico($obj);
           $this->respuesta[]= $oo; 
          }
          
      } 
  }
 public function Cambiar2($cambio)
  { 
    $this->getUser()->setAttribute('base_dato', $cambio);
  } 
public function executeIndex2(sfWebRequest $request)
{
  $max_per_page = 10;
  $query = Doctrine::getTable('NombreClase')->createQuery('n');
  $this->pager = new sfDoctrinePager('NombreClase', $max_per_page);
  $this->pager->setQuery($query);
  $this->pager->setPage($request->getParameter('pag', 1));
  $this->pager->init();
} 
}
