
<?php $web= sfConfig::get('app_url_web');  ?> 
        <!-- title -->
        <div id="page-title">
                <span class="title"><?php echo $objeto->getNombre();?></span>
        </div>
        <div id="content" style="margin-left: 9%">        
           <div class="">
               <div>
                <ul class="">
                   <li style=" display: block;overflow: visible;width: 650px;">
                       <?php if ($objeto->getTipo()){ ?> 
                            <b><?php echo __('Type of the Object')?>:</b><span class="subtitle"><?php echo $objeto->getTipo()?></span>
                       <?php } 
                              if ($objeto->getDescripcion()){ ?>     
                            <b><?php echo __('Generic Description')?>: </b><span class="subtitle"><?php echo $objeto->getDescripcion()?></span>
                       <?php }
                              if ($objeto->getAutor()){?>     
                            <b><?php echo __('Author')?>: </b><span class="subtitle"><?php echo $objeto->getAutor()?></span>
                       <?php }
                               if ($objeto->getMateriales()){?>     
                            <b><?php echo __('Materials')?>: </b><span class="subtitle"><?php echo $objeto->getMateriales()?></span>
                       <?php } 
                               if ($objeto->getColecciones()){?>     
                            <b><?php echo __('Colections')?>: </b><span class="subtitle"><?php echo $objeto->getColecciones()?></span>
                       <?php } 
                               if ($objeto->getAnio()){?>     
                            <b><?php echo __('Year of origin')?>: </b><span class="subtitle"><?php echo $objeto->getAnio()?></span>
                       <?php } 
                               if ($objeto->getTextoAntiguedad()){?>     
                            <b><?php echo __('Comes')?> : </b><span class="subtitle"><?php echo $objeto->getTextoAntiguedad()?></span>
                       <?php } ?> 
                            <b><?php echo __('Institution')?>: </b><span class="subtitle"><?php echo $objeto->getMuseoName().' '.$objeto->getMuseoSubName()?></span>
                   </li>
                </ul>
               </div>
               
            </div>
            <br/>
            <div id="content_1">
                <ul id="portfolio-list" class="gallery">
                   <?php foreach ($objeto->getPublicImagen() as $pic):?>
                         <li class="" >
                             <a   style="background: #444 "href="/museo/<?php echo $web?>uploads/<?php echo $objeto->getDato()?>/fotos/<?php echo $pic->getArchivo() ?>" rel="group1" class="fancybox" title="<?php echo $pic->getTitulo()?>">
                                    <center>
                                        <?php echo image_tag('../uploads/'.$objeto->getDato().'/fotos/'.$pic->getArchivo(), 'style=" height: 148px; max-width: 204px"')?>
                                    </center> 
                             </a>

                        </li>
                    <?php endforeach;?>    
                </ul>
            </div> 
            <center>
                <div class="fb-like" data-href="http://patrimonio.chaco.gov.ar/gallery/<?php echo $objeto->getDato()?>/<?php echo $objeto->getObjetoId()?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
            </center>
      </div>