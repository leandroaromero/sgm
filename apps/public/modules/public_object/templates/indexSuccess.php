<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('jQuery'); ?>

<!-- title -->
  <div id="page-title">
      <span class="title"><?php echo __('Objects')?></span>
      <span class="subtitle"><?php  __('Of the Objects')?></span>
  </div>
<!-- headline -->
  <div class="clear"></div>
  <div id="headline">
    <span class="main"><?php echo __('Others')?></span>
                                  <!-- post -->
  </div>  
    <div class="post">
    <div id="CargarResultadoAzar"> 

            <?php include_partial('public_object/list_azar', array('result' => $objeto_azar)) ?>
            
            <div class="clear"></div>
  </div>

<!-- headline -->
  <div class="clear"></div>
  <div id="headline">
    <span class="main"><?php echo __('The most visited')?></span>
                                  <!-- post -->
  </div>  
  
  <div class="post">
    <div id="CargarResultadoBest"> 
      <?php include_partial('public_object/list_best', array('result' => $pager->getResults())) ?>
      <div class="clear"></div>
      <div style="display: inline;float: right;">
        <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?>
        <?php if ($pager->haveToPaginate()): ?>
          <?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->  getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
        <?php endif; ?>

      </div>  
      <?php if ($pager->haveToPaginate()): ?>
            <?php include_partial('public_object/pagination_best', array('pager' => $pager)) ?>
      <?php endif; ?>

        </div>               
      </div>
                                <!-- ENDS post -->



<!-- title -->
  <div id="page-title">
      <span class="title"><?php echo __('Search')?></span>
      <span class="subtitle"><?php echo __('Objects')?></span>
  </div>


    <div id="posts" class="single">

                                <!-- post -->
      <div class="post">
        <div id="CargarResultado"> </div>               
      </div>
                                <!-- ENDS post -->
    </div>
                    <!-- sidebar -->
      <ul id="sidebar" style="float: right; width: 200px;margin-right: 6px;">
        <form action="javascript:void(0);" method="post" >
          <center>
            <table class="table">
              <tbody>
                <tr>
                  <td  style="border: 0px">
                    <?php echo $form['museo_name']->renderLabel() ?>:</br>
                    <?php echo $form['museo_name']->renderError() ?>
                    <?php echo $form['museo_name']->render() ?>
                  </td>
                </tr>
                <tr>
                  <td style="border: 0px">
                    <?php echo $form['general']->renderLabel() ?>:</br>
                    <?php echo $form['general']->renderError() ?>
                    <?php echo $form['general']->render(array('class' => 'number')) ?>
                  </td>
                </tr>
                <tr>
                <tr>
                  <td style="border: 0px">
                    <?php echo $form['tipo']->renderLabel() ?>:</br>
                    <?php echo $form['tipo']->renderError() ?>
                    <?php echo $form['tipo']->render(array('class' => 'number')) ?>
                  </td>
                </tr>
                <tr>
                <tr>
                  <td style="border: 0px">
                    <?php echo $form['categoria']->renderLabel() ?>:</br>
                    <?php echo $form['categoria']->renderError() ?>
                    <?php echo $form['categoria']->render(array('class' => 'number')) ?>
                  </td>
                </tr>
                <tr>
                  <td colspan="">
                    <?php echo jq_submit_to_remote( ' __()'   , __('Send'),
                        $options = array(
                        'update' => 'CargarResultado',
                        'url'    => 'public_object/searchResult?',
                      ), 
                      $options_html = array()
                    )?>
                  </td>  
                </tr>
              </tbody>
            </table>
          </center>
        </form>
                    </ul>
                    <!-- ENDS sidebar -->
        <br/>                 
    </div>
                    

   

<script type="text/javascript">
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top-$('#header').height()
                }, 1000);
            return false;
            }
        }
    });
</script>






































