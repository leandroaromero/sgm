<style type="text/css">
#portfolio{
	display:block;
	width:100%;
	line-height:1.6em;
	}

#portfolio {
	display:block;
	width:100%;
	margin-bottom:20px;
	padding:0;
	border-bottom:0.1px solid #ffffff;
	}



#portfolio ul, #portfolio h2, #portfolio p, #portfolio img{
	margin:0;
	padding:0;
	list-style:none;
	}

#portfolio  li{
	float:left;
	margin:0 20px 20px 0;
	}

#portfolio  li.last{
	margin-right:0;
	}

#portfolio  li img{
	border:5px solid #DFDFDF;
	}

#portfolio .fl_right p.name{
	font-weight:bold;
	}

#portfolio .fl_right p.readmore{
	text-transform:uppercase;
	}
</style>


<div class="sf_admin_list">
  
    <table cellspacing="0">

      <tbody>
      <div id="portfolio">

          <ul>
           <?php  while($objeto = $result->fetchPagedRow()):  ?>
            
            
              <li style="list-style:none"  >
                  <div align="center" style="background-color: #DFDFDF; min-height: 150px; min-width: 210px">
               <?php if($foto = $objeto->getImagen1()){
                            $dir= $objeto->getFile();
                            echo link_to( image_tag('../uploads/'.$dir.'/fotos/'.$foto, 'style=" height: 150px; max-width: 210px"'),'gallery/result?index='.$objeto->getIndex(),
                                    array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=850,height=600,left=320,top=0'), ''));
                        }else{
                                echo link_to( image_tag('images/demo/210x150.gif'),'gallery/result?index='.$objeto->getIndex(),
                                    array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=850,height=600,left=320,top=0'), ''));
                        }
                 ?>

              </div>
              <p class="name"><?php echo $objeto->getNombreTitulo() ?></p>
              <p class="readmore">
                   <p>
                  <div class="fb-like" data-href="http://patrimonio.chaco.gov.ar/gallery/<?php echo $objeto->getFile()?>/<?php echo $objeto->getId()?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
              
                <ul class="sf_admin_td_actions">
                    <li class="sf_admin_action_show">
                      <?php //echo link_to(__('Visualizar', array(), 'messages'), 'objeto/ListShow?id='.$objeto->getId(), array()) ?>
                    </li>
                    
               </ul>
              </p>
             
            </li>

             
           <?php endwhile; ?>
          
          </ul>
        </div>

      
                <tr>
                    <td><?php echo $result->getTotal().' Resultados - ( página '.$result->getPageNumber().' / '.$result->fetchNumberPages().' )' ?></td>
                    
                </tr>
      </tbody>
    </table>

</div>
<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.className == 'sf_admin_batch_checkbox') box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked } return true;
}
/* ]]> */
</script>












<table>
  <thead>
    <tr>

      <th>Institucíon</th>
      <th>Acervo</th>      
      <th>File</th>
      <th>Anio</th>
      <th>Texto antiguedad</th>
      <th>Tipo</th>
      <th>Tipo</th>
      <th>Autor</th>
      <th>Autor</th>
      <th>Colecciones</th>
      <th>Materiales</th>
      <th>Imagen</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($public_objects as $public_object): ?>
    <tr>
      
      <td><?php echo $public_object->getMuseoName() ?><br>
          <?php echo $public_object->getMuseoSubName() ?>
      </td>
      <td>
        <a href="<?php echo url_for('public_object/edit?id='.$public_object->getId()) ?>"><?php echo $public_object->getId() ?></a></br>
      <b><?php echo $public_object->getNombre() ?></b><br>
      <?php echo $public_object->getDescripcion() ?></td>
      
      <td>
  
        <div align="center" style="background-color: #000; min-height: 110px; width: 150px; ">
          <p>
            <?php  if($public_object->getImagen())
              {
                echo link_to( 
                            image_tag('../uploads/'.$public_object->getFile().'/fotos/thumbs/'.$public_object->getImagen(), array('style'=>'min-height: 80px; max-width:145px; padding:3px')),
                            '@acervos?index='.$public_object->getId().'&museo='.$public_object->getFile().'&acervo_id='.$public_object->getId(),
                            array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=850,height=600,left=320,top=0'), ''));
              }else{
                echo link_to( image_tag('images/demo/150x110.gif'),'@acervos?index='.$public_object->getId().'&museo='.$public_object->getFile().'&acervo_id='.$public_object->getId(),
                array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=850,height=600,left=320,top=0'), ''));
                         }; ?>
          </p>
        </div>                 

      </td>
      <td><?php echo $public_object->getAnio() ?></td>
      <td><?php echo $public_object->getTextoAntiguedad() ?></td>
      <td><?php echo $public_object->getTipo() ?></td>
      <td><?php echo $public_object->getTipoId() ?></td>
      <td><?php echo $public_object->getAutor() ?></td>
      <td><?php echo $public_object->getAutorId() ?></td>
      <td><?php echo $public_object->getColecciones() ?></td>
      <td><?php echo $public_object->getMateriales() ?></td>
      <td><?php echo $public_object->getImagen() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>


 <?php //include_partial('public_object/list', array('public_objects' => $public_objects)) ?>