

<?php include_partial('public_object/list_best', array('result' => $pager->getResults())) ?>
            
<div class="clear"></div>
<div style="display: inline;float: right;">
  	<?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?>
	<?php if ($pager->haveToPaginate()): ?>
		<?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->	getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
	<?php endif; ?>

</div>	
<?php if ($pager->haveToPaginate()): ?>
	<?php include_partial('public_object/pagination_best', array('pager' => $pager)) ?>
<?php endif; ?>
