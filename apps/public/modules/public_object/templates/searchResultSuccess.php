
<?php include_partial('public_object/list', array('public_objects' => $pager->getResults())) ?>

<?php if ($pager->haveToPaginate()): ?>
        <?php include_partial('public_object/pagination', array('pager' => $pager)) ?>
<?php endif; ?>

<?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?>
<?php if ($pager->haveToPaginate()): ?>
        <?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
<?php endif; ?>