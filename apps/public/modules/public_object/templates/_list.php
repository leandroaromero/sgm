
<table>

    <thead>
        <tr>
            <th>
                    Imagen
            </th>
            <th>
                <?php echo __('Name of the Object')?>
            </th>
            <th>

            </th>

        </tr>
    </thead>
        <?php $cont=0;    
            foreach ($public_objects as $public_object): 
            $cont++;
        ?>
            <tr>   
                <td> 
                    <div align="center" style="background-color: #000; min-height: 110px; width: 150px; ">
                    <p>
                        <?php  if($public_object->getImagen()){
                            echo link_to( image_tag('../uploads/'.$public_object->getDato().'/fotos/thumbs/'.$public_object->getImagen(), array('style'=>'min-height: 80px; max-width:145px; padding:3px')),'@acervo?id='.$public_object->getId(),
                                    array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=850,height=600,left=320,top=0'), ''));
                                  }else{
                                     echo link_to( image_tag('images/demo/150x110.gif'),'@acervo?id='.$public_object->getId(),
                                    array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=850,height=600,left=320,top=0'), ''));
                                  }; ?>
                            </p>
                        </div>    
                    </td>             
                    <td> 
                        <b> <?php echo $public_object->getNombre() ?></b></br>
                            <?php echo $public_object->getDescripcion() ?></br>
                            <?php echo $public_object->getColecciones()?></br>      
                            <i><b><?php echo $public_object->getMuseoName() ?></b>
                        <?php echo $public_object->getMuseoSubName() ?></i>
                </td> 
                    </td>
                    <td> <?php
                                echo link_to(image_tag('public/img/search-submit.png'),'@acervo?id='.$public_object->getId(),
                                array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=850,height=600,left=320,top=0'), 'size=100x100'))
                            ?>
                     </td>   
                </tr>
         <?php  endforeach;?>
</table>