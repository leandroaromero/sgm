
<?php use_helper('jQuery'); ?>
<!-- pagination --> 


<div id="to-top">

<ul class='pager'>
    <li class='first-page' >
        <?php echo jq_link_to_remote('<<', 
            $options = array(
                'update' => 'CargarResultadoBest',
                'url'    => 'public_object/searchResultBest?page=1',     
                
            ), array(
                 'href' => '#CargarResultadoBest'   
            )
        );
            
        ?>
    </li>
    <li>            
        <?php echo jq_link_to_remote('<', 
            $options = array(
            'update' => 'CargarResultadoBest',
            'url'    => 'public_object/searchResultBest?page='.$pager->getPreviousPage()),
             array(
                 'href' => '#CargarResultadoBest',   
            )

        );
            ?>
    </li>
    <?php foreach ($pager->getLinks() as $page): ?>
        <?php if ($page == $pager->getPage()): ?>
            <li class='active'> <a> <?php echo $page ?></a></li>
        <?php else: ?>
            <li>
                <?php echo jq_link_to_remote( $page, 
                    $options = array(
                       'update' => 'CargarResultadoBest',
                       'url'    => 'public_object/searchResultBest?page='. $page ),
                array(
                    'href' => '#CargarResultadoBest',   
            ));
                ?>   
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
    <li>
        <?php echo jq_link_to_remote( '>', 
            $options = array(
               'update' => 'CargarResultadoBest',
               'url'    => 'public_object/searchResultBest?page='. $pager->getNextPage() ),
             array(
                 'href' => '#CargarResultadoBest',   
            ));
        ?>  
    </li>
    <li class='last-page'>
        <?php echo jq_link_to_remote( '>>', 
            $options = array(
               'update' => 'CargarResultadoBest',
               'url'    => 'public_object/searchResultBest?page='. $pager->getLastPage() ),
             array(
                 'href' => '#CargarResultadoBest',   
            ));
        ?>  
    </li>
</ul>
                        <!-- ENDS pagination -->
                    </div>


