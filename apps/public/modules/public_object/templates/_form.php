<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('public_object/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('public_object/index') ?>">Back to list</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'public_object/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['id_object']->renderLabel() ?></th>
        <td>
          <?php echo $form['id_object']->renderError() ?>
          <?php echo $form['id_object'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['nombre']->renderLabel() ?></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['descripcion']->renderLabel() ?></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['museo_name']->renderLabel() ?></th>
        <td>
          <?php echo $form['museo_name']->renderError() ?>
          <?php echo $form['museo_name'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['museo_sub_name']->renderLabel() ?></th>
        <td>
          <?php echo $form['museo_sub_name']->renderError() ?>
          <?php echo $form['museo_sub_name'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['file']->renderLabel() ?></th>
        <td>
          <?php echo $form['file']->renderError() ?>
          <?php echo $form['file'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['anio']->renderLabel() ?></th>
        <td>
          <?php echo $form['anio']->renderError() ?>
          <?php echo $form['anio'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['texto_antiguedad']->renderLabel() ?></th>
        <td>
          <?php echo $form['texto_antiguedad']->renderError() ?>
          <?php echo $form['texto_antiguedad'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['tipo']->renderLabel() ?></th>
        <td>
          <?php echo $form['tipo']->renderError() ?>
          <?php echo $form['tipo'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['tipo_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['tipo_id']->renderError() ?>
          <?php echo $form['tipo_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['autor']->renderLabel() ?></th>
        <td>
          <?php echo $form['autor']->renderError() ?>
          <?php echo $form['autor'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['autor_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['autor_id']->renderError() ?>
          <?php echo $form['autor_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['colecciones']->renderLabel() ?></th>
        <td>
          <?php echo $form['colecciones']->renderError() ?>
          <?php echo $form['colecciones'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['materiales']->renderLabel() ?></th>
        <td>
          <?php echo $form['materiales']->renderError() ?>
          <?php echo $form['materiales'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['imagen']->renderLabel() ?></th>
        <td>
          <?php echo $form['imagen']->renderError() ?>
          <?php echo $form['imagen'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
