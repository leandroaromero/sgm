<?php

/**
 * public_object actions.
 *
 * @package    museo
 * @subpackage public_object
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class public_objectActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {

     $this->counter = Doctrine_Core::getTable('Counter')
      ->createQuery('a')
      ->where('a.is_other = ?', 0 ) 
      ->orderBy('quantity Desc');
   
    $this->pager = new sfDoctrinePager( 'Counter', 4);
    $this->pager->setQuery($this->counter);
    $this->pager->setPage($request->getParameter('page', 1));
    $this->pager->init();

    $this->objeto_azar= $this->getAzar();  
    $this->form = new  PublicObjectUserFormFilter();    
  }

  public function executeEdit(sfWebRequest $request)
  {
   /* $this->forward404Unless($public_object = Doctrine_Core::getTable('PublicObject')->find(array($request->getParameter('id'))), sprintf('Object public_object does not exist (%s).', $request->getParameter('id')));
    $this->form = new PublicObjectForm($public_object);
    */
  }

  public function executeSearchResult(sfWebRequest $request){

    $cambio = $request->getParameter('public_object_filters');      
    
    if( $request->getParameter('first') == 'false'){
      $cambio = $this->getUser()->getAttribute('search_public_object');  
    }else{
      $this->getUser()->setAttribute('search_public_object', $cambio);
    }

    $this->public_objects = $this->getResult($cambio);
    $this->pager = new sfDoctrinePager( 'PublicObject', 10);
    $this->pager->setQuery($this->public_objects);
    $this->pager->setPage($request->getParameter('page', 1));
    $this->pager->init();
      
  }
  protected function getResult($search ){

    $public_object = Doctrine_Query::create()
                      ->from('PublicObject p_o');

    if($search['tipo'] != 'todos'){                       
      $public_object->andWhere('p_o.tipo like ?', '%'.$search['tipo'].'%'); 
    
    }

    if($search['museo_name'] != 'todos'){                       
      $public_object->andWhere('p_o.dato = ?', $search['museo_name']);
    }                      
    if($search['categoria'] != 'todos'){                       
      $public_object->andWhere('p_o.categoria like ?', '%'.$search['categoria'].'%'); 
    }   

    if ($search['general'] != '') {
      $public_object->orwhere('p_o.nombre like ?', '%'.$search['general'].'%')
                    ->orWhere('p_o.descripcion like ?', '%'.$search['general'].'%')
                    ->orWhere('p_o.autor like ?', '%'.$search['general'].'%')
                    ->orWhere('p_o.colecciones like ?', '%'.$search['general'].'%') 
                    ->orWhere('p_o.materiales like ?', '%'.$search['general'].'%'); 
    }
    return $public_object;

  }
 public function executeResultado($request) {
    $this->forward404Unless($this->objeto = Doctrine_Core::getTable('PublicObject')->find(array($request->getParameter('id'))), sprintf('Object public_object does not exist (%s).', $request->getParameter('id'))); 
    $this->getUser()->aumentarContador($this->objeto->getDato(), $this->objeto->getObjetoId());
    $this->setLayout('layout_acervo');

  }  
public function getAzar(){
    $cantidad = Doctrine_Core::getTable('PublicObject')
                              ->createQuery('a')
                              ->execute()
                              ->count();
    $objetos=  array();                          
    for ($i=0; $i < 4; $i++) { 
      $azar=  mt_rand( 1 , $cantidad);    
      $objeto= Doctrine::getTable('PublicObject')->findOneById($azar);
      $objetos[]=$objeto;
    }                          
    return $objetos;  
  } 
  public function executeSearchResultBest(sfWebRequest $request)
  {
    $this->counter = Doctrine_Core::getTable('Counter')
      ->createQuery('a')
      ->where('a.is_other != ?', '1' ) 
      ->orderBy('quantity Desc');
    
    $this->pager = new sfDoctrinePager( 'Counter', 4);
    $this->pager->setQuery($this->counter);
    $this->pager->setPage($request->getParameter('page', 1));
    $this->pager->init();

  } 
}
