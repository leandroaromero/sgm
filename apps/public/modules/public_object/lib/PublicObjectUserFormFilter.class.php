<?php

/**
 * PublicObject filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PublicObjectUserFormFilter extends BasePublicObjectFormFilter
{

  public function configure()
  {
  	$parameter = sfConfig::get('app_institucion_data', array(''=>'') );
    sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));

    $this->widgetSchema['museo_name'] = new sfWidgetFormChoice(
                                            array(
                                                  'choices'   => $parameter,
                                                  'multiple'  => false,
                                                  'label'     => __('Institutions'),
                                                  'expanded'  => false,
                                                ),
                                            array( 
                                                  'style'=>'width:190px;height:17px;font-size:12px',
                                                  )
                                          );
    $this->widgetSchema['general']= new sfWidgetFormInput(
                                        array(
                                             'label'=>__('Search')
                                            ),
                                        array( 'style'=>'width:170px;height:17px;font-size:12px')
                                            );
        
  
    $parameter = sfConfig::get('app_search_tipo', array(''=>'') );
    $this->widgetSchema['tipo'] = new sfWidgetFormChoice(
                                            array(
                                                  'choices'   => $parameter,
                                                  'multiple'  => false,
                                                  'label'     => __('Type'),
                                                  'expanded'  => false,
                                                ),
                                            array( 
                                                  'style'=>'width:190px;height:17px;font-size:12px',
                                                  )
                                          );
    $parameter = sfConfig::get('app_search_categoria', array(''=>'') );
    $this->widgetSchema['categoria'] = new sfWidgetFormChoice(
                                            array(
                                                  'choices'   => $parameter,
                                                  'multiple'  => false,
                                                  'label'     => __('Category'),
                                                  'expanded'  => false,
                                                ),
                                            array( 
                                                  'style'=>'width:190px;height:17px;font-size:12px',
                                                  )
                                         );
  }  
}
