	   <!-- title -->
    <div id="page-title">
            <span class="title"><?php echo __('About Us')?></span>
            <span class="subtitle"><?php echo __('Una gran familia')?>.</span>
    </div>
    <!-- ENDS title -->
					<!-- content -->
					<div id="content">
						
							<!-- TABS -->
							<!-- the tabs -->
							<ul class="tabs">
								<li><a href="#"><span>Desarrollado</span></a></li>
							</ul>
							
							<!-- tab "panes" -->
							<div class="panes">
							
								<!-- Posts -->
								<div>
									<ul class="blocks-thumbs thumbs-rollover">
										<li>
											<a href="mailto:fernandez.juanca@gmail.com" class="thumb" title="Juan Carlos Fernández">
                                                                                            <?php echo image_tag('juan.jpg', 'style=" height: 150px; width: 282px"');?>
                                                                                        </a>
											<div class="excerpt">
												<a href="mailto:fernandez.juanca@gmail.com" class="header">Juan Carlos Fernández</a>
                                                                                                Ingeniero en Sistemas de Información.
											</div>
										</li>
										<li>
											<a href="mailto:leandroaromero@yahoo.com.ar" class="thumb" title="Leandro Antonio Romero">
                                                                                            <?php echo image_tag('leandro.jpg', 'style=" height: 150px; width: 282px"');?>
                                                                                        </a>
											<div class="excerpt">
												<a href="mailto:leandroaromero@yahoo.com.ar" class="header">Leandro Antonio Romero</a>
												Estudiante avanzado de Ingenieria en Sistemas de Información.
											</div>
										</li>
										<li>
											<a href="#" class="thumb" title="Patrimonio">
                                                                                            <?php echo image_tag('public/images/01.jpg', 'style=" height: 150px; width: 282px"');?>
                                                                                        </a>
											<div class="excerpt">
												<a href="single.html" class="header">Patrimonio Chaco</a>
												Cada una de las personas que trabajo y que sigue trabajando día a día, en nuestro patrimonio.
											</div>
											
										</li>
									</ul>
								</div>
								<!-- ENDS posts -->
							</div>
							<!-- ENDS TABS -->
		
		