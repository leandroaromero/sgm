<?php

/**
 * componentes actions.
 *
 * @package    museo
 * @subpackage componentes
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class componentesActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeCambiar(sfWebRequest $request)
  {   
    $cambio= $request->getParameter('institucion');
    $this->getUser()->setAttribute('base_dato', $cambio['cambio']);
    $this->redirect('@objeto_pagina');
    
  }
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }

  public function executeSetMenu($request) {
      $this->getUser()->setAttribute('menu',$request->getParameter('menu'));
  }  
  
  public function executeAbout(){
      
  }

  /*pasajes por el ajax
    envia las noticias por entero
  */
  public function executeGetNoticias($request)
  {
      header("Access-Control-Allow-Origin: * ");
      $this->getResponse()->setContentType('application/json');

      $query = Doctrine::getTable('News')->retrieveSearchSelect(
                  $request->getParameter('general'),
                  $request->getParameter('tipos'),
                  $request->getParameter('carpeta')
      );
      $objetos= $this->armarRespuestaNews($query,
                  $request->getParameter('cantidad', 10),
                  $request->getParameter('pagina', 1) 
      );

      return $this->renderText(json_encode($objetos));
  }
      /*pasajes por el ajax
        envia un acervo por el id
    */
  public function executeGetNoticia($request)
  {
      header("Access-Control-Allow-Origin: * ");
      $this->getResponse()->setContentType('application/json');

      $this->forward404Unless($objeto = Doctrine_Core::getTable('News')->find(array($request->getParameter('id'))), sprintf('Object Noticias does not exist (%s).', $request->getParameter('id'))); 

    return $this->renderText(json_encode( $objeto->getArrayString()));

  }

  /*pasajes por el ajax
    envia los museos por entero
  */
  public function executeGetMuseos($request)
  {
      header("Access-Control-Allow-Origin: * ");
      $this->getResponse()->setContentType('application/json');

      $objetos = Doctrine::getTable('Institution')->retrieveForSelect(
                  $request->getParameter('q'),
                  $request->getParameter('limit')
      );

      return $this->renderText(json_encode($objetos));
  }

  /*pasajes por el ajax
    envia las categorias
  */
  public function executeGetCategorias($request)
  {
      header("Access-Control-Allow-Origin: * ");
      $this->getResponse()->setContentType('application/json');      
      $categorias = Doctrine_Query::create()
            ->from('Categoria c');
            $arreglo= array();

      foreach ( $categorias->execute() as $key => $value) 
      {
              $arreglo[]= (string) $value->__toString();
      }      

    return $this->renderText(json_encode( $arreglo));

  }          
    /*pasajes por el ajax
        envia un acervo por el id
    */
  public function executeGetAcervo($request)
  {
      header("Access-Control-Allow-Origin: * ");
      $this->getResponse()->setContentType('application/json');

      $this->forward404Unless($objeto = Doctrine_Core::getTable('PublicObject')->find(array($request->getParameter('id'))), sprintf('Object public_object does not exist (%s).', $request->getParameter('id'))); 

    return $this->renderText(json_encode( $objeto->getArrayString()));

  }

  /*
        general = string de lo que escribio el usuario
        categorias =categorias
        tipos  = tipos
        cantidad =cantidad de resultados por pagina
        pagina =pagina devuelta
        carpeta= nombre del museo
*/
  public function executeGetAcervos($request)
  {
      header("Access-Control-Allow-Origin: * ");
      $this->getResponse()->setContentType('application/json');

      $query = Doctrine::getTable('PublicObject')->retrieveSearchSelect(
                  $request->getParameter('general'),
                  $request->getParameter('categorias'),
                  $request->getParameter('tipos'),
                  $request->getParameter('carpeta')
      );
      $objetos= $this->armarRespuesta($query,
                  $request->getParameter('cantidad', 10),
                  $request->getParameter('pagina', 1) 
      );
      return $this->renderText(json_encode($objetos));
  }

  protected function armarRespuesta($query, $cantidad, $pagina)
  {
      $pager = new sfDoctrinePager( 'PublicObject', $cantidad);
      $pager->setQuery($query);
      $pager->setPage($pagina);
      $pager->init();
      $dato = array();

      $dato['total']= $pager->getNbResults();
      if ($pager->haveToPaginate()): 
        $dato['pagina']=  $pager->getPage(); 
        $dato['pagina_total']=  $pager->getLastPage();
      endif; 

      $resultado= array();
      $resultado[]= $dato;
      foreach ($pager->getResults() as $key => $objeto) {
        $resultado[]= $objeto->getArrayString();
      }
      return $resultado;
  }

  protected function armarRespuestaNews($query, $cantidad, $pagina)
  {
      $pager = new sfDoctrinePager( 'News', $cantidad);
      $pager->setQuery($query);
      $pager->setPage($pagina);
      $pager->init();
      $dato = array();

      $dato['total']= $pager->getNbResults();
      if ($pager->haveToPaginate()): 
        $dato['pagina']=  $pager->getPage(); 
        $dato['pagina_total']=  $pager->getLastPage();
      endif; 

      $resultado= array();
      $resultado[]= $dato;

      foreach ($pager->getResults() as $key => $objeto) {
        $resultado[]= $objeto->getArrayString();
      }
      return $resultado;
  }

  /*
        general = string de lo que escribio el usuario
        categorias =categorias
        tipos  = tipos
        cantidad =cantidad de resultados por pagina
        pagina =pagina devuelta
        carpeta= nombre del museo
*/
  public function executeGetCarrusel($request)
  {
      header("Access-Control-Allow-Origin: * ");
      $this->getResponse()->setContentType('application/json');

/*
      $image= sfConfig::get('sf_images_dir').'/public/banner/museo.jpg';         
      ob_start();
      imagepng($image);
      $imageData = ob_get_contents();
      ob_clean(); 
      */
      //$imageData = imagejpeg($image, '', 100); 
    /*ob_start();
    $imagen= sfConfig::get('sf_images_dir').'/public/banner/museo.jpg';
    $imagedata = file_get_contents($imagen);

    $codigo_ima=   base64_encode( $imagedata );
    ob_clean(); */
$material     = sfConfig::get('sf_web_dir').'/images/public/banner/patrimonio-material.jpeg';
$c_material   = $this->getImagenCode($material);
$l_material   = 'http://patrimonio.chaco.gov.ar/images/public/banner/patrimonio-material.jpeg';

$inmaterial     = sfConfig::get('sf_web_dir').'/images/public/banner/patrimonio-inmaterial.jpg';
$c_inmaterial = $this->getImagenCode($inmaterial);
$l_inmaterial   = 'http://patrimonio.chaco.gov.ar/images/public/banner/patrimonio-inmaterial.jpg';

$museos     = sfConfig::get('sf_web_dir').'/images/public/banner/museos.jpg';
$c_museos     = $this->getImagenCode($museos);
$l_material   = 'http://patrimonio.chaco.gov.ar/images/public/banner/museos.jpg';

$objetos     = sfConfig::get('sf_web_dir').'/images/public/banner/objetos.jpg';
$c_objetos   = $this->getImagenCode($objetos);
$l_material   = 'http://patrimonio.chaco.gov.ar/images/public/banner/objetos.jpg';


      $arreglo= array(

        array('imagen'   => $c_material,
              'absoluta' => $material,
              'relativa' => $l_material,
              'enlace'   => '/patrimonio-material'          
      ),
        array('imagen' => $c_inmaterial,
              'absoluta' => $inmaterial,
              'relativa' => $l_inmaterial,
          'enlace'=>'/patrimonio-inmaterial'),
        array('imagen' => $c_objetos,
              'absoluta' => $objetos,
              'relativa' => $l_objetos,
          'enlace'=>'/objetos'),
        array('imagen' => $c_museos,
              'absoluta' => $museos,
              'relativa' => $l_museos,
          'enlace'=>'/museos'));

      return $this->renderText(json_encode($arreglo));
  }

protected function getImagenCode($path){

    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = base64_encode($data);
    return $base64;
}


  public function executeGetCarpetas($request)
  {
      header("Access-Control-Allow-Origin: * ");
      $this->getResponse()->setContentType('application/json');

      $parameter = sfConfig::get('app_institucion_data', array(''=>'') );      

      return $this->renderText(json_encode($parameter));
  }

  /*pasajes por el ajax
    envia un solo museo seleccionando
  */
  public function executeGetMuseo($request)
  {
      header("Access-Control-Allow-Origin: * ");
      $this->getResponse()->setContentType('application/json');

      $objetos = Doctrine::getTable('Institution')->retrieveForSelectByCarpeta(
                  $request->getParameter('carpeta') );
      

       return $this->renderText(json_encode($objetos));
  }


  public function executeDocumento($request){
    $carpeta=$request->getParameter('carpeta');
    $archivo=$request->getParameter('archivos');

    $obj = Doctrine_Core::getTable('Institution')->findOneBy('file', $carpeta); 

    $material = sfConfig::get('sf_web_dir').'/uploads/'.$carpeta.'/fotos/'.$archivo;
    header("Content-disposition: attachment; filename=Patrimonio Chaco: ".$obj->getName());
    header("Content-type: application/pdf");
    readfile($material);

    //Load file content
    $this->setLayout('layout_acervo');
 }


}
