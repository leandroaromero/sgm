<?php

/**
 * home actions.
 *
 * @package    museo
 * @subpackage home
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class homeActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
   $this->getUser()->setAttribute('pagina_actual', 'Home');


  }
  public function executeChangeLanguage(sfWebRequest $request)
   {
         $form = new sfFormLanguage( $this->getUser(), array('languages' => array('en', 'es')) );
         $form->process($request);
         $this->getUser()->aumentarContador('languages');
         return $this->redirect('homepage');
}

}
