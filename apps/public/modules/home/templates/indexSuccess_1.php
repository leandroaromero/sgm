<script type="text/javascript">
$(document).ready(function () {
    $('#portfolioslider').coinslider({
        width: 480,
        height: 280,
        navigation: false,
        links: false,
        hoverPause: false
    });
    $("#tabcontainer").tabs({
        event: "click"
    });
});
</script>

<!-- ####################################################################################################### -->
<div class="wrapper col1">
  <div id="featured_slide">
    <!-- ####################################################################################################### -->
    <div id="slider">
      <ul id="categories">

      <?php foreach($objs as $obj ): ?>
        <li class="category">
          <h2><?php echo $obj->getNombreObjeto()?></h2>
           <a href="<?php echo url_for('object/index?id='.$obj->getId())?>">
              <?php
                    if($foto = $obj->getFoto()){
                         echo image_tag('../uploads/fotos/'.$foto['archivo']);
                      }else{
                                      echo image_tag('images/demo/150x110.gif');
                     }
                 ?>
          </a>
          <p><?php echo $obj->getDescripcionGenerica()?></p>
          <p class="readmore"><a href="<?php echo url_for('object/index?id='.$obj->getId())?>"><?php echo __('Read more')?> &raquo;</a></p>
        </li>
       <?php endforeach ?>
      </ul>
      <a class="prev disabled"></a> <a class="next disabled"></a>
      <div style="clear:both"></div>
    </div>
    <!-- ####################################################################################################### -->
  </div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper col2">
  <div id="container" class="clear">
    <!-- ####################################################################################################### -->
    <div id="tabcontainer">
      <ul id="tabnav">
        <li><a href="#tabs-1"><?php echo __('Our Museums')?></a></li>
        <li><a href="#tabs-2"><?php echo __('The law')?></a></li>
        <li><a href="#tabs-3"><?php echo __('News')?></a></li>
        <li><a href="#tabs-4"><?php echo __('Reasons')?></a></li>
        <li><a href="#tabs-5"><?php echo __('Consensus')?></a></li>
      </ul>
      <div id="tabs-1" class="tabcontainer">
        <div id="hpage_portfolio" class="clear">
          <div class="fl_left">
            <div id="portfolioslider">
              <ul>
                <li><?php echo image_tag('museos/florentino/imagen.gif')?></li> 
                <li><?php echo image_tag('museos/hombre_chaquenio/imagen.gif')?></li> 
                <li><?php echo image_tag('images/demo/portfolioslider/1.gif')?></li>
                <li><?php echo image_tag('images/demo/portfolioslider/2.gif')?></li>
                <li><?php echo image_tag('images/demo/portfolioslider/3.gif')?></li>
                <li><?php echo image_tag('images/demo/portfolioslider/4.gif')?></li>
                <li><?php echo image_tag('images/demo/portfolioslider/5.gif')?></li>
                
              </ul>
            </div>
          </div>
          <div class="fl_right">
            <h2><?php echo __('Our Museums')?></h2>
            <p><?php echo __('The museums that are reported here depends on the Chaco culture Institute')?></p>
            <ul>
              <li><?php echo __('Museum of')?> Bellas Artes "René Bruseau".</li>
              <li><?php echo __('Museum of')?> Ciencias Naturales "Augusto Schulz".</li>
              <li><?php echo __('Museum of')?> Medios de Comunicación "Raúl D. Berneri".</li>
              <li><?php echo __('Museum of')?> Hombre Chaqueño “Ertivio Acosta.</li>
              <li><?php echo __('Museum')?> Florentino Ameghino.</li>
              <li><?php echo __('Museum')?> Icholay.</li>
              <li><?php echo __('Museum')?> Jardín Botánico “Casa Augusto Schulz".</li>
              <li><?php echo __('Historic place, Home and Museum "Luis Geraldi"')?>.</li>
              <li><?php echo __('Regional Historic Museum of')?> Isla del Cerrito.</li>
            </ul>
            <p></p>
            <p class="readmore"><a href="<?php echo url_for('@museos')?>"> <?php echo __('Read more')?> &raquo;</a></p>
          </div>
        </div>
      </div>
      <!-- ########### -->


      <div id="tabs-2" class="tabcontainer">
         <h2>LEY DE CULTURA</h2>
        
         <p>23 noviembre 2010</p>
         <p>La sanción de la ley nº 6255 permite la creación del instituto de cultura “como ente autárquico y descentralizado “y la incorporación a su presupuesto, como mínimo, del 1 por ciento del presupuesto general anual de la provincia.</p>
         <p>La Cámara de Diputados sancionó por unanimidad el pasado 19 de noviembre la primera Ley Provincial de Cultura del Chaco, que establece la creación de un Instituto de Cultura e incorpora a su presupuesto –destinado para el diseño estratégico y el desarrollo de políticas culturales de Estado –, tal como lo recomienda la UNESCO, el 1 porciento del Presupuesto General Anual de la Provincia. Se trata de la primera legislación cultural del país que no sólo incorpora como ley el 1 por ciento aconsejado por la UNESCO: también instituye que la cultura es un derecho social inalienable y el Estado es su principal garante. Reza la ley N° 6255 en su artículo 1° al definir la Cultura como “un conocimiento y una creación colectiva que producen los sujetos sociales para comprender su realidad, intervenir y transformarla. Es un derecho social inalienable y por lo tanto el Estado en su garante indelegable”.</p>
         <p>Pero el concepto no sólo abarca al acervo competente a las bellas artes tradicionales. La ley N° 6255 también señala que el hecho cultural está conformado por “nuestros modos de vivir cotidianos, de percibir el mundo, de indagar y replantear las relaciones humanas, tanto sociales como económicas y políticas en la búsqueda de la construcción colectiva de nuestra identidad y en el marco del pleno respeto pos nuestras diversidades. En su sentido más amplio, la cultura se considera como el conjunto de rasgos distintivos, espirituales y materiales, intelectuales y afectivos que caracterizan a una sociedad o a un grupo social, comprendiendo los derechos fundamentales del ser humano, los sistemas de valores, las tradiciones y las creencias, las letras y las artes”.</p>
         <p>Los chaqueños tienen así derecho a “crear, producir y definir la obra cultural y colectiva, en libertad y sin censura; desarrollar la identidad cultural en el marco de la diversidad e interculturalidad, característica de la provincia, respetando el origen étnico y cultural del grupo al que pertenece; preservar la lengua, identidad étnica y cultural, cosmovisión, valores y espiritualidad de los pueblos originarios, en el marco de la indispensable reparación histórica, en concordancia con el artículo 37 de la Constitución Provincial 1957 – 1994”.</p>
         <p>La norma establece así que el Estado debe promover las actividades culturales sustentadas en los principios que comprenden a “la cultura como política de Estado”, “la cultura como contribución a la construcción de la paz y la consolidación de los valores democráticos”, “la igualdad y equidad en el acceso a los diversos bienes y valores culturales, el derecho a la cultura integra los derechos humanos, que son universales, indisociables e interdependientes”, “la protección de una identidad pluralista y multiétnica, desde las concepciones de diversidad e interculturalidad”.</p>
         <p>El Instituto de Cultura del Chaco, con domicilio en Resistencia, podrá establecer delegaciones en distintas regiones de la provincia, y su función será la de asistir al poder Ejecutivo en el diseño, ejecución y supervisión de las políticas culturales de Estado y garantizar a los habitantes la igualdad en el acceso a los diversos bienes y valores culturales. Su administración estará a cargo de un directorio compuesto por cinco miembros, quienes se desempeñarán como presidente, vicepresidente, y tres vocales, uno de ellos será representante de los pueblos originarios.</p>
         <p>El artículo N° 20 crea el Fondo Provincial de Cultura “que se constituirá con el equivalente al 1 por ciento como mínimo, del presupuesto general anual de la Provincia del Chaco”. Y establece que dicho fondo “se distribuirá en un 84,5 por ciento para el Instituto de Cultura y un 15,5 para los municipios, que deberán aplicar estos fondos a programas culturales aprobados previamente por el Instituto”. De acuerdo a la Ley, vencido cada ejercicio presupuestario las partidas no ejecutadas se incorporarán a las partidas presupuestarias del nuevo ejercicio en las proporciones previamente establecidas.</p>
         <p>Cabe informar que el Instituto de Cultura administrará los recursos destinados al financiamiento de las políticas culturales que se integrarán a través del Fondo Provincial de Cultura, los fondos que por ley de presupuesto se le asigne, los que provengan de actividades propias del Instituto, los afectados por normas y convenios específicos, los provenientes de subsidios, legados y donaciones, y todo otro ingreso no previsto que se derive de la aplicación de la Ley. Y el texto deja claro que “los recursos que se destinen para el financiamiento de las políticas culturales tendrán carácter de inembargable en el marco de la calidad de derecho social inalienable que declara esta ley”.</p>
     </div>
     <div id="tabs-3" class="tabcontainer">
         <h2>CHACO, ENTRE LAS PRIMERAS PROVINCIAS CON MEJOR INVERSIÓN CULTURAL DEL PAÍS</h2>
         <p>Según datos aportados por el Sistema de Información Cultural de la Argentina (Sinca) hasta el 2007, después de San Luis, la Ciudad Autónoma de Buenos Aires y Tierra del Fuego, Chaco se convertirá a partir del 2009 en la provincia con más financiamiento anual cultural per cápita.</p>
         <p>La sanción por unanimidad de Diputados de la Ley Provincial de Cultura colocará al Chaco, a partir del 2009, entre las primeras provincias con más y mejor financiamiento para el desarrollo estratégico de políticas culturales de Estado. La creación del Instituto de Cultura destinará a su presupuesto el 1 porciento del Presupuesto General Anual de la Provincia, tal como lo recomienda la UNESCO. Después de San Luis, la Ciudad Autónoma de Buenos Aires y Tierra del Fuego, Chaco se convertirá en la provincia con mayor inversión en cultura por habitante por año.</p>
         <p>Según los datos aportados por el Sistema de Información Cultural de la Argentina (Sinca) de la Secretaría de Cultura de la Nación, sobre la base de datos aportados por las áreas culturales de las provincias, San Luis destinó por habitante en cultura el año pasado $119,40; la ciudad Autónoma de Buenos Aires, $118,22, y Tierra del Fuego $64,80. Sin embargo, Chaco invirtió en cultura en el 2007 sólo $7,39 per cápita, y en períodos anteriores las cifras fueron incluso mucho menores: $3,87 en el 2003, $3,07 en el 2005 y $1,80 en el 2006.</p>
         <p>Con la aprobación de la Ley de Cultura, se creará el Instituto de Cultura como ente autárquico del Estado provincial y destinará como mínimo para su presupuesto el 1 porciento del Presupuesto General Anual de la Provincia. El Ejecutivo provincial prevé para su Presupuesto 2009 ingresos por $5.115 millones. El 1 porciento de esa cantidad garantizaría para cultura $51.550.000 anuales, y significaría una inversión en cultura por habitante de $52,36.</p>
         <p>La Provincia del Chaco se convertirá así, a partir del 2009, en la cuarta provincia con más y mejor financiamiento para el desarrollo de políticas culturales de Estado y la primera con una Ley Provincial de Cultura en todo el territorio nacional que aplica la recomendación de la UNESCO.</p>
     </div>
     <div id="tabs-4" class="tabcontainer">
         <h2>CÓMO NACIÓ LA LEY DE CULTURA</h2>
         <p>La cultura no es un gasto, es una inversión estratégica para el desarrollo y crecimiento de los pueblos; la cultura es un derecho social inalienable y el Estado es su garante indelegable. Éstos son los conceptos fundamentales acordados por más de 600 referentes culturales provenientes de 42 rincones del Chaco, protagonistas del Congreso Provincial de Cultura “Hacia una Ley Provincial de Cultura”, realizado entre el 29 y 30 de agosto pasados, en el Domo del Centenario “Zitto Segovia”.</p>
         <p>Organizado por el Corto Toba Chelalaapí, la Fundación Chaco Artesanal y la Organización Napalpí, el Primer Congreso de los Pueblos Indígenas del Chaco, que se realizó el 31 de octubre y el 1 de noviembre en Puerto Tirol, enriqueció y enalteció el anteproyecto de ley, y en palabras del escritor qom Juan Chico, representó “la primera vez que los pueblos originarios fueron consultados para tratar una ley”.</p>
         <p>Previamente, a través tanto de las asambleas del Consejo Provincial de Cultura puestas en marcha en San Martín, Villa Ángela, Resistencia, Tres Isletas, Santa Sylvina y Puerto Tirol, como de 53 audiencias públicas desarrolladas en todo el territorio provincial, se decidió impulsar la sanción de una Ley Provincial de Cultura.</p>
         <p>Durante tal proceso, surgió la necesidad de deliberar y llegar al consenso de un anteproyecto de ley, que plantea en sus aspectos más relevantes la aplicación de la noción de cultura, como conocimiento y creación colectiva que producen los sujetos para comprender su realidad e intervenir y transformarla, con el propósito de la justa redistribución de los bienes culturales, para descentralizar y democratizar sus recursos.</p>
         <p>Sin embargo, el proceso de gestación de la Ley de Cultura comenzó con los foros Pensar la Cultura, que se realizaron entre 2005 y 2007, e incluso con una serie de normativas decisivas, entre las que se pueden mencionar: la ley del Premio Consagración al Mérito Artístico, la Ley de Mecenazgo, la ley de creación del Fondo Editorial –aún sin reglamentar– y la ley de Museos de la Provincia del Chaco.</p>
     </div>
     <div id="tabs-5" class="tabcontainer"> 
         <h2>EL CONSENSO EN LA CÁMARA DE DIPUTADOS</h2>
         <p>La Cámara de Diputados aprobó la Ley de Cultura N° 6255 alrededor de las 23 del miércoles 19 de noviembre. Los 31 legisladores presentes ese día votaron por unanimidad a favor de la sanción de la norma, y más allá de las diferencias políticas e ideológicas propias de todo orden democrático, supieron encontrar en la cultura un espacio de consenso y enriquecimiento. “Es el mejor homenaje que le podemos hacer al pueblo del Chaco a 25 años del retorno de la Democracia”, señaló días antes de la sanción de la ley el subsecretario de Cultura, Francisco Tete Romero.</p>
         <p>Esa tarde en la Legislatura, el tratamiento de la ley N°6255 se inició a las 21.30 y los fundamentos para su aprobación fueron vertidos en el recinto por los legisladores María Dolores Cristófani, María Lidia Cáceres, Daniel San Cristóbal, Carlos Martínez, Omar Raffín, Alicia Terada, Ricardo Sánchez, Hugo Maldonado, Juan Chaquires, Martín Nievas, Inicencia Charole y Luis Verdún.</p>
         <p>Cristófani, ex subsecretario de Cultura, inició las exposiciones de los fundamentos, y recalcó que “los hacedores de la cultura, los habitantes y ciudadanos de esta Provincia son los mismos, aunque cambien los gobiernos. Ellos vinieron construyendo las bases para que hoy podamos tener una Ley de Cultura –que es la que nuestro pueblo se merece, la que nos merecemos todos–, creando el Instituto de Cultura con un presupuesto propio –estamos hablando de un piso del uno por ciento, que es lo que recomienda la UNESCO–; respetando también la carrera, creando el escalafón específico y el concurso para todos los empleados y asegurando la participación de toda la ciudadanía a través del Consejo Provincial de Cultura”.</p>
         <p>Daniel San Cristóbal resumió por su parte el valioso aporte que hicieron los pueblos originarios para construir la ley: “Quiero destacar lo que ellos mismos han dicho: es la primera vez que a la hora de tratar una ley se consulta a los pueblos indígenas; hay un cambio, no piensan ni deciden por nosotros, dijo Juan Chico con relación al proyecto. Y en varias partes de la ley se recoge la cuestión aborigen, que es nuestra cultura, la que durante muchos años nosotros, los blancos, ocultamos y no permitimos su desarrollo”.</p>
         <p>Por eso, “quiero resaltar la importancia de la creación del Instituto de Cultura; ascenderla al rango de ministerio, con un grado de autarquía financiera de importancia, incorporando en su conducción a los pueblos originarios y, de acuerdo con el último texto acordado, también a representantes de los sectores de la Cultura, en el directorio y, de alguna manera, el poder Ejecutivo da mayor poder a los sectores de la cultura en la administración de su patrimonio; es una decisión muy importante”, señaló el legislador.</p>
         <p>A su turno, el diputado Raffín agradeció “la inquietud, la perseverancia, la tenacidad de la gente de la cultura, del subsecretario, de la legisladora Marylin Cristófani, quien tendió los puentes de plata necesarios para que este diálogo y este consenso sean realidad”. Y reflexionó: “Sin el decidido apoyo de los hacedores culturales, convencimiento y transmisión de lo que significa la Cultura para nuestro pueblo y la necesidad de ella, posiblemente hoy no estaríamos sancionando esta ley”.</p>
         <p>El diputado Maldonado citó un poema de El libro de los abrazos, de Eduardo Galeano, “Los nadies”: “Este poema–dijo el legislador– hace a la síntesis de nuestras aspiraciones, hace a la síntesis de que se haga real la sanción de la ley como un instrumento libertario y democrático de emancipación. Dice el poema de Galeano: Sueñan las pulgas con comprarse un perro y sueñan los nadies con salir de pobres, que algún mágico día llueva de pronto la buena suerte, que llueva a cántaros la buena suerte; pero la buena suerte no llueve ayer, ni hoy, ni mañana, ni nunca, ni en lloviznita cae del cielo la buena suerte, por mucho que los nadies la llamen y aunque les pique la mano izquierda, o se levanten con el pie derecho, o empiecen el año cambiando de escoba./ Los nadies: los hijos de nadie, los dueños de nada./ Los nadies: los ningunos, los ninguneados, corriendo la liebre, muriendo la vida, jodidos, rejodidos: Que no son, aunque sean./ Que no hablan idiomas, sino dialectos./ Que no profesan religiones, sino supersticiones./ Que no hacen el arte, sino artesanía./ Que no practican cultura, sino folclore./ Que no son seres humanos, sino recursos humanos./ Que no tienen cara, sino brazos./ Que no tienen nombre, sino número./ Que no figuran en la historia universal, sino en la crónica roja de la prensa local./ Los nadies, que cuestan menos que la bala que los mata.</p>
         <p>”Tiene sentido llegar al consenso y ofrecer la mayoría que tiene este sector cuando se justifican los fines. Vale la pena hablar del consenso y vale la pena escuchar, cuando usted diga que vamos a votar y se convierta en legislación la Ley Provincial de Cultura”, concluyó Hugo Maldonado.</p>

        <p>Constitución Provincial 1957 – 1994. Artículo 37: La Provincia reconoce la preexistencia de los pueblos indígenas, su identidad étnica y cultural; la personaría jurídica de sus comunidades y organizaciones; y promueve su protagonismo a través de sus propias instituciones; propiedad comunitaria inmediata de la tierra que tradicionalmente ocupan y las otorgadas en reserva. Dispondrá la entrega de otras aptas y suficientes para su desarrollo humano, que serán adjudicadas como reparación histórica, en forma gratuita exentas de todo gravamen. Serán inembargables, imprescriptibles indivisibles e intransferibles a terceros. El Estado les asegurará: a) La educación bilingüe e intercultural, b) La participación en la protección, preservación, recuperación de los recursos naturales y de los demás intereses que los afecten y en el desarrollo sustentable, c) Su elevación socio-económica con planes adecuados, d) La creación de un registro especial de comunidades y organizaciones indígenas. </p>
        
      </div>
      <!-- ########### -->
    </div>
    <!-- ####################################################################################################### -->
  </div>
</div>
