<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<h4>Patrimonio Histórico Cultural y Natural de la Provincia del Chaco</h4>
<p>LEY 5556
RESISTENCIA, 18 de Mayo de 2005<br>
Boletín Oficial, 17 de Junio de 2005<br>
Vigente, de alcance general<br>
Id Infojus: LPH0005556</p>
<h6>Sumario</h6>
<p><i>Cultura y educación, patrimonio cultural, lugares históricos, sitios arqueológicos, Recursos naturales<br>
La Cámara de Diputados de la Provincia del Chaco Sanciona con Fuerza de Ley:</i></p>
<div id="motivos" style="display: none">
                                                                         
        <h8>OBJETO</h8>
        <p  align="justify" >Art. 1: Establécese el marco legal para la investigación, preservación, salvaguarda, protección, restauración, promoción,
        acrecentamiento y transmisión a las generaciones futuras del Patrimonio Histórico Cultural y Natural de la Provincia.
        Toda normativa que se dicte en el ámbito provincial referida a esta materia, deberá ajustarse a esta ley.</p>
        <h8>CONCEPTO</h8>
        <p align="justify" >Art. 2: Considéranse, a los efectos de la presente ley, como Patrimonio Histórico Cultural y Natural de la Provincia, al
        conjunto de bienes muebles e inmuebles, tangibles e intangibles, materiales y simbólicos, que por su significación
        intrínseca o convencionalmente atribuida, definen la identidad y memoria colectiva de sus habitantes.</p>
        <h8>CARACTER</h8>
        <p align="justify" >Art. 3: Defínese como bienes que integran el Patrimonio Histórico Cultural y Natural, a aquellos que se constituyen en
        únicos, irremplazables e insustituibles por su valor testimonial o de esencial importancia para la ciencia, historia,
        arqueología, arte, antropología, paleontología, etnografía, lingüística, arquitectura, urbanismo, paisajística, tecnología y el
        denominado patrimonio cultural viviente.</p>
        <h8>CATEGORIAS</h8>
        <p align="justify" >Art. 4: Son susceptibles de integrar el Patrimonio Histórico, Cultural y Natural de la Provincia y merecedores de
        protección por la presente ley, los que a continuación se detallan:
        </p><p align="justify" >a) SITIOS O LUGARES HISTORICOS: Vinculados con acontecimientos del pasado, de destacado valor histórico,
        antropológico, arquitectónico, urbanístico o social.
        </p><p align="justify" >b) MONUMENTOS: Que constituyan obras singulares de índole arquitectónica, ingeniería, pictórica, escultórica u otras,
        que sobresalgan por su valor arquitectónico, técnico, histórico, social o artístico, vinculado a un entorno o marco
        referencial que concurra a su protección.
        </p><p align="justify" >b) CONJUNTO O GRUPO DE CONSTRUCCIONES: Areas que por su arquitectura, unidad o integración con el paisaje,
        tengan valor especial desde el punto de vista arquitectónico, urbano o tecnológico. Dentro de esta categoría serán
        considerados como especiales al casco histórico así como a centros, barrios o sectores históricos que conforman una
        unidad de alto valor social y cultural, entendiendo por tales a aquellos asentamientos fuertemente condicionados por una
        estructura física de interés como exponente de una comunidad.
        </p><p align="justify" > c) JARDINES HISTORICOS: Que sean productos de la ordenación humana de elementos naturales, caracterizados por
        sus valores estéticos, paisajísticos y botánicos y que ilustren la evolución y el asentamiento humano en el curso de lahistoria.
        </p><p align="justify" > e) ESPACIOS PUBLICOS: Constituidos por plazas, plazoletas, bulevares, costaneras, calles, caminos, senderos u otro,
        cuyo valor radica en función del grado de calidad ambiental, homogeneidad típica espacial, así como de la presencia en
        cantidad y calidad de edificios de valor histórico y de las condiciones espaciales y funcionales ofrecidas para el uso social
        pleno.
        </p><p align="justify" > f) ZONAS ARQUEOLOGICAS: Constituidas por sitios o enclaves claramente definidos, en los que se compruebe la
        existencia real o potencial de restos y testimonios de interés relevante.
        </p><p align="justify" > g) BIENES ARQUEOLOGICOS DE INTERES RELEVANTE: Extraídos o no, tanto de la superficie terrestre o del
        subsuelo, como de medios subacuáticos.
        </p><p align="justify" >h) COLECCIONES Y OBJETOS: Existentes en museos, bibliotecas y archivos así como otros bienes de destacado valor
        histórico, artístico, antropológico, científico, técnico o social.
        </p><p> i) FONDOS DOCUMENTALES: En cualquier tipo de soporte.
        </p><p align="justify" > j) EXPRESIONES Y MANIFESTACIONES INTANGIBLES: De la cultura ciudadana, que estén conformadas por las
        tradiciones, las costumbres y los hábitos de la comunidad, así como espacios o formas de expresión de la cultura popular
        y tradicional de valor histórico, artístico, antropológico o lingüístico, vigentes o en riesgo de desaparición.
        </p><p align="justify" > k) PATRIMONIO CULTURAL VIVIENTE: Serán aquellas personas o grupos sociales que por su aporte a las tradiciones,
        en las diversas manifestaciones de la cultura popular, ameriten ser consideradas como integrantes del patrimonio
        chaqueño, estableciendo mecanismos para promover la transmisión de las destrezas, habilidades y técnicas
        tradicionales de artistas, artesanos y toda otra persona que sobresalga en la sociedad, antes de su desaparición por
        causa de abandono o falta de reconocimiento del saber que ésta porta.
        </p><p align="justify" >Art. 5: La enumeración precedente es enunciativa, siendo susceptible de integrar otros bienes merecedores de la
        protección de esta ley.
        </p><h8>AUTORIDAD DE APLICACION</h8><p>
        Art. 6: La autoridad de aplicación de la presente ley y de las normas que en su consecuencia se dicten, será el Instituto
        de Cultura del Chaco.
        </p><p>arte_10,[Modificaciones]</p>
        <h8>COMISION PROVINCIAL DE PATRIMONIO HISTORICO CULTURAL Y NATURAL</h8>
        <p align="justify" >Art. 7: Créase la Comisión Provincial de Patrimonio Histórico Cultural y Natural, la que estará integrada por un Comité
        Ejecutivo de cinco miembros, que serán los siguientes: el titular del Instituto de Cultura de la Provincia del Chaco quien
        además presidirá dicho Comité, representantes de la Universidad Nacional del Nordeste, Universidad Nacional del Chaco
        Austral, Facultad Regional Resistencia de la Universidad Tecnológica Nacional, y la Comisión Nacional de Museos,
        Monumentos y Lugares Históricos. Además la integrarán representantes de entidades culturales y científicas, tanto
        oficiales como privadas de la Provincia, e invitados especiales con ingerencia sobre el patrimonio que al efecto ella
        misma convoque, procurando la mayor participación y representatividad de las entidades involucradas y de la
        comunidad, con mandato de un año de la totalidad de sus integrantes y "ad honorem", pudiendo ser reelectos. Dicha
        Comisión dictará su propio reglamento de funcionamiento. El Comité Ejecutivo, reunido con mayoría de sus miembros,
        podrá tomar decisiones "ad referéndum" de la Comisión Provincial de Patrimonio Histórico Cultural y Natural, cuando
        medidas de urgencia a criterio de la Presidencia lo requieran. La Presidencia del Instituto podrá nombrar aquellos
        colaboradores que considere necesarios para la gestión, como coordinador, secretario, asesor jurídico y otros.
        </p><p>parte_12,[Modificaciones]
        </p><p align="justify" >Art. 8: En la convocatoria de la Comisión Provincial de Patrimonio Histórico Cultural y Natural, para el tratamiento de
        cuestiones de interés de un municipio, el Intendente de la localidad participará "per sé" o a través de un delegado
        designado al efecto y en calidad de miembro titular, como representante de la misma, hasta culminar con las cuestiones
        inherentes a su localidad.
        parte_13,[Modificaciones]
        </p><p align="justify" >Art. 9: Los miembros de la Comisión Provincial de Patrimonio Histórico Cultural y Natural, desempeñarán sus funciones
        en los espacios físicos del Instituto de Cultura del Chaco.
        </p><p>parte_14,[Modificaciones]
        </p><p align="justify" >Art. 10: Los dictámenes de la Comisión Provincial de Patrimonio Histórico Cultural y Natural, tendrán carácter de
        vinculantes cuando se realice una asamblea en la que deberán estar presentes la mayoría de los miembros del Comité
        Ejecutivo, y no menos de cinco representantes de entidades culturales y científicas, según lo enunciado en el artículo 7
        de la presente, quienes deberán confeccionar y firmar el acta correspondiente.
        </p><p>parte_15,[Modificaciones]
        </p><p align="justify" >Art. 11: La Comisión Provincial de Patrimonio Histórico Cultural y Natural deberá convocar expertos cuando entre sus
        miembros no haya especialistas sobre el tema puntual sometido a su consideración.
        </p><h8>FUNCIONES DE LA COMISION</h8>
        <p>Art. 12: Son funciones de la Comisión de Patrimonio:
        </p><p align="justify" > a) Velar por la protección del conjunto de los bienes que constituyen el Patrimonio Histórico Cultural y Natural de la
        Provincia.
        </p><p align="justify" > b) Declarar los bienes afectados a la protección de esta ley, como también su eventual desafectación.
        </p><p align="justify" > c) Programar e implementar proyectos dirigidos a la tutela y protección del Patrimonio Provincial; como también planificar
        estrategias y mecanismos de estímulo para la conservación, restauración y puesta en valor de bienes patrimoniales.
        </p><p align="justify" > d) Dirigirse en forma directa e inmediata a cualquier autoridad u organismo internacional, nacional, provincial, municipal,
        privado o persona física en cometido del cumplimiento de la presente ley y suscribir convenios con cualquier persona
        física o jurídica, aún de derecho público.
        </p><p> e) Realizar constataciones permanentes para el cumplimiento de la presente ley.
        </p><p> f) Aceptar donaciones o legados.
        </p><p> g) Afectar con destino a expropiación.
        </p><p> h) Confeccionar el Registro Provincial de Bienes del Patrimonio Histórico Cultural y Natural del Chaco, describiendo el
        origen de los mismos.
        </p><p> i) Realizar el inventario de todos los bienes que integran el Patrimonio Histórico Cultural y Natural de la Provincia,
        mantenerlo actualizado y promover su resguardo y conocimiento en la sociedad y en los distintos niveles educativos, a
        través de los contenidos curriculares del sistema educativo formal, y no formal coordinando esas tareas con los
        establecimientos educativos, museos provinciales, archivos y bibliotecas.
        </p><p> j) Fomentar el acceso, uso y goce democrático de los bienes de Patrimonio Cultural por parte de la población.
        </p><p> k) Elevar a la autoridad de aplicación los dictámenes producidos para la incorporación de bienes al Registro Provincial de
        Bienes del Patrimonio Histórico Cultural y Natural.
        </p><p> l) Elevar a la autoridad de aplicación y ésta a su vez, al Ministerio de Educación, Cultura, Ciencia y Tecnología, una
        Memoria Anual de lo actuado.
        </p><p> m) Promover el turismo cultural.
        </p><p align="justify" > n) Constituirse en órgano de asesoramiento y consulta permanente de los organismos públicos dentro del área de su
        competencia.
        </p><p align="justify" > ñ) Intervenir en la protección de bienes culturales y naturales que aún no hayan sido declarados como pertenecientes al
        patrimonio histórico cultural y natural de la Provincia que sean objeto de ocultamiento, destrucción, modificación,
        alteración, abandono o cualquier otra causa de origen natural o antrópica que restrinja en todo o en parte su
        conservación como patrimonio histórico de carácter potencial, a criterio de la autoridad de aplicación.
        A estos fines la Comisión podrá realizar las gestiones pertinentes con cualquier persona física o jurídica de derecho
        público o privado a efectos de limitar, impedir y/o fiscalizar la realización de cualquier otro acto que revista peligrosidad
        para el bien que se pretenda salvaguardar.
        </p><p> parte_18,[Modificaciones]
        </p> <h8>DE LOS BIENES QUE INTEGRAN EL PATRIMONIO CULTURAL DE LAS RESTRICCIONES IMPUESTAS PARA SU
        CONSERVACION</h8>
        <p align="justify" >Art. 13: Créase el Registro Provincial de Patrimonio Histórico Cultural y Natural que dependerá de la autoridad de
        aplicación, cuya organización será establecida por la reglamentación de la presente ley y en el que se inscribirán los
        bienes declarados como pertenecientes al patrimonio cultural provincial. Toda información contenida en el Registro, será
        pública y la autoridad de aplicación deberá garantizar el acceso de la misma a la comunidad en general.
        </p><p align="justify" > Art. 14: Créase el inventario del Patrimonio Histórico Cultural y Natural de la Provincia, en el que se recopilará y
        sistematizará la información sobre los bienes culturales existentes en cualquier tipo de fuente y que pertenezcan tanto al
        sector público como al de los particulares. Dicha información será sistematizada a través de una base de datos que
        opere mediante un sistema en red. Su objetivo fundamental será el conocimiento, la difusión y el goce de los bienes
        susceptibles de ser declarados como pertenecientes al patrimonio cultural chaqueño.
        </p><p align="justify" > Art. 14 BIS: El Poder Ejecutivo a través de la autoridad de aplicación de la presente, instrumentará los mecanismos
        pertinentes para la exteriorización e identificación, de manera ostensible para la comunidad, de los bienes que hayan
        sido declarados como Patrimonio Histórico, Cultural y Natural en el marco de esta ley.
        </p><p> parte_22,[Modificaciones]
        </p><p align="justify" > Art. 15: La autoridad de aplicación determinará, previo dictamen de la Comisión Provincial de Patrimonio Histórico
        Cultural y Natural, los bienes o nómina de bienes que integran el Patrimonio Histórico Cultural y Natural de la Provincia,
        con comunicación a sus titulares registrados, poseedores de dominio o tenedores a cualquier título.
        </p><p align="justify" > Art. 16: Los bienes que se declaren o que se consideren declarados en virtud de lo dispuesto en el inciso b) del artículo
        12 de la presente ley, no podrán ser enajenados, transferidos, modificados o destruidos en todo o en parte, sin la previa
        intervención de la Comisión Provincial de Patrimonio Histórico Cultural y Natural, sin perjuicio de las facultades que en
        casos específicos, le competa a la Comisión Nacional de Museos, Monumentos y Lugares Históricos.
        </p><p align="justify" > Art. 17: Establécense, como restricciones al dominio privado en virtud del interés público, respecto de los bienes
        precitados, las siguientes:
        </p><p> a) De custodia y conservación.
        </p><p> b) De solicitud de autorización, previo a su traslado fuera de la Provincia.
        </p><p> c) De comunicación sobre su venta o cambio de domicilio de permanencia.
        </p><p> d) De notificación inmediata sobre su deterioro o destrucción;
        </p><p> e) De puesta a disposición de la autoridad de aplicación cuando ésta lo determine en función del interés público.
        </p><p> f) De utilización de manera que no ponga en riesgo los valores que aconsejan su conservación.
        </p><p> g) De notificación de cualquier modificación en los usos de los bienes protegidos.
        </p><p align="justify" > Art. 18: La autoridad de aplicación, previo dictamen de la Comisión Provincial de Patrimonio Histórico Cultural y Natural,
        podrá propiciar la adquisición de los bienes susceptibles de satisfacer las finalidades que establece el artículo 2° de esta
        ley. Dicha adquisición se realizará bajo el siguiente procedimiento:
        </p><p align="justify" > a) Instrumento legal que fundamente la necesidad de adquisición, con descripción de las características e identificación
        del o de los bienes a adquirir.
        </p><p align="justify" > b) Valuación realizada por la Junta de Evaluaciones del Ministerio de Economía, Obras y Servicios Públicos con la
        asistencia de un representante designado por la Comisión Provincial de Patrimonio Histórico, Cultural y Natural, un
        representante de la autoridad de aplicación, un representante del Ministerio de Economía, Obras y Servicios Públicos y
        un especialista acorde a la naturaleza del bien.
        </p><p> c) Lugar de destino del o de los bienes.
        </p><p align="justify" > d) Consentimiento de los titulares registrados o poseedores del dominio de los mismos.
        </p><p align="justify" > Art. 19: Las compras de los bienes se efectuarán conforme a la normativa vigente y en todos los casos, corresponderá ala autoridad de aplicación la obligación de preservación o custodia de los bienes adquiridos.
        </p><p align="justify" > Art. 20: En caso de negativa de los propietarios o cuando se tratase de propietarios desconocidos, se declarará por ley la
        afectación del bien a los fines de la expropiación.
        </p><p align="justify" > Art. 21: Los bienes que sean declarados integrantes del Patrimonio Histórico Cultural y Natural del Chaco, conforme a las
        categorías de protección que fije la reglamentación y que estén incluidos en el Registro correspondiente, gozarán de
        protección y tutela específica.
        </p><p align="justify" > Art. 22: Cualquier persona u organismo público o privado, podrá requerir a la autoridad de aplicación la declaración de
        pertenencia al patrimonio cultural provincial de uno o más bienes, su incorporación al Registro pertinente, mediante el
        proceso que al efecto se determine.
        </p><p align="justify" > Art. 23: Facúltase a la autoridad de aplicación a establecer por instrumento legal, restricciones al uso, transporte o
        modificación de la situación fáctica de cualquier bien mueble o inmueble susceptible de incorporarse al Registro
        Provincial de Patrimonio Histórico Cultural y Natural, cuando el cumplimiento de los plazos que prevea el procedimiento
        administrativo torne inútil su declaración. Tales restricciones tendrán carácter transitorio y obligan a la comunidad
        provincial a su inmediato cumplimiento.
        </p><p align="justify" > Art. 24: Los propietarios y los poseedores de los bienes que integran el Patrimonio Histórico Cultural y Natural, inscriptos
        en el Registro previsto en la presente ley, necesitarán para su exportación y salida de la Provincia, autorización expresa
        y previa de la Comisión Provincial de Patrimonio Histórico Cultural y Natural y del Ministerio de Educación, Cultura,
        Ciencia y Tecnología, en la forma y condiciones que se establezcan por vía reglamentaria.
        </p><p align="justify" > Art. 25: Sin perjuicio de lo que esta ley establece y lo dispuesto en el artículo precedente, queda prohibida la salida de la
        Provincia de los bienes declarados de interés cultural, así como la de aquellos otros que por pertenencia al Patrimonio
        Histórico Cultural y Natural, el Estado declare expresamente inexportable como medida cautelar, hasta tanto se inicie el
        expediente para incluir el bien en algunas de las categorías de protección especial previstas en esta ley.
        </p><p align="justify" > Art. 26: El Estado Provincial a través del Fondo Permanente, creado por el artículo 29 de la presente ley, proveerá la
        realización de las obras necesarias para la custodia y conservación de los bienes inscriptos en el Registro Provincial de
        Patrimonio Cultural, cuando sus propietarios, titulares de derechos reales, poseedores y tenedores no puedan
        afrontarlos, sujetas a la contraprestación específica que determine la Autoridad de Aplicación.
        </p><p align="justify" > Art. 27: La Dirección General de Rentas exceptuará de toda clase de impuestos, gravámenes, tasas y contribuciones de
        carácter provincial, los bienes muebles e inmuebles registrados como pertenecientes al Patrimonio Cultural de la
        Provincia del Chaco. La autoridad de aplicación podrá definir los alcances de la excepción y las condiciones bajo las
        cuales se mantiene el beneficio.
        </p><p align="justify" > Art. 28: La Comisión Provincial de Patrimonio Histórico Cultural y Natural evaluará y ejecutará, según criterio
        debidamente fundado, las acciones que permitan proteger los bienes patrimoniales mediante:
        </p><p> a) Premios estímulo.
        </p><p> b) Créditos y subsidios.
        </p><p align="justify" > c) Toda otra acción estratégica que permita la protección del bien atendido.
        <h8>RECURSOS PARA LA CONSERVACION DEL PATRIMONIO HISTORICO CULTURAL Y NATURAL PROVINCIAL</h8>
        </p><p align="justify" > Art. 29: Créase un Fondo Permanente para el Patrimonio Histórico Cultural y Natural, imputado a la Jurisdicción del
        Ministerio de Educación, Cultura, Ciencia y Tecnología, con destino al cumplimiento de la presente ley, conformado por
        los siguientes recursos:
        </p><p align="justify" > a) Legados, donaciones y cualquier otro ingreso de carácter gratuito.
        </p><p align="justify" > b) Los fondos ingresados por las multas aplicadas por incumplimiento de lo previsto por esta ley, conforme lo queestablezca el régimen de penalidades impuesto por la reglamentación de la presente.
        </p><p align="justify" > c) Asignaciones específicas a la preservación del patrimonio cultural de recursos provenientes de organismos nacionales
        e internacionales.
        </p><p align="justify" > d) Cualquier otro ingreso que disponga el Poder Ejecutivo en orden al cumplimento de los objetivos de la presente ley.
        <h8>DE LAS SANCIONES</h8>
        <p align="justify" >Art. 30: El Poder Ejecutivo reglamentará la presente ley, instrumentando un régimen de sanciones contra los actos que
        lesionen el Patrimonio Histórico Cultural y Natural, mediante:
        </p><p> a) Ocultamiento.
        </p><p> b) Destrucción.
        </p><p> c) Modificación.
        </p><p> d) Alteración.
        </p><p> e) Abandono.
        </p><p align="justify" > f) Transferencias ilegítimas de los bienes declarados de interés cultural.
        </p><p align="justify" > g) Tráfico ilícito en todo o en parte de los bienes registrados.
        </p><p align="justify" > Art. 31: A los efectos del artículo anterior, la reglamentación establecerá el procedimiento aplicable respecto de la
        constatación de las infracciones enunciadas precedentemente y con relación a las sanciones a aplicarse.
        <h8>DISPOSICIONES FINALES</h8>
        </p><p align="justify" > Art. 32: Para cuestiones no previstas en los presentes artículos, resultará de aplicación supletoria la ley 3.911 y sus
        modificatorias.
        </p><p> parte_43,[Contenido relacionado]
        </p><p align="justify" > Art. 33: El Poder Ejecutivo reglamentará la presente ley, en el término de noventa días, contados a partir de su
        promulgación.
        </p><p align="justify" > Art. 34: Invítase a los Municipios de la Provincia, a adherir a las disposiciones de la presente y a dictar, en el marco de
        sus respectivas competencias, las normas que aseguren la conservación, custodia y difusión de los bienes que forman el
        Patrimonio Histórico Cultural y Natural de la Provincia.
        </p><p> Art. 35: Deróganse las leyes 4.076, 4.416 y 4.896.
        </p><p> parte_46,[Normas que modifica]
        </p><p> Art. 36: Regístrese y comuníquese al Poder Ejecutivo.
        <p><b>Firmantes</b></p>
        <p>BACILEFF IVANOFF - Acevedo</p>
 </div>    
 <a href="#tabs-4" onclick="verElemento('motivos' )" class="link-button"><span> <?php echo __('Read more')?> &#8594;</span></a>
                                                                    
