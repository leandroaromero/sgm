<script type="text/javascript" >
function verElemento( elemento, boton){
   
   div = document.getElementById(elemento);
  // bot = //document.getElementById(boton);
if (div.style.display == 'none'){
        div.style.display = ''; 
        
     } else{
         div.style.display ='none' ;
        
     }
     
}
</script>
    
                       <?php $web= sfConfig::get('app_url_web');  ?>

			<!-- Slider -->
			<div id="slider-block">
				<div id="slider-holder">
					<div id="slider">
						<a href="https://www.facebook.com/programapatrimonio.chaco"><img src="/museo/<?php echo $web?>images/public/images/01.jpg" title="<?php echo __('Visit us in Facebook') ?>" alt="" /></a>
                                                <a href="https://www.facebook.com/programapatrimonio.chaco"><img src="/museo/<?php echo $web?>images/public/images/03.jpg" title="<?php echo __('Visit us in Facebook') ?>" alt="" /></a>
						<a href="https://www.facebook.com/programapatrimonio.chaco"><img src="/museo/<?php echo $web?>images/public/images/02.jpg" title="<?php echo __('Visit us in Facebook') ?>" alt="" /></a>
					</div>
				</div>
			</div>
			<!-- ENDS Slider -->
                        

				<!-- wrapper-main -->
				<div class="wrapper">
					
					<!-- headline -->
					<div class="clear"></div>
					<div id="headline">
						<span class="main"><?php echo __('Our Museums')?></span>
						<span class="sub">
                                                      ( <?php echo __('Extract from Law of Culture')?>  )
                                                </span>
                                                    <p align="justify"> Los chaqueños tienen así derecho a “crear, producir y definir la obra cultural y colectiva, en libertad y sin censura; desarrollar la identidad cultural en el marco de la diversidad e interculturalidad, característica de la provincia, respetando el origen étnico y cultural del grupo al que pertenece; preservar la lengua, identidad étnica y cultural, cosmovisión, valores y espiritualidad de los pueblos originarios, en el marco de la indispensable reparación histórica, en concordancia con el artículo 37 de la Constitución Provincial 1957 – 1994”.</p>                                                     
					</div>
					<!-- ENDS headline -->
					
					<!-- content -->
					<div id="content_2">
						
							<!-- TABS -->
							<!-- the tabs -->
							<ul class="tabs">
                                                                <li><a href="#tabs-2"><span><?php echo __('Law of Culture') ?></span></a></li>
                                                                <li><a href="#tabs-3"><span><?php echo __('Law of Museums') ?></span></a></li>
                                                                <li><a href="#tabs-4"><span><?php echo __('Law of Patrimony') ?></span></a></li>
							</ul>
							
							<!-- tab "panes" -->
							<div class="panes">
                                                            <!-- Information  -->
								<div id="tabs-2">
									<div class="plain-text">
   
                                                                            <?php include_partial('home/ley_provincial_cultura', array()) ?>
                                                                         
                                                                     </div>
                                                                </div>
                                                                <div id="tabs-3" >
                                                                    <div class="plain-text">
                                                                       <?php include_partial('home/ley_museos', array()) ?>
                                                                     
                                                                    </div>
                                                                 </div>
                                                                 <div id="tabs-4">
                                                                    <div class="plain-text"> 
                                                                         <?php include_partial('home/patrimonio', array()) ?>
                                                                    
                                                                    </div> 
                                                                 </div>
								<!-- ENDS Information -->
								
							</div>
							<!-- ENDS TABS -->
		
		
		
					</div>
					<!-- ENDS content_2 -->
				</div>
				<!-- ENDS wrapper-main -->
			
