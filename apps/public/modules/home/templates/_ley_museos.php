<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<h4>Ley de Museos de la Provincia Del Chaco</h4>
<p>LEY 6.201<br/>
RESISTENCIA, 20 de 2008<br/>
Boletín Oficial, 7 de Noviembre de 2008<br/>
Vigente, de alcance general<br/>
Id Infojus: LPH0006201</p>
<h6>Sumario</h6>
<p><i>Cultura y educación, patrimonio cultural, museos</br>
LA CAMARA DE DIPUTADOS DE LA PROVINCIA DEL CHACO SANCIONA CON FUERZA DE LEY:</i></p>
<div id="noticias" style="display: none">
<p align="justify" >Art. 1: Objeto. La presente ley tiene por objeto regular la actividad de los museos de la Provincia del Chaco, sean éstos
provinciales, municipales y privados.</p>
<p align="justify" >Art. 2: Definición. a) Son museos, en los términos de la presente ley, las instituciones de carácter permanente, sin fines
de lucro, abiertas al público, al servicio de la sociedad y de su desarrollo, que adquieren, documentan, conservan,
investigan, interpretan, comunican, difunden y exhiben de manera científica, estética y didáctica, evidencias materiales e
inmateriales de valor histórico, artístico, científico y técnico. b) Los conjuntos de objetos culturales o colecciones de
objetos conservados por una persona física o jurídica que no reúnan todos los requisitos enunciados en el inciso a) del
presente artículo, no se considerarán museos.
</p><p align="justify" >Art. 3: Organo de aplicación. El órgano de aplicación de la presente ley es el Ministerio de Educación, Cultura, Ciencia y
Tecnología. El decreto reglamentario asignará las incumbencias que le correspondan a las reparticiones de su estructura
orgánico-funcional que resulten pertinentes.
</p><p align="justify" >Art. 4: Categorías de museos con sede en la Provincia del Chaco. a) Museos provinciales: los que poseen colecciones
propiedad del Estado Provincial. b) Museos municipales: los que poseen colecciones propiedad de jurisdicción municipal.
c) Museos Privados: los que poseen colecciones de instituciones, empresas o coleccionistas privados. d) Museos
Universitarios: instituciones de dependencia universitaria, con colecciones que pertenecen al patrimonio cultural o natural
de la Provincia.
</p><p align="justify">Art. 5: Son funciones de los museos: a) La adquisición, investigación, documentación, conservación, restauración,
interpretación, exhibición y comunicación de las colecciones. b) La investigación referida a los objetos y conjuntos de
objetos museales, materiales e inmateriales, de su disciplina de base o entorno cultural. c) La organización periódica de
exposiciones acordes con su naturaleza o especialidad. d) La elaboración y publicación de catálogos y monografías
relacionados con su acervo. e) La elaboración y realización de actividades culturales tendientes a la consecución de sus
fines. f) La realización y desarrollo de actividades pedagógicas y recreativas, acordes con su naturaleza o especialidad.
g) Otras funciones que se les encomiende por sus normas estatutarias, por la incorporación de nuevas tecnologías o por
disposición legal o reglamentaria. Los museos podrán desarrollar actividades complementarias atinentes a sus fines
siempre y cuando cuenten con instalaciones adecuadas y la actividad a realizar no perjudique el patrimonio ni el normal
desarrollo de sus funciones.
</p><p align="justify" >Art. 6: Museos dependientes del Gobierno de la Provincia del Chaco: a) Cada museo que se cree a partir de la fecha de
la sanción de la presente, en el marco de sus misiones y funciones específicas, establecerá los criterios de selección de
su patrimonio cultural y los servicios profesionales, técnicos y de mantenimiento bajo la supervisión y contralor delorganismo del Estado del cual dependan. b) Los museos ya existentes a la sanción de la presente, mantendrán los
criterios de selección de su patrimonio cultural actuales, según las misiones y funciones que tienen establecidas. c) Los
museos podrán contar para su afianzamiento y proyección cultural con fundaciones, asociaciones de amigos o
instituciones similares.
</p><p align="justify" >Art. 7: Creación de Museos. a) El Gobierno de la Provincia del Chaco, a través del Ministerio de Educación, Cultura,
Ciencia y Tecnología, propiciará la creación de museos, en especial de aquellos que por sus planteamientos, contenidos
temáticos y riqueza patrimonial ofrezcan una visión representativa de la cultura local. b) las personas físicas o jurídicas
de carácter privado que propongan la creación de un museo, deberán garantizar el mantenimiento, conservación y
exhibición de su patrimonio cultural y natural en la forma prevista por la presente ley. c) El Gobierno de la Provincia del
Chaco, se ocupará de que los edificios sede de museos reúnan condiciones de accesibilidad para personas de movilidad
reducida y brinden particular atención a aquéllas con necesidades especiales.
</p><p align="justify" >Art. 8: Creación y contenido del Registro y Colecciones. Créase un Registro de Museos en el Ministerio de Educación,
Cultura, Ciencia y Tecnología, en el área de la Subsecretaría de Cultura, donde figurarán los datos relativos a las
personas o entidades titulares del museo, órganos rectores, domicilio de la sede del museo y los bienes culturales que
custodia en los términos que se determinen reglamentariamente.
</p><p align="justify"  >Art. 9: Inscripción de Museos. Los museos provinciales, municipales o privados, para inscribirse en el Registro de
Museos deberán cumplir con los siguientes requisitos: a) Presentar un estatuto, constitución, declaración o cualquier otro
documento escrito que acredite objetivos, misión y funciones, condición de entidad sin fines de lucro y carácter
permanente. b) Contar con un espacio adecuado y suficiente para su funcionamiento, de acuerdo con las normativas
vigentes. c) Poseer un inventario de su patrimonio cultural y natural. d) Presentar una exposición ordenada de las
colecciones. e) Contar con personal calificado que garantice la conservación y mantenimiento de su patrimonio cultural. f)
Disponer de recursos humanos y tecnológicos adecuados para el resguardo y la seguridad de sus colecciones. g)
Brindar acceso a las colecciones para la investigación, consulta, enseñanza, comunicación y divulgación. h) Ofrecer un
horario para la visita pública con la correspondiente difusión en los medios. Los museos inscriptos, previa verificación,
recibirán un Certificado de Acreditación. La acreditación supondrá el reconocimiento oficial del Gobierno de la Provincia
del Chaco de esa institución como Museo. La inscripción en el registro permitirá al museo acceder o participar en las
políticas de fomento y estímulo y demás beneficios que el Gobierno de la Provincia del Chaco establezca al efecto.
</p><p align="justify" >Art. 10: Fomento y participación del Gobierno de la Provincia del Chaco en la creación y gestión de museos. El Gobierno
de la Provincia del Chaco promoverá la cooperación con instituciones museística públicas o privadas, de jurisdicciones
provinciales, nacionales o extranjeras, con los que podrá suscribir convenios o acuerdos.
</p><p align="justify" >Art. 11: De las obligaciones de los Museos: a) Contar con un inventario actualizado de su patrimonio. b) Fijar días y
horarios de apertura y publicitarlos. c) Difundir los valores culturales de los bienes custodiados y publicitar los servicios al
público. d) Facilitar el acceso a las colecciones a investigadores acreditados. e) Garantizar la seguridad y conservación
de su acervo. f) Todos los procedimientos de restauración y conservación deberán ser reversibles, identificables y
documentados.
</p><p align="justify" >Art. 12: Copias y reproducciones. La realización de reproducciones de los objetos de los museos por cualquier
procedimiento deberá ser autorizada por los directivos de la institución y, en caso de corresponder, por el autor o sus
descendientes. En las reproducciones, deberá constar en todos los casos la documentación del objeto y los datos de la
institución depositaria.
</p><p align="justify" >Art. 13: Personal y dirección: Todos los museos deberán contar con personal profesional y técnico especializado. Los
cargos directivos de los museos estatales, cuando se produzcan vacantes, deberán ser cubiertos por concurso de
antecedentes. El postulante deberá acreditar, como mínimo, titulación universitaria acorde a la temática del museo,
gestión cultural y/o museología. Son funciones de los directivos de los museos entre otras: a) Velar por la preservación
del patrimonio a su cargo y adoptar las medidas pertinentes para su salvaguarda. b) Dirigir y coordinar las áreas
técnicas, administrativas y de servicios. c) Organizar y gestionar la prestación de servicios. d) Ejercer las relaciones
públicas. e) Facilitar la investigación, la incorporación de nuevas tecnologías y el intercambio de información. Los
directivos y el personal de los museos acreditados en el registro no podrán realizar, por sí mismos ni por terceros,
actividades comerciales relativas a bienes culturales de naturaleza semejante a los custodiados en su museo, de
acuerdo con el Código de Etica y Deontología profesional del ICOM.
</p><p>Art. 14: Regístrese y comuníquese
    <p><b>   Firmantes</b></p>
<p>MASTANDREA - Bosch</p>

 </div>    
<a href="#tabs-3" onclick="verElemento('noticias' )" class="link-button"><span> <?php echo __('Read more')?> &#8594;</span></a>
