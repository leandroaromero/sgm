<?php

?>

 <h4>Ley Provincial de Cultura</h4>
    <p>LEY 6.255</br>
    RESISTENCIA, 19 de Noviembre de 2008</br>
    Boletín Oficial, 19 de Diciembre de 2008</br>
    Vigente, de alcance general</br>
    Id Infojus: LPH0006255</p>
    <h6>Sumario</h6>
    <p>
        <i>Cultura y educación, política cultural, promoción cultural<br/>
        La Cámara de Diputados de la Provincia del Chaco Sanciona con Fuerza de Ley:</br>
        </i>
    </p>
<div id="leyes" style="display: none">    
    <h8>CAPITULO I CONCEPTO, OBJETO Y PRINCIPIOS<h8>
    <p align="justify" >
        Art. 1: Concepto: Cultura es un conocimiento y una creación colectiva que producen los sujetos sociales para
    comprender su realidad, intervenir y transformarla. Es un derecho social inalienable y por lo tanto el Estado es su garante
    indelegable. A la vez conforma nuestros modos de vivir cotidiano, de percibir el mundo, de indagar y replantear las
    relaciones humanas, tanto sociales como económicas y políticas en la búsqueda de la construcción colectiva de nuestra
    identidad y en el marco del pleno respeto de nuestras diversidades. En su sentido más amplio la cultura se considera
    como el conjunto de rasgos distintivos, espirituales y materiales, intelectuales y afectivos que caracterizan a una
    sociedad o a un grupo social, comprendiendo los derechos fundamentales del ser humano, los sistemas de valores, las
    tradiciones y las creencias, las letras y las artes.
    </p>
    <p align="justify" >Art. 2: Objeto: La presente ley tiene por objeto la promoción de los derechos culturales previstos en los artículos 78 y 84
    de la Constitución Provincial 1957 - 1994, en Tratados Internacionales con rango constitucional, en leyes y otras normas
    que regulen la materia, asegurando la indispensable redistribución social de bienes y servicios culturales.
    </p><p>parte_2,[Contenido relacionado]
    </p>
    <p align="justify" >Art. 3: Disciplinas: La política cultural de la Provincia del Chaco, atenderá en especial, y de forma no taxativa, las
    siguientes disciplinas: a) Antropología. b) Arquitectura y patrimonio arquitectónico. c) Patrimonio Cultural y Natural. d)
    Historia. e) Arqueología. f) Enseñanza Artística. g) Museología. h) Bibliotecología y demás actividades de recolección,
    conservación y exhibición de bienes pertenecientes al patrimonio cultural. i) Música. j) Literatura. k) Arte digital,
    electrónico y electroacústico. l) Artes visuales. m) Artes escénicas en su diversidad de géneros. n) Artes audiovisuales. ñ)
    Tecnologías aplicadas a las distintas disciplinas. o) Diseño. p) Radio y televisión educativas o culturales. q) Costumbres y
    tradiciones populares. r) Artesanías. s) Investigación y experimentación, conservación y crítica, dentro del campo de las
    disciplinas culturales. t) Cultura alimentaria en su diversidad cultural. u) Promoción y animación socio-cultural. v) Toda
    otra disciplina que en le futuro se incorpore por parte de la autoridad de aplicación.
    </p>
    <p align="justify" >Art. 4: Alcances. Las disposiciones de la presente ley son el marco referencial de todas las leyes vigentes en su
    competencia y de las que se dictaren en el futuro.
    </p>
    <p align="justify" >Art. 5: Derechos. Los habitantes de la Provincia del Chaco, gozan del acceso universal, equitativo e inclusivo a los
    derechos culturales. Son derechos culturales: a) Crear, producir y difundir la obra cultural, individual y colectiva, en
    libertad y sin censura previa. b) Desarrollar la identidad cultural en el marco de la diversidad e interculturalidad,
    característica de la Provincia, respetando el origen étnico y cultural del grupo al que pertenece. c) Preservar la lengua,
    identidad étnica y cultural, cosmovisión, valores y espiritualidad de los pueblos originarios, en el marco de una
    indispensable reparación histórica, en concordancia con el artículo 37 de la Constitución Provincial 1957-1994. d)
    Acceder a la educación y la formación artística, artesanal y profesional que respete plenamente la identidad cultural,promoviendo la creación de escuelas de arte de las distintas culturas que conforman el mosaico étnico del Chaco. e)
    Garantizar la educación y formación artística, artesanal y profesional, estableciendo convenios con el Ministerio de
    Educación, Cultura, Ciencia y Tecnología, para incluir especialmente dentro de los programas interculturales bilingües, la
    enseñanza de culturas indígenas. f) Garantizar la participación de indígenas para desarrollar actividades propias de su
    cultura, dentro de los programas culturales del orden provincial y/o municipal, actuales y los que se definan a futuro. g)
    Participar en la vida cultural que cada ciudadano elija y ejercer sus propias prácticas culturales, sin distinción de etnia,
    credo, sexo, o condiciones biopsicosocial y económica, en el marco de los derechos fundamentales y garantías
    constitucionales. h) Participar en el diseño y la evaluación de las políticas culturales, en el marco de una democracia
    cultural. i) Acceder a la plena participación de las actividades culturales, por parte de las personas con necesidades
    especiales y con capacidades diferentes.
    </p>
    <p align="justify" > Art. 6: Principios. El organismo de aplicación de esta ley asegurará a los habitantes de la Provincia del Chaco, el goce de
    los derechos enunciados en el artículo 5° de la presente y promoverá las actividades culturales sustentadas en los
    siguientes principios: a) La cultura como política de Estado. b) La cultura como contribución a la construcción de la paz y
    consolidación de los valores democráticos. c) La igualdad y equidad en el acceso a los diversos bienes y valores
    culturales. d) El derecho a la cultura integra los derechos humanos, que son universales, indisociables e
    interdependientes. e) La protección de una identidad pluralista y multiétnica, desde las concepciones de diversidad e
    interculturalidad. f) La superación de las barreras comunicacionales para el intercambio y difusión del patrimonio cultural.
    g) La cultura como componente de un desarrollo estratégico central, para una política de inclusión y cohesión social,
    orientada a la generación del empleo, al desarrollo sustentable de las nuevas formas de economía social, a la
    recuperación socioeconómica y a la distribución de la riqueza. h) La formulación de políticas orientadas a la construcción
    de una identidad productiva, sobre la base de la promoción y difusión de las industrias culturales. i) La revalorización de
    la propia identidad cultural, sustentándose en la identidad local, regional, nacional y latinoamericana, con apertura a los
    procesos regionales de integración a los demás pueblos del mundo. j) El reconocimiento de las culturas qom, wichi y
    moqoi como valores constitutivos del acervo cultural de la Provincia. k) Difundir, a través de programas especiales, el
    patrimonio cultural de los pueblos originarios locales y su aporte a la cultura nacional, fomentando sus artesanías,
    consideradas como fuente de trabajo y expresión cultural, que garanticen la obtención de la materia prima, que es su
    condición de posibilidad. l) Libertad de expresión, preservación y desarrollo de la cultura. m) La descentralización de las
    políticas de gestión cultural, asegurando la participación y protagonismo de la comunidad, en el diseño de la planificación
    de las mismas. n) La cultura como promotora del desarrollo humano integrado al desarrollo social, económico, científico,
    tecnológico, educativo, artístico y deportivo. ñ) La protección del patrimonio cultural, que define la identidad y la memoria
    colectiva de los habitantes de la Provincia del Chaco. o) La orientación preventiva de la cultura en la aplicación de las
    políticas culturales destinadas a los sectores con mayor necesidad de atención. p) La cultura en su dimensión
    terapéutica, como potenciadora de capacidades, frente a personas con dificultades biopsicosociales.
    CAPITULO II INSTITUTO DE CULTURA DEL CHACO
    </p>
    <p align="justify" >Art. 7: Creación. Créase el Instituto de Cultura del Chaco, como ente autárquico del Estado Provincial, de conformidad
    con el inciso b) del artículo 4° de la ley 4.787 -de Organización y Funcionamiento de la Administración Financiera del
    Sector Público Provincial-.</p>
    <p>parte_8,[Contenido relacionado]
    </p>
    <p align="justify" >Art. 8: Domicilio. El Instituto de Cultura del Chaco tendrá domicilio, como sede central de sus actividades y a todos los
    efectos legales, en la Ciudad de Resistencia y podrá establecer delegaciones en distintas regiones de la Provincia, a los
    fines de la descentralización de su gestión y efectivización de la políticas participativas de los programas a desarrollarse,
    articulando con las áreas respectiva de los municipios.
    </p>
    <p align="justify" > Art. 9: Función. El Instituto de Cultura del Chaco, es el organismo encargado de asistir al Poder Ejecutivo en el diseño,
    ejecución y supervisión de las políticas provinciales en materia de conservación, promoción, enriquecimiento, difusión,
    acrecentamiento y transmisión a las generaciones futuras del patrimonio cultural y natural, tangible e intangible del
    Chaco.
    </p>
    <p align="justify" >Art. 10: Objetivos. Son objetivos del Instituto de Cultura del Chaco, sin perjuicio de los que por leyes o convenios
    especiales se le asignen: a) Garantizar a los Habitantes de la Provincia la igualdad en el acceso a los diversos bienes y
    valores culturales. b) Preservar, enriquecer y difundir el patrimonio cultural y natural. c) Apoyar las manifestaciones
    culturales que afirmen la identidad local, regional, provincial, nacional y latinoamericana, propiciando los procesos de
    organización comunitaria. d) Coordinar y promover acciones conjuntas con los municipios, en el ámbito del Consejo
    Provincial de la Cultura Chaqueña, para la ejecución de programas y actividades en beneficio de la comunidad. e)
    Promover la creación y funcionamiento de áreas de cultura en los municipios, para la articulación de las políticas que se
    planifiquen y ejecuten. f) Articular con la Comisión Provincial por la Memoria, a través de programas especiales, acciones
    tendientes a rescatar la memoria respecto de los hechos ocurridos en los años de dictadura cívico militar, autoritarismo yterrorismo de estado en nuestro país, contribuyendo a que estos hechos no se repitan en estado democrático. g)
    Promover y estimular los modos de crear, hacer, vivir y ser de los habitantes de la Provincia. h) Propender a la
    distribución equitativa y democrática de los recursos públicos destinados a la cultura. i) Garantizar la gestión cultural
    pública y participativa, por medio de mecanismos que promuevan el diálogo permanente entre el Estado y la sociedad. j)
    Promover el potencial de artistas, artesanos y demás creadores de la cultura popular, como expresión del patrimonio vivo
    e intangible de la Provincia. k) Registrar y preservar sistematizadamente la obra artística de los chaqueños. l) Impulsar la
    sanción de la legislación que contemple las normas para el trabajador de la cultura y el escalafón artístico. m) Fortalecer
    la presencia cultural de la Provincia en escenarios regionales, nacionales e internacionales. n) Instalar políticas de
    comunicación tendientes a la difusión de los valores de la cultura democrática que preserven y proyecten nuestra
    memoria histórica y nuestra diversidad cultural y lingüística, gestionando la producción de medios de comunicación
    públicos, la instalación de sistemas electrónicos de radio, televisión, Internet u otros sistemas para la promoción cultural.
    ñ) Destinar recursos al financiamiento de la promoción de la investigación, formación, creación artística y producción
    cultural. o) Propiciar la investigación de los consumos, las aspiraciones y las demandas culturales de la población. p)
    Propiciar la inversión del sector privado en la realización de actividades culturales, en concordancia con lo establecido
    por la Ley de Mecenazgo. q) Establecer políticas de promoción para nuevas tendencias artísticas. r) Fomentar el
    desarrollo de las pequeñas y medianas empresas que tengan objetivos culturales. s) Fomentar el funcionamiento y
    accionar del Consejo Provincial de Cultura asegurando y propiciando de manera permanente, la participación de todos
    los sectores que lo conforman, en el análisis y diseño de las acciones a implementarse. t) Fomentar y ejecutar programas
    especiales tendientes a la incorporación de niños, jóvenes y personas adultas con capacidades diferentes, en la
    iniciación y práctica de actividades artísticas y creativas, propendiendo a su efectiva integración e inclusión social. u)
    Promover la inclusión de contenidos y recursos humanos de la cultura de los pueblos originarios en los programas de las
    escuelas de arte.
    </p>
    <p align="justify" >
    Art. 11: Atribuciones y deberes. Serán atribuciones y deberes del Instituto de Cultura del Chaco: a) Diseñar y proponer al
    Poder Ejecutivo, en concordancia con las definiciones surgidas del Congreso Provincial de Cultura Chaqueña, las
    políticas públicas culturales a aplicar a través del Consejo Provincial de la Cultura, creado por decreto 327/08,
    garantizando la participación directa de las diferentes expresiones y de los protagonistas de la cultura provincial. b)
    Convocar anualmente al Congreso Provincial de Cultura Chaqueña como espacio democrático de construcción del
    consenso en la búsqueda de la agenda cultural provincial. c) Ejecutar las políticas culturales procurando la
    descentralización de las acciones y la participación democrática de la comunidad en el acceso a los bienes culturales. d)
    Propiciar la generación de recursos en el marco de la legislación provincial y nacional vigentes. e) Desarrollar y ejecutar
    programas de asistencia técnica y de capacitación de los actores de la cultura provincial. f) Fomentar la organización
    laboral de los actores culturales, facilitando profesionalización y perfeccionamiento continuo de los mismos, mediante
    acciones de apoyo planificadas y sostenidas en forma conjunta. g) Coordinar con entidades públicas o privadas de
    carácter regional, nacional o internacional, las acciones necesarias para la promoción de las actividades culturales. h)
    Propiciar la instrumentación de mecanismos de cogestión, en base a actividades afines, localización geográfica u otros
    criterios que permitan la fluida participación de las distintas expresiones culturales. i) Aplicar la legislación vigente en
    materia cultural, planificando y ejecutando acciones de manera integrada y coordinada con instituciones públicas y
    privadas. j) Generar espacios artísticos e intelectuales para desarrollar debates democráticos e independientes,
    tendientes a rescatar valores como la solidaridad, los derechos humanos, la distribución de la renta, la defensa de los
    pueblos originarios, la distribución de la tierra para quien la trabaja, la preservación y el rescate del medio ambiente, la
    defensa de las minorías, el derecho a la información y demás preceptos que contribuyan al crecimiento social.</p>
    <p>parte_12,[Contenido relacionado]
    </p>
    <p align="justify" >
        Art. 12: Gobierno y Administración. La administración y gobierno del Instituto de Cultura del Chaco, estará a cargo de un
        Directorio compuesto por cinco miembros, quienes se desempeñarán como Presidente, Vicepresidente y tres Vocales.
        Uno de ellos será representante de los Pueblos Originarios y otro de las entidades culturales elegidos en los términos
        que establezca la reglamentación. Las misiones y funciones serán establecidas por el decreto reglamentario de la
        presente ley. El Presidente, Vicepresidente y los Vocales serán designados por el Poder Ejecutivo, con acuerdo de la
        Cámara de Diputados. Los directores generales, directores y jefes de departamento, serán cargos de carrera del
        Instituto. Para cubrir los cargos vacantes en la actualidad y los que se creen en el futuro, el Poder Ejecutivo deberá
        convocar a concurso de antecedentes entre el personal de Planta Permanente de la actual Subsecretaría de Cultura en
        primera instancia. Si resultare desierta esta convocatoria, se llamará a concurso en una segunda instancia entre el
        personal de Planta del Poder Ejecutivo Provincial. Si se declara desierta esta segunda instancia, podrá convocarse a
        concurso de antecedentes entre el personal con contratos de servicios. Para los cargos de directores generales y
        directores, se deberá contar indefectiblemente con titulación universitaria relacionada al cargo, expedido por universidad
        provincial, nacional o internacional debidamente reconocida, idoneidad o trayectoria demostrada. Para los cargos de los
        Museos que dependan del Instituto, regirán los requisitos establecidos en la ley 6.201 -Ley de Museos de la Provincia del
        Chaco. En el organigrama funcional del Instituto de Cultura del Chaco, se contemplará una Dirección de Cultura
        Indígena, que contará con presupuesto específico y personal indígena. El Presidente del Instituto percibirá una
        retribución equivalente a la de Ministro del Poder Ejecutivo. El Vicepresidente percibirá una retribución mensual
        correspondiente al 90% de la retribución del Presidente. Los Vocales recibirán una retribución mensual correspondienteal 80% de la retribución del Presidente. Hasta tanto el Instituto tenga aprobado su escalafón, se aplicará el vigente para
        el escalafón General de la Provincia.</p>
        <p>parte_13,[Contenido relacionado]</p>
        <p>Art. 13: Condiciones de los cargos: Para las designaciones de los cargos
        </p>
        <p align="justify" >Art. 14: Organización y formas de gestión: El Instituto de Cultura del Chaco organizará sus actividades a través de
        programas permanentes y programas temporales específicos. 1. Programas Permanentes: serán aquellos destinados al
        mantenimiento de las estructuras administrativas del Instituto de Cultura del Chaco y al desarrollo de las actividades que
        garanticen el goce de los derechos declarados por esta ley. 2. Programas Temporales: serán aquellos destinados a la
        ejecución de acciones culturales específicas no permanentes, adoptados como estrategia de gestión.
        </p>
        <p>Art. 15: Programación y financiamiento: Los Programas Permanentes y Temporales serán diseñados, implementados y
        ejecutados de acuerdo con los criterios y principios que en materia presupuestaria adopte la Provincia del Chaco,
        enfatizando la eficiencia y la consecución efectiva de los fines y objetivos propuestos en esta ley.
        </p>
        <p align="justify" >Art. 16: Funciones y atribuciones del Presidente: Son funciones del Presidente: a) Ejercer el gobierno y la administración
        del Instituto de Cultura del Chaco. b) Asumir la representación legal del Instituto de Cultura del Chaco. c) Asistir a las
        reuniones de Ministros del Poder Ejecutivo en representación del Instituto. d) Asesorar al Poder Ejecutivo en la definición
        de las políticas públicas culturales de la Provincia. e) Celebrar contratos y convenios que resulten necesarios para el
        desenvolvimiento de las actividades o consecución de los fines y objetivos del Instituto de Cultura del Chaco. f) Elaborar
        el Manual de Misiones y Funciones de la estructura jerárquica del personal del Instituto. g) Diseñar anualmente el plan de
        acción del Instituto de Cultura del Chaco con inclusión de actividades de apoyo, de los distintos programas a ejecutarse,
        de los objetivos a cumplir en el período y el respectivo cálculo de recursos e inversión. h) Proponer al Poder Ejecutivo el
        presupuesto anual del Instituto de Cultura de la Provincia del Chaco. i) Ejercitar en el ámbito de su competencia, toda
        otra facultad conferida al Poder Ejecutivo por las leyes de presupuesto y administración financiera del sector público
        provincial. j) Delegar en funcionarios del Organismo las facultades que resulten necesarias para el mejor cumplimiento de
        sus funciones. k) Diseñar la división del territorio de la Provincia en regiones culturales atendiendo a la diversidad cultural
        y étnica. l) Aceptar o rechazar donaciones, legados, herencias u otros aportes m) Acordar con el Ministerio de Educación,
        Cultura, Ciencia y Tecnología que las herencias vacantes con valor arquitectónico, natural o cultural relevantes, sean
        transferidas al Instituto de Cultura del Chaco. n) Ejercer todos los demás actos necesarios para cumplir los objetivos
        establecidos en la presente ley y los que se le asignen por vía reglamentaria. En los casos de las funciones detalladas en
        los incisos e), f), g), k) y l) deberá contar con autorización del Directorio. En los restantes incisos, pondrá en conocimiento
        del Directorio lo actuado.
        </p>
        <p align="justify" >Art. 17: Funciones y atribuciones del Vicepresidente: Serán funciones del Vicepresidente: a) Colaborar con el Presidente
        en las acciones que éste le asigne, pudiendo reemplazarlo en los casos y ámbitos en los que, por el ejercicio de la
        función le resulte imposible su desempeño. b) Supervisar y evaluar las acciones y programas que se ejecuten. c) Hacer
        cumplir el Manual de Misiones y Funciones del Instituto de Cultura del Chaco. d) Colaborar en la preparación del
        presupuesto de recursos e inversión del Instituto. e) Contribuir en el diseño del plan de acción del Instituto de Cultura del
        Chaco. f) Ejercer los actos necesarios que le asigne el Presidente para el cumplimiento de los objetivos establecidos en
        la presente ley.</p>

        <p align="justify" >Art. 18: Ejercicio de los cargos: El Presidente, Vicepresidente y Vocales deberán ser ciudadanos argentinos, con no
        menos de cinco años de residencia inmediata en la Provincia. No podrán ejercer dichos cargos: a) Quienes tengan
        relaciones comerciales, financieras, profesionales o técnicas con el Instituto de Cultura del Chaco. b) Quienes se
        encuentren condenados por delitos cometidos contra el Estado. c) Los condenados en causa penal por delitos comunes
        cometidos con dolo. d) Los inhabilitados legalmente. e) Los comprendidos en las inhabilidades de orden ético o legal
        que, para los funcionarios de la Administración Pública, establece la reglamentación vigente. f) Los que se encuentren
        desempeñando cargos electivos nacionales, provinciales o municipales, mientras no les sea aceptada su renuncia
        indeclinable al mismo.</p>
        <h8>CAPITULO III REGIMEN ECONOMICO-FINANCIERO</h8>
        <p align="justify" >Art. 19: Inversión del Estado Provincial. El Estado Provincial tiene la obligación irrenunciable e ineludible de invertir en
        políticas públicas culturales, garantizadas a través de la asignación regular de los recursos establecidos en el artículo
        siguiente. La participación privada sólo será entendida como complemento de la estatal y será estimulada sin que
        implique delegar el diseño e implementación de la política cultural.Art. 20: Fondo Provincial de Cultura: Créase el Fondo Provincial de Cultura que se constituirá con el equivalente al 1%,
        como mínimo, del presupuesto general anual de la Provincia del Chaco. El Instituto de Cultura del Chaco administrará los
        recursos destinados al financiamiento de las políticas culturales que se integrarán de la siguiente manera: 1) El Fondo
        Provincial de Cultura. 2) Los que por ley de presupuesto se le asigne. 3) Los que provengan de actividades propias del
        Instituto. 4) Los afectados por normas y convenios específicos. 5) Los provenientes de subsidios, legados y donaciones.
        6) Todo otro ingreso no previsto en los artículos anteriores y que se derive de la aplicación de la presente ley. 7) Los
        recursos que se destinen para el financiamiento de las políticas culturales tendrán carácter inembargable, en razón de la
        calidad de derecho social inalienable que declara esta ley. El Fondo Provincial de Cultura referido en el inciso 1) de este
        artículo se distribuirá, un 84,5% para el Instituto creado por esta ley y un 15,5% para los municipios en la siguiente
        proporción: a) Municipios de Primera Categoría: treinta por ciento (30%). b) Municipios de Segunda categoría: treinta por
        ciento (30%). c) Municipio de Tercera categoría: cuarenta por ciento (40%). Los municipios deberán aplicar estos fondos
        a programas culturales, aprobados previamente por el Instituto. Vencido cada ejercicio presupuestario, las partidas no
        ejecutadas se incorporarán a las partidas presupuestarias del nuevo ejercicio en las proporciones previamente
        establecidas.</p>
        <p>parte_22,[Modificaciones]</p>

        <p align="justify" >Art. 21: El Instituto de Cultura del Chaco, tendrá facultad para constituirse en administrador fiduciario de fideicomisos
        públicos o privados para el cumplimiento específico de determinadas políticas culturales, en el marco de lo dispuesto por
        la presente ley y la ley nacional 24.441</p>
        <p>parte_23,[Contenido relacionado]</p>
        <h8>CAPITULO IV DISPOSICIONES GENERALES Y TRANSITORIAS</h8>
        <p align="justify" >Art. 22: La planta funcional del Instituto de Cultura del Chaco estará constituida por: 1) Los agentes de planta
        permanente y transitorio de la ex Subsecretaría de Cultura, los que serán transferidos en su respectiva situación de
        revista. 2) El personal de otras jurisdicciones, cualquiera sea su situación de revista o escalafonaria que al momento de
        vigencia de esta ley, cumplieren funciones en el Instituto y solicitaren su incorporación como agentes del mismo y que
        por su experiencia, idoneidad o capacitación cuenten con la expresa aceptación del Instituto. 3) El personal que en
        función de las necesidades del Instituto se incorporare a través de los mecanismos establecidos por las normativas
        vigentes.</p>
        <p align="justify" >Art. 23: El personal del Instituto de Cultura del Chaco, estará comprendido en la normativa vigente para el personal de la
        Administración Pública Provincial.</p>
        <p align="justify" >Art. 24: Para la conformación de las plantas permanente y transitoria del Instituto de Cultura del Chaco, el Presidente
        queda expresamente facultado para definir y modificar el modelo de estructura funcional que resulte más adecuado al
        cumplimiento de los objetivos institucionales y aprobar la planta orgánica del Instituto, pudiendo reasignar las funciones
        de sus agentes en el marco de la normativa vigente, respetando los derechos adquiridos por el personal que, con
        anterioridad a la sanción de la presente, cumple funciones en la Subsecretaría de Cultura.</p>
        <p align="justify" >Art. 25: Transfiérense al patrimonio del Instituto de Cultura del Chaco todos los bienes de dominio y/o afectados al
        funcionamiento de la ex Subsecretaría de Cultura. A tal efecto, la Contaduría General de la Provincia, la Fiscalía de
        Estado y la Asesoría General de Gobierno tomarán la intervención que les compete. Estos bienes no podrán incluirse
        dentro de fondo fiduciario alguno.</p>
        <p align="justify" >
        Art. 26: Facúltase al Poder Ejecutivo a realizar la afectación y transferencia de los recursos humanos de la
        Administración Pública Provincial que por su perfil, formación y desempeño en el campo de la cultura y de la
        preservación del patrimonio, resultaren aptos para desempeñarse en el ámbito del Instituto de Cultura del Chaco. Para
        ello se abrirá un llamado a inscripción luego de la reglamentación de la presente ley, por un plazo de ciento ochenta días
        los que deberán contar con la expresa aceptación de ese Organismo.
        </p><p align="justify" >Art. 27: El Poder Ejecutivo instrumentará las modificaciones que resulten necesarias al Presupuesto de la Provincia del
        Chaco, para el cumplimiento de lo establecido en esta ley.
        </p><p align="justify" >Art. 28: Dispónese el mantenimiento de la estructura y régimen de funcionamiento de la ex Subsecretaría de Cultura,
        conforme a la legislación vigente, hasta tanto se implementen los mecanismos y procedimientos necesarios para el
        funcionamiento del ente que se crea por esta ley. El plazo para la adecuación no podrá exceder de ciento ochenta días apartir de la promulgación de la presente.
        </p>
        <p>Art. 29: El Poder Ejecutivo reglamentará la presente ley, dentro de los sesenta días posteriores a su promulgación.
        </p>
        <p>Art. 30: Regístrese y comuníquese al Poder Ejecutivo.</p>
        <p><b>Firmantes</b></p>
        <p>MASTANDREA - Bosch</p>
        </div>
                                                                         <a  id="read_leyes" href="#tabs-2" onclick="verElemento('leyes', 'read_leyes' )" class="link-button"><span> <?php echo __('Read more')?> &#8594;</span></a>
