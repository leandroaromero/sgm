

<!-- Portfolio -->
        <div id="projects-list">
                 <?php foreach ($museums as $i => $museum): ?>
            <div class="project" style="min-height: 450px">
                                <h1>
                                    <a href="<?php echo url_for('@museum?id='.$museum->getId())?>">
                                       <?php echo $museum->getName()?>
                                       <?php if( strlen($museum->getName()) < 30):?> 
                                        </br> </br> 
                                       <?php endif ?> 
                                    </a>
                                </h1>

                                <!-- shadow -->
                                <div class="project-shadow">
                                        <!-- project-thumb -->
                                        <div class="project-thumbnail">

                                                <!-- meta -->
                                                <ul class="meta">
                                                        <li><strong><?php echo __('Responsible')?>: </strong> <?php echo $museum->getHolder()?></li>  
                                                        <li><strong><?php echo __('Phone')?>: </strong><?php echo $museum->getPhone()?> </li>
                                                        <li><strong><?php echo __('Address')?>: </strong><?php echo $museum->getAddress()?> </li> 
                                                        <li><strong><?php echo __('Email')?>: </strong> <a href="#"><?php echo $museum->getEmail()?></a></li>
                                                </ul>
                                                <!-- ENDS meta -->

                                                <a href="<?php echo url_for('@museum?id='.$museum->getId())?>" class="cover">
                                                        <?php if($museum->getImagenPortada()):?>
                                                               <?php echo image_tag('../uploads/museo/'.$museum->getImagenPortada(), 'style=" height: 267px; width: 438px"');?>
                                                        <?php else: ?>
                                                               <?php echo image_tag('public/img/dummies/438x267.gif')?>   
                                                        <?php endif?> 
                                                </a>
                                        </div>
                                        <!-- ENDS project-thumb -->

                                        <div class="the-excerpt" style=" max-width: 437px;">
                                              <?php  substr( $museum->getSummary(ESC_RAW),0, 140 ).'...'?>  
                                        </div>	
                                        <a href="<?php echo url_for('@museum?id='.$museum->getId())?>" class="read-more link-button"><span><?php echo __('Read more')?></span></a>

                                </div>
                                <!-- ENDS shadow -->
                        </div>
                <!-- ENDS project -->
            
                         
                 <?php endforeach;?>
                <!-- project -->
        </div>
