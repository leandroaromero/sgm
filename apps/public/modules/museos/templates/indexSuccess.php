
<?php echo use_stylesheet('ui-lightness/jquery-ui-1.8.11.custom.css')?>
<?php echo use_stylesheet('jquery-ui-1.8.7.custom.css')?>
<?php echo use_stylesheet('layout.css')?>
<?php echo use_stylesheet('main.css')?>
<?php echo use_javascript('jquery-1.4.4.min.js')?>	
<?php echo use_javascript('jquery-ui-1.8.11.custom.min.js')?>        
        
        





						
    <!-- title -->
    <div id="page-title">
            <span class="title"><?php echo __('Our Museums')?></span>
            <span class="subtitle"><?php echo __('in the Province of Chaco')?></span>
    </div>
    <!-- ENDS title -->

    
  <?php include_partial('museos/list', array('museums' => $pager->getResults())) ?>

     
            <?php if ($pager->haveToPaginate()): ?>
                 <?php include_partial('museos/pagination', array('pager' => $pager)) ?>
            <?php endif; ?>

            <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?>
            <?php if ($pager->haveToPaginate()): ?>
                <?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
            <?php endif; ?>