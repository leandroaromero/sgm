<?php $web= sfConfig::get('app_url_web');  ?>
   <!-- title -->
    <div id="page-title">
            <span class="title"><?php echo $museum->getName()?></span>
            <span class="subtitle"><?php echo $museum->getSubtitle()?>.</span>
    </div>
    <!-- ENDS title -->
    	<!-- Posts -->
                        <div id="posts" class="single">

                                <!-- post -->
                                <div class="post">
                                        <h1><a href="#">
                                                <?php if($museum->getImagenPortada()){
                                                    echo $museum->getPortada()->getTitle();
                                                }   
                                                    ?>

                                            </a></h1>

                                        <!-- shadow -->
                                        <div class="thumb-shadow">

                                                <!-- post-thumb -->
                                                <div class="post-thumbnail">
                                                          <?php if($museum->getImagenPortada()):?>
                                                                   <?php echo image_tag('../uploads/museo/big/'.$museum->getImagenPortada(), 'style=" height: 270px; width: 596px"');?>
                                                          <?php else: ?>
                                                                   <?php echo image_tag('public/img/dummies/596x270.gif')?>   
                                                          <?php endif?> 
                                                </div>
                                                <!-- ENDS post-thumb -->
                                                <div>
                                                        <?php echo $museum->getSummary(ESC_RAW);?>
                                                </div>
                                        </div>
                                        <!-- ENDS shadow -->

                                        <!-- meta -->
                                        <ul class="meta">
                                                        <li><strong><?php echo __('Responsible')?>: </strong> <?php echo $museum->getHolder()?></li>  
                                                        <li><strong><?php echo __('Phone')?>: </strong><?php echo $museum->getPhone()?> </li>
                                                        <li><strong><?php echo __('Address')?>: </strong><?php echo $museum->getAddress()?> </li> 
                                                        <li><strong><?php echo __('Email')?>: </strong> <a href="#"><?php echo $museum->getEmail()?></a></li>
                                                </li>
                                        </ul>
                                        <!-- ENDS meta -->	

                                </div>
                                <!-- ENDS post -->

    </div>
                    <!-- sidebar -->
                    <ul id="sidebar">
                            <!-- init sidebar -->
                                    <li>
                                    <h6><?php echo __('Our Museums')?></h6>		
                                     <?php $museos =Doctrine_Query::create()
                                                     ->from('Institution')
                                                     ->where('is_active =?', 0)
                                                     ->orderBy('name')
                                                      ->execute();?>                                                               
                                    <ul>
                                         <?php foreach ($museos as $museo):?>
                                               <?php if($museo->getId() != $museum->getId() ):?>
                                                <li class="cat-item" ><a title="<?php echo __('Read more')?>" href="<?php echo url_for('museos/museum?id='.$museo->getId());?>"><?php echo $museo->getName()?></a></li>  
                                               <?php endif?> 
                                         <?php endforeach;?> 
                                    </ul>

                            </li>
                            <!-- ENDS init sidebar -->
                    </ul>
                    <!-- ENDS sidebar -->
				<br/>									
    </div>
                    
    
   <div id="content_2">
        <div id="content" style="margin-left: ; color: #369 ; font-size:25px;">    
                <ul id="portfolio-list" class="gallery" style="color: #369;">
                   <?php foreach ($museum->getPictures() as $pic):?>
                         <li class="" >
                             <a   style="background: #444; color: #369; "href="/museo/<?php echo $web?>uploads/museo/<?php echo $pic->getFile() ?>" rel="group1" class="fancybox" title="<?php echo  $pic->getTitle() ?> <?php echo  $pic->getDescription() ?>" >
                                    <center>
                                        <?php echo image_tag('../uploads/museo/'. $pic->getFile(), 'style=" height: 148px; max-width: 204px"')?>
                                    </center> 
                             </a>

                        </li>
                    <?php endforeach;?>    
                </ul>
            </div>   
   </div>     