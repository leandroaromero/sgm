
<!-- pagination -->	
						<div class="clear"></div>
						<ul class='pager'>
							<li class='first-page'><a href='<?php echo url_for('@museos') ?>?page=1'>&laquo;</a></li>
							<li><a href='<?php echo url_for('@museos') ?>?page=<?php echo $pager->getPreviousPage() ?>' >&lsaquo;</a></li>
                                                          <?php foreach ($pager->getLinks() as $page): ?>
                                                            <?php if ($page == $pager->getPage()): ?>
                                                            <li class='active'> <a> <?php echo $page ?></a></li>
                                                            <?php else: ?>
                                                            <li>  <a href="<?php echo url_for('@museos') ?>?page=<?php echo $page ?>"><?php echo $page ?></a></li>
                                                            <?php endif; ?>
                                                          <?php endforeach; ?>
							<li><a href='<?php echo url_for('@museos') ?>?page=<?php echo $pager->getNextPage() ?>' >&rsaquo;</a></li>
							<li class='last-page'><a href='<?php echo url_for('@museos') ?>?page=<?php echo $pager->getLastPage() ?>'>&raquo;</a></li>
						</ul>
						<!-- ENDS pagination -->