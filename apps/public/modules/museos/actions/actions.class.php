<?php

/**
 * museos actions.
 *
 * @package    museo
 * @subpackage museos
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class museosActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->museums = Doctrine_Core::getTable('Institution')
      ->createQuery('a')
      ->where('a.is_active = ?', 0)
      ->orderBy('a.name');
   //   ->execute();
    $this->pager = new sfDoctrinePager( 'Institution', 4);
    $this->pager->setQuery($this->museums);
    $this->pager->setPage($request->getParameter('page', 1));
    $this->pager->init();
    
  }
  public function executeMuseum($request) {
      $this->museum= Doctrine::getTable('Institution')->find($request->getParameter('id'));
      
  }
}
