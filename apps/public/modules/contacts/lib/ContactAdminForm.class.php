<?php

/**
 * Contact form.
 *
 * @package    museo
 * @subpackage form
 * @author     LeandroARomero
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ContactAdminForm extends BaseContactForm
{
  public function configure()
  {
      unset ($this['created_at'], $this['updated_at']);
      
        sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));
      $this->widgetSchema['comment']->setOption('label',__('Message'));
      $this->widgetSchema['institucion_id']->setOption('label',__('Institutions'));
      $this->widgetSchema['ip_address'] = new sfWidgetFormInputHidden();
      $this->widgetSchema['ip_address']->setAttribute('value',  $_SERVER['REMOTE_ADDR']);
      
      $this->widgetSchema['captcha'] = new sfWidgetCaptchaGD();
      $this->validatorSchema['captcha'] = new sfCaptchaGDValidator(array('length' => 5));
      
      $this->validatorSchema['email'] = new sfValidatorAnd( array( $this->validatorSchema['email'], new sfValidatorEmail(), ));
      $this->validatorSchema['email']->setMessage('required', 'Este campo es obligatorio');
      $this->validatorSchema['email']->setMessage('invalid', '"%value%" no es una dirección de email válida');

  }
}
