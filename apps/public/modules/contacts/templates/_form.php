<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('jQuery'); ?>

<div id="formulario"> 
        <form  id="contactForm" action="<?php echo url_for('contacts/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
        <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
        <?php endif; ?>
          
         
            <fieldset>
		<div>
                   <?php echo $form->renderGlobalErrors() ?>
                </div>    
                <div>
                  <label><?php echo $form['name']->renderLabel() ?></label>  
                  <?php echo $form['name']->renderError() ?>
                  <?php echo $form['name'] ?>
               </div>    
               <div>
                  <label> <?php echo $form['email']->renderLabel() ?></label> 
                  <?php echo $form['email']->renderError() ?>
                  <?php echo $form['email'] ?>
                    
                </div>
                <div>
                  <label><?php echo $form['institucion_id']->renderLabel() ?></label>  
                  <?php echo $form['institucion_id']->renderError() ?>
                  <?php echo $form['institucion_id'] ?> </br>
                  <i>"No es obligatorio seleccionar institución"</i>
               </div> 
                <div> 
                  <label> <?php echo $form['comment']->renderLabel() ?></label>   
                  <?php echo $form['comment']->renderError() ?>
                  <?php echo $form['comment'] ?>
                </div> 
                <div> 
                  <label> <?php echo $form['captcha']->renderLabel() ?></label>   
                  <?php echo $form['captcha']->renderError() ?>
                  <?php echo $form['captcha'] ?>
                </div>
                
                <p>
                  <?php echo $form->renderHiddenFields(false) ?>
                  &nbsp;<a href="<?php echo url_for('contacts/index') ?>"><?php echo __('Cancel')?></a>
                  <?php if (!$form->getObject()->isNew()): ?>
                    &nbsp;
                  <?php endif; ?>
                    <?php echo jq_submit_to_remote(  '' , __('Send'),
                      $options = array(
                                        'update' => 'formulario',
                                        'url'    => 'contacts/create',
                                        'loading'  => "$('#indicator').show(); $('#formulario').hide()", 
                                        //'loading'  => "Element.show('indicator'); \$('tag').value = ''",
                                        'complete' => "$('#formulario').show(); $('#indicator').hide();",
                                      ), 
                                        $options_html = array( 
                                          'class'=> 'submit',
                                          'id' => 'submit'
                                          #'style'=>'color:#fff'
                                      ))?>  
                  </p>
           </fieldset>   
        </form>

</div>
	<div id="indicator" style="display: none ">
      <h2>Enviando mensaje... </h2>
  </div>