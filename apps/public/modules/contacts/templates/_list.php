<ul class="commentlist">
    <?php foreach ($comments as $i => $comment): ?>
            <li class="comment_<?php echo fmod($i, 2) ? 'even' : 'odd' ?>">
            <div class="author"><img class="avatar" src="../images/images/demo/avatar.gif" width="32" height="32" alt="" />
                <span class="name"><a href="#"><?php echo $comment->getName()?></a></span> <span class="wrote"><?php echo __('wrote')?>:</span>
            </div>
            <div class="submitdate"><a href="#"><?php echo $comment->getCreatedAt()?></a></div>
            <p><?php echo $comment->getComment()?>.</p>
          </li>
    <?php endforeach; ?>
</ul>

