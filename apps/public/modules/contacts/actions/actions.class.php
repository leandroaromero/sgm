<?php

/**
 * contacts actions.
 *
 * @package    museo
 * @subpackage contacts
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class contactsActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->contacts = Doctrine_Core::getTable('Contact')
      ->createQuery('a')
      ->orderBy('a.created_at Desc');
   //   ->execute();
    $this->pager = new sfDoctrinePager( 'Contact', 10);
    $this->pager->setQuery($this->contacts);
    $this->pager->setPage($request->getParameter('page', 1));
    $this->pager->init();

    $this->form = new ContactAdminForm();
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new ContactAdminForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new ContactAdminForm();

    $this->processForm($request, $this->form);
/*
    $this->contacts = Doctrine_Core::getTable('Contact')
      ->createQuery('a')
      ->orderBy('a.created_at Desc');
   //   ->execute();
    $this->pager = new sfDoctrinePager( 'Contact', 10);
    $this->pager->setQuery($this->contacts);
    $this->pager->setPage($request->getParameter('page', 10));
    $this->pager->init();*/
    $this->setTemplate('index');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($contact = Doctrine_Core::getTable('Contact')->find(array($request->getParameter('id'))), sprintf('Object contact does not exist (%s).', $request->getParameter('id')));
    $this->form = new ContactAdminForm($contact);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($contact = Doctrine_Core::getTable('Contact')->find(array($request->getParameter('id'))), sprintf('Object contact does not exist (%s).', $request->getParameter('id')));
    $this->form = new ContactAdminForm($contact);

    $this->processForm($request, $this->form);

    $this->setTemplate('index');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($contact = Doctrine_Core::getTable('Contact')->find(array($request->getParameter('id'))), sprintf('Object contact does not exist (%s).', $request->getParameter('id')));
    $contact->delete();

    $this->redirect('contacts/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $contact = $form->save();
      $this->sendConfirmationEMail($contact);
      $this->redirect('contacts/send');
    }
  }
  public function executeSend($request) {
      
  }

  
  protected function sendConfirmationEMail($contact) {

/*
      $message = $this->getMailer()->compose(
        array('elderleans@gmail' => 'Patrimonios Chaco'), 'leandroaromero@yahoo.com.ar',
         'Gracias');
      $message->addPart($this->getPartial('contacts/email_verification_mail', array('name'=> $contact->getName())), 'text/html');
      $this->getMailer()->send($message);*/
  // correo de saludos a la persona que se contacta
    $from= array('sgpchaco@gmail.com ' => 'Patrimonios Chaco');
    $to  = $contact->getEmail();
    $subject = 'Saludos';
    $body= '';
    $message = $this->getMailer()->compose( $from, $to, $subject );
    $message->addPart($this->getPartial('contacts/email_verification_mail', array( 'contacto' => $contact )), 'text/html');
    
      $this->getMailer()->send($message); 

     // correo de saludos a la persona que se contacta
    $from= array('sgpchaco@gmail.com ' => 'Patrimonios Chaco');
    $to  =  array( sfConfig::get('app_correos_webmaster'), sfConfig::get('app_correos_admin') );
    $subject = 'Aviso';
    $body= '';
    $message = $this->getMailer()->compose( $from, $to, $subject );
    $message->addPart($this->getPartial('contacts/email_alert', array( 'contacto' => $contact )), 'text/html');
      $this->getMailer()->send($message); 
  }
  public function executeComment(){
      
  }
}
