<!-- title -->
        <div id="page-title">
                <span class="title"><?php echo __('Search')?></span>
                <span class="subtitle"><?php echo __('Advanced of Object')?></span>
        </div>


                    <form action="<?php echo url_for('gallery/searchAvanzado') ?>" method="post" >
                        <div id="fields">
                            
                            <table class="table">
                            <tbody>
                                <tr>
                                    <td style="border: 0px">
                                        <?php echo $form['museo']->renderLabel() ?>
                                    </td>
                                    <td style="border: 0px">
                                        <?php echo $form['museo']->renderError() ?>
                                        <?php echo $form['museo']->render(array('class' => 'number')) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 0px">
                                        <?php echo $form['nombre']->renderLabel() ?>
                                    </td>
                                    <td style="border: 0px">
                                        <?php echo $form['nombre']->renderError() ?>
                                        <?php echo $form['nombre']->render() ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 0px">
                                        <?php echo $form['descripcion']->renderLabel() ?>
                                    </td>
                                    <td style="border: 0px">
                                        <?php echo $form['descripcion']->renderError() ?>
                                        <?php echo $form['descripcion']->render(array('class' => 'number')) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 0px">
                                        <?php echo $form['autor']->renderLabel() ?>
                                    </td>
                                    <td style="border: 0px">
                                        <?php echo $form['autor']->renderError() ?>
                                        <?php echo $form['autor']->render(array('class' => 'number')) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 0px">
                                        <?php echo $form['coleccion']->renderLabel() ?>
                                    </td>
                                    <td style="border: 0px">
                                        <?php echo $form['coleccion']->renderError() ?>
                                        <?php echo $form['coleccion']->render(array('class' => 'number')) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 0px">
                                        <?php echo $form['material']->renderLabel() ?>
                                    </td>
                                    <td style="border: 0px">
                                        <?php echo $form['material']->renderError() ?>
                                        <?php echo $form['material']->render(array('class' => 'number')) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 0px">
                                        <?php echo $form['tipo']->renderLabel() ?>
                                    </td>
                                    <td style="border: 0px" >
                                        <?php echo $form['tipo']->renderError() ?>
                                        <?php echo $form['tipo']->render(array('class' => 'number')) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 0px">
                                        <?php echo $form['categoria']->renderLabel() ?>
                                    </td>
                                    <td style="border: 0px" >
                                        <?php echo $form['categoria']->renderError() ?>
                                        <?php echo $form['categoria']->render(array('class' => 'number')) ?>
                                    </td>
                                </tr>
                             </tbody>
                                <tfoot>
                                  <tr>
                                    <td colspan="2">
                                        <center><input type="submit" value="<?php echo __('Send') ?>" /></center> 

                                    </td>
                                  </tr>
                                </tfoot>
                          </table>
                       </div>
                </form>

