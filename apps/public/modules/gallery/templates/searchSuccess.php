        <!-- title -->
        <div id="page-title">
                <span class="title"><?php echo __('The Gallery')?> </span>
                <span class="subtitle"><?php echo __('Object list')?></span>
               
        </div>
         <div style="display: inline; float: right; margin-right: 40px ">
             <a href="<?php echo url_for('gallery/avanzada')?>" title="<?php echo __('Advanced search')?>"><?php echo image_tag('public/img/mono-icons/linedpaper32.png', array('width'=>'16' ))?></a>
             <a href="<?php echo url_for('gallery/index')?>" title="<?php echo __('Search')?>"><?php echo image_tag('public/img/mono-icons/search32.png', array('width'=>'16' ))?></a>
             <a href="<?php echo url_for('gallery/cambiarVista')?>" title="<?php echo __('Change views')?>" >
                <?php if( $sf_user->getAttribute('listado_cuadrado', false)== true ):?>
                        <?php echo image_tag('lista_g.png')?>
                <?php else:?>
                        <?php echo image_tag('galeria-g.png')?>
                <?php endif?>
            </a>
        </div>
        <div>
            <b><?php echo __('Institution')?>: </b><?php echo $search['museo']?><br/>
            
            <?php if(isset($search['general'])&& ($search['general'] !='')) { ?>
                    <b><?php echo __('Search')?>:      </b><?php echo $search['general']?><br/>
            <?php }
                if(isset($search['nombre'])&& ($search['nombre'] !='')){ ?>
                    <b><?php echo __('Name of the Object')?>: </b><?php echo $search['nombre']?><br/>
            <?php }
                if(isset($search['descripcion']) && ($search['descripcion'] !='')){ ?>
                    <b><?php echo __('Generic Description')?>: </b><?php echo $search['descripcion']?><br/>
            <?php }
                if(isset($search['autor'])&& ($search['autor'] !='')){ ?>
                    <b><?php echo __('Author')?>:      </b><?php echo $search['autor']?><br/>
           
            <?php }
                if(isset($search['coleccion'])&& ($search['coleccion'] !='')){ ?>
                    <b><?php echo __('Colection')?>: </b><?php echo $search['coleccion']?><br/>
            <?php }
                if(isset($search['material'])&& ($search['material'] !='')){ ?>
                    <b>Material: </b><?php echo $search['material']?><br/> 
            <?php }
                if(isset($search['tipo'])&& ($search['tipo'] !='')){ ?>
                    <b><?php echo __('Type of the Object')?>: </b><?php echo $search['tipo']?><br/>
            <?php }
                if(isset($search['categoria'])&& ($search['categoria'] !='')){ ?>
                    <b><?php echo __('Category')?>: </b><?php echo $search['categoria']?><br/>
            <?php } ?>

        </div> 
        
        
            <?php if( $sf_user->getAttribute('listado_cuadrado', false)== true ):?>
                  <?php include_partial('gallery/list_cuadrado',array('result'=> $objetos_pa) ) ?>   
            <?php else: ?>   
                  <?php include_partial('gallery/list',array('result'=> $objetos_pa)) ?>     
            <?php endif?>
            
            <?php include_partial('gallery/pagination', array('pager' => $objetos_pa)) ?>