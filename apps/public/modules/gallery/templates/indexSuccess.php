<!-- title -->
        <div id="page-title">
                <span class="title"><?php echo __('Search')?></span>
                <span class="subtitle"><?php echo __('Of the Objects')?></span>
        </div>

<div class="wrapper col2">
      <div id="container" class="clear">
          <div id="content" style="min-height: 300px;">
                 <div id="respond">
                        <form action="<?php echo url_for('gallery/search') ?>" method="post" >
                            <center>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td  style="border: 0px">
                                                <?php echo $form['museo']->renderLabel() ?>
                                            </td>
                                            <td style="border: 0px" >
                                                <?php echo $form['museo']->renderError() ?>
                                                <?php echo $form['museo']->render(array('class' => 'number')) ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: 0px">
                                                <?php echo $form['general']->renderLabel() ?>
                                            </td>
                                            <td style="border: 0px">
                                                <?php echo $form['general']->renderError() ?>
                                                <?php echo $form['general']->render(array('class' => 'number')) ?>
                                                <input type="submit" value="<?php echo __('Send') ?>" />
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                           </center>
                        </form>
                  </div>
            </div>
      </div>
</div>

        