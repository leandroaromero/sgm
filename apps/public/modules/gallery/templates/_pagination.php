
<!-- pagination -->	
    <div class="clear"></div>
    <ul class='pager'>
            <li class='first-page'><a href='<?php echo url_for('@objeto_pag') ?>?page=1'>&laquo;</a></li>
            <li><a href='<?php echo url_for('@objeto_pag') ?>?page=<?php echo $pager->getPreviousPage() ?>' >&lsaquo;</a></li>
              
            <?php foreach ($pager->getLinksPage() as $pageL): ?>
                <?php if ($pager->getPageNumber() == $pageL): ?>
                      <li class='active'> <a> <?php echo $pageL ?></a></li>
                <?php else: ?>
                      <li>  <a href="<?php echo url_for('@objeto_pag') ?>?page=<?php echo $pageL ?>"><?php echo $pageL ?></a></li>
                <?php endif; ?>
             <?php endforeach; ?>
                
            <li><a href='<?php echo url_for('@objeto_pag') ?>?page=<?php echo $pager->getNextPage() ?>' >&rsaquo;</a></li>
            <li class='last-page'><a href='<?php echo url_for('@objeto_pag') ?>?page=<?php echo $pager->fetchNumberPages() ?>'>&raquo;</a></li>
    </ul>
    <!-- ENDS pagination -->