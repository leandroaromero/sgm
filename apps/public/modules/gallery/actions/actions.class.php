<?php

/**
 * gallery actions.
 *
 * @package    museo
 * @subpackage gallery
 * @author     LeandroARomero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class galleryActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
      $this->form= new ObjetoPublicAFormFilter();
    
  }
  public function executeAvanzada(sfWebRequest $request)
  {
      $this->form= new ObjetoPublicAFormFilter();
    
  }
  public function executeSearch(sfWebRequest $request)
  {
      
      ini_set("max_execution_time",0);
      ini_set('memory_limit', '-1');
      $cambio= $request->getParameter('search');
      $first=true;
      
      $this->search = new SearchBuilder($cambio);
      $this->getUser()->setAttribute('search_more', $this->search->getBusquedas());
      $general= $cambio['general'];
      $repetidos=array();
      $buscar= $this->armarBusqueda($general);
$t='';
foreach ($buscar as $b)
      $t= $t.'+'.$b;

//die($t);
      
      $this->objetos= array();
      $cont_index=0;
      
      foreach ($buscar as $b ){  
          foreach($this->search->getBasesDatos() as $bd){

              $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);


              $q = Doctrine_Query::create($conn)
                   ->select('c.id, c.fecha_baja')
                   ->from('Objeto c')  
                   ->leftJoin('c.Autor a')   
                   ->leftJoin('a.Persona p')  
                   ->leftJoin('c.TipoObjeto too')
                   ->leftJoin('c.ObjetoColeccion oc')
                   ->leftJoin('oc.Coleccion occ')   
                   ->leftJoin('c.CategoriaObjeto oCat')
                   ->leftJoin('oCat.Categoria cat')   
                   ->leftJoin('c.ObjetoMaterial om')
                   ->leftJoin('om.Material m');
                     if($first){
                    //    $search= array_splice($buscar,1,0 ); 
                        $search= $buscar;  
                        $q= $this->getQuerySearchFirst($q, $search );
                      } else{
                          $q=  $this->getQuerySearchSecond($q, $b);
                      }
                  $q->groupBy('c.id');
                           

              $objetos= array();    
              $key= null;
              foreach ($q->fetchArray() as $key => $value) {       
                  if ( is_null($value['fecha_baja'])){
                        $objetos[]= $value['id'];
                  }
                }  
              $objetos_a = array();                     
              if( !is_null($key)){            
                $q = Doctrine_Query::create($conn)
                  ->from('Objeto c')
                  ->leftJoin('c.Imagen i')  
                  ->whereIn('c.id', $objetos) 
                  ->andWhere('i.objeto_id > ? ', 0 ) 
                  ->groupBy('c.id');
              
             $objetos_a = $q->execute();
             }  
            foreach ($objetos_a as $obj){
                if(! in_array($bd['file'].$obj->getId() , $repetidos)){
                    $objn= new Publico($obj, $bd, $cont_index);
                    $this->objetos[]= $objn;
                    $cont_index++;
                    $repetidos[]= $bd['file'].$obj->getId();
                }
            } 

          } 
       $first=false;
       $objetos_a = array();
      }
    
    $this->getUser()->setAttribute('result', $this->objetos);
    $this->objetos_pa=  new Paginar($this->objetos, 12); 
    $this->search = $this->search->getBusquedas();
  }  
  
  public function armarBusqueda($buscar){
      $busquedas=  $this->multiexplode(array(",",".","|",":"," ",";","+"), $buscar);
   //   $busqueda= array_unshift($busquedas, $buscar );
     if(empty($busquedas))
        $busquedas[]='';
      return $busquedas;
  }
function multiexplode ($delimiters,$string) {
    
    $ready = str_replace($delimiters, $delimiters[0], $string);
    $launch = explode($delimiters[0], $ready);
    return  array_filter($launch);
}
  public function executeSearchAvanzado(sfWebRequest $request)
  {
      
     // ini_set("max_execution_time",0);
     // ini_set('memory_limit', '-1');
      $cambio= $request->getParameter('search');
      
      $this->search = new SearchBuilder($cambio);
      $this->getUser()->setAttribute('search_more', $this->search->getBusquedas());
      
      $this->objetos= array();
      $cont_index=0;
      $searchElto=array();
       if($cambio['nombre'])    
           $searchElto['nombre']= $this->armarBusqueda($cambio['nombre']);
       if($cambio['descripcion'])    
           $searchElto['nombre']= $this->armarBusqueda($cambio['descripcion']);
       if($cambio['autor'])
           $searchElto['autor']= $this->armarBusqueda($cambio['autor']); 
       if($cambio['coleccion'])
           $searchElto['coleccion']= $this->armarBusqueda($cambio['coleccion']);    
       if($cambio['material'])
           $searchElto['material']= $this->armarBusqueda($cambio['material']);
      
      foreach($this->search->getBasesDatos() as $bd){
          
          
          $conn= Doctrine_Manager::getInstance()->getConnection($bd['file']);
          $q = Doctrine_Query::create($conn)
               ->from('Objeto c')
               ->andWhere('c.fecha_baja is NULL');
              if($cambio['nombre'])    
                $q = $this->getQueryNombre ($q, $searchElto['nombre']);
              
              if($cambio['descripcion'])    
               $q->andWhere('c.descripcion_generica like ?', '%'.$cambio['descripcion'].'%');
              
              
              if($cambio['autor']){
                $q->leftJoin('c.Autor a')   
                  ->leftJoin('a.Persona p');  
               $q= $this->getQueryAutor($q, $searchElto['autor']);
              } 
             
              
              if($cambio['tipo'] != 'todos'){    
               $q->leftJoin('c.TipoObjeto too')
               ->andWhere('too.nombre like ?', '%'.$cambio['tipo'].'%')   ;
              }
              if($cambio['categoria'] != 'todos'){
               $q->leftJoin('c.CategoriaObjeto oCat')
               ->leftJoin('oCat.Categoria cat')   
               ->andWhere('cat.nombre like ?', '%'.$cambio['categoria'].'%');   
              }        
             
              ////////////////////////////////
              if($cambio['coleccion']){    
               $q= $this->getQueryColeccion($q, $searchElto['coleccion']);
              }
              ///////////////////////////////// 
              if($cambio['material']){   
                $q= $this->getQueryMateriales($q, $searchElto['material']);
              }
         $objetos_a = $q->execute();
        
        foreach ($objetos_a as $obj){
                if ( is_null($obj->getFechaBaja())){
                    $objn= new Publico($obj, $bd, $cont_index);
                    $this->objetos[]= $objn;
                    $cont_index++;
                }
        } 

        $objetos_a = array();
      }
    $this->getUser()->setAttribute('result', $this->objetos);
    $this->objetos_pa=  new Paginar($this->objetos, 20); 
    $this->search = $this->search->getBusquedas();
    $this->setTemplate('search');
     
  }
  
  
  public function executeResult($request) {
      if($this->getUser()->hasAttribute('result')){
              $result =$this->getUser()->getAttribute('result');
              $this->index=$request->getParameter('index');
              $this->objeto = $result[$this->index];
              $cont=0;
              $this->setLayout('layout_acervo');
      }else{
          $museo= $request->getParameter('museo');
          $acervo= $request->getParameter('acervo_id');
                    
          $conn= Doctrine_Manager::getInstance()->getConnection($museo);
          $obj= Doctrine_Query::create($conn)
               ->from('Objeto c')
               ->where('c.id =?', $acervo)
               ->fetchOne();   
//          $obj= $q->execute();
          $this->getUser()->setDataBase($museo);
          $array= array('file'=>$museo,'name' =>$this->getUser()->getInstitutionName(), 'subname'=>$this->getUser()->getInstitutionSubName());
          $this->index=$request->getParameter('index');
          $this->objeto= new Publico($obj, $array, 0);
      }       
    $this->getUser()->aumentarContador($this->objeto->getFile(), $this->objeto->getId());

  }  
  
  public function executePaginar($request) {
      
      $this->objetos  = $this->getUser()->getAttribute('result');
      $this->search   = $this->getUser()->getAttribute('search_more');
      
      $pager=$request->getParameter('page');
      $this->getUser()->setAttribute('pagination', $pager);
      $this->objetos_pa=  new Paginar($this->objetos,20,$pager);
      $this->setTemplate('search');
      
  }
   public function executeCambiarVista($request)
    {
      if($this->getUser()->getAttribute('listado_cuadrado', false)== true){
          
            $this->getUser()->setAttribute('listado_cuadrado', false );
      }else{
          $this->getUser()->setAttribute('listado_cuadrado', true );
      }
      
      $page   = $this->getUser()->getAttribute('pagination');
      $this->redirect('gallery/paginar?page='.$page);

   }

   function getQuerySearchFirst( $q, $buscar){
           $banderas= array(); 
               foreach ($buscar as $bi ){ 
                        if(! in_array('nombre', $banderas)){
                             $q->where('c.nombre_objeto like ?', '%'.$bi.'%')->andWhere('c.fecha_baja is NULL');
                             $banderas[]='nombre';
                        }else{
                             $q->andWhere('c.nombre_objeto like ?', '%'.$bi.'%');
                        }  
                     }
                     
                        ///////////////////////////////
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('descripcion', $banderas)){
                             $q->orWhere('c.descripcion_generica like ?', '%'.$bi.'%')->andWhere('c.fecha_baja is NULL') ;
                             $banderas[]='descripcion';
                        }else{     
                             $q->andWhere('c.descripcion_generica like ?', '%'.$bi.'%');
                        }
                     }  
                        //////////////////////////////////             
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('persona', $banderas)){
                              $q->orWhere('p.nombre like ?', '%'.$bi.'%');
                              $banderas[]='persona';        
                        }else{
                            $q->andWhere('p.nombre like ?', '%'.$bi.'%');
                        }      
                     }  
                        //////////////////////////////////             
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('apellido', $banderas)){
                              $q->orWhere('p.apellido like ?', '%'.$bi.'%');
                              $banderas[]='apellido';        
                        }else{
                            $q->andWhere('p.apellido like ?', '%'.$bi.'%');
                        }      
                     }  
                        //////////////////////////////////             
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('tiponombre', $banderas)){
                              $q->orWhere('too.nombre like ?', '%'.$bi.'%');
                              $banderas[]='tiponombre';        
                        }else{
                            $q->andWhere('too.nombre like ?', '%'.$bi.'%');
                        }      
                     } 
                        //////////////////////////////////             
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('categoria', $banderas)){
                              $q->orWhere('cat.nombre like ?', '%'.$bi.'%');
                              $banderas[]='categoria';        
                        }else{
                            $q->andWhere('cat.nombre like ?', '%'.$bi.'%');
                        }      
                     }
                        //////////////////////////////////             
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('coleccion', $banderas)){
                              $q->orWhere('occ.nombre_coleccion like ?', '%'.$bi.'%');
                              $banderas[]='coleccion';        
                        }else{
                            $q->andWhere('occ.nombre_coleccion like ?', '%'.$bi.'%');
                        }      
                     }
                        ////////////////////////////////             
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('material', $banderas)){
                              $q->orWhere('m.nombre_material like ?', '%'.$bi.'%');
                              $banderas[]='material';        
                        }else{
                            $q->andWhere('m.nombre_material like ?', '%'.$bi.'%');
                        }      
                     }
                     return $q;
       
   }

   function getQuerySearchSecond($q, $search){
                      $q->orwhere('c.nombre_objeto like ?', '%'.$search.'%')
                       ->orWhere('c.descripcion_generica like ?', '%'.$search.'%')
                       ->orWhere('p.nombre like ?', '%'.$search.'%')
                       ->orWhere('p.apellido like ?', '%'.$search.'%')
                       ->orWhere('too.nombre like ?', '%'.$search.'%') 
                       ->orWhere('cat.nombre like ?', '%'.$search.'%')                     
                       ->orWhere('occ.nombre_coleccion like ?', '%'.$search.'%') 
                       ->orWhere('m.nombre_material like ?', '%'.$search.'%');  
                        
       return $q;
   }
   public function getQueryNombre($q, $buscar)
       {
             foreach ($buscar as $bi ){ 

                             $q->andWhere('c.nombre_objeto like ?', '%'.$bi.'%');              

             }
                  return $q;    
    }
   public function getQueryAutor($q, $buscar){
                  
         foreach ($buscar as $bi ){    

                  $q->andWhere('p.nombre like ? OR p.apellido like ?', array('%'.$bi.'%', '%'.$bi.'%'));

         }  
         return $q;

   }
   public function getQueryColeccion($q, $buscar){
                  
         foreach ($buscar as $key => $bi ){    
             
                $q->andWhere('c.id IN (
                                         SELECT obj'.$key.'.id 
                                         FROM Objeto obj'.$key.'
                                         LEFT JOIN obj'.$key.'.ObjetoColeccion oc'.$key.'
                                         LEFT JOIN oc'.$key.'.Coleccion occ'.$key.'
                                         WHERE occ'.$key.'.nombre_coleccion like ?)', '%'.$bi.'%');

         }  
         return $q;

   }
   public function getQueryMateriales($q, $buscar){
                  
         foreach ($buscar as $key => $bi ){    
                $q->andWhere('c.id IN (
                                         SELECT obj_m'.$key.'.id 
                                         FROM Objeto obj_m'.$key.'
                                         LEFT JOIN obj_m'.$key.'.ObjetoMaterial om'.$key.'
                                         LEFT JOIN om'.$key.'.Material m'.$key.'
                                         WHERE m'.$key.'.nombre_material like ?)', '%'.$bi.'%');

         }  
         return $q;

   }
}