<?php

/**
 * Objeto filter form.
 *
 * @package    museo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObjetoPublicAFormFilter extends BaseForm #BaseObjetoFormFilter
{
  public function configure()
  {
        $parameter = sfConfig::get('app_institucion_data', array(''=>'') );
        sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));
        $choices    = $parameter; //array('' => '') + $parameter; 

        $this->widgetSchema['museo'] = new sfWidgetFormChoice(
                                            array(
                                                    'choices'   => $choices,
                                                    'multiple'  => false,
                                                    'label'     => __('Institutions'),
                                                    'expanded'  => false,
                                                  ),
                                            array( 'style'=>'width:376px;height:37px;font-size:17px')
                                                            );
        $this->widgetSchema['general']= new sfWidgetFormInput(
                                            array(
                                                'label'=>__('Search')
                                            ),
                                            array( 'style'=>'width:360px;height:25px;font-size:17px'));
        
        $this->widgetSchema['nombre']= new sfWidgetFormInput(array(
            'label'=> __('Name of the Object')
        ),array( 'style'=>'width:360px;height:25px;font-size:17px'));
        
        $this->widgetSchema['descripcion']= new sfWidgetFormInput(array(
                                                                       'label'=>__('Generic Description')
                                                                        ),array( 'style'=>'width:360px;height:25px;font-size:17px'));
        $this->widgetSchema['autor']= new sfWidgetFormInput(array(
            'label'=>__('Author')
        ),array( 'style'=>'width:360px;height:25px;font-size:17px'));
        
        $parameter = sfConfig::get('app_search_tipo', array(''=>'') );
        $choices    = $parameter; //array('' => '') + $parameter; 

        $this->widgetSchema['tipo'] = new sfWidgetFormChoice(
                                                                array(
                                                                    'choices'   => $choices,
                                                                    'multiple'  => false,
                                                                    'label'     => __('Type of the Object'),
                                                                    'expanded'  => false,
                                                                    ),
                                                                array( 'style'=>'width:376px;height:37px;font-size:17px')
                                                            );
        $parameter = sfConfig::get('app_search_categoria', array(''=>'') );
        $choices    = $parameter; //array('' => '') + $parameter; 

        $this->widgetSchema['categoria'] = new sfWidgetFormChoice(
                                                array(
                                                    'choices'   => $choices,
                                                    'multiple'  => false,
                                                    'label'     => __('Category'),
                                                    'expanded'  => false,
                                                ),
                                                array( 'style'=>'width:376px;height:37px;font-size:17px')
                                            );

        $this->widgetSchema['coleccion']= new sfWidgetFormInput(
                                                array(
                                                    'label'=> __('Colection'),
                                                ),
                                                array( 'style'=>'width:360px;height:25px;font-size:17px')
                                            );


        $this->widgetSchema['material']= new sfWidgetFormInput(
                                             array(),
                                             array( 'style'=>'width:360px;height:25px;font-size:17px')
                                         );
        
        
        
        $this->widgetSchema->setNameFormat('search[%s]'); 
  }


}
