
<script type="text/javascript">
$(document).ready(function () {
    $("#tabcontainer").tabs({
        event: "click"
    });
    $("a[rel^='prettyPhoto']").prettyPhoto({
        theme: 'dark_rounded'
    });
});
</script>

<!-- ####################################################################################################### -->
<div class="wrapper col2">
  <div id="container" class="clear">
    <!-- ####################################################################################################### -->

    <div id="tabcontainer">
      <ul id="tabnav">
        <li><a href="#tabs-1"><?php echo __('Pictures')?></a></li>
        <li><a href="#tabs-2"><?php echo __('Information')?></a></li>
        <li><a href="#tabs-3"><?php echo __('Materials')?></a></li>
       
      </ul>
      <!-- ########### -->
      <div id="tabs-1" class="gallery clear">
       <div id="cuerpo"  style="min-height:300px">
        <ul>
          <?php  $con=0; $i=1; foreach ($objeto->getImagen() as $picture): $odd = fmod(++$i, 2) ;$band=false; ?>
          <?php endforeach?>
          <li><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 1"><img src="../images/images/demo/gallery/1.gif" alt="" /></a></li>
          <li><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 2"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
          <li><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 3"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
          <li><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 4"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
          <li><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 5"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
          <li class="last"><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 6"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
          <li><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 7"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
          <li><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 8"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
          <li><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 9"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
          <li><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 10"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
          <li><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 11"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
          <li class="last"><a href="../images/images/demo/portfolioslider/1.gif" rel="prettyPhoto[gallery1]" title="Image 12"><img src="../images/images/demo/gallery/150x150.gif" alt="" /></a></li>
        </ul>
       </div>
      </div>
      <!-- ########### -->
      <div id="tabs-2" class="gallery clear">
          <div id="cuerpo"  style="min-height:300px">
              <h2><b><?php echo $objeto->getNombreObjeto() ?></b></h2>
        
        <p><b> Descripción genérica: </b><?php echo $objeto->getDescripcionGenerica() ?>.</p>
        <p><b> Estado de conservación: </b><?php echo $objeto->getEstadoDeConservacion() ?>.</p>
        <?php if($objeto->getAutorId()): ?>
          <p><b> Autor: </b><?php echo $objeto->getAutor() ?>.</p>
        <?php endif;
              if($objeto->getAnioOrigen()): ?>
          <p><b> Año origen: </b><?php echo $objeto->getAnioOrigen() ?>.</p>
        <?php endif;
              if($objeto->getTextoAntiguedad()): ?>
          <p><b> Texto antigüedad: </b><?php echo $objeto->getTextoAntiguedad() ?>.</p>
        <?php endif;
              if($objeto->getHistoriaDelObjeto()): ?>
          <p><b> Historia del objeto: </b><?php echo $objeto->getHistoriaDelObjeto() ?>.</p>
        <?php endif; ?>
        
      </div>  
      </div>
      <!-- ########### -->
      <div id="tabs-3" class="gallery clear">
          <div id="cuerpo"  style="min-height:300px">
          <h2><b>Materiales</b> </h2><br/>
           
             El acervo esta compuesto de los siguientes materiales: <br/><br/>
             <ul>
             <?php foreach($objeto->getObjetoMaterial() as $ma):?>
               
                 <p><?php echo $ma->getMaterial(); ?> </p>

             <?php endforeach?>
             </ul>
            
          </div>
      </div>
      <!-- ########### -->
    </div>
    <!-- ####################################################################################################### -->

     </div>
        </div>
