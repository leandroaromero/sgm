
<!-- title -->
        <div id="page-title">
                <span class="title"><?php echo $objeto->getNombreObjeto()?></span>
                <span class="subtitle"><?php echo $objeto->getDescripcionGenerica()?></span>
        </div>

	
      	<ul id="portfolio-list" class="gallery three-cols">
          <?php  foreach ($objeto->getImagen() as $picture):  ?>
              <li class="">
                  <a href="../uploads/<?php echo $sf_user->getFile()?>/fotos/<?php echo $picture->getArchivo()?>" rel="group1" class="fancybox" title="<?php echo $picture->getTitulo()?>">
                        <?php echo image_tag('../uploads/'.$sf_user->getFile().'/fotos/'.$picture->getArchivo())?>
                  </a>
              </li>
          <?php endforeach?>
        
         </ul>

<div>
          <?php
              if($objeto->getAnioOrigen()): ?>
          <p><b> <?php  echo __('Source years')?>: </b><?php echo $objeto->getAnioOrigen() ?>.</p>
        <?php endif;

              if($objeto->getTextoAntiguedad()): ?>
          <p><b> <?php  echo __('Source years')?>: </b><?php echo $objeto->getTextoAntiguedad() ?>.</p>
        <?php endif;
              if($objeto->getHistoriaDelObjeto()): ?>
          <p><b><?php echo __('History')?>: </b><?php echo $objeto->getHistoriaDelObjeto() ?>.</p>
        <?php endif; ?>
         <?php if($objeto->getLocalidadId() > 0): ?>
          <p><b><?php echo __('Place')?>: </b><?php echo $objeto->getLocalidad()->getProcedencia()?>.</p>
        <?php endif; ?>
</div>

