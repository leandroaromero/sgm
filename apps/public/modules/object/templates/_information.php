<?php //ss$objeto = new Objeto()?>
          <p><b> N° <?php echo __('Inventory')?>: </b><?php echo $objeto->getId() ?>.</p>
        <?php if($objeto->getNroInventarioUnificado() > 0): ?>
          <p><b> N° <?php echo __('Provincial inventory')?>: </b><?php echo $objeto->getNroInventarioUnificado() ?>.</p>
        <?php endif;?>
        <?php if($objeto->getNroInventarioAntiguo() > 0): ?>
          <p><b> N° <?php echo __('Old inventory')?>: </b><?php echo $objeto->getNroInventarioUnificado() ?>.</p>
        <?php endif;?>
        <p><b> <?php echo __('Generic description')?>: </b><?php echo $objeto->getDescripcionGenerica() ?>.</p>
        <p><b> <?php echo __('Conservation status')?>: </b><?php echo $objeto->getEstadoDeConservacion() ?>.</p>
        
        <?php if($objeto->getAutorId()): ?>
          <p><b> <?php echo __('Author')?> : </b><?php echo $objeto->getAutor() ?>.</p>
        <?php endif;?>
        <?php if($objeto->getUbicacionId() > 0): ?>
          <p><b> <?php echo __('Location')?>: </b><?php echo $objeto->getUbicacion() ?>.</p>
          
        <?php endif;
              if($objeto->getAnioOrigen()): ?>
          <p><b> <?php  echo __('Source years')?>: </b><?php echo $objeto->getAnioOrigen() ?>.</p>
        <?php endif;

              if($objeto->getTextoAntiguedad()): ?>
          <p><b> <?php  echo __('Source years')?>: </b><?php echo $objeto->getTextoAntiguedad() ?>.</p>
        <?php endif;
              if($objeto->getHistoriaDelObjeto()): ?>
          <p><b><?php echo __('History')?>: </b><?php echo $objeto->getHistoriaDelObjeto() ?>.</p>
        <?php endif; ?>
         <?php if($objeto->getLocalidadId() > 0): ?>
          <p><b><?php echo __('Place')?>: </b><?php echo $objeto->getLocalidad()->getProcedencia()?>.</p>
        <?php endif; ?>