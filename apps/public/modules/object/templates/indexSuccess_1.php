
<script type="text/javascript">
$(document).ready(function () {
    $("#tabcontainer").tabs({
        event: "click"
    });
    $("a[rel^='prettyPhoto']").prettyPhoto({
        theme: 'dark_square'
    });
});
</script>

<!-- ####################################################################################################### -->
<div class="wrapper col2">
  <div id="container" class="clear">
    <!-- ####################################################################################################### -->

    <div id="tabcontainer">
      <ul id="tabnav">
        <li><a href="#tabs-1"><?php echo __('Pictures')?></a></li>
        <li><a href="#tabs-2"><?php echo __('Information')?></a></li>
        <li><a href="#tabs-3"><?php echo __('Materials')?></a></li>
       
      </ul>
      <!-- ########### -->
      <div id="tabs-1" class="gallery clear">
       <div id="cuerpo"  style="min-height:300px">
        <ul>
          <?php  $con=0; $i=1; foreach ($objeto->getImagen() as $picture): $odd = fmod(++$i, 2) ;$band=false; ?>
            <li><a href="../uploads/fotos/big/<?php echo $picture->getArchivo()?>" rel="prettyPhoto[gallery1]" title="<?php echo $picture->getDescripcion()?>"><?php echo image_tag('../uploads/fotos/'.$picture->getArchivo())?></a></li>
          <?php endforeach?>
        </ul>
       </div>
      </div>
      <!-- ########### -->
      <div id="tabs-2" class="gallery clear">
          <div id="cuerpo"  style="min-height:300px">
              <h2><b><?php echo $objeto->getNombreObjeto() ?></b></h2>
                     <?php include_partial('object/information', array('objeto' => $objeto)) ?>  
          </div>  
      </div>
      <!-- ########### -->
      <div id="tabs-3" class="gallery clear">
          <div id="cuerpo"  style="min-height:300px">
          <h2><b><?php echo __('Materials')?></b> </h2><br/>
           <?php echo __('The object has the following materials')?>: <br/><br/> 
             <ul>
             <?php foreach($objeto->getObjetoMaterial() as $ma):?>
               <li><?php echo $ma->getMaterial(); ?></li>
             <?php endforeach?>
             </ul>
            
          </div>
      </div>
      <!-- ########### -->
    </div>
    <!-- ####################################################################################################### -->

     </div>
        </div>
