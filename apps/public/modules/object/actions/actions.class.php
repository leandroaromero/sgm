<?php

/**
 * object actions.
 *
 * @package    museo
 * @subpackage object
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class objectActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
   $this->getUser()->setAttribute('pagina_actual', 'Objects');   
   $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($request->getParameter('id'))), sprintf('Esta acervo existe (%s).', $request->getParameter('id')));
  }
  public function executeObject(sfWebRequest $request)
  {
   $this->forward404Unless($this->objeto = Doctrine::getTable('Objeto')->find(array($request->getParameter('id'))), sprintf('Esta acervo existe (%s).', $request->getParameter('id')));
  }
}
