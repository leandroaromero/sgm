<?php

class myUser extends sfBasicSecurityUser
{  
  /**
   * @var string
   */
  public $base;
  /**
   * @var string
   */
  public $base_datos;

  
  public function setDataBase($base)
  {
      
      $this->base= $this->getAttribute('base_dato');//$base;
      
  }

//  public function getDataBase()
//  {
//    
//      if( $this->base == NULL)
//          {
//           
//          $this->setDataBase('ichoalay');//'hombre_chaquenio') ; //sfConfig::get('app_institucion_default');
//         }
//         
//      return $this->getAttribute('base_dato'); //$this->base ;
//  }
  
public function getDataBase(){
$base= $this->getAttribute('base_dato');

$default = 'hombre_chaquenio' ; //sfConfig::get('app_institucion_default');

$base= $this->getAttribute('base_dato');
  if ($base== 'architecture' || $base == 'archeology' ){
      $base= NULL;
    }

   if( is_null($base) &&  !$this->isAuthenticated() ){
        $base= $default;
        $this->setAttribute('base_dato', $default);
	}
	return $base;
  }
  
public function getInstitutionName(){
    
    $valor = $this->getDataBase();
    $name='';
    switch ($valor){
      case 'archivo_historico':
          $name='ARCHIVO HISTORICO';
          break;
      case 'bellas_artes':
          $name='MUSEO BELLAS ARTES';
          break;
      case 'ccias_naturales':
          $name='MUSEO CIENCIAS NATURALES';
          break;
      case 'medios_comunicacion':
          $name='MUSEO DE MEDIOS';
          break;
      case 'ichoalay':
          $name='MUSEO ICHOALAY';
          break;
      case 'isla_cerrito':
          $name='MUSEO DE LA ISLA';
          break;
      case 'geraldi':
          $name='CASA, MUSEO Y SITIO HISTORICO';
          break;
      case 'hombre_chaquenio':
          $name='MUSEO DEL HOMBRE CHAQUEÑO';
          break;
    }
          return $name;
    
         
}

public function getInstitutionSubName(){
    $valor = $this->getDataBase();
    $name='';
    switch ($valor){
      case 'archivo_historico':
          $name='';
          break;
      case 'bellas_artes':
          $name='Rene Brusau';
          break;
      case 'ccias_naturales':
          $name='Augusto Schulz';
          break;
      case 'medios_comunicacion':
          $name='';
          break;
      case 'ichoalay':
          $name='';
          break;
      case 'isla_cerrito':
          $name='';
          break;
      case 'geraldi':
          $name='Luis Geraldi';
          break;
      case 'hombre_chaquenio':
          $name='Profesor Ertivio Acosta';
          break;
    }
          return $name;
}

public function getFile(){
   return $this->getDataBase(); 
   } 	
       
   public function aumentarContador( $institution, $acervo =0 ){
        
       $obj= Doctrine_Query::create()
               ->from('Counter c')
               ->where('c.institucion =?', $institution)
               ->where('c.acervo_id =?', $acervo)
               ->fetchOne();   
       
           if($obj && $obj->getId() > 0){
               $obj->setQuantity($obj->getQuantity() + 1 );
           }else{
               $obj = new Counter();
               $obj->setInstitution($institution);
               $obj->setAcervoId($acervo);
               $obj->setQuantity(1);
           }
      $obj->save();
      }
}
