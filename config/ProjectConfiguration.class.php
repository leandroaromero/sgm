<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    $this->enablePlugins('sfDoctrinePlugin');
    $this->enablePlugins('sfFormExtraPlugin');
    $this->enablePlugins('sfJQueryUIPlugin');
    $this->enablePlugins('sfJqueryReloadedPlugin');
    $this->enablePlugins('sfDoctrineGuardPlugin');
    $this->enablePlugins('sfThumbnailPlugin');
    $this->enablePlugins('sfDependentSelectPlugin');
    $this->enablePlugins('sfAdminDashPlugin');
    $this->enablePlugins('sfTCPDFPlugin');
    $this->enablePlugins('mpRealityAdminPlugin');
    $this->enablePlugins('sfDoctrineGuardLoginHistoryPlugin');
    $this->enablePLugins('sfDoctrineActAsSignablePlugin');
    $this->enablePLugins('csSettingsPlugin');
    $this->enablePlugins('ahAdminGeneratorThemesPlugin');
    $this->enablePlugins('sfCaptchaGDPlugin');
    $this->enablePlugins('sfPhpExcelPlugin');
  
  }

   

  
}
